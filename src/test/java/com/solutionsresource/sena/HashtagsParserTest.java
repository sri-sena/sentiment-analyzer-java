/*
package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.SAConstant;
import com.solutionsresource.sena.util.parse.HashtagParser;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

*//**
 * Unit test for simple App.
 *//*
public class HashtagsParserTest
        extends TestCase {
    *//**
     * Create the test case
     *
     * @param testName name of the test case
     *//*
    public HashtagsParserTest(String testName) {
        super(testName);
    }

    *//**
     * @return the suite of tests being tested
     *//*
    public static Test suite() {
        return new TestSuite(HashtagsParserTest.class);
    }

    public void testParse() throws Exception {
        HashtagParser parser = new HashtagParser(Constants.LEXICON_SEEDS);

        assertParsedLexiconShouldContain(parser, "binay2016", "binay", "2016");
        assertParsedLexiconShouldContain(parser, "du302016", "du30", "2016");
        assertParsedLexiconShouldContain(parser, "goduterte2016", "go", "duterte", "2016");
        assertParsedLexiconShouldContain(parser, "marcos2016", "marcos", "2016");
        assertParsedLexiconShouldContain(parser, "marleni2016", "mar", "leni", "2016");
        assertParsedLexiconShouldContain(parser, "gomarleni2016", "go", "mar", "leni", "2016");
        assertParsedLexiconShouldContain(parser, "labanleni", "laban", "leni");
        assertParsedLexiconShouldContain(parser, "labanleni2016", "laban", "leni", "2016");
        assertParsedLexiconShouldContain(parser, "labanmarleni2016", "laban", "leni", "mar", "2016");
        assertParsedLexiconShouldContain(parser, "labanbinayhonasan2016", "laban", "binay", "honasan", "2016");
        assertParsedLexiconShouldContain(parser, "bihon2016", "bihon", "2016");
        assertParsedLexiconShouldContain(parser, "noToBinay2016", "no", "to", "binay", "2016");
        assertParsedLexiconShouldContain(parser, "no2binay", "no", "2", "binay");
        assertParsedLexiconShouldContain(parser, "duterteZoned", "duterte", "zoned");
        assertParsedLexiconShouldContain(parser, "justDuIt", "just", "du", "it");
    }

    private void assertParsedLexiconShouldContain(HashtagParser parser, String lexicon, String... expectedResults) {
        List<String> results = parser.parse(lexicon);

        assertEquals(String.format("%s results size: expected:<%d> but was:<%d>",
                        lexicon, results.size(), expectedResults.length), results.size(), expectedResults.length);

        for (String result : expectedResults) {
            assertTrue(String.format("%s should contain %s", lexicon, result), results.contains(result));
        }
    }
}
*/

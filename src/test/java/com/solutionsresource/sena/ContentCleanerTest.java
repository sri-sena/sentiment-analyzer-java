package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.featuretransformation.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import scala.Tuple2;

import java.util.Arrays;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class ContentCleanerTest extends TestCase {
    public ContentCleanerTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(ContentCleanerTest.class);
    }

    public void testCleaner() throws Exception {
        Constants constants = new Constants();
        ContentCleaner contentCleaner = new ContentCleaner(new RemoveLinks(constants),
                new RemoveStopWords(), new RemoveHandlers(constants), new DatesNormalizer(), new EmojiNormalizer());

        List<Tuple2<String, String>> testCases = Arrays.asList(
                new Tuple2<>(
                        "RT oy galing mo ha",
                        "oy galing mo ha"
                ),
                new Tuple2<>(
                        "MARumi ang tubig dito!",
                        "mar rumi ang tubig dito !"
                ),
                new Tuple2<>(
                        "RT MARupok pa!",
                        "mar rupok pa !"
                ),
                new Tuple2<>(
                        "RT DU30 #DU30!",
                        "du 30 # du 30 !"
                ),
                new Tuple2<>(
                        "@SenGracePOE @VPJojoBinay @utoutoboy #du30 yeah",
                        "@ sengracepoe @ vpjojobinay # du 30 yeah"
                ),
                new Tuple2<>(
                        "Palaban talaga si Cayetano. January 12, 2016 jun2016",
                        "palaban talaga si cayetano . 2016_JANUARY_12 2016_JUNE"
                ),
                new Tuple2<>(
                        "Hindi yung mandadaya ka. \uD83D\uDE02 #Halalan2016 #Halalan2016",
                        "hindi yung mandadaya ka . UNICODE_55357 UNICODE_56834  # halalan 2016 # halalan 2016"
                ),
                new Tuple2<>(
                        "#BBMforVicePresident",
                        "# bbm mfor vice president"
                )
        );

        for (Tuple2<String, String> testCase : testCases) {
            assertEquals(testCase._2(), contentCleaner.clean(testCase._1()));
        }
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.function.featuretransformation.GetSentimentOfTopic;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class SentimentTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SentimentTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(SentimentTest.class);
    }

    public void testMessageToWeightedLabeledPoint() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        assertEquals("negative", message.getDomain("election").getTopic("jejomar-binay").getSentiment().getName());
    }

    public void testGetHasDomainTopic() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        GetSentimentOfTopic getHasDomainTopic = new GetSentimentOfTopic("election", "jejomar-binay");

        Message message = new Message("1", "ang panget panget ni binay", domains, true);
        Message message2 = new Message("2", "pika pika pi!", new ArrayList<Domain>(), true);

        assertTrue(getHasDomainTopic.call(message).contains("negative"));
        assertTrue(getHasDomainTopic.call(message2).contains(""));
    }
}

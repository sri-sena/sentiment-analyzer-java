package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTable;
import com.solutionsresource.sena.function.featuretransformation.ConvertToUsageTable;
import com.solutionsresource.sena.function.featuretransformation.GetHasDomainString;
import com.solutionsresource.sena.function.featuretransformation.GetHasDomainTopic;
import com.solutionsresource.sena.function.featuretransformation.GetSentimentOfTopic;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class UsageTableTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public UsageTableTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(UsageTableTest.class);
    }

    public void testElectionUsages() throws Exception {
        GetHasDomainString getHasElectionDomain = new GetHasDomainString("election");
        GetHasDomainString getHasRestaurantDomain = new GetHasDomainString("restaurant");

        ConvertToUsageTable convertToElectionUsageTable = new ConvertToUsageTable(getHasElectionDomain);
        ConvertToUsageTable convertToRestaurantUsageTable = new ConvertToUsageTable(getHasRestaurantDomain);

        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);
        UsageTable electionUsageTable = convertToElectionUsageTable.call(message);
        UsageTable restaurantUsageTable = convertToRestaurantUsageTable.call(message);

        assertTrue(getHasElectionDomain.call(message).contains("true"));
        assertEquals(1, electionUsageTable.get("true").intValue());
        assertEquals(1, electionUsageTable.size());

        assertTrue(getHasRestaurantDomain.call(message).contains("false"));
        assertEquals(1, restaurantUsageTable.get("false").intValue());
        assertEquals(1, restaurantUsageTable.size());
    }

    public void testTopicUsages() throws Exception {
        GetHasDomainTopic getHasBinayTopic = new GetHasDomainTopic("election", "jejomar-binay");
        GetHasDomainTopic getHasJollibeeTopic = new GetHasDomainTopic("restaurant", "jollibee");

        ConvertToUsageTable convertToBinayUsageTable = new ConvertToUsageTable(getHasBinayTopic);
        ConvertToUsageTable convertToJollibeeUsageTable = new ConvertToUsageTable(getHasJollibeeTopic);

        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);
        UsageTable binayUsageTable = convertToBinayUsageTable.call(message);
        UsageTable JollibeeUsageTable = convertToJollibeeUsageTable.call(message);

        assertTrue(getHasBinayTopic.call(message).contains("true"));
        assertEquals(1, binayUsageTable.get("true").intValue());
        assertEquals(1, binayUsageTable.size());

        assertTrue(getHasJollibeeTopic.call(message).contains("false"));
        assertEquals(1, JollibeeUsageTable.get("false").intValue());
        assertEquals(1, JollibeeUsageTable.size());
    }

    public void testSentimentUsages() throws Exception {
        GetSentimentOfTopic getBinaySentiment = new GetSentimentOfTopic("election", "jejomar-binay");
        GetSentimentOfTopic getJollibeeSentiment = new GetSentimentOfTopic("restaurant", "jollibee");

        ConvertToUsageTable convertToBinayUsageTable = new ConvertToUsageTable(getBinaySentiment);
        ConvertToUsageTable convertToJollibeeUsageTable = new ConvertToUsageTable(getJollibeeSentiment);

        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);
        UsageTable binayUsageTable = convertToBinayUsageTable.call(message);
        UsageTable jollibeeUsageTable = convertToJollibeeUsageTable.call(message);

        assertTrue(getBinaySentiment.call(message).contains("negative"));
        assertEquals(1, binayUsageTable.get("negative").intValue());
        assertEquals(1, binayUsageTable.size());

        assertEquals(0, jollibeeUsageTable.size());
    }

    public void testReduce() throws Exception {
        UsageTable usageTable1 = new UsageTable();
        UsageTable usageTable2 = new UsageTable();
        UsageTable usageTable3 = new UsageTable();
        UsageTable usageTable4 = new UsageTable();

        usageTable1.put("true", 1);
        usageTable2.put("false", 4);
        usageTable3.put("true", 8);
        usageTable4.put("false", 2);

        ReduceUsageTable reduceUsageTable = new ReduceUsageTable();

        UsageTable result1 = reduceUsageTable.call(usageTable1, usageTable2);
        UsageTable result2 = reduceUsageTable.call(usageTable3, usageTable4);
        UsageTable result3 = reduceUsageTable.call(usageTable1, usageTable3);
        UsageTable result4 = reduceUsageTable.call(usageTable2, usageTable4);
        UsageTable result5 = reduceUsageTable.call(result1, result2);
        UsageTable result6 = reduceUsageTable.call(result3, result4);

        assertEquals(2, result1.size());
        assertEquals(1, result1.get("true").intValue());
        assertEquals(4, result1.get("false").intValue());

        assertEquals(2, result2.size());
        assertEquals(8, result2.get("true").intValue());
        assertEquals(2, result2.get("false").intValue());

        assertEquals(1, result3.size());
        assertEquals(9, result3.get("true").intValue());

        assertEquals(1, result4.size());
        assertEquals(6, result4.get("false").intValue());

        assertEquals(2, result5.size());
        assertEquals(9, result5.get("true").intValue());
        assertEquals(6, result5.get("false").intValue());

        assertEquals(2, result6.size());
        assertEquals(9, result6.get("true").intValue());
        assertEquals(6, result6.get("false").intValue());

        assertEquals(15, result6.getTotal());
    }
}

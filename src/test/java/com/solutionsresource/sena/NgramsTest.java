package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.function.featuretransformation.ExtractNGrams;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.HashMap;

/**
 * Unit test for simple App.
 */
public class NgramsTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public NgramsTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(NgramsTest.class);
    }

    public void testNgrams() throws Exception {
        ExtractNGrams extractNGrams1 = new ExtractNGrams(1);
        ExtractNGrams extractNGrams2 = new ExtractNGrams(2);
        ExtractNGrams extractNGrams3 = new ExtractNGrams(3);

        Message message1 = extractNGrams1
                .call(new Message("\"Anu ba yan! :/ sabi na nga ba eh\" yan ang sabe nya"));
        Message message2 = extractNGrams1
                .call(new Message("\"hehehe\" yan ang nagsabe nya"));
        Message message3 = extractNGrams1
                .call(new Message("http://twibbon.com/support/miriam2016"));
        Message message4 = extractNGrams3
                .call(new Message("#du30 #justDuIt go duterte 2016 #duterte2016 #thuglife"));
        Message message5 = extractNGrams2
                .call(new Message("grace poe"));
        Message message6 = extractNGrams3
                .call(new Message("# just du it"));
        Message message7 = extractNGrams1
                .call(new Message("@SenGracePoe go!!!"));

        HashMap<String, Integer> ngrams1 = message1.getNgrams();
        HashMap<String, Integer> ngrams2 = message2.getNgrams();
        HashMap<String, Integer> ngrams3 = message3.getNgrams();
        HashMap<String, Integer> ngrams4 = message4.getNgrams();
        HashMap<String, Integer> ngrams5 = message5.getNgrams();
        HashMap<String, Integer> ngrams6 = message6.getNgrams();
        HashMap<String, Integer> ngrams7 = message7.getNgrams();

        HashMap<String, Integer> hashtagAndHandlerNgrams1 = message1.getHashtagAndHandlerNgrams();
        HashMap<String, Integer> hashtagAndHandlerNgrams2 = message2.getHashtagAndHandlerNgrams();
        HashMap<String, Integer> hashtagAndHandlerNgrams3 = message3.getHashtagAndHandlerNgrams();
        HashMap<String, Integer> hashtagAndHandlerNgrams4 = message4.getHashtagAndHandlerNgrams();
        HashMap<String, Integer> hashtagAndHandlerNgrams5 = message5.getHashtagAndHandlerNgrams();
        HashMap<String, Integer> hashtagAndHandlerNgrams6 = message6.getHashtagAndHandlerNgrams();
        HashMap<String, Integer> hashtagAndHandlerNgrams7 = message7.getHashtagAndHandlerNgrams();

        assertContainsKey(ngrams1, ":/");

        assertContainsKey(ngrams2, "\"");
        assertContainsKey(ngrams2, "sabe");
        assertContainsKey(ngrams2, "nagsabe");

        assertContainsKey(ngrams3, "/");
        assertContainsKey(ngrams3, "miriam2016");

        assertContainsKey(ngrams4, "du30");
        assertContainsKey(ngrams4, "justduit");
        assertContainsKey(ngrams4, "duterte2016");

        assertContainsKey(ngrams4, "go duterte");
        assertContainsKey(ngrams5, "grace");
        assertContainsKey(ngrams5, "poe");
        assertContainsKey(ngrams5, "grace poe");

        assertContainsKey(ngrams6, "just du it");

        assertContainsKey(ngrams7, "sengracepoe");
    }

//    public void testSkippingNgrams() throws Exception {
//        ExtractNGrams extractNGrams4 = new ExtractNGrams(4);
//
//        Message message6 = extractNGrams4
//                .call(new Message("Duterte tops Magdalo survey"));
//        Message message7 = extractNGrams4
//                .call(new Message("Binay is not that good"));
//
//        HashMap<String, Integer> ngrams6 = message6.getNgrams();
//        HashMap<String, Integer> ngrams7 = message7.getNgrams();
//
//        HashMap<String, Integer> hashtagAndHandlerNgrams6 = message6.getHashtagAndHandlerNgrams();
//        HashMap<String, Integer> hashtagAndHandlerNgrams7 = message7.getHashtagAndHandlerNgrams();
//
//        assertContainsKey(ngrams6, "duterte");
//        assertContainsKey(ngrams6, "duterte tops");
//        assertContainsKey(ngrams6, "duterte tops dalo");
//        assertContainsKey(ngrams6, "duterte tops dalo survey");
//        assertContainsKey(ngrams6, "duterte tops survey");
//
//        assertContainsKey(ngrams7, "binay");
//        assertContainsKey(ngrams7, "binay is not");
//        assertContainsKey(ngrams7, "binay not");
//        assertContainsKey(ngrams7, "binay not good");
//        assertContainsKey(ngrams7, "binay not good .");
//    }

    public void assertContainsKey(HashMap<String, Integer> hashtable, String key) {
        assertTrue("ngrams6 should contain ".concat(key), hashtable.containsKey(key));
    }
}

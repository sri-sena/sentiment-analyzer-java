package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.function.featuretransformation.GetHasDomainTopic;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class TopicTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TopicTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TopicTest.class);
    }

    public void testMessageToWeightedLabeledPoint() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        assertEquals(true, message.getDomain("election").hasTopic("jejomar-binay"));
    }

    public void testGetHasDomainTopic() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        GetHasDomainTopic getHasDomainTopic = new GetHasDomainTopic("election", "jejomar-binay");

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        assertTrue(getHasDomainTopic.call(message).contains("true"));
    }
}

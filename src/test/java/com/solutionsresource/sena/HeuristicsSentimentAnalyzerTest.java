package com.solutionsresource.sena;

import com.solutionsresource.sena.util.parse.HeuristicsSentimentAnalyzer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import scala.Tuple5;

/**
 * Unit test for simple App.
 */
public class HeuristicsSentimentAnalyzerTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public HeuristicsSentimentAnalyzerTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(HeuristicsSentimentAnalyzerTest.class);
    }

    public void testHeuristicsSentimentAnalyzer() throws Exception {
        HeuristicsSentimentAnalyzer hsa = new HeuristicsSentimentAnalyzer("sentiment.xml");
        Tuple5[] testCases = new Tuple5[]{ // (sentence, domain, topic, sentiment, throw exception expected)
                new Tuple5<>("si bongbong marcos ang magaling", "election", "marcos", "positive", false),
                new Tuple5<>("parang robot si chiz", "election", "escudero", "negative", false),
                new Tuple5<>("teamchiz pa rin kami", "election", "escudero", "positive", false),
                new Tuple5<>("i admire manny pacquiao", "election", "pacquiao", "positive", false),
                new Tuple5<>("wala ka talaga roxas", "election", "roxas", "not found", false),
                new Tuple5<>("si leni for vp, leni2016", "election", "robredo", "positive", false),
                new Tuple5<>("i really hate cayetano and trillanes", "election", "trillanes", "negative", false),
                new Tuple5<>("ayon kay chiz \"what you see is what you get\"", "election", "escudero", "neutral", false),
                new Tuple5<>("ganyan ka naman eh", "election", "trillanes", "not found", false),
                new Tuple5<>("parang wala lang si gringo honasan", "election", "honasan", "not found", false),
                new Tuple5<>("we salute bongbong marcos", "election", "marcos", "positive", false),
                new Tuple5<>("pikon naman pala si bbm eh", "election", "marcos", "negative", false),
                new Tuple5<>("kay maam leni kami # roxasrobredo", "election", "robredo", "positive", false),
                new Tuple5<>("escudero for vice president para sa gobyernong may puso",
                        "election", "escudero", "positive", false),
                new Tuple5<>("si trillanes out of place", "election", "trillanes", "negative", false),
                new Tuple5<>("shut up trillanes puro ka nalang satsat", "election", "trillanes", "negative", false),
                new Tuple5<>("kahit mababa sa survey iboboto pa rin kita", "election", "trillanes", "positive", false),
                new Tuple5<>("hindi alam ng kabataan #neveragainmarcos", "election", "marcos", "negative", false),
                new Tuple5<>("shit that video made me cry. gusto ko sila ihug......... #neveragainmarcos #neveragain",
                        "election", "marcos", "negative", false),
                new Tuple5<>("#bakitsibongbong sila lang ni duterte ang makakapagbago ng pilipinas! duterte-marcos!",
                        "election", "marcos", "positive", false),
                new Tuple5<>("ang gandang ipatrend. #marroxasvotebuying !!! mapapamura ka tlga",
                        "election", "roxas", "negative", false),
                new Tuple5<>("sunday morning super well spent ??? #roxasrobredo #marleni (look its sir tuano??)",
                        "election", "roxas", "positive", false),
                new Tuple5<>("\"thank you pres mar, it means so much to my boy who is campaigning for you " +
                        "in his own little way.\" #mayonaroxasna", "election", "roxas", "positive", false),
                new Tuple5<>("make it work please! #miriammagic", "election", "santiago", "positive", false),
                new Tuple5<>("good luck to manny pacquiao, great boxer, loving father, and a politician. " +
                        "#mannypacquiao #pacquiaoforsenator","election", "pacquiao", "positive", false),
                new Tuple5<>("i push pa natin to guys! calling all vp binay supporters! #maynabinayna",
                        "election", "binay", "positive", false),
                new Tuple5<>("may ipis ang pagkain sa chowking", "restaurant", "chowking", "negative", false),
                new Tuple5<>("sulit sa jollibee", "restaurant", "jollibee", "negative", true)
        };


        for (Tuple5 tc : testCases) {
            Boolean shouldThrowException = Boolean.valueOf(tc._5().toString());
            try {
                assertEquals(tc._1().toString(), tc._4(),
                        hsa.call(tc._1().toString(), tc._2().toString(), tc._3().toString()));
                assertFalse(tc._1().toString() + " should throw exception", shouldThrowException);
            } catch (Exception e) {
                assertTrue(tc._1().toString() + " shouldn't throw exception", shouldThrowException);
            }
        }
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.function.featureselection.FilterFinalizedUnfinalized;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Unit test for simple App.
 */
public class FilterFinalizedUnfinalizedTest
    extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public FilterFinalizedUnfinalizedTest(String testName) {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(FilterFinalizedUnfinalizedTest.class);
  }

  /**
   * Rigourous Test :-)
   */
  public void testMap() throws Exception {
    Constants constants = new Constants();

    FilterFinalizedUnfinalized filterNone = new FilterFinalizedUnfinalized(constants, false, false);
    FilterFinalizedUnfinalized filterFinalized = new FilterFinalizedUnfinalized(constants, true, false);
    FilterFinalizedUnfinalized filterUnfinalized = new FilterFinalizedUnfinalized(constants, false, true);
    FilterFinalizedUnfinalized filterAll = new FilterFinalizedUnfinalized(constants, true, true);

    ResultHashMap result1 = new ResultHashMap(Bytes.toBytes("1431-twitter-1234"));
    result1.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT, Bytes.toBytes("true"));

    ResultHashMap result2 = new ResultHashMap(Bytes.toBytes("1432-twitter-1244"));
    result2.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT, Bytes.toBytes("false"));

    ResultHashMap result3 = new ResultHashMap(Bytes.toBytes("1433-twitter-1244"));
    result3.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT, Bytes.toBytes(true));

    ResultHashMap result4 = new ResultHashMap(Bytes.toBytes("1444-twitter-1245"));
    result4.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT, Bytes.toBytes(false));

    ResultHashMap result5 = new ResultHashMap(Bytes.toBytes("1432-twitter-1245"));
    result5.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT, Bytes.toBytes(new Boolean(false)));

    ResultHashMap result6 = new ResultHashMap(Bytes.toBytes("1432-twitter-1246"));
    result6.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT, Bytes.toBytes("false123"));

    assertTrue("1431 should be accepted.", filterNone.call(result1));
    assertTrue("1432 should be accepted.", filterNone.call(result2));
    assertTrue("1433 should be accepted.", filterNone.call(result3));
    assertTrue("1444 should be accepted.", filterNone.call(result4));
    assertTrue("1445 should be accepted.", filterNone.call(result5));
    assertTrue("1446 should be accepted.", filterNone.call(result6));

    assertTrue("1431 should be accepted.", filterFinalized.call(result1));
    assertFalse("1432 should be rejected.", filterFinalized.call(result2));
    assertTrue("1432 should be accepted.", filterFinalized.call(result3));
    assertFalse("1444 should be rejected.", filterFinalized.call(result4));
    assertFalse("1445 should be rejected.", filterFinalized.call(result5));
    assertFalse("1446 should be rejected.", filterFinalized.call(result6));

    assertFalse("1431 should be rejected.", filterUnfinalized.call(result1));
    assertTrue("1432 should be accepted.", filterUnfinalized.call(result2));
    assertFalse("1433 should be rejected.", filterUnfinalized.call(result3));
    assertTrue("1444 should be accepted.", filterUnfinalized.call(result4));
    assertTrue("1445 should be accepted.", filterUnfinalized.call(result5));
    assertTrue("1446 should be accepted.", filterUnfinalized.call(result6));

    assertTrue("1431 should be accepted.", filterAll.call(result1));
    assertTrue("1432 should be accepted.", filterAll.call(result2));
    assertTrue("1433 should be accepted.", filterAll.call(result3));
    assertTrue("1444 should be accepted.", filterAll.call(result4));
    assertTrue("1445 should be accepted.", filterAll.call(result5));
    assertTrue("1446 should be accepted.", filterAll.call(result6));
  }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.FloatCountTable;
import com.solutionsresource.sena.function.featurescaling.ReduceFloatCountTables;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class FloatCountTableTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FloatCountTableTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FloatCountTableTest.class);
    }

    public void testReduce() throws Exception {
        FloatCountTable countTable1 = new FloatCountTable();
        FloatCountTable countTable2 = new FloatCountTable();

        countTable1.put(1.0f, 200);
        countTable1.put(2.0f, 100);
        countTable1.put(3.0f, 160);

        countTable2.put(1.0f, 100);
        countTable2.put(2.0f, 600);
        countTable2.put(3.0f, 260);

        FloatCountTable resultTable = new ReduceFloatCountTables().call(countTable1, countTable2);

        assertEquals(300, resultTable.get(1.0f).intValue());
        assertEquals(700, resultTable.get(2.0f).intValue());
        assertEquals(420, resultTable.get(3.0f).intValue());
    }

    public void testFloatKeyAt() throws Exception {
        FloatCountTable countTable = new FloatCountTable();

        countTable.put(1.0f, 200);
        countTable.put(2.0f, 100);
        countTable.put(3.0f, 160);

        assertEquals(2.0f, countTable.getFloatKeyAt(250));
        assertEquals(3.0f, countTable.getFloatKeyAt(150));
        assertEquals(3.0f, countTable.getFloatKeyAt(0));
        assertEquals(1.0f, countTable.getFloatKeyAt(300));
        assertEquals(1.0f, countTable.getFloatKeyAt(400));
        assertEquals(1.0f, countTable.getFloatKeyAt(500));
        assertEquals(2.0f, countTable.getFloatKeyAt(201));
        assertEquals(1.0f, countTable.getFloatKeyAt(301));
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.function.featuretransformation.ConvertToCsv;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class CsvTopicFilterTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CsvTopicFilterTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CsvTopicFilterTest.class);
    }

    public static void testFilter() {
        String[] topicsFilter = {
                "poe",
                "jollibee"
        };
        assertTrue("duterte should be filtered", ConvertToCsv.topicIsFiltered(topicsFilter, "duterte"));
        assertFalse("poe should not be filtered", ConvertToCsv.topicIsFiltered(topicsFilter, "poe"));
        assertTrue("jollibae should be filtered", ConvertToCsv.topicIsFiltered(topicsFilter, "jollibae"));
        assertFalse("jollibee should be filtered", ConvertToCsv.topicIsFiltered(topicsFilter, "jollibee"));
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.config.Config;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.featureselection.RemoveStopHashtags;
import com.solutionsresource.sena.function.featuretransformation.RemoveHandlers;
import com.solutionsresource.sena.function.featuretransformation.RemoveLinks;
import com.solutionsresource.sena.function.featuretransformation.RemoveStopWords;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class StopWordsAndLinksRemovalTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public StopWordsAndLinksRemovalTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(StopWordsAndLinksRemovalTest.class);
    }

    public void testStopWords() {
        String sample2 = "Yan ba ang gusto nyong maging pangulo? No to Mar Roxas! #noToMarLeni2016";
        String sample3 = "MARumi MARahas MARupok!";
        String sample4 = "#noToMarLeni2016";
        String sample5 = "@RodrigoDuterte oy! Ang galing mo talaga du30!";
        String sample6 = "ANO BA MAR";
        String sample7 = "#MarWithAPlan #SolusyonNiMar";

        RemoveStopWords remover = new RemoveStopWords();
        List<String> sampleWords2 = remover.removeStopWords(sample2);
        List<String> sampleWords3 = remover.removeStopWords(sample3);
        List<String> sampleWords4 = remover.removeStopWords(sample4);
        List<String> sampleWords5 = remover.removeStopWords(sample5);
        List<String> sampleWords6 = remover.removeStopWords(sample6);
        List<String> sampleWords7 = remover.removeStopWords(sample7);

        assertFalse(sampleWords2.contains("notomarleni2016"));
        assertTrue(sampleWords3.contains("mar"));
        assertTrue(sampleWords3.contains("rumi"));
        assertTrue(sampleWords3.contains("rahas"));
        assertTrue(sampleWords3.contains("rupok"));
        assertTrue(sampleWords4.contains("no"));
        assertTrue(sampleWords4.contains("mar"));
        assertTrue(sampleWords4.contains("leni"));
        assertTrue(sampleWords4.contains("2016"));
        assertTrue(sampleWords5.contains("du"));
        assertTrue(sampleWords5.contains("30"));
        assertTrue(sampleWords6.contains("ano"));
        assertTrue(sampleWords6.contains("ba"));
        assertTrue(sampleWords6.contains("mar"));

        assertTrue(sampleWords7.contains("mar"));
        assertTrue(sampleWords7.contains("with"));
        assertTrue(sampleWords7.contains("ap"));
        assertTrue(sampleWords7.contains("plan"));
        assertTrue(sampleWords7.contains("solusyon"));
        assertTrue(sampleWords7.contains("ni"));
        assertTrue(sampleWords7.contains("mar"));
    }

    public void testLinks() throws Exception {
        String sample = "Ang ganda nito: https://t.co/1234/54/32 para kay du30!";
        String sample2 = "http://fb.com dito ko kinukuha jokes ko";
        String sample3 = "astig! http://fb.com/432/54";
        String sample4 = "astig! http://fb.com/432/54/";

        RemoveLinks remover = new RemoveLinks();
        String linklessSample1 = remover.removeLinks(sample);
        String linklessSample2 = remover.removeLinks(sample2);
        String linklessSample3 = remover.removeLinks(sample3);
        String linklessSample4 = remover.removeLinks(sample4);

        assertEquals("Ang ganda nito:  para kay du30!", linklessSample1);
        assertEquals(" dito ko kinukuha jokes ko", linklessSample2);
        assertEquals("astig! ", linklessSample3);
        assertEquals("astig! ", linklessSample4);
    }

    public void testHandlers() throws Exception {
        String sample1 = "Galing mo talaga @sengracepoe idol";
        String sample2 = "sino ba tong @jejomardighay na to?";
        String sample3 = "ituloy mo lang ang kulubot na daan @MARoxas dyan ka magaling!";
        String sample4 = "@sengrace look at this!";
        String sample5 = "@VPjojobinay @MARoxas @du30 @sengracepoe @meriam gera na!";

        RemoveHandlers remover = new RemoveHandlers();

        String cleanedSample1 = remover.removeHandlers(sample1);
        String cleanedSample2 = remover.removeHandlers(sample2);
        String cleanedSample3 = remover.removeHandlers(sample3);
        String cleanedSample4 = remover.removeHandlers(sample4);
        String cleanedSample5 = remover.removeHandlers(sample5);

        assertEquals(sample1, cleanedSample1);
        assertEquals("sino ba tong  na to?", cleanedSample2);
        assertEquals(sample3, cleanedSample3);
        assertEquals(" look at this!", cleanedSample4);
        assertEquals("@VPjojobinay @MARoxas  @sengracepoe  gera na!", cleanedSample5);
    }

    public void testHashtags() throws Exception {
        String[] samples = new String[]{
                "Go #MARLENI2016 #PiliPinasDebates2016",
                "Go Cayetano!!! #PiliPinasDebates2016 kaya nyo yan! #DuterteCayetano2016",
                "#OnlyBinay #OnlyBinayCan"
        };

        String[] expectedResults = new String[]{
                "go # marleni 2016 ",
                "go cayetano ! ! !  kaya nyo yan ! # duterte cayetano 2016",
                "# only binay # only binay can",
        };

        Constants constants = new Constants();

        RemoveLinks removeLinks = new RemoveLinks(constants);
        RemoveHandlers removeHandlers = new RemoveHandlers(constants);
        RemoveStopWords removeStopWords = new RemoveStopWords();
        RemoveStopHashtags removeStopHashtags = new RemoveStopHashtags(Config.STOP_HASHTAGS);

        for (int i = 0; i < samples.length; i++) {
            String sample = samples[i];
            sample = removeLinks.removeLinks(sample);
            sample = removeHandlers.removeHandlers(sample);
            sample = removeStopWords.combineNonstopWords(sample);
            sample = removeStopHashtags.removeStopHashtags(sample);

            assertEquals(expectedResults[i], sample);
        }
    }
}

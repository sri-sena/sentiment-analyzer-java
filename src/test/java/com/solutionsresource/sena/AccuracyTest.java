package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.function.accuracy.ConvertToCorrectnessTuple;
import com.solutionsresource.sena.function.accuracy.ReduceTuple2;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AccuracyTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AccuracyTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AccuracyTest.class);
    }

    public void testConvertToCorrectnessTuple() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        List<Domain> emptyDomains = new ArrayList<Domain>();

        ConvertToCorrectnessTuple convertToCorrectnessTuple = new ConvertToCorrectnessTuple();

        Message message1 = new Message("1", "", domains, true);
        Message message2 = new Message("2", "", domains, true);
        Message message3 = new Message("3", "", emptyDomains, true);
        Message message4 = new Message("4", "", emptyDomains, true);

        Topic anotherTopic = new Topic("Another topic", Sentiment.mock(), false);
        Topic duterte = new Topic("Rodrigo Duterte", Sentiment.mock(), false);

        Domain electionDomain = new Domain("election");
        String electionDomainName = electionDomain.getName();

        message2.addPredictedTopic(electionDomainName, Topic.mock());
        message2.addPredictedTopic(electionDomainName, duterte);
        message2.addPredictedTopic(electionDomainName, anotherTopic);

        message4.addPredictedTopic(electionDomainName, duterte);

        Tuple2<Integer, Integer> tuple1 = convertToCorrectnessTuple.call(message1);
        assertFalse(message1.isCorrect());
        assertEquals(0, (int) tuple1._1());
        assertEquals(1, (int) tuple1._2());

        Tuple2<Integer, Integer> tuple2 = convertToCorrectnessTuple.call(message2);
        assertTrue(message2.isCorrect());
        assertEquals(1, (int) tuple2._1());
        assertEquals(0, (int) tuple2._2());

        Tuple2<Integer, Integer> tuple3 = convertToCorrectnessTuple.call(message3);
        assertTrue(message3.isCorrect());
        assertEquals(1, (int) tuple3._1());
        assertEquals(0, (int) tuple3._2());

        Tuple2<Integer, Integer> tuple4 = convertToCorrectnessTuple.call(message4);
        assertFalse(message4.isCorrect());
        assertEquals(0, (int) tuple4._1());
        assertEquals(1, (int) tuple4._2());
    }

    public void testReduceTuple2() throws Exception {
        ReduceTuple2 reduceTuple2 = new ReduceTuple2();

        Tuple2<Integer, Integer> tuple1 = reduceTuple2.call(
                new Tuple2<Integer, Integer>(5, 6), new Tuple2<Integer, Integer>(8, 2));
        Tuple2<Integer, Integer> tuple2 = reduceTuple2.call(
                new Tuple2<Integer, Integer>(7, 1), new Tuple2<Integer, Integer>(9, 6));

        assertEquals(13, (int) tuple1._1());
        assertEquals(8, (int) tuple1._2());

        assertEquals(16, (int) tuple2._1());
        assertEquals(7, (int) tuple2._2());
    }

    public void testAccuracyComputation() throws Exception {
        assertEquals(60, (int) new Metric(new Integer[][]{},
                new Tuple2<Integer, Integer>(24, 16), 100, 100).getAccuracy());
        assertEquals(65, (int) new Metric(new Integer[][]{},
                new Tuple2<Integer, Integer>(26, 14),100, 100).getAccuracy());
        assertEquals(80, (int) new Metric(new Integer[][]{},
                new Tuple2<Integer, Integer>(80, 20),100, 100).getAccuracy());
    }
}

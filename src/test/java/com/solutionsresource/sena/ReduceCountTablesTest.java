package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.CountTable;
import com.solutionsresource.sena.function.featurescaling.ReduceCountTables;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ReduceCountTablesTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ReduceCountTablesTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ReduceCountTablesTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() throws Exception {
        CountTable countTable1 = new CountTable();
        CountTable countTable2 = new CountTable();

        countTable1.put(1.0, 5);
        countTable2.put(1.0, 12);

        ReduceCountTables function = new ReduceCountTables();
        CountTable result = function.call(countTable1, countTable2);

        assertTrue(result.get(1.0) == 17);
        assertTrue(result.size() == 1);

        countTable1.put(2.0, 18);
        countTable2.put(2.0, 6);

        result = function.call(countTable1, countTable2);

        assertTrue(result.get(1.0) == 17);
        assertTrue(result.get(2.0) == 24);
        assertTrue(result.size() == 2);
    }
}

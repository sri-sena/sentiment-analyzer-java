package com.solutionsresource.sena;

import com.google.gson.Gson;
import com.solutionsresource.sena.entity.gnip.Tweet;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class GsonTweetTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public GsonTweetTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(GsonTweetTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testParseActor() throws Exception {
        String line = "{\"id\":\"tag:search.twitter.com,2005:727620775088300033\",\"objectType\":\"activity\",\"actor\":{\"objectType\":\"person\",\"id\":\"id:twitter.com:524939820\",\"link\":\"http://www.twitter.com/garigalAnj\",\"displayName\":\"Garigal Anj\",\"postedTime\":\"2012-03-15T02:11:16.000Z\",\"image\":\"https://pbs.twimg.com/profile_images/723463391977738240/3E52ha-M_normal.jpg\",\"summary\":\"#actOnClimate #transparency Biodiversity * Safe Food * Universal Health Care and Education * Constructive Paranoia • IT\",\"links\":[{\"href\":null,\"rel\":\"me\"}],\"friendsCount\":2038,\"followersCount\":820,\"listedCount\":92,\"statusesCount\":25853,\"twitterTimeZone\":\"Sydney\",\"verified\":false,\"utcOffset\":\"36000\",\"preferredUsername\":\"garigalAnj\",\"languages\":[\"en\"],\"location\":{\"objectType\":\"place\",\"displayName\":\"Great Southern Land\"},\"favoritesCount\":1439},\"verb\":\"share\",\"postedTime\":\"2016-05-03T22:08:05.000Z\",\"generator\":{\"displayName\":\"Twitter for iPhone\",\"link\":\"http://twitter.com/download/iphone\"},\"provider\":{\"objectType\":\"service\",\"displayName\":\"Twitter\",\"link\":\"http://www.twitter.com\"},\"link\":\"http://twitter.com/garigalAnj/statuses/727620775088300033\",\"body\":\"RT @350EastAsia: crowds are already starting to mass up for #piglaspilipinas #breakfree2016 https://t.co/1Qs4m8FujA\",\"object\":{\"id\":\"tag:search.twitter.com,2005:727619609528926209\",\"objectType\":\"activity\",\"actor\":{\"objectType\":\"person\",\"id\":\"id:twitter.com:2766632238\",\"link\":\"http://www.twitter.com/350EastAsia\",\"displayName\":\"350 East Asia\",\"postedTime\":\"2014-08-25T15:08:51.000Z\",\"image\":\"https://pbs.twimg.com/profile_images/503924138323353600/k7TLz3-B_normal.png\",\"summary\":\"Join a global movement that's inspiring the world to rise to the challenge of the climate crisis. 350=safe upper limit of CO2 in atmosphere.\",\"links\":[{\"href\":null,\"rel\":\"me\"}],\"friendsCount\":368,\"followersCount\":631,\"listedCount\":33,\"statusesCount\":1562,\"twitterTimeZone\":null,\"verified\":false,\"utcOffset\":null,\"preferredUsername\":\"350EastAsia\",\"languages\":[\"en\"],\"location\":{\"objectType\":\"place\",\"displayName\":\"East Asia\"},\"favoritesCount\":669},\"verb\":\"post\",\"postedTime\":\"2016-05-03T22:03:28.000Z\",\"generator\":{\"displayName\":\"Twitter for iPhone\",\"link\":\"http://twitter.com/download/iphone\"},\"provider\":{\"objectType\":\"service\",\"displayName\":\"Twitter\",\"link\":\"http://www.twitter.com\"},\"link\":\"http://twitter.com/350EastAsia/statuses/727619609528926209\",\"body\":\"crowds are already starting to mass up for #piglaspilipinas #breakfree2016 https://t.co/1Qs4m8FujA\",\"object\":{\"objectType\":\"note\",\"id\":\"object:search.twitter.com,2005:727619609528926209\",\"summary\":\"crowds are already starting to mass up for #piglaspilipinas #breakfree2016 https://t.co/1Qs4m8FujA\",\"link\":\"http://twitter.com/350EastAsia/statuses/727619609528926209\",\"postedTime\":\"2016-05-03T22:03:28.000Z\"},\"favoritesCount\":2,\"twitter_entities\":{\"hashtags\":[{\"text\":\"piglaspilipinas\",\"indices\":[43,59]},{\"text\":\"breakfree2016\",\"indices\":[60,74]}],\"urls\":[],\"user_mentions\":[],\"symbols\":[],\"media\":[{\"id\":727619578533060608,\"id_str\":\"727619578533060608\",\"indices\":[75,98],\"media_url\":\"http://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"media_url_https\":\"https://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"url\":\"https://t.co/1Qs4m8FujA\",\"display_url\":\"pic.twitter.com/1Qs4m8FujA\",\"expanded_url\":\"http://twitter.com/350EastAsia/status/727619609528926209/photo/1\",\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":1024,\"h\":768,\"resize\":\"fit\"},\"medium\":{\"w\":600,\"h\":450,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":255,\"resize\":\"fit\"}}}]},\"twitter_extended_entities\":{\"media\":[{\"id\":727619578533060608,\"id_str\":\"727619578533060608\",\"indices\":[75,98],\"media_url\":\"http://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"media_url_https\":\"https://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"url\":\"https://t.co/1Qs4m8FujA\",\"display_url\":\"pic.twitter.com/1Qs4m8FujA\",\"expanded_url\":\"http://twitter.com/350EastAsia/status/727619609528926209/photo/1\",\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":1024,\"h\":768,\"resize\":\"fit\"},\"medium\":{\"w\":600,\"h\":450,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":255,\"resize\":\"fit\"}}}]},\"twitter_filter_level\":\"low\",\"twitter_lang\":\"en\"},\"favoritesCount\":0,\"twitter_entities\":{\"hashtags\":[{\"text\":\"piglaspilipinas\",\"indices\":[60,76]},{\"text\":\"breakfree2016\",\"indices\":[77,91]}],\"urls\":[],\"user_mentions\":[{\"screen_name\":\"350EastAsia\",\"name\":\"350 East Asia\",\"id\":2766632238,\"id_str\":\"2766632238\",\"indices\":[3,15]}],\"symbols\":[],\"media\":[{\"id\":727619578533060608,\"id_str\":\"727619578533060608\",\"indices\":[92,115],\"media_url\":\"http://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"media_url_https\":\"https://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"url\":\"https://t.co/1Qs4m8FujA\",\"display_url\":\"pic.twitter.com/1Qs4m8FujA\",\"expanded_url\":\"http://twitter.com/350EastAsia/status/727619609528926209/photo/1\",\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":1024,\"h\":768,\"resize\":\"fit\"},\"medium\":{\"w\":600,\"h\":450,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":255,\"resize\":\"fit\"}},\"source_status_id\":727619609528926209,\"source_status_id_str\":\"727619609528926209\",\"source_user_id\":2766632238,\"source_user_id_str\":\"2766632238\"}]},\"twitter_extended_entities\":{\"media\":[{\"id\":727619578533060608,\"id_str\":\"727619578533060608\",\"indices\":[92,115],\"media_url\":\"http://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"media_url_https\":\"https://pbs.twimg.com/media/ChkGJs4VIAAM2iZ.jpg\",\"url\":\"https://t.co/1Qs4m8FujA\",\"display_url\":\"pic.twitter.com/1Qs4m8FujA\",\"expanded_url\":\"http://twitter.com/350EastAsia/status/727619609528926209/photo/1\",\"type\":\"photo\",\"sizes\":{\"thumb\":{\"w\":150,\"h\":150,\"resize\":\"crop\"},\"large\":{\"w\":1024,\"h\":768,\"resize\":\"fit\"},\"medium\":{\"w\":600,\"h\":450,\"resize\":\"fit\"},\"small\":{\"w\":340,\"h\":255,\"resize\":\"fit\"}},\"source_status_id\":727619609528926209,\"source_status_id_str\":\"727619609528926209\",\"source_user_id\":2766632238,\"source_user_id_str\":\"2766632238\"}]},\"twitter_filter_level\":\"low\",\"twitter_lang\":\"en\",\"retweetCount\":3,\"gnip\":{\"matching_rules\":[{\"tag\":null}],\"urls\":[{\"url\":\"https://t.co/1Qs4m8FujA\",\"expanded_url\":\"http://twitter.com/350EastAsia/status/727619609528926209/photo/1\",\"expanded_status\":200}],\"klout_score\":51,\"language\":{\"value\":\"en\"}}}";
        Gson gson = new Gson();
        Tweet tweet = gson.fromJson(line, Tweet.class);
        assertEquals(tweet.actor.toString(),
                "id:twitter.com:524939820 garigalAnj 2012-03-15T02:11:16.000Z 2038 820 25853 1439 false");
    }
}

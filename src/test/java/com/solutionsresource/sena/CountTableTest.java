package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.CountTable;
import com.solutionsresource.sena.function.featurescaling.ReduceCountTables;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class CountTableTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CountTableTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(CountTableTest.class);
    }

    public void testReduce() throws Exception {
        CountTable countTable1 = new CountTable();
        CountTable countTable2 = new CountTable();

        countTable1.put(1.0, 200);
        countTable1.put(2.0, 100);
        countTable1.put(3.0, 160);

        countTable2.put(1.0, 100);
        countTable2.put(2.0, 600);
        countTable2.put(3.0, 260);

        CountTable resultTable = new ReduceCountTables().call(countTable1, countTable2);

        assertEquals(300, resultTable.get(1.0).intValue());
        assertEquals(700, resultTable.get(2.0).intValue());
        assertEquals(420, resultTable.get(3.0).intValue());
    }

    public void testLeastValue() throws Exception {
        CountTable countTable = new CountTable();

        countTable.put(1.0, 200);
        countTable.put(2.0, 100);
        countTable.put(3.0, 160);

        assertEquals(100, countTable.getLeastValue());
    }
}

package com.solutionsresource.sena;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

//import com.solutionsresource.sena.main.MessageClassifier;

/**
 * Unit test for simple App.
 */
public class MessageClassifierTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MessageClassifierTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(MessageClassifierTest.class);
    }

    public void testMock() throws Exception {
        /*
        MessageClassifier classifier = new MessageClassifier();

        String[] messageContents = new String[]{
                "Some message about binay",
                "About binay too",
        };

        Message[] classifiedMessages = classifier.classify(messageContents);

        for (Message message : classifiedMessages) {
            assertTrue("A mock classifier should return binay negative for all messages.",
                    message.hasPredictedTopic("election", "jejomar-binay"));
        }

        classifier = new MessageClassifier(new ArrayList<Domain>());

        try {
            classifier.classify(messageContents);

            assertTrue("Classifier did not throw an exception", false);
        } catch (ClassifierMustBeMockException ignored) {

        }
        */
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.function.featuretransformation.EmojiNormalizer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class EmojiNormalizerTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public EmojiNormalizerTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(EmojiNormalizerTest.class);
    }

    public void testParse() throws Exception {
        EmojiNormalizer emojiNormalizer = new EmojiNormalizer();

        assertEquals(
                "hindi yung mandadaya ka . UNICODE_55357 UNICODE_56834  # halalan 2016 # halalan 2016",
                emojiNormalizer.normalize("hindi yung mandadaya ka . \uD83D\uDE02 # halalan 2016 # halalan 2016")
        );
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Feature;
import com.solutionsresource.sena.entity.hashmap.IntegerCountTable;
import com.solutionsresource.sena.entity.WeightedLabeledPoint;
import com.solutionsresource.sena.function.featurescaling.ReduceFeatures;
import com.solutionsresource.sena.function.featuretransformation.GetFeatureIndexCountTable;
import com.solutionsresource.sena.util.build.FeatureReducer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class FeatureReducerTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FeatureReducerTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FeatureReducerTest.class);
    }

    public void testFeatureReducer() throws Exception {
        FeatureReducer reducer = new FeatureReducer();

        List<Feature> features = new ArrayList<Feature>();

        features.add(new Feature("hello", null, null));
        features.add(new Feature("world", null, null));
        features.add(new Feature("santa", null, null));
        features.add(new Feature("claus", null, null));
        features.add(new Feature("!", null, null));

        features = reducer.reduce(features, new Integer[] {0, 2, 3});

        assertEquals(3, features.size());
        assertEquals("hello", features.get(0).getValue());
        assertEquals("santa", features.get(1).getValue());
        assertEquals("claus", features.get(2).getValue());
    }

    public void testLabeledPointFeaturesReducer() throws Exception {
        WeightedLabeledPoint labeledPoint = new WeightedLabeledPoint(1.0,
                Vectors.dense(new double[]{0.0, 2.0, 3.0, 0.0, 5.0}), 1.0f);

        GetFeatureIndexCountTable countTableGetter = new GetFeatureIndexCountTable();

        IntegerCountTable countTable = countTableGetter.call(labeledPoint);

        Integer[] featureIndexes = countTable.getIntKeys();

        assertEquals(3, countTable.size());
        assertEquals(2, countTable.get(1).intValue());
        assertEquals(3, countTable.get(2).intValue());
        assertEquals(5, countTable.get(4).intValue());

        ReduceFeatures reducer = new ReduceFeatures(featureIndexes);

        labeledPoint = reducer.call(labeledPoint);

        double[] features = labeledPoint.features().toArray();

        assertEquals(3, features.length);
        assertEquals(2.0, features[0]);
        assertEquals(3.0, features[1]);
        assertEquals(5.0, features[2]);
    }
}

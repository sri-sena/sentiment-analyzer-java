package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import ph.talas.alexis.engine.LoadRules;
import ph.talas.alexis.engine.TopicAnalyzer;
import ph.talas.alexis.exception.ExcludedTopicException;
import ph.talas.alexis.pojo.Topic;
import scala.Tuple2;
import scala.Tuple3;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class TopicAnalyzerTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TopicAnalyzerTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TopicAnalyzerTest.class);
    }

    public void testBiasRules() throws Exception {

        Constants constants = new Constants();

        List<Topic> topics = LoadRules.loadTopics(constants.BIAS_RULES_XML);
        TopicAnalyzer topicAnalyzer = new TopicAnalyzer(topics);

        Tuple3<Tuple2<String, String[]>[], String[], String[]> tests =
                new Tuple3<Tuple2<String, String[]>[], String[], String[]>(
                        new Tuple2[]{ // included
                                new Tuple2<String, String[]>(
                                        "sino pinagloloko mo binay?",
                                        new String[]{
                                                "jejomar-binay",
                                        }
                                ),
                                new Tuple2<String, String[]>(
                                        "RT @noemimejia__: Si Grace Poe yung kaklase mong " +
                                                "kahit jokes ng teacher, nasa notes",
                                        new String[]{
                                                "grace-poe",
                                        }
                                ),
                                new Tuple2<String, String[]>(
                                        "Game! Rodrigo Duterte vs Mar Roxas!",
                                        new String[]{
                                                "rodrigo-duterte",
                                                "mar-roxas",
                                        }
                                ),
                                new Tuple2<String, String[]>(
                                        "Sampalan na this! Rodrigo Duterte vs Mar Roxas!",
                                        new String[]{
                                                "rodrigo-duterte",
                                                "mar-roxas",
                                        }
                                )
                        },
                        new String[]{ // excluded
                                "your grace, edgar allan poe!",
                                "umulan ng yelo sa may roxas city",
                                "I love Roxas from Kingdom Hearts!",
                                "Trapik sa may Roxas Blvd",
                        },
                        new String[]{ // unknown
                                "wala lang. tweet lang.",
                                "ewan ko sayo beh",
                                "Grace, go to http://twitter.com/ASDwerdsfPOEqwerasdf",
                                "Grace, go to https://twitter.com/ASDwerdsfPOEqwerasdf",
                                "RT AxelNqpedersen: RT lizquen_fanboys: EverydayILoveYou Holiday " +
                                        "#PushAwardsLizQuens LizQuen Graces Bench/Kashieca? " +
                                        "https://t.co/VWXQSSSKAf #PushAwardsLizQuens",
                                "@servirealiis @divinesapplaud @WhoisLonnieG   Ah. 13 followers. Poe",
                                "@fedward @ReadingRoomDC looks like a Poe man's cocktail." +
                                        " #badumbumCHING! #badumbumCHING",
                                "Grace opening presents... #80dollars @AlezzBerton2 @graceamick99 #80dollars",
                        }
                );

        for (Tuple2<String, String[]> includedTweet : tests._1()) {
            try {
                List<String> matchedTopics = topicAnalyzer.match(includedTweet._1());
                for (String topicName : includedTweet._2()) {
                    assertTrue(String.format("\"%s\" should include %s.", includedTweet._1(), topicName),
                            matchedTopics.contains(topicName));
                }
            } catch (ExcludedTopicException e) {
                assertFalse(String.format("\"%s\" should not be excluded.", includedTweet._1()), true);
            }
        }

        for (String excludedTweet : tests._2()) {
            assertThrowsException("\"%s\" should be excluded.", excludedTweet, topicAnalyzer);
        }

        for (String unknownTweet : tests._3()) {
            try {
                List<String> topicMatches = topicAnalyzer.match(unknownTweet);
                assertTrue(String.format("\"%s\" should be unclassified. Result: %s", unknownTweet, topicMatches),
                        topicMatches.size() == 0);
            } catch (ExcludedTopicException e) {
                assertFalse(String.format("\"%s\" should not be excluded.", unknownTweet), true);
            }
        }
    }

    private void assertThrowsException(String format, String content, TopicAnalyzer topicAnalyzer) {
        try {
            List<String> match = topicAnalyzer.match(content);
            assertTrue(String.format("%s Result: %s", String.format(format, content), match), false);
        } catch (Exception ignored) {
        }
    }
}

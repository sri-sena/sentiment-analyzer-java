package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import com.solutionsresource.sena.function.featurescaling.GetUniqueSlugs;
import com.solutionsresource.sena.function.featuretransformation.GetDomains;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.model.SlugsGetter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Unit test for simple App.
 */
public class SlugsGetterTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SlugsGetterTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(SlugsGetterTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testMap() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain election = Domain.mock();
        Domain restaurant = Domain.mock();
        restaurant.setName("Restaurants");

        domains.add(election);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        DomainSlugs acceptedDomainSlugs = AcceptedSlugsGetter.mock();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));
        DomainSlugs domainSlugs = domainSlugsGetter.get(message);

        assertEquals(1, domainSlugs.size());

        Set<String> domainNames = domainSlugs.keySet();
        String domainName = domainNames.toArray(new String[domainNames.size()])[0];
        TopicSlugs topicSlugs = domainSlugs.get(domainName);

        assertEquals("election", domainName);

        assertEquals(2, topicSlugs.size());

        assertTrue(topicSlugs.containsKey("rodrigo-duterte"));
        assertTrue(topicSlugs.containsKey("jejomar-binay"));

        domains.add(restaurant);

        domainSlugs = domainSlugsGetter.get(message);

        assertTrue(domainSlugs.containsKey("election"));
        assertTrue(domainSlugs.containsKey("restaurants"));

        assertEquals(2, domainSlugs.size());

        assertTrue(topicSlugs.containsKey("jejomar-binay"));
        assertTrue(topicSlugs.containsKey("rodrigo-duterte"));

        assertEquals(2, topicSlugs.size());

        topicSlugs = domainSlugs.get("restaurants");

        assertEquals(0, topicSlugs.size());

        assertTrue(message.hasDomain("election"));
        assertTrue(message.hasDomain("restaurants"));
        assertTrue(message.hasTopic("election", "jejomar-binay"));
        assertTrue(message.hasTopic("election", "rodrigo-duterte"));
    }

    public void testReduce() throws Exception {
        GetUniqueSlugs getter = new GetUniqueSlugs();
        DomainSlugs slugs1 = new DomainSlugs();
        DomainSlugs slugs2 = new DomainSlugs();

        TopicSlugs topicSlugs1 = new TopicSlugs();
        topicSlugs1.put("duterte", Sentiment.getNames());
        topicSlugs1.put("roxas", Sentiment.getNames());
        topicSlugs1.put("santiago", Sentiment.getNames());

        TopicSlugs topicSlugs2 = new TopicSlugs();
        topicSlugs2.put("jollibee", Sentiment.getNames());
        topicSlugs2.put("mcdo", Sentiment.getNames());
        topicSlugs2.put("kfc", Sentiment.getNames());

        slugs1.put("election", topicSlugs1);
        slugs1.put("restaurants", topicSlugs2);

        TopicSlugs topicSlugs3 = new TopicSlugs();
        topicSlugs3.put("binay", Sentiment.getNames());
        topicSlugs3.put("poe", Sentiment.getNames());
        topicSlugs3.put("santiago", Sentiment.getNames());

        TopicSlugs topicSlugs4 = new TopicSlugs();
        topicSlugs4.put("smart-customer-service", Sentiment.getNames());
        topicSlugs4.put("smart-888", Sentiment.getNames());
        topicSlugs4.put("pldt-fixed-line", Sentiment.getNames());

        slugs2.put("election", topicSlugs3);
        slugs2.put("telecommunications", topicSlugs4);

        DomainSlugs slugs3 = getter.call(slugs1, slugs2);

        assertTrue(slugs3.containsKey("election"));
        assertTrue(slugs3.containsKey("restaurants"));
        assertTrue(slugs3.containsKey("telecommunications"));
        assertEquals(3, slugs3.size());

        assertTrue(slugs3.get("election").containsKey("duterte"));
        assertTrue(slugs3.get("election").containsKey("binay"));
        assertTrue(slugs3.get("election").containsKey("roxas"));
        assertTrue(slugs3.get("election").containsKey("santiago"));
        assertTrue(slugs3.get("election").containsKey("poe"));
        assertEquals(5, slugs3.get("election").size());

        assertTrue(slugs3.get("restaurants").containsKey("jollibee"));
        assertTrue(slugs3.get("restaurants").containsKey("mcdo"));
        assertTrue(slugs3.get("restaurants").containsKey("kfc"));
        assertEquals(3, slugs3.get("restaurants").size());

        assertTrue(slugs3.get("telecommunications").containsKey("smart-customer-service"));
        assertTrue(slugs3.get("telecommunications").containsKey("smart-888"));
        assertTrue(slugs3.get("telecommunications").containsKey("pldt-fixed-line"));
        assertEquals(4, slugs3.get("telecommunications").get("pldt-fixed-line").length);
        assertEquals(3, slugs3.get("telecommunications").size());
    }
}

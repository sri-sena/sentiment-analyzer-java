package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.bias.ClassBiases;
import com.solutionsresource.sena.bias.DomainBiases;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ClassBiasesTest extends TestCase {
  private final DomainBiases domainBiases;
  private final Constants constants;

  public ClassBiasesTest(String testName) throws Exception {
    super(testName);

    this.constants = new Constants();
    this.domainBiases = constants.DOMAIN_BIASES;
  }


  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(ClassBiasesTest.class);
  }

  public void testDomainBiases() throws Exception {
    ClassBiases electionBiases = domainBiases.get("election");
    ClassBiases restaurantBiases = domainBiases.get("restaurant");

    double unknown = constants.DOUBLE_UNKNOWN;
    double true_ = constants.DOUBLE_TRUE;
    double false_ = constants.DOUBLE_FALSE;

    assertEquals(unknown, electionBiases.score("This is not related to elections"));
    assertEquals(true_, electionBiases.score("This is related to elections #halalan2016"));
    assertEquals(true_, electionBiases.score("This is it! #Duterte2016"));
    assertEquals(true_, electionBiases.score("Go go go go go! #BBM2016"));
    assertEquals(true_, electionBiases.score("Go go go go go! #SaMa2016"));
    assertEquals(true_, electionBiases.score("Go go go go go! #SiMiriamAngSagot"));
    assertEquals(unknown, electionBiases.score("guys sama kayo sa outing?"));
    assertEquals(true_, electionBiases.score("bihon 2016 <3"));
    assertEquals(false_, electionBiases.score("pansit bihon <3"));

    assertEquals(true_, restaurantBiases.score("kainan"));
    assertEquals(true_, restaurantBiases.score("craving"));
    assertEquals(true_, restaurantBiases.score("dinner at"));
    assertEquals(true_, restaurantBiases.score("lunch at"));
    assertEquals(true_, restaurantBiases.score("breakfast at"));
    assertEquals(true_, restaurantBiases.score("kain tayo sa"));
    assertEquals(unknown, restaurantBiases.score("yummy"));
  }

  public void testTopicBiases() throws Exception {
    ClassBiases electionBiases = domainBiases.get("election");
    ClassBiases restaurantBiases = domainBiases.get("restaurant");

    double unknown = constants.DOUBLE_UNKNOWN;
    double true_ = constants.DOUBLE_TRUE;
    double false_ = constants.DOUBLE_FALSE;

    String content1 = "This is it! No to mar roxas! #Duterte2016";
    String content2 = "#justDuIt!!!";


    assertEquals(-1.0, electionBiases.topicScore("jejomar-binay", "This is related to elections #halalan2016"));
    assertEquals(1.0, electionBiases.topicScore("miriam-defensor-santiago", "#MIRIAMforPRESIDENT go go go!"));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", "Du30 Du30 Du30"));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", "#Du31 na guys!"));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", "Hello everyone! Si #Du32!"));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", "#Du34president"));
    assertEquals(-1.0, electionBiases.topicScore("rodrigo-duterte", "#Du35"));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", content1));
    assertEquals(1.0, electionBiases.topicScore("mar-roxas", content1));
    assertEquals(-1.0, electionBiases.topicScore("miriam-defensor-santiago", content1));
    assertEquals(-1.0, electionBiases.topicScore("leni-robredo", content1));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", content2));
    assertEquals(-1.0, electionBiases.topicScore("mar-roxas", content2));
    assertEquals(-1.0, electionBiases.topicScore("jejomar-binay", content2));
    assertEquals(-1.0, electionBiases.topicScore("miriam-defensor-santiago", "Go go go go go! #BBM2016"));
    assertEquals(-1.0, electionBiases.topicScore("jejomar-binay", "Go go go go go! #BBM2016"));
    assertEquals(1.0, electionBiases.topicScore("rodrigo-duterte", "Cayetano will be duterte's downfal! #BBM2016"));
    assertEquals(1.0, electionBiases.topicScore("miriam-defensor-santiago", "Go go go go go! #SaMa2016"));
    assertEquals(-1.0, electionBiases.topicScore("miriam-defensor-santiago", "bihon 2016 <3"));
    assertEquals(1.0, electionBiases.topicScore("jejomar-binay", "kaya yan ni jejomar!"));
    assertEquals(1.0, electionBiases.topicScore("jejomar-binay", "bihon 2016 <3"));
    assertEquals(true_, electionBiases.topicScore("miriam-defensor-santiago", "#MDSforPRESIDENT2016"));
    assertEquals(true_, electionBiases.topicScore("miriam-defensor-santiago", "#MiriamTapangAtLakas"));
    assertEquals(true_, electionBiases.topicScore("leni-robredo", "#LeniIsMyVP"));


    assertEquals(true_, restaurantBiases.topicScore("yellow-cab", "#YellowCabPizza"));
  }


  public void testSentimentBiases() throws Exception {
    ClassBiases electionBiases = domainBiases.get("election");
    ClassBiases restaurantBiases = domainBiases.get("restaurant");

    double unknown = constants.DOUBLE_UNKNOWN; // -1
    double negative = constants.DOUBLE_NEGATIVE; // 1
    double neutral = constants.DOUBLE_NEUTRAL; // 2
    double positive = constants.DOUBLE_POSITIVE; // 3

    String content1 = "This is it! No to mar roxas! #Duterte2016";
    String content2 = "Cayetano will be duterte's downfall! #BBM2016";
    String content3 = "Go go go go go! #BBM2016";

    assertEquals(-1.0, domainBiases.get("election").sentimentScore("jejomar-binay", "This is related to elections #halalan2016"));
    assertEquals(1.0, domainBiases.get("election").sentimentScore("mar-roxas", content1));
    assertEquals(-1.0, domainBiases.get("election").sentimentScore("leni-robredo", content1));
    assertEquals(3.0, domainBiases.get("election").sentimentScore("rodrigo-duterte", "#justDuIt!!!"));
    assertEquals(1.0, domainBiases.get("election").sentimentScore("rodrigo-duterte", content2));
    assertEquals(-1.0, domainBiases.get("election").sentimentScore("miriam-defensor-santiago", content3));
    assertEquals(-1.0, domainBiases.get("election").sentimentScore("jejomar-binay", content3));
    assertEquals(positive, domainBiases.get("election").sentimentScore("miriam-defensor-santiago", "Go go go go go! #SaMa2016"));
    assertEquals(positive, domainBiases.get("election").sentimentScore("jejomar-binay", "bihon 2016 <3"));

    assertEquals(positive, restaurantBiases.sentimentScore("chowking", "masarap talaga"));
    assertEquals(negative, restaurantBiases.sentimentScore("chowking", "may gagamba"));
    assertEquals(positive, restaurantBiases.sentimentScore("chowking", "masarap"));
    assertEquals(negative, restaurantBiases.sentimentScore("chowking", "may daga"));
    assertEquals(positive, restaurantBiases.sentimentScore("chowking", "chowking please"));
    assertEquals(negative, restaurantBiases.sentimentScore("chowking", "panget ng service"));
    assertEquals(negative, restaurantBiases.sentimentScore("chowking", "pangit ng service"));
    assertEquals(negative, restaurantBiases.sentimentScore("chowking", "pangit ng serbisyo"));

    assertEquals(positive, restaurantBiases.sentimentScore("yellow-cab", "sarap ng pizza"));
    assertEquals(neutral, restaurantBiases.sentimentScore("yellow-cab", "pizza"));
    assertEquals(positive, restaurantBiases.sentimentScore("yellow-cab", "21 pizza boxes"));
    assertEquals(positive, restaurantBiases.sentimentScore("yellow-cab", "pepperoni friday pizza"));
    assertEquals(positive, restaurantBiases.sentimentScore("yellow-cab", "#DearDarlaChallenge"));
    assertEquals(positive, restaurantBiases.sentimentScore("yellow-cab", "#TwoPizzaFriday"));

  }
}

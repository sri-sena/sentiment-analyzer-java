package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Feature;
import com.solutionsresource.sena.entity.WeightedLabeledPoint;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featureselection.MutualInformation;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class MultiplyFeaturesTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MultiplyFeaturesTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(MultiplyFeaturesTest.class);
    }

    public void testMultiplication() throws Exception {
        UsageTable exportUsages = new UsageTable();
        exportUsages.put("true", 49);
        exportUsages.put("false", 141);
        UsageTable exportInPoultry = new UsageTable();
        exportInPoultry.put("used", 190);
        exportInPoultry.put("not used", 801758);
        UsageTable poultryUsages = new UsageTable();
        poultryUsages.put("true", 27701);
        poultryUsages.put("false", 774247);
        MutualInformation exportMutualInformation = new MutualInformation("export", exportUsages,
                exportInPoultry, poultryUsages, 801948);

        UsageTable theUsages = new UsageTable();
        theUsages.put("true", 27701);
        theUsages.put("false", 401758);
        UsageTable theInPoultry = new UsageTable();
        theInPoultry.put("used", 429459);
        theInPoultry.put("not used", 372489);
        MutualInformation theMutualInformation = new MutualInformation("the", theUsages,
                theInPoultry, poultryUsages, 801948);

        Feature export = new Feature("export", null, exportMutualInformation);
        Feature the = new Feature("the", null, theMutualInformation);
        List<Feature> features = new ArrayList<Feature>();

        features.add(export);
        features.add(the);

        WeightedLabeledPoint w1 = new WeightedLabeledPoint(1.0, Vectors.dense(2.0, 1.0), 2.0f);
        WeightedLabeledPoint w2 = new WeightedLabeledPoint(0.0, Vectors.dense(6.0, 1.0), 1.0f);

        double[] weightedFeatures1 = w1.withMultipliedFeatures(features).features().toArray();
        double[] weightedFeatures2 = w2.withMultipliedFeatures(features).features().toArray();

        assertTrue(weightedFeatures1[0] < weightedFeatures1[1]);
        assertTrue(weightedFeatures2[0] < weightedFeatures2[1]);

    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.MultiplierTable;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.GetWeightedLabeledPoints;
import com.solutionsresource.sena.function.featurescaling.ReduceFeatures;
import com.solutionsresource.sena.function.featureselection.FilterByWeightThreshold;
import com.solutionsresource.sena.function.featuretransformation.ExtractNGrams;
import com.solutionsresource.sena.util.build.Features;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class WeightedLabeledPointTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public WeightedLabeledPointTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(WeightedLabeledPointTest.class);
    }

    public void testFloatKeyAt() throws Exception {
        WeightedLabeledPoint w1 = new WeightedLabeledPoint(1.0, Vectors.dense(1.0, 2.0, 1.0), 3.0f);
        WeightedLabeledPoint w2 = new WeightedLabeledPoint(1.0, Vectors.dense(1.0, 2.0, 1.0), 1.0f);
        WeightedLabeledPoint w3 = new WeightedLabeledPoint(1.0, Vectors.dense(1.0, 2.0, 1.0), 2.0f);

        FilterByWeightThreshold filter = new FilterByWeightThreshold(2.0f);

        assertEquals(true, (boolean) filter.call(w1));
        assertEquals(false, (boolean) filter.call(w2));
        assertEquals(true, (boolean) filter.call(w3));
    }

    public void testReduceFeatures() throws Exception {
        WeightedLabeledPoint w1 = new WeightedLabeledPoint(1.0, Vectors.dense(1.0, 2.0, 1.0, 4.0, 8.0), 3.0f);
        WeightedLabeledPoint w2 = new WeightedLabeledPoint(1.0, Vectors.dense(9.0, 2.0, 3.0, 5.0, 10.0), 1.0f);
        WeightedLabeledPoint w3 = new WeightedLabeledPoint(1.0, Vectors.dense(2.0, 2.0, 7.0, 1.0, 7.0), 2.0f);

        ReduceFeatures reduceFeatures = new ReduceFeatures(new Integer[] {0, 2, 3});

        assertEquals(Vectors.dense(1.0, 1.0, 4.0), reduceFeatures.call(w1).features());
        assertEquals(Vectors.dense(9.0, 3.0, 5.0), reduceFeatures.call(w2).features());
        assertEquals(Vectors.dense(2.0, 7.0, 1.0), reduceFeatures.call(w3).features());
    }

    public void testMessageToWeightedLabeledPoint() throws Exception {
        List<Feature> features = new ArrayList<Feature>();


        UsageTable ngramUsages = new UsageTable();
        UsageTable classUsages = new UsageTable();

        classUsages.put("true", 30);
        classUsages.put("false", 70);

        MultiplierTable multiplierTable = MultiplierTable.create(30.0);

        GetWeightedLabeledPoints getWeightedLabeledPoints = new GetWeightedLabeledPoints(
                features, "election", "jejomar-binay", multiplierTable);

        ExtractNGrams extractNGrams = new ExtractNGrams();

        ngramUsages.put("binay", 20);
        ngramUsages.put("duterte", 0);

        features.add(new Feature("binay", ngramUsages, Features.mutualInformation(
                "binay", ngramUsages, classUsages, 40)));

        ngramUsages.put("binay", 10);
        ngramUsages.put("duterte", 10);

        features.add(new Feature("panget", ngramUsages, Features.mutualInformation(
                "panget", ngramUsages, classUsages, 30)));

        ngramUsages.put("binay", 10);
        ngramUsages.put("duterte", 10);

        features.add(new Feature("duterte", ngramUsages, Features.mutualInformation(
                "duterte", ngramUsages, classUsages, 30)));

        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        List<Domain> domains2 = new ArrayList<Domain>();
        domain = Domain.mock();
        domain.getTopics().get(0).setName("duterte");
        domains.add(domain);

        Message message2 = new Message("2", "ang yabang yabang ni duterte", domains2, true);

        extractNGrams.call(message);
        extractNGrams.call(message2);

        WeightedLabeledPoint w1 = getWeightedLabeledPoints.call(message);
        WeightedLabeledPoint w2 = getWeightedLabeledPoints.call(message2);

        // todo: multiplier is ignored because classUsages is not true or false
        assertEquals(Vectors.dense(1.0, 2.0, 0.0), w1.features());
        assertEquals(1.0, w1.label());
        assertEquals(Vectors.dense(0.0, 0.0, 30.0), w2.features());
        assertEquals(0.0, w2.label());
    }
}

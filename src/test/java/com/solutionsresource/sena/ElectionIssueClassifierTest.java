package com.solutionsresource.sena;

import com.solutionsresource.sena.util.parse.ElectionIssueClassifier;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Unit test for simple App.
 */
public class ElectionIssueClassifierTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ElectionIssueClassifierTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ElectionIssueClassifierTest.class);
    }

    public void testElectionIssueClassifier() throws Exception {
        ElectionIssueClassifier eic = new ElectionIssueClassifier();
        Tuple2[] testCases = new Tuple2[]{
                new Tuple2<>("meron nang pila sa balota!", new ArrayList<>(Arrays.asList("others", "presinto-issue"))),
                new Tuple2<>("ayaw gumana ng pcos machine", new ArrayList<>(Arrays.asList("presinto-issue")))
        };


        for (Tuple2 tc : testCases) {
            String tweet = tc._1().toString();
            ArrayList<String> topics = eic.classify(tweet);
            ArrayList<String> topic = ((ArrayList)tc._2());
            for(int index = 0; index < topic.size(); index++ ){
                assertTrue( tweet + " must contain " + topic, topics.contains(topic.get(index)));
            }
        }
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.BytesTable;
import com.solutionsresource.sena.entity.gnip.fb.Comment;
import com.solutionsresource.sena.entity.gnip.fb.FacebookPost;
import com.solutionsresource.sena.function.featuretransformation.FacebookBytesTableGenerator;
import com.solutionsresource.sena.function.featuretransformation.FacebookJsonReader;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class ReadFacebookJsonTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ReadFacebookJsonTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ReadFacebookJsonTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() throws Exception {
        FacebookJsonReader reader = new FacebookJsonReader();

        FacebookPost post = reader.read(new BufferedReader(new FileReader("FBComment.json")));

        assertEquals(2, post.comments.size());
        assertEquals("", post.Post.caption);

        Constants constants = new Constants();

        FacebookBytesTableGenerator generator = new FacebookBytesTableGenerator(constants);
        // // TODO: 5/18/2016  ; get post content

        List<BytesTable> tables = new ArrayList<>();

        for (Comment comment : post.comments) {
            tables.add(generator.generate(post, comment));
        }

        BytesTable table0 = tables.get(0);
        assertEquals("1039383852772544_1039400042770925", table0.get(constants.TABLE_MESSAGE_MSGDATA_REF_ID_TEXT));
        assertEquals("Welcome to the Philippines  Sir n madam.God bless", table0.get(constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT));
        assertEquals("2015-11-15T11:10:11+0000", table0.get(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_TEXT));
        assertEquals("Alexis Alexis", table0.get(constants.TABLE_MESSAGE_MSGDATA_USERNAME_TEXT));
        assertEquals("459050784282908", table0.get(constants.TABLE_MESSAGE_MSGDATA_USER_ID_TEXT));
        assertEquals("2", table0.get(constants.TABLE_MESSAGE_MSGDATA_LIKES_COUNT_TEXT));
        assertEquals("test1, test2", table0.get(constants.TABLE_MESSAGE_MSGDATA_HASHTAG_TEXT));


        BytesTable table1 = tables.get(1);
        assertEquals("1039383852772544_1039672749410321", table1.get(constants.TABLE_MESSAGE_MSGDATA_REF_ID_TEXT));
        assertEquals("Sa halip na ipagdasal natin na maging mapayapa at matagumpay ang usapin sa APEC..mga walang ", table1.get(constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT));
        assertEquals("2015-11-16T00:16:26+0000", table1.get(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_TEXT));
        assertEquals("Jhony Balzano", table1.get(constants.TABLE_MESSAGE_MSGDATA_USERNAME_TEXT));
        assertEquals("180130418998521", table1.get(constants.TABLE_MESSAGE_MSGDATA_USER_ID_TEXT));
        assertEquals("0", table1.get(constants.TABLE_MESSAGE_MSGDATA_LIKES_COUNT_TEXT));
        assertEquals("test1, test2", table1.get(constants.TABLE_MESSAGE_MSGDATA_HASHTAG_TEXT));



    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.function.featurescaling.ParseFeature;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class FirstElementInSplitGetterTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public FirstElementInSplitGetterTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(FirstElementInSplitGetterTest.class);
    }

    public void testSplit() throws Exception {
        ParseFeature parser = new ParseFeature();

        assertEquals(parser.call("hi ===== 0.001 | beautiful").getValue(), "hi");
        assertEquals(parser.call("hello world ===== 0.001 | hi").getValue(), "hello world");
        assertEquals(parser.call("hi world ===== 0.001 | hello").getValue(), "hi world");
        assertEquals(parser.call("nothing ===== 0.001 | impossible").getValue(), "nothing");
        assertEquals(parser.call(" ===== 0.001 |  ").getValue(), "");
        assertEquals(parser.call("? ===== 0.001").getValue(), "?");

    }
}

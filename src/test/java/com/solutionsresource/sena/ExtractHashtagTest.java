package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featuretransformation.ExtractHashtags;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class ExtractHashtagTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ExtractHashtagTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ExtractHashtagTest.class);
    }

    public void testExtraction() throws Exception {
        ExtractHashtags extractHashtags = new ExtractHashtags();
        String content1 = "Go talaga #Duterte2016 #NoToMarLeni2016 #Duterte2016";
        String content2 = "#NoToDuterte2016 kaya nyo yan #marleni2016";

        UsageTable hashtagUsages1 = extractHashtags.getHashtagUsages(content1);
        UsageTable hashtagUsages2 = extractHashtags.getHashtagUsages(content2);

        assertTrue("hashtagUsages1 should have 2 keys", hashtagUsages1.size() == 2);
        assertTrue("hashtagUsages1 should have #duterte2016", hashtagUsages1.containsKey("#duterte2016"));
        assertTrue("hashtagUsages1 #duterte2016 should have value of 2", hashtagUsages1.get("#duterte2016") == 2);
        assertTrue("hashtagUsages1 should have #notomarleni2016", hashtagUsages1.containsKey("#notomarleni2016"));
        assertTrue("hashtagUsages1 #notomarleni2016 should have value of 1",
                hashtagUsages1.get("#notomarleni2016") == 1);

        assertTrue("hashtagUsages2 should have 2 keys", hashtagUsages2.size() == 2);
        assertTrue("hashtagUsages2 should have #notoduterte2016", hashtagUsages2.containsKey("#notoduterte2016"));
        assertTrue("hashtagUsages2 #notoduterte2016 should have value of 1",
                hashtagUsages2.get("#notoduterte2016") == 1);
        assertTrue("hashtagUsages2 should have #marleni2016", hashtagUsages2.containsKey("#marleni2016"));
        assertTrue("hashtagUsages2 #marleni2016 should have value of 1", hashtagUsages2.get("#marleni2016") == 1);
    }
}

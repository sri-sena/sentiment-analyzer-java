package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTables;
import com.solutionsresource.sena.function.featuretransformation.ConvertToUsageTables;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;

/**
 * Unit test for simple App.
 */
public class UsageTablesTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public UsageTablesTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(UsageTablesTest.class);
    }

    public void testBinary() throws Exception {
        LabeledPoint training1 = new LabeledPoint(0.0, Vectors.dense(3.0, 4.0, 1.0, 0.0));
        LabeledPoint training2 = new LabeledPoint(0.0, Vectors.dense(0.0, 6.0, 7.0, 1.0));
        LabeledPoint training3 = new LabeledPoint(1.0, Vectors.dense(4.0, 9.0, 1.0, 7.0));
        LabeledPoint training4 = new LabeledPoint(1.0, Vectors.dense(8.0, 2.0, 3.0, 1.0));

        String[] labels = new String[] {"false", "true"};

        ConvertToUsageTables convertToUsageTables = new ConvertToUsageTables(labels);
        ReduceUsageTables reduceUsageTables = new ReduceUsageTables();

        UsageTable[] usageTables1 = convertToUsageTables.call(training1);
        UsageTable[] usageTables2 = convertToUsageTables.call(training2);
        UsageTable[] usageTables3 = convertToUsageTables.call(training3);
        UsageTable[] usageTables4 = convertToUsageTables.call(training4);

        assertEquals(3, (int) usageTables1[0].get("false"));
        assertEquals(0, (int) usageTables1[3].get("false"));

        assertEquals(0, (int) usageTables2[0].get("false"));
        assertEquals(7, (int) usageTables2[2].get("false"));

        UsageTable[] reducedUsageTables1 = reduceUsageTables.call(usageTables1, usageTables3);
        UsageTable[] reducedUsageTables2 = reduceUsageTables.call(usageTables3, usageTables4);
        UsageTable[] reducedUsageTables3 = reduceUsageTables.call(
                reduceUsageTables.call(usageTables1, usageTables2),
                reduceUsageTables.call(usageTables3, usageTables4)
        );

        assertEquals(3, (int) reducedUsageTables1[0].get("false"));
        assertEquals(4, (int) reducedUsageTables1[0].get("true"));
        assertEquals(0, (int) reducedUsageTables1[3].get("false"));
        assertEquals(7, (int) reducedUsageTables1[3].get("true"));

        assertEquals(12, (int) reducedUsageTables2[0].get("true"));
        assertEquals(8, (int) reducedUsageTables2[3].get("true"));

        assertEquals(3, (int) reducedUsageTables3[0].get("false"));
        assertEquals(12, (int) reducedUsageTables3[0].get("true"));
        assertEquals(1, (int) reducedUsageTables3[3].get("false"));
        assertEquals(8, (int) reducedUsageTables3[3].get("true"));
    }

    public void testMultilabel() throws Exception {
        LabeledPoint training1 = new LabeledPoint(0.0, Vectors.dense(3.0, 4.0, 1.0, 0.0));
        LabeledPoint training2 = new LabeledPoint(1.0, Vectors.dense(0.0, 6.0, 7.0, 1.0));
        LabeledPoint training3 = new LabeledPoint(2.0, Vectors.dense(4.0, 9.0, 1.0, 7.0));
        LabeledPoint training4 = new LabeledPoint(1.0, Vectors.dense(8.0, 2.0, 3.0, 1.0));
        LabeledPoint training5 = new LabeledPoint(3.0, Vectors.dense(14.0, 8.0, 2.0, 4.0));

        String[] labels = new String[] {"???", "negative", "neutral", "positive"};

        ConvertToUsageTables convertToUsageTables = new ConvertToUsageTables(labels);
        ReduceUsageTables reduceUsageTables = new ReduceUsageTables();

        UsageTable[] usageTables1 = convertToUsageTables.call(training1);
        UsageTable[] usageTables2 = convertToUsageTables.call(training2);
        UsageTable[] usageTables3 = convertToUsageTables.call(training3);
        UsageTable[] usageTables4 = convertToUsageTables.call(training4);
        UsageTable[] usageTables5 = convertToUsageTables.call(training5);

        assertEquals(3, (int) usageTables1[0].get("???"));
        assertEquals(0, (int) usageTables1[3].get("???"));

        assertEquals(0, (int) usageTables2[0].get("negative"));
        assertEquals(7, (int) usageTables2[2].get("negative"));

        UsageTable[] reducedUsageTables13 = reduceUsageTables.call(usageTables1, usageTables3);
        UsageTable[] reducedUsageTables34 = reduceUsageTables.call(usageTables3, usageTables4);

        assertEquals(3, (int) reducedUsageTables13[0].get("???"));
        assertEquals(4, (int) reducedUsageTables13[0].get("neutral"));
        assertEquals(0, (int) reducedUsageTables13[3].get("???"));
        assertEquals(7, (int) reducedUsageTables13[3].get("neutral"));

        assertEquals(8, (int) reducedUsageTables34[0].get("negative"));
        assertEquals(7, (int) reducedUsageTables34[3].get("neutral"));

        UsageTable[] reducedUsageTables12 = reduceUsageTables.call(usageTables1, usageTables2);
        UsageTable[] reducedUsageTables345 = reduceUsageTables.call(reducedUsageTables34, usageTables5);
        UsageTable[] reducedUsageTables12345 = reduceUsageTables.call(reducedUsageTables12, reducedUsageTables345);

        assertEquals(14, (int) reducedUsageTables12345[0].get("positive"));
        assertEquals(4, (int) reducedUsageTables12345[0].get("neutral"));
        assertEquals(4, (int) reducedUsageTables12345[3].get("positive"));
        assertEquals(1, (int) reducedUsageTables12345[2].get("neutral"));
        assertEquals(8, (int) reducedUsageTables12345[0].get("negative"));
        assertEquals(10, (int) reducedUsageTables12345[2].get("negative"));
    }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.function.featureselection.FilterWithOffset;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Unit test for simple App.
 */
public class FilterWithOffsetTest extends TestCase {
  /**
   * Create the test case
   *
   * @param testName name of the test case
   */
  public FilterWithOffsetTest(String testName) throws Exception {
    super(testName);
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(FilterWithOffsetTest.class);
  }

  public void testFilter() throws Exception {
    ResultHashMap offset = new ResultHashMap(Bytes.toBytes("1432-twitter-1234"));

    FilterWithOffset filter = new FilterWithOffset(Bytes.toString(offset.getRow()));

    ResultHashMap result1 = new ResultHashMap(Bytes.toBytes("1431-twitter-1234"));
    ResultHashMap result2 = new ResultHashMap(Bytes.toBytes("1432-twitter-1244"));
    ResultHashMap result3 = new ResultHashMap(Bytes.toBytes("1433-twitter-1244"));

    assertFalse("1431 should be rejected.", filter.call(result1));
    assertFalse("1432 should be rejected.", filter.call(offset));
    assertTrue("1432 should be accepted.", filter.call(result2));
    assertTrue("1433 should be accepted.", filter.call(result3));
  }
}

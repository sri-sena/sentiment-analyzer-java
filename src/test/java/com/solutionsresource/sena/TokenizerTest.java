package com.solutionsresource.sena;

import com.solutionsresource.sena.config.Config;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Unit test for simple App.
 */
public class TokenizerTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public TokenizerTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(TokenizerTest.class);
    }

    public void testEmoticons() throws Exception {
        Pattern pattern = Pattern.compile(Config.TOKENIZER_REGEX);

        String[] strings = new String[]{
                "Binay: a corrupt person",
                "bi-hon, mag-aral ka nga!",
                "Mar roxas (along with leni robredo) is a great leader.",
                "Binay is a corrupt person :(",
                "HAHAHAHA XD THIS IS SO GREAT!",
                "job well done :-D",
                "job well done :P",
                "You guys are the worst >.< I hate you!",
                "This is really weird o_O",
                "How desperate of you ._.",
                "Labyu beh :* lam mo yan hehe ;) <3",
                "Ansakit talaga! </3",
                "DU30 forever <3 >3< labyu!",
        };

        List<Tuple2<List<String>, List<String>>> ngramTests = Arrays.asList(
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("binay", "a", "corrupt", "person", ":"),
                        Collections.EMPTY_LIST
                    ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("bi-hon", ",", "mag-aral", "ka", "nga", "!"),
                        Arrays.asList("bi", "-", "hon")
                    ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList(
                                "mar", "roxas", "(", "along", "with", "leni", "robredo",
                                ")", "is", "a", "great", "leader", "."
                        ),
                        Collections.EMPTY_LIST
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("binay", "is", "a", "corrupt", "person", ":("),
                        Arrays.asList(":", "(")
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("hahahaha", "xd", "this", "is", "so", "great", "!"),
                        Collections.EMPTY_LIST
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("job", "well", "done", ":-d"),
                        Collections.EMPTY_LIST
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("job", "well", "done", ":p"),
                        Collections.EMPTY_LIST
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("you", "guys", "are", "the", "worst", ">.<", "i", "hate", "you", "!"),
                        Arrays.asList(">", ".", "<")
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("this", "is", "really", "weird", "o_o"),
                        Arrays.asList("o", "_", "O")
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("how", "desperate", "of", "you", "._."),
                        Arrays.asList(".", "_")
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("labyu", "beh", ":*", "lam", "mo", "yan", "hehe", ";)", "<3"),
                        Arrays.asList(":", "*", ";", ")", "<", "3")
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("ansakit", "talaga", "!", "</3"),
                        Arrays.asList("<", "/", "3")
                ),
                new Tuple2<List<String>, List<String>>(
                        Arrays.asList("du30", "forever", "<3", ">3<", "labyu", "!"),
                        Arrays.asList("<", ">", "3")
                )
        );

        for (int i = 0; i < strings.length; i++) {
            String content = strings[i];
            Matcher matcher = pattern.matcher(content);

            List<String> ngrams = new ArrayList<String>();

            while (matcher.find()) {
                String ngram = matcher.group(0);
                ngrams.add(ngram.toLowerCase());
            }

            for (String includedNgram : ngramTests.get(i)._1()) {
                assertTrue(String.format("\"%s\" should have unigram: \"%s\".", content, includedNgram),
                        ngrams.contains(includedNgram));
            }

            assertEquals(String.format("Incorrect size of ngrams for \"%s\".", content),
                    ngramTests.get(i)._1().size(), ngrams.size());

            for (String notIncludedNgram : ngramTests.get(i)._2()) {
                assertFalse(String.format("\"%s\" should not have unigram: \"%s\".", content, notIncludedNgram),
                        ngrams.contains(notIncludedNgram));
            }
        }
    }
}

package com.solutionsresource.sena;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class MultiplierTableTest extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MultiplierTableTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(MultiplierTableTest.class);
    }

    public void testCreateFromUsageTable() throws Exception {
        /*
        UsageTable usageTable1 = new UsageTable();

        usageTable1.put("true", 15);
        usageTable1.put("false", 15);

        MultiplierTable multiplierTable1 = MultiplierTable.createFrom(usageTable1);

        assertEquals(0.5, multiplierTable1.get(1.0));
        assertEquals(0.5, multiplierTable1.get(0.0));

        UsageTable usageTable2 = new UsageTable();

        usageTable2.put("true", 20);
        usageTable2.put("false", 30);

        MultiplierTable multiplierTable2 = MultiplierTable.createFrom(usageTable2);

        assertEquals(0.6, multiplierTable2.get(1.0));
        assertEquals(0.4, multiplierTable2.get(0.0));

        UsageTable usageTable3 = new UsageTable();

        usageTable3.put("positive", 20);
        usageTable3.put("negative", 30);

        MultiplierTable multiplierTable3 = MultiplierTable.createFrom(usageTable3, true);

        assertEquals(0.6, multiplierTable3.get((double) Sentiment.idLookup(Constants.POSITIVE)));
        assertEquals(0.4, multiplierTable3.get((double) Sentiment.idLookup(Constants.NEGATIVE)));
        assertEquals(1.0, multiplierTable3.get((double) Sentiment.idLookup(Constants.NEUTRAL)));

        UsageTable usageTable4 = new UsageTable();

        usageTable4.put("true", 15);
        usageTable4.put("false", 85);

        MultiplierTable multiplierTable4 = MultiplierTable.createFrom(usageTable4);

        assertEquals(0.85, multiplierTable4.get(1.0));
        assertEquals(0.15, multiplierTable4.get(0.0));
        */
    }
}

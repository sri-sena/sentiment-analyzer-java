package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.function.featuretransformation.GetDomainNames;
import com.solutionsresource.sena.function.featuretransformation.GetHasDomainString;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class DomainTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DomainTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(DomainTest.class);
    }

    public void testGetDomainNames() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        GetDomainNames getDomainNames = new GetDomainNames();

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        assertTrue(getDomainNames.call(message).contains("Election"));
    }

    public void testGetHasDomain() throws Exception {
        List<Domain> domains = new ArrayList<Domain>();
        Domain domain = Domain.mock();
        domains.add(domain);

        GetHasDomainString getHasDomainString = new GetHasDomainString("Election");

        Message message = new Message("1", "ang panget panget ni binay", domains, true);

        assertTrue(getHasDomainString.call(message).contains("true"));
    }
}

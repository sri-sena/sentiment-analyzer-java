package com.solutionsresource.sena;

import com.solutionsresource.sena.util.parse.LexicalState;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class LexicalStateTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public LexicalStateTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(LexicalStateTest.class);
    }

    public static void assertShouldExist(LexicalState lexicalState, String lexicon, boolean acceptingState) {
        assertTrue(String.format("%s should exist in our lexicon", lexicon), lexicalState.containsKey(lexicon));
        if (acceptingState) {
            assertShouldBeAcceptingState(lexicalState, lexicon);
        } else {
            assertShouldNotBeAcceptingState(lexicalState, lexicon);
        }
    }

    public static void assertShouldNotExist(LexicalState lexicalState, String lexicon) {
        assertFalse(String.format("%s should not exist in our lexicon", lexicon), lexicalState.containsKey(lexicon));
    }

    public static void assertShouldBeAcceptingState(LexicalState lexicalState, String lexicon) {
        assertTrue(String.format("%s should be an accepting state", lexicon),
                lexicalState.analyze(lexicon).isAccepting());
    }

    public static void assertShouldNotBeAcceptingState(LexicalState lexicalState, String lexicon) {
        assertFalse(String.format("%s should not be an accepting state", lexicon),
                lexicalState.analyze(lexicon).isAccepting());
    }

    public void testBuild() throws Exception {

        String[] lexicons = new String[]{
                "duterte", "dummy", "destiny", "binay", "ban", "binomial", "bin"
        };

        LexicalState lexicalState = new LexicalState(lexicons);

        assertShouldExist(lexicalState, "d", false);
        assertShouldExist(lexicalState, "b", false);
        assertShouldExist(lexicalState, "du", false);
        assertShouldExist(lexicalState, "de", false);
        assertShouldExist(lexicalState, "bi", false);
        assertShouldExist(lexicalState, "bin", true);
        assertShouldExist(lexicalState, "bina", false);
        assertShouldExist(lexicalState, "binay", true);
        assertShouldExist(lexicalState, "ba", false);
        assertShouldExist(lexicalState, "ban", true);
        assertShouldExist(lexicalState, "bino", false);
        assertShouldExist(lexicalState, "binom", false);
        assertShouldExist(lexicalState, "binomial", true);


        assertShouldNotExist(lexicalState, "di");
        assertShouldNotExist(lexicalState, "bu");
        assertShouldNotExist(lexicalState, "bano");
        assertShouldNotExist(lexicalState, "bine");

        for (String lexicon : lexicons) {
            assertShouldExist(lexicalState, lexicon, true);
        }

        assertShouldEqualValueAndDepth(lexicalState, "duterte", "duterte", 7);
        assertShouldEqualValueAndDepth(lexicalState, "duterte2016", "duterte", 7);
        assertShouldEqualValueAndDepth(lexicalState, "destiny2016", "destiny", 7);
        assertNull(lexicalState.analyze("dutert").getValue());
        assertAnalysisShouldBeNull(lexicalState, "dutart");
        assertShouldEqualValueAndDepth(lexicalState, "binay", "binay", 5);
        assertShouldEqualValueAndDepth(lexicalState, "binay2016", "binay", 5);
        assertShouldEqualValueAndDepth(lexicalState, "bin", "bin", 3);
        assertShouldEqualValueAndDepth(lexicalState, "binererer", "bin", 3);
        assertShouldEqualValueAndDepth(lexicalState, "binomial2016", "binomial", 8);
        assertNull(lexicalState.analyze("bi").getValue());
    }

    private void assertAnalysisShouldBeNull(LexicalState lexicalState, String lexicon) {
        assertNull(String.format("%s should not exist in our lexicon", lexicon), lexicalState.analyze(lexicon));
    }

    private void assertShouldEqualValueAndDepth(LexicalState lexicalState, String ngram,
                                                String expectedLexicon,int expectedDepth) {
        LexicalState state = lexicalState.analyze(ngram);
        assertNotNull(String.format("%s analysis should not be null", ngram), state);
        assertShouldBeAcceptingState(lexicalState, ngram);
        assertEquals(expectedLexicon, state.getValue());
        assertEquals(expectedDepth, state.getDepth());
    }
}

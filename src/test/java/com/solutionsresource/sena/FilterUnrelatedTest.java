package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.ingest.FilterUnrelated;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * Unit test for simple App.
 */
public class FilterUnrelatedTest extends TestCase {
  public FilterUnrelatedTest(String testName) {
    super(testName);
  }

  public static Test suite() {
    return new TestSuite(FilterUnrelatedTest.class);
  }

  public void testCall() throws Exception {
    Constants constants = new Constants();
    DomainSlugs domainSlugs = AcceptedSlugsGetter.mock();

    FilterUnrelated filter = new FilterUnrelated(constants, domainSlugs);

    ResultHashMap result1 = new ResultHashMap(Bytes.toBytes("asdf"));
    ResultHashMap result2 = new ResultHashMap(Bytes.toBytes("qwer"));
    ResultHashMap result3 = new ResultHashMap(Bytes.toBytes("zxcv"));

    result1.put(constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_TEXT, Bytes.toBytes(
        "[]"
    ));
    result2.put(constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_TEXT, Bytes.toBytes(
        "[{\"name\":\"election\",\"description\":\"election\",\"deleted\":false,\"topics\":[]}]"
    ));
    result3.put(constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_TEXT, Bytes.toBytes(
        "[{\"name\":\"election\",\"description\":\"election\",\"deleted\":false,\"topics\":[]}," +
            "{\"name\":\"restaurants\",\"description\":\"Current loveteams\",\"active\":\"I\",\"deleted\":false," +
            "\"topics\":[{\"name\":\"jollibee\",\"sentiment\":{\"name\":\"neutral\"},\"deleted\":false}]," +
            "\"uuid\":\"1463974575581-80e2b3f7-2097-11e6-995e-2dc05e148716\"},{\"name\":\"telecoms\"," +
            "\"description\":\"List of Telcos in the Philippines\",\"active\":\"A\",\"deleted\":false," +
            "\"topics\":[{\"name\":\"talk \\x5Cu0026 text\",\"sentiment\":{\"name\":\"neutral\"}," +
            "\"deleted\":false}],\"uuid\":\"1464161846911-874beb9a-224b-11e6-8556-f3b185ae33ae\"}]"
    ));

    assertFalse(filter.call(result1));
    assertFalse(filter.call(result2));
    assertTrue(filter.call(result3));
  }
}

package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featureselection.MutualInformation;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class MutualInformationTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MutualInformationTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(MutualInformationTest.class);
    }

    public void testGreaterThanZero() throws Exception {
        UsageTable poultryUsages = new UsageTable();
        poultryUsages.put("true", 27701);
        poultryUsages.put("false", 774247);

        UsageTable exportUsages = new UsageTable();;
        exportUsages.put("true", 49);
        exportUsages.put("false", 141);
        UsageTable exportInPoultry = new UsageTable();
        exportInPoultry.put("used", 190);
        exportInPoultry.put("not used", 801758);
        MutualInformation exportMutualInformation = new MutualInformation("export", exportUsages,
                exportInPoultry, poultryUsages, 801948);

        assertTrue(exportMutualInformation.value > 0);

        UsageTable theUsages = new UsageTable();
        theUsages.put("true", 27701);
        theUsages.put("false", 401758);
        UsageTable theInPoultry = new UsageTable();
        theInPoultry.put("used", 429459);
        theInPoultry.put("not used", 372489);
        MutualInformation theMutualInformation = new MutualInformation("the", theUsages,
                theInPoultry, poultryUsages, 801948);

        assertTrue(theMutualInformation.value > 0);
    }

    public void testNaN() throws Exception {
        UsageTable miriamUsages = new UsageTable();
        miriamUsages.put("???", 4);
        miriamUsages.put("positive", 205);
        miriamUsages.put("neutral", 130);
        miriamUsages.put("negative", 38);

        UsageTable duterteUsages = new UsageTable();
        duterteUsages.put("???", 2);
        duterteUsages.put("positive", 140);
        duterteUsages.put("neutral", 94);
        duterteUsages.put("negative", 12);

        UsageTable duterteInMiriam = new UsageTable();
        duterteInMiriam.put("not used", 82);
        duterteInMiriam.put("used", 295);

        MutualInformation duterteMutualInformation = new MutualInformation("duterte", duterteUsages,
                duterteInMiriam, miriamUsages, 377);

        assertTrue(duterteMutualInformation.value > 0);
    }
}

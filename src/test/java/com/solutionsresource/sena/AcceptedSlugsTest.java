package com.solutionsresource.sena;

import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.model.Slugger;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.FileNotFoundException;

public class AcceptedSlugsTest extends TestCase {
  private final DomainSlugs domainSlugs;

  public AcceptedSlugsTest(String testName) throws FileNotFoundException {
    super(testName);
    this.domainSlugs = AcceptedSlugsGetter.readJson("slugs.json");
  }

  public static Test suite() {
    return new TestSuite(AcceptedSlugsTest.class);
  }

  public void testContainsKey() {
    assertTrue("Domain Slugs must contain election", domainSlugs.containsKey("election"));
    assertTrue("Domain Slugs must contain restaurant", domainSlugs.containsKey("restaurants"));
    assertTrue("Domain Slug election must contain cayetano",
        domainSlugs.get("election").containsKey("alan-cayetano"));
    assertTrue("Topic Slug cayetano must contain positive",
        domainSlugs.get("election").get("alan-cayetano")[3].equals("positive"));
  }
  public void testAccepts() {
    assertFalse("Election2 should be accepted",
        domainSlugs.accepts("Election2"));
    assertTrue("Election should not be accepted",
        domainSlugs.accepts("Election"));
    assertTrue("Restaurants should not be accepted",
        domainSlugs.accepts("Restaurants"));
    assertFalse("Election: Nguyen Minh should be accepted",
        domainSlugs.accepts("Election", "Nguyen Minh"));
    assertTrue("Restaurants: Maxs should not be accepted",
        domainSlugs.accepts("Restaurants", "Maxs"));
    assertTrue("Election: Rodrigo Duterte should not be accepted",
        domainSlugs.accepts("Election", "Rodrigo Duterte"));
    assertFalse("Restaurants: Jollibee should be accepted",
        domainSlugs.accepts("Restaurants", "Jollibee"));
    assertTrue("Positive should not be accepted",
        domainSlugs.accepts("Election", "Rodrigo Duterte", "Positive"));
    assertTrue("NEGATIVE should not be accepted",
        domainSlugs.accepts("Election", "Rodrigo Duterte", "NEGATIVE"));
    assertTrue("neutral should not be accepted",
        domainSlugs.accepts("Election", "Rodrigo Duterte", "neutral"));

  }

  public void testRejects() {
    assertTrue("Election2 should be rejected",
        domainSlugs.rejects("Election2"));
    assertFalse("Election should not be rejected",
        domainSlugs.rejects("Election"));
    assertFalse("Restaurants should not be rejected",
        domainSlugs.rejects("Restaurants"));
    assertTrue("Election: Nguyen Minh should be rejected",
        domainSlugs.rejects("Election", "Nguyen Minh"));
    assertFalse("Restaurants: Maxs should not be rejected",
        domainSlugs.rejects("Restaurants", "Maxs"));
    assertFalse("Election: Rodrigo Duterte should not be rejected",
        domainSlugs.rejects("Election", "Rodrigo Duterte"));
    assertTrue("Restaurants: Jollibee should be rejected",
        domainSlugs.rejects("Restaurants", "Jollibee"));
    assertFalse("Positive should not be rejected",
        domainSlugs.rejects("Election", "Rodrigo Duterte", "Positive"));
    assertFalse("NEGATIVE should not be rejected",
        domainSlugs.rejects("Election", "Rodrigo Duterte", "NEGATIVE"));
    assertFalse("neutral should not be rejected",
        domainSlugs.rejects("Election", "Rodrigo Duterte", "neutral"));
  }

  public void testSlugger() {
    assertEquals("#halalandian2016", Slugger.slug("#halalandian2016"));
    assertEquals("media-hashtags", Slugger.slug("media hashtags"));
  }
}

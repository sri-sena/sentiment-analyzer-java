package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.featuretransformation.RemoveStopWords;
import com.solutionsresource.sena.util.parse.WordStemmer;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class WordStemmingTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
   Constants constants;
    public WordStemmingTest(String testName) throws Exception {
        super(testName);
        constants = new Constants();
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(WordStemmingTest.class);
    }

    public void testWordStemming() throws Exception {
        WordStemmer wordStemmer = new WordStemmer(constants);

        assertEquals("duterte", wordStemmer.stem("nagduterte"));
        assertEquals("linis", wordStemmer.stem("naglinis"));
        assertEquals("linis", wordStemmer.stem("naglilinis"));
        assertEquals("sampay", wordStemmer.stem("magsampay"));
        assertEquals("sampay", wordStemmer.stem("magsasampay"));
        assertEquals("binay", wordStemmer.stem("nagbinay"));
        assertEquals("binay", wordStemmer.stem("magbinay"));
        assertEquals("ready", wordStemmer.stem("nagreready"));
        assertEquals("ready", wordStemmer.stem("magreready"));
        assertEquals("kanta", wordStemmer.stem("nagkakantahan"));
        assertEquals("binay", wordStemmer.stem("binay"));
        assertEquals("amaz", wordStemmer.stem("amazing"));
        assertEquals("thing", wordStemmer.stem("thing"));
        assertEquals("sing", wordStemmer.stem("sing"));
        assertEquals("sing", wordStemmer.stem("singing"));
        assertEquals("elect", wordStemmer.stem("election"));
        assertEquals("act", wordStemmer.stem("action"));
        assertEquals("read", wordStemmer.stem("reading"));
        assertEquals("ready", wordStemmer.stem("readying"));
    }

    public void testWordSplitting() {
        List<String> marumi = RemoveStopWords.splitNgram("MARumi");
        List<String> marupok = RemoveStopWords.splitNgram("MARupok");
        List<String> marahas = RemoveStopWords.splitNgram("MARahas");
        List<String> noToMar = RemoveStopWords.splitNgram("NoToMar");
        List<String> duterte = RemoveStopWords.splitNgram("DUTERTE");
        List<String> duterte2016 = RemoveStopWords.splitNgram("Duterte2016");
        List<String> duterteCayetano2016 = RemoveStopWords.splitNgram("DuterteCayetano2016");
        List<String> changeIsComing = RemoveStopWords.splitNgram("ChangeIsComing");

        assertTrue("MARumi split should contain mar", marumi.contains("mar"));
        assertTrue("MARumi split should contain umi", marumi.contains("rumi"));
        assertTrue("MARupok split should contain mar", marupok.contains("mar"));
        assertTrue("MARupok split should contain upok", marupok.contains("rupok"));
        assertTrue("MARahas split should contain mar", marahas.contains("mar"));
        assertTrue("MARahas split should contain ahas", marahas.contains("rahas"));
        assertTrue("NoToMar split should contain no", noToMar.contains("no"));
        assertTrue("NoToMar split should contain to", noToMar.contains("to"));
        assertTrue("NoToMar split should contain mar", noToMar.contains("mar"));
        assertTrue("DUTERTE split should contain duterte", duterte.contains("duterte"));
        assertTrue("Duterte2016 split should contain duterte", duterte2016.contains("duterte"));
        assertTrue("Duterte2016 split should contain 2016", duterte2016.contains("2016"));
        assertTrue("DuterteCayetano2016 split should contain duterte", duterteCayetano2016.contains("duterte"));
        assertTrue("DuterteCayetano2016 split should contain cayetano", duterteCayetano2016.contains("cayetano"));
        assertTrue("DuterteCayetano2016 split should contain 2016", duterteCayetano2016.contains("2016"));
        assertTrue("ChangeIsComing split should contain duterte", changeIsComing.contains("change"));
        assertTrue("ChangeIsComing split should contain cayetano", changeIsComing.contains("is"));
        assertTrue("ChangeIsComing split should contain 2016", changeIsComing.contains("coming"));
    }
}

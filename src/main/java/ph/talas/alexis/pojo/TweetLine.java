package ph.talas.alexis.pojo;

public class TweetLine {

	private String line;
	private String topic;
	private String sourceFile;
	private String sourceFolder;
	private boolean last;
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getSourceFolder() {
		return sourceFolder;
	}
	public void setSourceFolder(String sourceFolder) {
		this.sourceFolder = sourceFolder;
	}
	public String getSourceFile() {
		return sourceFile;
	}
	public void setSourceFile(String sourceFile) {
		this.sourceFile = sourceFile;
	}
	public boolean isLast() {
		return last;
	}
	public void setLast(boolean last) {
		this.last = last;
	}
	@Override
	public String toString() {
		return "TweetLine [line=" + line + ", topic=" + topic + ", sourceFile=" + sourceFile + ", sourceFolder="
				+ sourceFolder + ", last=" + last + "]";
	}
}

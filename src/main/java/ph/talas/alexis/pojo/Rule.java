package ph.talas.alexis.pojo;

import java.io.Serializable;
import java.util.List;

public class Rule implements Serializable {

	private String word;
	private Position position = Position.ANY;
	private boolean followcase; //case should be exact;
	private boolean exclude; //true means this word is to be avoided.
	/**
	 * hashtag has its own attribute
	 *  instead of adding a distinct # to the keyword,
	 *  in order to enable the engine to match #____keyword___
	 *
	 * false hashtag will still check hashtags
	 */
	private boolean hashtag; //means this should be a hashtag.
	private List<Rule> andWords;
	private List<Rule> orWords;
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public boolean isFollowcase() {
		return followcase;
	}
	public void setFollowcase(boolean followcase) {
		this.followcase = followcase;
	}
	public boolean isExclude() {
		return exclude;
	}
	public void setExclude(boolean exclude) {
		this.exclude = exclude;
	}
	public boolean isHashtag() {
		return hashtag;
	}
	public void setHashtag(boolean hashtag) {
		this.hashtag = hashtag;
	}
	public List<Rule> getAndWords() {
		return andWords;
	}
	public void setAndWords(List<Rule> andWords) {
		this.andWords = andWords;
	}
	public List<Rule> getOrWords() {
		return orWords;
	}
	public void setOrWords(List<Rule> orWords) {
		this.orWords = orWords;
	}

	public Position getPosition() {
		return position;
	}
	public void setPosition(Position position) {
		this.position = position;
	}

	public enum Position implements Serializable {
		START,
		END,
		ANY,
		EXACT
	}

	@Override
	public String toString() {
		return "Rule [word=" + word + ", position=" + position + ", followcase=" + followcase + ", exclude=" + exclude
				+ ", hashtag=" + hashtag + ", andWords=" + andWords + ", orWords=" + orWords + "]";
	}
}

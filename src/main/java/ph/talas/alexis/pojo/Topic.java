package ph.talas.alexis.pojo;

import java.io.Serializable;
import java.util.List;

public class Topic implements Serializable {

	private String topic;//brand, presidentiables, etc.
	private List<Rule> rules;
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public List<Rule> getRules() {
		return rules;
	}
	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}
}

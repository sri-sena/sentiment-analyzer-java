package ph.talas.alexis.exception;

public class ExcludedTopicException extends Exception {


	/**
	 * 
	 */
	private static final long serialVersionUID = -2135888467382218746L;

	public ExcludedTopicException() {
		// TODO Auto-generated constructor stub
	}

	public ExcludedTopicException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExcludedTopicException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ExcludedTopicException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ExcludedTopicException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}

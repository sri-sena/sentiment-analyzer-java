package ph.talas.alexis.engine;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.featuretransformation.RemoveHandlers;
import ph.talas.alexis.exception.ExcludedTopicException;
import ph.talas.alexis.pojo.Rule;
import ph.talas.alexis.pojo.Rule.Position;
import ph.talas.alexis.pojo.Topic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

public class TopicAnalyzer implements Serializable {
    private static final String SPACE = " ";
    //    private static final Logger logger = Logger.getLogger(TopicAnalyzer.class);
    private static final String NOTHING = "";
    private static final String HASH = "#";
    private final Constants constants;

    /**
     * 1) generate rule, AND, OR, INCLUSIVE, startsWith,EndsWith, hasText, 2)
     * segregate per topic.
     */
    private List<Topic> topics;

    private static final Pattern UNDESIRABLES = Pattern.compile("[\\p{Punct}&&[^#]&&[^@]&&[^http://]&&[^/]]");
    private static final Pattern PUNC = Pattern.compile("\\p{Punct}");

    private static String removePunctuations(Constants constants, String sentence) {
        RemoveHandlers removeHandlers = new RemoveHandlers(constants);

        sentence = sentence.replaceAll(constants.URL_REGEX, " ");
        sentence = removeHandlers.removeHandlers(sentence);

        return UNDESIRABLES.matcher(sentence).replaceAll(SPACE);
    }

    private static String removeAllPunctuations(String word) {
        return PUNC.matcher(word).replaceAll(NOTHING);
    }

    public TopicAnalyzer(List<Topic> topics) throws Exception {
//        logger.warn("loaded topics: " + topics.size());
        this.topics = topics;
        this.constants = new Constants();
    }

    /**
     * @param body
     * @return topic; null if not related
     */
    public List<String> match(String body) throws ExcludedTopicException {
        List<String> includedTopics = new ArrayList<String>();

        for (Topic topic : this.topics) {
            /**
             * Negate rules first to eliminate from the start.
             */
//            logger.debug("Searching topic: " + topic.getTopic() + "; body: " + body);
            for (Rule rule : topic.getRules()) {
                boolean match = matchRule(body, rule);
                if (match) {
//                        logger.debug("Found match for topic: " + topic.getTopic() + "; tweet: " + body);
                    includedTopics.add(topic.getTopic());
                    break;
                }
            }
        }

        return includedTopics;
    }

    private boolean matchRule(String body, Rule rule) throws ExcludedTopicException {
//        logger.debug("body: " + body + " -- rule: " + rule.toString());
        boolean matched = false;
        String word = rule.getWord();
        String tweetBody = body;
        String tweetWord = word.trim();
        if (!rule.isFollowcase()) {
            tweetBody = tweetBody.toLowerCase();
            tweetWord = tweetWord.toLowerCase();
        }
        if (tweetBody.contains(tweetWord)) {
            /**
             * remove punctuations, this could be slow
             */
            tweetBody = removePunctuations(constants, tweetBody);
            StringTokenizer tokenizer = new StringTokenizer(tweetBody);
            while (tokenizer.hasMoreTokens()) {
                String current = tokenizer.nextToken();
//                logger.trace("Token: " + current);
                //checks hastags as well.
                if (rule.isHashtag()) {
                    if (!current.startsWith(HASH)) {
                        continue;
                    }
                }
                current = removeAllPunctuations(current).trim();
//                logger.trace("Token without punctuation or hashtag: " + current);
                if (rule.getPosition() == Position.START && current.startsWith(tweetWord)) {
//                    logger.trace("Token is matched as START: " + current);
                    matched = true;
                    break;
                } else if (rule.getPosition() == Position.END && current.endsWith(tweetWord)) {
//                    logger.trace("Token is matched as END: " + current);
                    matched = true;
                    break;
                } else if (rule.getPosition() == Position.EXACT && current.equals(tweetWord)) {
//                    logger.trace("Token is matched as EXACT: " + current);
                    matched = true;
                    break;
                } else if (rule.getPosition() == Position.ANY && current.contains(tweetWord)) {
//                    logger.trace("Token is matched as ANY: " + current);
                    matched = true;
                    break;
                }
            }
            if (rule.getAndWords() != null) {
                for (Rule andRule : rule.getAndWords()) {
//                    logger.trace("Executing andRule: " + andRule.toString());
                    matched = matched && matchRule(body, andRule);
                }
            }
            if (rule.getOrWords() != null && rule.getOrWords().size() > 0) {
                boolean foundAnOr = false;
                for (Rule orRule : rule.getOrWords()) {
//                    logger.trace("Executing orRule: " + orRule.toString());
                    if (matchRule(body, orRule)) {
                        foundAnOr = true;
                        break;
                    }
                }
                matched = matched && foundAnOr;
            }
            if (rule.isExclude() && matched) {
                throw new ExcludedTopicException("Tweet is excluded from the topic.");
            }
//            logger.debug("body: " + body + " -- rule: " + rule.toString() + " -- matched: " + matched);
        }
        return matched;
    }

    public double score(String content) {
        try {
            if (match(content).size() > 0) {
                return 1.0;
            }
        } catch (ExcludedTopicException e) {
            return 0.0;
        }

        return -1.0;
    }

    public double score(String content, String topicName) {
        try {
            if (match(content).contains(topicName)) {
                return 1.0;
            }
        } catch (ExcludedTopicException e) {
            return 0.0;
        }

        return -1.0;
    }
}

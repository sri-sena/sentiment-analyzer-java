package ph.talas.alexis.engine;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ph.talas.alexis.pojo.Rule;
import ph.talas.alexis.pojo.Rule.Position;
import ph.talas.alexis.pojo.Topic;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LoadRules {

//	private static final Logger logger = Logger.getLogger(LoadRules.class);
	public static List<Topic> loadTopics(String file){
		List<Topic> topics = new ArrayList<Topic>();
		File fXmlFile = new File(file);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("topic");
			for (int i = 0; i < nodeList.getLength(); i++){
				Node node = nodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					String topicName = element.getAttribute("name");
					Topic topic = new Topic();
					topic.setTopic(topicName);
					topic.setRules(loadRules(element.getChildNodes()));
					topics.add(topic);
//					logger.warn("Topic: loaded: " + topicName);
				}
			}
		} catch (Exception e) {
//			logger.error(e);
		}
		Collections.sort(topics, new Comparator<Topic>() {
			public int compare(Topic o1, Topic o2) {
				return o1.getTopic().compareTo(o2.getTopic());
			}
		});
		return topics;
	}

	private static List<Rule> loadRules(NodeList childNodes) {
		List<Rule> rules = new ArrayList<Rule>();
		for (int i = 0; i < childNodes.getLength(); i++){
			Node node = childNodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				Rule rule = new Rule();
				String word = element.getAttribute("word");
				rule.setWord(word);
				Position position = Position.valueOf(element.getAttribute("position").toUpperCase());
				rule.setPosition(position);
				boolean followcase = Boolean.valueOf(element.getAttribute("followcase"));
				rule.setFollowcase(followcase);
				boolean exclude = Boolean.valueOf(element.getAttribute("exclude"));
				rule.setExclude(exclude);
				boolean hashtag = Boolean.valueOf(element.getAttribute("hastag"));
				rule.setHashtag(hashtag);
				NodeList andElements = element.getElementsByTagName("andWords");
				if (andElements != null && andElements.getLength() > 0){
					NodeList andRules = andElements.item(0).getChildNodes();
					List<Rule> andRuleList = loadRules(andRules);
					rule.setAndWords(andRuleList);
				}
				NodeList orElements = element.getElementsByTagName("orWords");
				if (orElements != null && orElements.getLength() > 0){
					NodeList orRules = orElements.item(0).getChildNodes();
					List<Rule> orRuleList = loadRules(orRules);
					rule.setOrWords(orRuleList);
				}
				/**
				 * sort rules, exclude should come first, so that it can be eliminated right away.
				 */
				if (rule.isExclude()){
					rules.add(0, rule);
				} else {
					rules.add(rule);
				}
//				logger.info("Loaded rule: " + rule.toString());
			}
		}
		
		return rules;
	}
}

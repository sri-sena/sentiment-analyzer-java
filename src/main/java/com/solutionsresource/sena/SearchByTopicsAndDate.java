package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.client.coprocessor.AggregationClient;
import org.apache.hadoop.hbase.client.coprocessor.LongColumnInterpreter;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SearchByTopicsAndDate {


  public static void main(String[] args) throws Exception, Throwable {

    Constants constants = new Constants();
    /** Search Criteria **/
    String[] topics = new String[] { "rodrigo duterte" };
    String[] sentiments = new String[] { "neutral" };
    /** Search Criteria **/
    /** =================== **/

    // to remove `null` records
    Filter domainColumnFilter = new ColumnPrefixFilter(Bytes.toBytes("domains"));

    // to filter by topic names
    FilterList topicFilter = new FilterList(FilterList.Operator.MUST_PASS_ALL);
    for (String topic : topics) {
      topicFilter.addFilter(new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("domains"), CompareOp.EQUAL, new SubstringComparator("\"name\":\"" + topic + "\"")));
    }

    // to filter by sentiment
    FilterList sentimentFilter = new FilterList(FilterList.Operator.MUST_PASS_ONE);
    for (String sentiment : sentiments) {
      sentimentFilter.addFilter(new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("domains"), CompareOp.EQUAL, new SubstringComparator("\"name\":\"rodrigo duterte\",\"sentiment\":{\"name\":\"" + sentiment + "\"")));
    }

    List<Long> ts = new ArrayList<Long>();
    ts.add(new Long(1447527411709L));
    Filter timestampFilter = new TimestampsFilter(ts);
    
    // to merge all filter
    FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);
    filter.addFilter(domainColumnFilter);
    filter.addFilter(topicFilter);
//    filter.addFilter(sentimentFilter);
//    filter.addFilter(timestampFilter);

    // to create new scan & set filter
    Scan scan = new Scan();
    scan.setFilter(filter);
    scan.setTimeRange(1443763015080L, 1452671999082L);
    
    /** =================== **/
    
    Configuration config = new HBaseConnector().getConfiguration();
    AggregationClient aggregationClient = new AggregationClient(config);
    long rowCount = aggregationClient.rowCount(TableName.valueOf("message"), new LongColumnInterpreter(), scan);
    System.out.println(rowCount);
//    aggregationClient.close();

    HConnection connection = HConnectionManager.createConnection(config);
    HTableInterface table = connection.getTable(TableName.valueOf("message"));
    int z = 1;
    Iterator<Result> i = table.getScanner(scan).iterator();
    while (i.hasNext()) {
      Result result = i.next();
      System.out.println(z + " = " + Bytes.toString(result.getRow()));
      System.out.println(z + " = " + Bytes.toString(result.getValue(Bytes.toBytes("message_data"), Bytes.toBytes("content"))));
      System.out.println(z + " = " + Bytes.toString(result.getValue(Bytes.toBytes("message_data"), Bytes.toBytes("domains"))));
      System.out.println(z + " = " + (result.rawCells()[0].getTimestamp()));
      z++;
    }
    connection.close();
  }

}

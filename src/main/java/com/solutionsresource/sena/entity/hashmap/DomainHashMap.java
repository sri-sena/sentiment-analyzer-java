package com.solutionsresource.sena.entity.hashmap;

import com.solutionsresource.sena.entity.Domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class DomainHashMap extends AddingHashMap<String, TopicHashMap> {
    public void add(String domain, String topic, String sentiment) {
        getOrCreate(domain).put(topic, sentiment);
    }

    @Override
    public TopicHashMap getDefaultValue() {
        return new TopicHashMap();
    }

    public List<Domain> toDomains() {
        List<Domain> domains = new ArrayList<>();

        String domainFlag = "csv";

        for (Entry<String, TopicHashMap> domain : entrySet()) {
            String domainName = domain.getKey();
            domains.add(new Domain(domainName, domainName,
                    domainFlag, false, domain.getValue().toTopics()));
        }

        return domains;
    }
}

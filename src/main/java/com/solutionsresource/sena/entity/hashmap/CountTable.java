package com.solutionsresource.sena.entity.hashmap;

import java.util.HashMap;

public class CountTable extends HashMap<Double, Integer> {
    public int getLeastValue() {
        int leastValue = 0;
        boolean first = true;

        for (double key : this.keySet()) {
            int value = this.get(key);

            if (first) {
                first = false;
                leastValue = value;
            } else if (value < leastValue) {
                leastValue = value;
            }
        }

        return leastValue;
    }
}

package com.solutionsresource.sena.entity.hashmap;

import com.solutionsresource.sena.util.model.Slugger;
import scala.Tuple2;

import java.util.Map.Entry;

public class DomainSlugs extends AddingHashMap<String, TopicSlugs> {

  public DomainSlugs() {
    super();
  }

  public DomainSlugs(Tuple2<String, TopicSlugs>[] keyValues) {
    super();

    for (Tuple2<String, TopicSlugs> keyValue : keyValues) {
      put(keyValue._1(), keyValue._2());
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (String domainName : keySet()) {
      builder.append(domainName);
      builder.append("\n");
      builder.append(get(domainName).toString());
    }
    return builder.toString();
  }

  @Override
  public TopicSlugs getDefaultValue() {
    return new TopicSlugs();
  }

  public boolean accepts(String domain) {
    return !rejects(domain);
  }

  public boolean rejects(String domain) {
    domain = Slugger.slug(domain);
    return !containsKey(domain);
  }

  public boolean accepts(String domain, String topic) {
    return !rejects(domain, topic);
  }

  public boolean rejects(String domain, String topic) {
    domain = Slugger.slug(domain);
    topic = Slugger.slug(topic);
    return get(domain).rejects(topic);
  }

  public boolean accepts(String domain, String topic, String sentiment) {
    return !rejects(domain, topic, sentiment);
  }

  public boolean rejects(String domain, String topic, String sentiment) {
    domain = Slugger.slug(domain);
    topic = Slugger.slug(topic);
    sentiment = Slugger.slug(sentiment);
    return get(domain).rejects(topic, sentiment);
  }

  public long getModelsCount() {
    long modelsCount = 0;

    for (Entry<String, TopicSlugs> entry : entrySet()) {
      modelsCount += 1 + entry.getValue().getModelsCount();
      // domain recognizer, topic recognizer and sentiment analyzer
    }

    return modelsCount;
  }
}

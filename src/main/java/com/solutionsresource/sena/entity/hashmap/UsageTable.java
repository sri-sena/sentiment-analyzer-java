package com.solutionsresource.sena.entity.hashmap;

import com.solutionsresource.sena.function.featuretransformation.SortUsageTableTuples;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UsageTable extends HashMap<String, Integer> {

    public UsageTable() {
        super();
    }

    public UsageTable(String key, int value) {
        super();
        this.put(key, value);
    }

    @Override
    public synchronized Integer put(String key, Integer value) {
        return key == null || key.isEmpty() ? new Integer(-1) : super.put(key, value);
    }

    public List<Tuple2<String, Integer>> toSortedTuples(JavaSparkContext sc) {
        List<Tuple2<String, Integer>> tuples = new ArrayList<Tuple2<String, Integer>>();

        for (String key : this.keySet()) {
            tuples.add(new Tuple2<>(key, this.get(key)));
        }

        tuples = sc.parallelize(tuples).sortBy(new SortUsageTableTuples(), true, 100).collect();

        return tuples;
    }

    public Integer add(String topicName, int i) {
        if (!containsKey(topicName)) {
            return put(topicName, i);
        }
        return put(topicName, get(topicName) + i);
    }

    public int getTotal() {
        int total = 0;

        for (String key : keySet()) {
            total += get(key);
        }

        return total;
    }

    public int getOrZero(String key) {
        return containsKey(key) ? get(key) : 0;
    }
}

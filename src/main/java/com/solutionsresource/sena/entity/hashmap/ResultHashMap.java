package com.solutionsresource.sena.entity.hashmap;

import org.apache.hadoop.hbase.util.Bytes;

import java.util.HashMap;
import java.util.Map;

public class ResultHashMap extends HashMap<String, byte[]> {
    private byte[] row;

    public ResultHashMap(byte[] row) {
        this.row = row;
    }

    public byte[] getRow() {
        return row;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Map.Entry<String, byte[]> entry : entrySet()) {
            stringBuilder.append(String.format("%s : %s\n",
                    entry.getKey(),
                    Bytes.toString(entry.getValue())));
        }
        return stringBuilder.toString();
    }
}

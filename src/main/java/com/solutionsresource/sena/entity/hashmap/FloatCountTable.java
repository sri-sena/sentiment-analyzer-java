package com.solutionsresource.sena.entity.hashmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class FloatCountTable extends HashMap<Float, Integer> {
    public Float[] getFloatKeys() {
        List<Float> keys = new ArrayList<Float>();

        for (float key : this.keySet()) {
            keys.add(key);
        }

        Collections.sort(keys, Collections.reverseOrder());

        return keys.toArray(new Float[keys.size()]);
    }

    public Float getFloatKeyAt(int index) {
        Float[] keys = this.getFloatKeys();

        for (float key : keys) {
            index -= get(key);

            if (index <= 0) {
                return key;
            }
        }

        return keys[keys.length - 1];
    }
}

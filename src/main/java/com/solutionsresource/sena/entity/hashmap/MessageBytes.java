package com.solutionsresource.sena.entity.hashmap;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.gnip.Hashtag;
import com.solutionsresource.sena.entity.gnip.Tweet;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class MessageBytes extends BytesTable {
    public MessageBytes(Constants constants, Tweet tweet, String source, long timestamp, long userTimestamp,
                        DateFormat df, byte[] domainsBytes, Boolean isFinalized) {
        put(constants.TABLE_MESSAGE_MSGDATA_REF_ID_BYTE, tweet.id);

        put(constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE, tweet.body);

        if (tweet.generator != null) {
            put(constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, tweet.generator.displayName);
        }

        if (tweet.location != null) {
            put(constants.TABLE_MESSAGE_MSGDATA_LOCATION_BYTE, tweet.location.displayName);
        }
        if (tweet.gnip != null && tweet.gnip.profileLocations != null && !tweet.gnip.profileLocations.isEmpty() &&
                tweet.gnip.profileLocations.get(0).geo != null && tweet.gnip.profileLocations.get(0).geo.coordinates
                != null && tweet.gnip.profileLocations.get(0).geo.coordinates.size() == 2) {
            List<String> coordinates = tweet.gnip.profileLocations.get(0).geo.coordinates;
            put(constants.TABLE_MESSAGE_MSGDATA_LATITUDE_BYTE, coordinates.get(1));
            put(constants.TABLE_MESSAGE_MSGDATA_LONGITUDE_BYTE, coordinates.get(0));
        }

        put(constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE, tweet.actor.preferredUsername);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_ID_BYTE, tweet.actor.id);

        put(constants.TABLE_MESSAGE_MSGDATA_USER_CREATED_BYTE, df.format(new Date(userTimestamp)));
        put(constants.TABLE_MESSAGE_MSGDATA_USER_FRIENDS_COUNT_BYTE, tweet.actor.friendsCount);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_FOLLOWERS_COUNT_BYTE, tweet.actor.followersCount);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_STATUSES_COUNT_BYTE, tweet.actor.statusesCount);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_FAVORITES_COUNT_BYTE, tweet.actor.favoritesCount);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_VERIFIED_BYTE, tweet.actor.verified);

        put(constants.TABLE_MESSAGE_MSGDATA_SOURCE_FOLDER_BYTE, source);

        String hashtags = "";
        for (Hashtag hashtag : tweet.twitter_entities.hashtags) {
            hashtags += ", " + hashtag.text;
        }
        hashtags = hashtags.length() > 0 ? hashtags.substring(2) : "";
        put(constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, hashtags);

        put(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE, df.format(new Date(timestamp)));
        put(constants.TABLE_MESSAGE_MSGDATA_CREATED_DATETIME_BYTE, df.format(new Date()));
        put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, isFinalized);
        put(constants.TABLE_MESSAGE_MSGDATA_ISEXPORTED_BYTE, false);

        put(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, domainsBytes);
    }

    public MessageBytes(Constants constants, String refId, String[] values, byte[] domainsBytes, String source) {
        put(constants.TABLE_MESSAGE_MSGDATA_REF_ID_BYTE, refId);
        put(constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE, values[1]);
        put(constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, values[2]);
        put(constants.TABLE_MESSAGE_MSGDATA_LOCATION_BYTE, values[3]);

        put(constants.TABLE_MESSAGE_MSGDATA_LATITUDE_BYTE, values[4]);
        put(constants.TABLE_MESSAGE_MSGDATA_LONGITUDE_BYTE, values[5]);

        put(constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE, values[6]);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_ID_BYTE, values[7]);

        put(constants.TABLE_MESSAGE_MSGDATA_USER_CREATED_BYTE, String.format("%s %s", values[8], values[9]));
        put(constants.TABLE_MESSAGE_MSGDATA_USER_FRIENDS_COUNT_BYTE, values[10]);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_FOLLOWERS_COUNT_BYTE, values[11]);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_STATUSES_COUNT_BYTE, values[12]);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_FAVORITES_COUNT_BYTE, values[13]);
        put(constants.TABLE_MESSAGE_MSGDATA_USER_VERIFIED_BYTE, values[14]);

        put(constants.TABLE_MESSAGE_MSGDATA_SOURCE_FOLDER_BYTE, source);
        put(constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, values[15]);

        put(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE, String.format("%s %s", values[16], values[17]));
        put(constants.TABLE_MESSAGE_MSGDATA_CREATED_DATETIME_BYTE, String.format("%s %s", values[18], values[19]));
        put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, true);
        put(constants.TABLE_MESSAGE_MSGDATA_ISEXPORTED_BYTE, false);

        put(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, domainsBytes);
    }
}

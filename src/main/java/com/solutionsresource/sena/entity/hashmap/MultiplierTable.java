package com.solutionsresource.sena.entity.hashmap;

import com.solutionsresource.sena.constant.Constants;

import java.util.HashMap;

public class MultiplierTable extends HashMap<Double, Double> {

    private MultiplierTable() {
        super();
    }

    public static MultiplierTable create(double falseMultiplier) {
        return create(falseMultiplier, false);
    }

    public static MultiplierTable create(boolean isSentiment) {
        return create(1.0, isSentiment);
    }

    public static MultiplierTable create(double falseMultiplier, boolean isSentiment) {
        MultiplierTable multiplierTable = new MultiplierTable();

        if (isSentiment) {
            for (String sentimentName : Constants.SENTIMENT_NAMES) {
                multiplierTable.put(
                        (double) com.solutionsresource.sena.entity.Sentiment.idLookup(sentimentName),
                        sentimentName.equals(Constants.NEUTRAL) ? 1.0 : Constants.POLARED_SENTIMENT_BIAS_MULTIPLIER
                );
            }
        } else {
            multiplierTable.put(1.0, 1.0);
            multiplierTable.put(0.0, falseMultiplier);
        }

        return multiplierTable;
    }

    public Double get(Double key) {
        return containsKey(key) ? super.get(key) : 1.0;
    }
}

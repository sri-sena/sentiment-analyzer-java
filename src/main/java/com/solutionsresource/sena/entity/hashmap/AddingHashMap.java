package com.solutionsresource.sena.entity.hashmap;

import java.util.HashMap;

public abstract class AddingHashMap<K, V> extends HashMap<K, V> {
    public abstract V getDefaultValue();
    public V getOrCreate(K key) {
        if (!containsKey(key)) {
            put(key, getDefaultValue());
        }
        return get(key);
    }
}

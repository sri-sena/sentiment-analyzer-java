package com.solutionsresource.sena.entity.hashmap;

import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.Topic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class TopicHashMap extends HashMap<String, String> {
    public List<Topic> toTopics() {
        List<Topic> topics = new ArrayList<>();

        String topicFlag = "csv";

        for (Entry<String, String> topic : entrySet()) {
            String sentimentName = topic.getValue();
            topics.add(new Topic(topic.getKey(), topicFlag,
                    new Sentiment(sentimentName, sentimentName, topicFlag), false));
        }

        return topics;
    }
}

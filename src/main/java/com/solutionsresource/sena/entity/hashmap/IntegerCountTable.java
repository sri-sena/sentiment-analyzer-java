package com.solutionsresource.sena.entity.hashmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class IntegerCountTable extends HashMap<Integer, Integer> {
    public Integer[] getIntKeys() {
        List<Integer> keys = new ArrayList<Integer>();

        for (int key : this.keySet()) {
            keys.add(key);
        }

        Collections.sort(keys);

        return keys.toArray(new Integer[keys.size()]);
    }
}

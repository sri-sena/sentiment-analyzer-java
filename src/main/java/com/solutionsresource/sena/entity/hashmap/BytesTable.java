package com.solutionsresource.sena.entity.hashmap;

import org.apache.hadoop.hbase.util.Bytes;

import java.util.HashMap;
import java.util.Map.Entry;

public class BytesTable extends HashMap<byte[], byte[]> {
    public byte[] put(byte[] key, int value) {
        return super.put(key, Bytes.toBytes(value));
    }
    public byte[] put(byte[] key, String value) {
        return super.put(key, Bytes.toBytes(value));
    }
    public byte[] put(byte[] key, float value) {
        return super.put(key, Bytes.toBytes(value));
    }
    public byte[] put(byte[] key, double value) {
        return super.put(key, Bytes.toBytes(value));
    }
    public byte[] put(byte[] key, boolean value) {
        return super.put(key, Bytes.toBytes(value));
    }

    public String get(String key) {
        for (byte[] keyByte : keySet()) {
            if (Bytes.toString(keyByte).equals(key)) {
                return Bytes.toString(get(keyByte));
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append("{");

        boolean entryIsFirst = true;
        for (Entry<byte[], byte[]> entry : this.entrySet()) {
            if (!entryIsFirst) {
                builder.append(", ");
            }
            builder.append("\"");
            builder.append(Bytes.toString(entry.getKey()));
            builder.append("\":");
            builder.append("\"");
            builder.append(Bytes.toString(entry.getValue()));
            builder.append("\"");
            if (entryIsFirst) {
                entryIsFirst = false;
            }
        }

        builder.append("}");
        return builder.toString();
    }
}

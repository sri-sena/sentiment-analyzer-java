package com.solutionsresource.sena.entity.hashmap;

import scala.Tuple2;

import java.util.HashMap;

public class TopicSlugs extends HashMap<String, String[]> {

    public TopicSlugs() {
        super();
    }

    public TopicSlugs(Tuple2<String, String[]>[] keyValues) {
        super();

        for (Tuple2<String, String[]> keyValue : keyValues) {
            put(keyValue._1(), keyValue._2());
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (String topicName : keySet()) {
            String[] sentimentNames = get(topicName);

            builder.append("\t");
            builder.append(topicName);
            builder.append("\n");

            for (String sentiment : sentimentNames) {
                builder.append("\t\t");
                builder.append(sentiment);
                builder.append("\n");
            }
        }

        return builder.toString();
    }

    public boolean rejects(String topic) {
        return !containsKey(topic);
    }

    public boolean rejects(String topic, String sentiment) {
        for (String s : get(topic)) {
            if (s.equals(sentiment)) {
                return false;
            }
        }
        return true;
    }

    public long getModelsCount() {
        return size() * 2;
        // topic recognizer and sentiment analyzer
    }
}

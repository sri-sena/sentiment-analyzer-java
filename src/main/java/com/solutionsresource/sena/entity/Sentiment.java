package com.solutionsresource.sena.entity;

import com.google.gson.annotations.Expose;
import com.solutionsresource.sena.constant.Constants;

public class Sentiment extends BaseEntity {

    @Expose
    private String name;
    @Expose
    private String description;
    private String flag;

    public Sentiment() throws Exception {
        super();
    }

    public Sentiment(String name) {
        super();
        this.name = name;
    }

    public Sentiment(String name, String description) {
        super();
        this.name = name;
        this.description = description;
    }

    public Sentiment(String name, String description, String flag) {
        super();
        this.name = name;
        this.description = description;
        this.flag = flag;
    }

    public static String nameLookup(int id)
    {
        if (id == 3) {
            return Constants.POSITIVE;
        }
        if (id == 2) {
            return Constants.NEUTRAL;
        }
        if (id == 1) {
            return Constants.NEGATIVE;
        }
        return Constants.NEUTRAL;
    }

    public static int idLookup(String name) {
        if (name.equals(Constants.POSITIVE)) {
            return 3;
        }
        if (name.equals(Constants.NEUTRAL)) {
            return 2;
        }
        if (name.equals(Constants.NEGATIVE)) {
            return 1;
        }
        return 2;
    }

    public static String[] getNames() {
        return new String[] {Constants.UNKNOWN, Constants.NEGATIVE, Constants.NEUTRAL, Constants.POSITIVE};
    }

    public String toString() {
        return this.name;
    }

    public String getName() {
        return nameLookup(idLookup(this.name));
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSentimentIndex() {
        return idLookup(this.name);
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public static Sentiment mock() {
        return new Sentiment(Constants.NEGATIVE);
    }
}

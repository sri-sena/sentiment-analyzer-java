package com.solutionsresource.sena.entity;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class BaseEntity implements Serializable{

  @Expose
  protected String uuid;
  
  public String getUuid() {
    return uuid;
  }
  
  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
  
}

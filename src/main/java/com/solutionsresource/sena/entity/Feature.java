package com.solutionsresource.sena.entity;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featureselection.MutualInformation;

import java.util.HashMap;

public class Feature extends BaseEntity {
    private String value;
    private HashMap<String, Integer> usages;
    private MutualInformation mutualInformation;

    public Feature(String value, float mutualInformationValue) {
        this.value = value;
        this.usages = new UsageTable();
        this.mutualInformation = new MutualInformation(mutualInformationValue);
    }

    public Feature(String value, HashMap<String, Integer> usages, MutualInformation mutualInformation) {
        this.value = value;
        this.usages = usages;
        this.mutualInformation = mutualInformation;
    }

    public String getValue() {
        return value;
    }

    public HashMap<String, Integer> getUsages() {
        return usages;
    }

    public void setUsages(HashMap<String, Integer> usages) {
        this.usages = usages;
    }

    public MutualInformation getMutualInformation() {
        return mutualInformation;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(mutualInformation.toString());

        builder.append(" \t\t=============");

        for (String label : usages.keySet()) {
            builder.append(" ");
            builder.append(label);
            builder.append("=");
            builder.append(usages.get(label));
        }

        return builder.toString();
    }
}

package com.solutionsresource.sena.entity;

import java.util.List;

import com.google.gson.annotations.Expose;

public class Report {
  
  @Expose
  private String label;
  
  @Expose
  private List<Double> numbers;
  
  @Expose
  private List<String> legends;

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public List<Double> getNumbers() {
    return numbers;
  }

  public void setNumbers(List<Double> numbers) {
    this.numbers = numbers;
  }

  public List<String> getLegends() {
    return legends;
  }

  public void setLegends(List<String> legends) {
    this.legends = legends;
  }

}

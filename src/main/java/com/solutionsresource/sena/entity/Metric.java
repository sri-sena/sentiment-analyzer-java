package com.solutionsresource.sena.entity;

import com.google.gson.annotations.Expose;
import scala.Tuple2;

import java.util.Date;

public class Metric extends BaseEntity {

    @Expose
    private Integer[][] confusionMatrix;
    @Expose
    private float precision;
    @Expose
    private float recall;
    @Expose
    private float accuracy;
    @Expose
    private float f1Score;
    @Expose
    private long trainCount;
    @Expose
    private long testCount;
    @Expose
    private long createdDate;

    private Tuple2<Integer, Integer> correctnessRatio;

    public Metric(Integer[][] confusionMatrix, float precision, float recall, float accuracy, float f1Score,
                  int trainCount, int testCount) {
        this.confusionMatrix = confusionMatrix;
        this.precision = precision;
        this.recall = recall;
        this.accuracy = accuracy;
        this.f1Score = f1Score;
        this.trainCount = trainCount;
        this.testCount = testCount;
    }

    public Metric(Integer[][] confusionMatrix, Tuple2<Integer, Integer> correctnessRatio,
                  long trainCount, long testCount) {
        this.confusionMatrix = confusionMatrix;
        this.correctnessRatio = correctnessRatio;
        this.precision = initializePrecision();
        this.recall = initializeRecall();
        this.accuracy = initializeAccuracy();
        this.f1Score = initializeF1Score();
        this.trainCount = trainCount;
        this.testCount = testCount;
        this.createdDate = new Date().getTime();
    }

    private float initializePrecision() {
        int length = confusionMatrix.length;

        float[] precisions = new float[length];

        for (int j = 0; j < length; j++) {
            float tp = 0;
            float fp = 0;

            for (int i = 0; i < length; i++) {
                if (i == j) {
                    tp += confusionMatrix[i][j];
                } else {
                    fp += confusionMatrix[i][j];
                }
            }

            if (tp + fp == 0) {
                precisions[j] = 1;
            } else {
                precisions[j] = tp / (tp + fp);
            }
        }

        return getAverage(length, precisions) * 100;
    }

    private float initializeRecall() {
        int length = confusionMatrix.length;

        float[] recalls = new float[length];

        for (int i = 0; i < length; i++) {
            float tp = 0;
            float fn = 0;

            for (int j = 0; j < length; j++) {
                if (i == j) {
                    tp += confusionMatrix[i][j];
                } else {
                    fn += confusionMatrix[i][j];
                }
            }

            if (tp + fn == 0) {
                recalls[i] = 1;
            } else {
                recalls[i] = tp / (tp + fn);
            }
        }

        return getAverage(length, recalls) * 100;
    }

    private float initializeF1Score() {
        return 2 * (precision * recall) / (precision + recall);
    }

    public float initializeAccuracy() {
        return correctnessRatio._1() / (float) (correctnessRatio._1() + correctnessRatio._2()) * 100;
    }

    private float getAverage(int length, float[] floats) {
        float sum = 0;

        for (float f : floats) {
            sum += f;
        }

        return sum / length;
    }

    public Integer[][] getConfusionMatrix() {
        return confusionMatrix;
    }

    public void setConfusionMatrix(Integer[][] confusionMatrix) {
        this.confusionMatrix = confusionMatrix;
    }

    public float getPrecision() {
        return precision;
    }

    public void setPrecision(float precision) {
        this.precision = precision;
    }

    public float getRecall() {
        return recall;
    }

    public void setRecall(float recall) {
        this.recall = recall;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public float getF1Score() {
        return f1Score;
    }

    public void setF1Score(float f1Score) {
        this.f1Score = f1Score;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getTrainCount() {
        return trainCount;
    }

    public void setTrainCount(long trainCount) {
        this.trainCount = trainCount;
    }

    public long getTestCount() {
        return testCount;
    }

    public void setTestCount(long testCount) {
        this.testCount = testCount;
    }

    private static int totalCount(Integer[][] integerMatrix) {
        int sum = 0;
        for (Integer[] integers : integerMatrix) {
            for (int integer : integers) {
                sum += integer;
            }
        }
        return sum;
    }

    private static int randomInteger(int max) {
        return (int) (Math.random() * max);
    }

    public static Metric mock() {
        Integer[][] confusionMatrix = new Integer[][]{
                new Integer[]{
                        randomInteger(8000),
                        randomInteger(1000),
                        randomInteger(1000),
                        randomInteger(1000),
                },
                new Integer[]{
                        randomInteger(1000),
                        randomInteger(8000),
                        randomInteger(1000),
                        randomInteger(1000),
                },
                new Integer[]{
                        randomInteger(1000),
                        randomInteger(1000),
                        randomInteger(8000),
                        randomInteger(1000),
                },
                new Integer[]{
                        randomInteger(1000),
                        randomInteger(1000),
                        randomInteger(1000),
                        randomInteger(8000),
                },
        };

        int trainingCount = randomInteger(100000);
        int testCount = totalCount(confusionMatrix);

        return new Metric(confusionMatrix, new Tuple2<Integer, Integer>(8000, 2000), trainingCount, testCount);
    }
}

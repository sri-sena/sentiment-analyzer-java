package com.solutionsresource.sena.entity;

import com.google.gson.annotations.Expose;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.mllib.linalg.Vector;

import java.util.*;


public class Message extends BaseEntity {

  @Expose
  private String content;
  @Expose
  private String source;
  @Expose
  private String username;
  @Expose
  private List<Domain> domains;
  @Expose
  private boolean finalize;
  @Expose
  private String postedDate;

  private UsageTable ngrams;
  private Vector features;
  private List<Domain> predictedDomains;
  private HashMap<String, Integer> hashtagAndHandlerNgrams;

  public Message() {
    super();
  }

  public Message(NavigableMap<byte[], NavigableMap<byte[], NavigableMap<Long, byte[]>>> resultMap) {
    //TODO: need to constructure source from resultMap
//		NavigableMap<byte[], NavigableMap<Long, byte[]>> messageData = resultMap
//				.get(Bytes.toBytes("message_data"));
//		NavigableMap<Long, byte[]> domains = messageData.get(Bytes
//				.toBytes("domains"));
//		NavigableMap<Long, byte[]> content = messageData.get(Bytes
//				.toBytes("content"));
//
//		String value = Bytes.toString(content.get(content.firstKey()));
//		List<TopicSentiment> topicSentiments = new ArrayList<TopicSentiment>();
//
//		this.value = value;
//		this.topicSentiments = topicSentiments
//				.toArray(new TopicSentiment[topicSentiments.size()]);
  }

  public Message(String content) {
    this.content = content;
    this.ngrams = new UsageTable();
    this.hashtagAndHandlerNgrams = new HashMap<String, Integer>();
    this.predictedDomains = new ArrayList<Domain>();
  }

  public Message(String uuid, String content, List<Domain> domains, boolean isFinalized) {
    this(uuid, content, domains, "NA", isFinalized);
  }

  public Message(String uuid, String content, List<Domain> domains, String username, boolean isFinalized) {
    this(uuid, content, domains, username, isFinalized, null);
  }

  public Message(String uuid, String content, List<Domain> domains, String username, boolean isFinalized,
                 String postedDate) {
    this.postedDate = postedDate;
    this.uuid = uuid;
    this.content = content;
    this.domains = domains;
    this.ngrams = new UsageTable();
    this.hashtagAndHandlerNgrams = new HashMap<String, Integer>();
    this.predictedDomains = new ArrayList<Domain>();
    this.username = username;
    this.finalize = isFinalized;
  }

  public String toString() {
    return this.content;
  }

  public Domain getDomain(String label) {
    for (Domain domain : domains) {
      if (domain.getName() != null && domain.getSlug().equalsIgnoreCase(label) && domain.hasTopics()) {
        return domain;
      }
    }
    return null;
  }

  public boolean hasDomain(String label) {
    return getDomain(label) != null;
  }


  public Topic getTopic(String domainName, String topicName) {
    Domain domain = getDomain(domainName);
    return domain == null ? null : domain.getTopic(topicName);
  }

  public boolean hasTopic(String domainName, String topicName) {
    return getTopic(domainName, topicName) != null;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public List<Domain> getDomains() {
    return domains;
  }

  public DomainSlugs getDomainSlugs(DomainSlugs acceptedDomainSlugs) {
    DomainSlugs domainSlugs = new DomainSlugs();
    for (Domain domain : domains) {
      String domainSlug = domain.getSlug();
      if (domainSlug != null && acceptedDomainSlugs.containsKey(domainSlug)) {
        domainSlugs.put(domain.getSlug(), domain.getTopicSlugs(acceptedDomainSlugs.get(domainSlug)));
      }
    }
    return domainSlugs;
  }

  public void setDomains(List<Domain> domains) {
    this.domains = domains;
  }

  public UsageTable getNgrams() {
    return ngrams;
  }

  public void setHashtagAndHandlerNgrams(UsageTable hashtagAndHandlerNgrams) {
    this.hashtagAndHandlerNgrams = hashtagAndHandlerNgrams;
  }

  public void setNgrams(UsageTable ngrams) {
    this.ngrams = ngrams;
  }

  public Vector getFeatures() {
    return features;
  }

  public void setFeatures(Vector features) {
    this.features = features;
  }

  public List<Domain> getPredictedDomains() {
    return predictedDomains;
  }

  public boolean hasPredictedDomain(String label) {
    return getPredictedDomain(label) != null;
  }

  public Domain getPredictedDomain(String label) {
    for (Domain domain : predictedDomains) {
      if (domain.getSlug().equalsIgnoreCase(label)) {
        return domain;
      }
    }
    return null;
  }

  public boolean hasPredictedTopic(String domainName, String topicName) {
    return getPredictedTopic(domainName, topicName) != null;
  }

  public Topic getPredictedTopic(String domainName, String topicName) {
    Domain domain = getPredictedDomain(domainName);
    return domain == null ? null : domain.getTopic(topicName);
  }


  public void addPredictedDomain(Domain domain) {
    this.predictedDomains.add(domain);
  }

  public Set<String> getSentiments() {
    Set<String> sentiments = new TreeSet<String>();

    if (domains.isEmpty()) {
      return sentiments;
    }

    Domain domain = domains.get(0);

    for (Topic topic : domain.getTopics()) {
      sentiments.add(topic.getSentiment().getName());
    }

    return sentiments;
  }

  public void initializeTopicSentiments() {
    this.predictedDomains = new ArrayList<Domain>();
  }

  public boolean isUnrelated() {
    return domains.isEmpty();
  }

  public boolean isFinalize() {
    return finalize;
  }

  public void setFinalize(boolean finalize) {
    this.finalize = finalize;
  }

  public String logNgrams() {
    List<String> keys = new ArrayList<String>();
    for (String key : ngrams.keySet()) {
      keys.add(key);
    }
    return StringUtils.join(keys, "==$==");
  }

  public HashMap<String, Integer> getHashtagAndHandlerNgrams() {
    return hashtagAndHandlerNgrams;
  }

  public void addPredictedTopic(String domainName, Topic topic) {
    Domain domain = getPredictedDomain(domainName);

    if (domain == null) {
      domain = new Domain(domainName);
      addPredictedDomain(domain);
    }

    domain.addTopic(topic);
  }

  public boolean isCorrect() {
    if (this.getDomains().size() != this.getPredictedDomains().size()) {
      return false;
    }

    for (Domain domain : this.getDomains()) {
      String domainName = domain.getName();
      for (Topic topic : domain.getTopics()) {
        String topicName = topic.getSlug();
        Topic predictedTopic = this.getPredictedTopic(domainName, topicName);

        if (predictedTopic == null) {
          return false;
        }

        String sentimentName = topic.getSentiment().getName();
        String predictedSentimentName = predictedTopic.getSentiment().getName();

        if (!sentimentName.equalsIgnoreCase(predictedSentimentName)) {
          return false;
        }
      }
    }

    for (Domain predictedDomain : this.getPredictedDomains()) {
      String predictedDomainName = predictedDomain.getName();
      for (Topic predictedTopic : predictedDomain.getTopics()) {
        String predictedTopicName = predictedTopic.getSlug();

        Topic topic = this.getTopic(predictedDomainName, predictedTopicName);

        if (topic == null) {
          return false;
        }

        String predictedSentimentName = predictedTopic.getSentiment().getName();
        String sentimentName = topic.getSentiment().getName();

        if (!predictedSentimentName.equalsIgnoreCase(sentimentName)) {
          return false;
        }
      }
    }

    return true;
  }

  public String getPostedDate() {
    return postedDate;
  }

  public void setPostedDate(String postedDate) {
    this.postedDate = postedDate;
  }
}

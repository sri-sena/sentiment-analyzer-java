package com.solutionsresource.sena.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import org.apache.spark.mllib.classification.NaiveBayesModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Domain extends BaseEntity {

    public static final Type TYPE = new TypeToken<List<Domain>>() {
    }.getType();

    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private boolean deleted;
    @Expose
    private List<Topic> topics;

    private String slug;

    private List<Feature> selectedFeatures;
    private NaiveBayesModel naiveBayesModel;
    private String flag;

    public Domain() {
        super();
    }

    public Domain(String name) {
        super();
        this.name = name;
        this.slug = getSlug();
        this.topics = new ArrayList<Topic>();
    }

    public Domain(String name, String description,
                  boolean deleted, List<Topic> topics) {
        super();
        this.name = name;
        this.slug = getSlug();
        this.description = description;
        this.deleted = deleted;
        this.topics = topics;
    }

    public Domain(String name, String description, String flag,
                  boolean deleted, List<Topic> topics) {
        super();
        this.name = name;
        this.flag = flag;
        this.slug = getSlug();
        this.description = description;
        this.deleted = deleted;
        this.topics = topics;
    }

    public Domain(String name, List<Feature> selectedFeatures, NaiveBayesModel naiveBayesModel) {
        super();
        this.name = name;
        this.selectedFeatures = selectedFeatures;
        this.naiveBayesModel = naiveBayesModel;
        this.slug = getSlug();
        this.topics = new ArrayList<Topic>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getSlug() {
        if (this.name == null) {
            return null;
        }
        return this.name.toLowerCase().replace(" ", "-");
    }

    public List<Feature> getSelectedFeatures() {
        return selectedFeatures;
    }

    public NaiveBayesModel getNaiveBayesModel() {
        return naiveBayesModel;
    }

    public Topic getTopic(String label) {
        for (Topic topic : topics) {
            if (topic.getName().equalsIgnoreCase(label) || topic.getSlug().equalsIgnoreCase(label)) {
                return topic;
            }
        }
        return null;
    }

    public boolean hasTopic(String label) {
        return getTopic(label) != null;
    }

    public static Domain mock() throws Exception {
        List<Topic> topics = new ArrayList<Topic>();
        topics.add(Topic.mock());
        topics.add(new Topic("Rodrigo Duterte", Sentiment.mock(), false));
        topics.add(new Topic("Another topic", Sentiment.mock(), false));
        return new Domain("Election", "Election", false, topics);
    }

    public TopicSlugs getTopicSlugs(TopicSlugs acceptedTopicSlugs) {
        TopicSlugs topicSlugs = new TopicSlugs();
        for (Topic topic : this.topics) {
            String topicSlug = topic.getSlug();
            if (acceptedTopicSlugs.containsKey(topicSlug)) {
                topicSlugs.put(topicSlug, topic.getSentimentSlugs());
            }
        }
        return topicSlugs;
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
    }

    @Override
    public String toString() {
        return name.concat(" ").concat(topics.toString());
    }

    public boolean hasTopics() {
        return topics.size() > 0;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}

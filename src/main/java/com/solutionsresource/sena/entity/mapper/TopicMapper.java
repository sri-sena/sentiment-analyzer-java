package com.solutionsresource.sena.entity.mapper;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Topic;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class TopicMapper implements Function<Tuple2<ImmutableBytesWritable, Result>, Topic> {

    Constants constants;

    public TopicMapper() throws Exception {
        constants = new Constants();
    }

    public Topic call(Tuple2<ImmutableBytesWritable, Result> tuple) throws Exception {
        Result result = tuple._2();

        String name = Bytes.toString(result.getValue(
                constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_NAME_BYTE));

        String description = Bytes.toString(result.getValue(
                constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_DESCRIPTION_BYTE));

        return new Topic(name, description);
    }
}

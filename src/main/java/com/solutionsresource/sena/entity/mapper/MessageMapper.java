package com.solutionsresource.sena.entity.mapper;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

import java.util.ArrayList;
import java.util.List;

public class MessageMapper implements Function<ResultHashMap, Message> {

  Constants constants;

  public MessageMapper() throws Exception {
    constants = new Constants();
  }

  public Message call(ResultHashMap result) throws Exception {
    String uuid = Bytes.toString(result.getRow());

    String content = Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT));
    String domainsJson = Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT));
    String username = Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_USERNAME_TEXT));
    String postedDate = Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_TEXT));
    boolean isFinalized = Bytes.toBoolean(result.get(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT));

    List<Domain> domains = new ArrayList<>();
    if (StringUtils.isNotBlank(domainsJson)) {
      domains = new Gson().fromJson(domainsJson, Domain.TYPE);
    }

    return new Message(uuid, content, domains, username, isFinalized, postedDate);
  }
}

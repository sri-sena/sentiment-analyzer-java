package com.solutionsresource.sena.entity;

import com.google.gson.annotations.Expose;
import org.apache.spark.mllib.classification.NaiveBayesModel;

import java.util.List;

public class Topic extends BaseEntity {

    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private Sentiment sentiment;
    @Expose
    private boolean deleted;

    public String slug;
    private String domain;

    private List<Feature> topicSelectedFeatures;
    private NaiveBayesModel topicNaiveBayesModel;

    private List<Feature> sentimentSelectedFeatures;
    private NaiveBayesModel sentimentNaiveBayesModel;
    private String flag;

    public Topic() {
        super();
    }

    public Topic(String topicName, List<Feature> topicSelectedFeatures, NaiveBayesModel topicNaiveBayesModel,
                 List<Feature> sentimentSelectedFeatures, NaiveBayesModel sentimentNaiveBayesModel) {
        this.name = topicName;
        this.topicSelectedFeatures = topicSelectedFeatures;
        this.topicNaiveBayesModel = topicNaiveBayesModel;
        this.sentimentSelectedFeatures = sentimentSelectedFeatures;
        this.sentimentNaiveBayesModel = sentimentNaiveBayesModel;
        this.slug = getSlug();
    }

    public Topic(String topicName) {
        this.name = topicName;
        this.slug = getSlug();
    }

    public Topic(String name, String flag, Sentiment sentiment,
                 boolean deleted) {
        super();
        this.name = name;
        this.flag = flag;
        this.sentiment = sentiment;
        this.deleted = deleted;
        this.slug = getSlug();
    }

    public Topic(String name, Sentiment sentiment,
                 boolean deleted) {
        super();
        this.name = name;
        this.sentiment = sentiment;
        this.deleted = deleted;
        this.slug = getSlug();
    }

    public Topic(String name, String description) {
        this.name = name;
        this.setDescription(description);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getSlug() {
        return this.name.toLowerCase().replace(" ", "-");
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getNameAndSentiment() {
        return this.name + ": " + this.sentiment.getName();
    }

    public List<Feature> getTopicSelectedFeatures() {
        return topicSelectedFeatures;
    }

    public NaiveBayesModel getTopicNaiveBayesModel() {
        return topicNaiveBayesModel;
    }

    public List<Feature> getSentimentSelectedFeatures() {
        return sentimentSelectedFeatures;
    }

    public NaiveBayesModel getSentimentNaiveBayesModel() {
        return sentimentNaiveBayesModel;
    }

    public static Topic mock() throws Exception {
        return new Topic("Jejomar Binay", Sentiment.mock(), false);
    }

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

    public String[] getSentimentSlugs() {
        return Sentiment.getNames();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return String.format("%s (%s)", name, description);
    }
}

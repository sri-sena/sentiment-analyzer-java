package com.solutionsresource.sena.entity;

import java.util.Date;
import java.util.List;

public class Tweet {

  public String id;
  public String objectType;
  public Actor actor;
  public String verb;
  public Date postedTime;
  public Generator generator;
  public Provider provider;
  public String link;
  public String body;
  public TObject object;
  public ReplyTo inReplyTo;
  public Integer favoritesCount;
  public Object twitter_entities;     // TODO
  public String twitter_filter_level;
  public String twitter_lang;
  public Integer retweetCount;
  public Object gnip;                 // TODO

  public class Actor {
    public String objectType;
    public String id;
    public String link;
    public String displayName;
    public Date postedTime;
    public String image;
    public String summary;
    public List<Link> links;
    public Integer friendsCount;
    public Integer followersCount;
    public Integer listedCount;
    public Integer statusesCount;
    public String twitterTimeZone;
    public Boolean verified;
    public String utcOffset;
    public String preferredUsername;
    public List<String> languages;
    public Location location;
    public Integer favoritesCount;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t actor-objectType: " + this.objectType + "\n";
      x += "\t actor-id: " + this.id + "\n";
      x += "\t actor-link: " + this.link + "\n";
      x += "\t actor-displayName: " + this.displayName + "\n";
      x += "\t actor-postedTime: " + this.postedTime + "\n";
      x += "\t actor-image: " + this.image + "\n";
      x += "\t actor-summary: " + this.summary + "\n";
      x += "\t actor-friendsCount: " + this.friendsCount + "\n";
      x += "\t actor-followersCount: " + this.followersCount + "\n";
      x += "\t actor-listedCount: " + this.listedCount + "\n";
      x += "\t actor-statusesCount: " + this.statusesCount + "\n";
      x += "\t actor-twitterTimeZone: " + this.twitterTimeZone + "\n";
      x += "\t actor-verified: " + this.verified + "\n";
      x += "\t actor-utcOffset: " + this.utcOffset + "\n";
      x += "\t actor-preferredUsername: " + this.preferredUsername + "\n";
      x += "\t actor-location: " + this.location + "\n";
      x += "\t actor-favoritesCount: " + this.favoritesCount + "\n";
      return x;
    }
  }
  
  public class Link {
    public String href;
    public String rel;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t link-objectType: " + this.href + "\n";
      x += "\t link-rel: " + this.rel + "\n";
      return x;
    }
  }
  
  public class Location {
    public String objectType;
    public String displayName;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t location-objectType: " + this.objectType + "\n";
      x += "\t location-displayName: " + this.displayName + "\n";
      return x;
    }
  }
  
  public class Generator {
    public String displayName;
    public String link;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t generator-displayName: " + this.displayName + "\n";
      x += "\t generator-link: " + this.link + "\n";
      return x;
    }
  }
  
  public class Provider {
    public String objectType;
    public String displayName;
    public String link;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t provider-objectType: " + this.objectType + "\n";
      x += "\t provider-displayName: " + this.displayName + "\n";
      x += "\t provider-link: " + this.link + "\n";
      return x;
    }
  }
  
  public class TObject {
    public String objectType;
    public String id;
    public String summary;
    public String link;
    public Date postedTime;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t object-objectType: " + this.objectType + "\n";
      x += "\t object-id: " + this.id + "\n";
      x += "\t object-summary: " + this.summary + "\n";
      x += "\t object-link: " + this.link + "\n";
      x += "\t object-postedTime: " + this.postedTime + "\n";
      return x;
    }
  }
  
  public class ReplyTo {
    public String link;
    
    @Override
    public String toString() {
      String x = "\n";
      x += "\t inReplyTo-link: " + this.link + "\n";
      return x;
    }
  }
  
  @Override
  public String toString() {
    String x = "-------------------- \n";
    x += "\t id: " + id + "\n";
    x += "\t objectType: " + this.objectType + "\n";
    x += "\t actor: " + this.actor;
    x += "\t verb: " + this.verb + "\n";
    x += "\t postedTime: " + this.postedTime + "\n";
    x += "\t generator: " + this.generator;
    x += "\t provider: " + this.provider;
    x += "\t link: " + this.link + "\n";
    x += "\t body: " + this.body + "\n";
    x += "\t object: " + this.object;
    x += "\t inReplyTo: " + this.inReplyTo;
    x += "\t favoritesCount: " + this.favoritesCount + "\n";
    x += "\t twitter_entities: " + this.twitter_entities + "\n";
    x += "\t twitter_filter_level: " + this.twitter_filter_level + "\n";
    x += "\t twitter_lang: " + this.twitter_lang + "\n";
    x += "\t retweetCount: " + this.retweetCount + "\n";
    x += "\t gnip: " + this.gnip + "\n";
    return x;
  }

}

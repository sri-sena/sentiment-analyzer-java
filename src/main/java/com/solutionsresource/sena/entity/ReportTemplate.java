package com.solutionsresource.sena.entity;

import com.google.gson.annotations.Expose;

import java.util.List;

public class ReportTemplate extends BaseEntity {

  @Expose
  private String type;
  
  @Expose
  private String name;

  @Expose
  private String title;

  @Expose
  private String subtitle;

  @Expose
  private String sentiment;
  
  @Expose
  private List<String> partitions;

  @Expose
  private List<Topic> topics;

  @Expose
  private List<Source> sources;

  @Expose
  private List<Tophandle> tophandles;
  
  @Expose
  private Boolean finalize;

  @Expose
  private Boolean unfinalize;

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public String getSentiment() {
    return sentiment;
  }

  public void setSentiment(String sentiment) {
    this.sentiment = sentiment;
  }

  public List<String> getPartitions() {
    return partitions;
  }

  public void setPartitions(List<String> partitions) {
    this.partitions = partitions;
  }

  public List<Topic> getTopics() {
    return topics;
  }

  public void setTopics(List<Topic> topics) {
    this.topics = topics;
  }

  public List<Source> getSources() {
    return sources;
  }

  public void setSources(List<Source> sources) {
    this.sources = sources;
  }

  public List<Tophandle> getTophandles() {
    return tophandles;
  }

  public void setTophandles(List<Tophandle> tophandle) {
    this.tophandles = tophandle;
  }

  public Boolean getFinalize() {
    return finalize;
  }

  public void setFinalize(Boolean finalize) {
    this.finalize = finalize;
  }

  public Boolean getUnfinalize() {
    return unfinalize;
  }

  public void setUnfinalize(Boolean unfinalize) {
    this.unfinalize = unfinalize;
  }
}

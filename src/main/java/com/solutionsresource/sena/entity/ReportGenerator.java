package com.solutionsresource.sena.entity;

import java.util.Date;
import java.util.List;

import com.google.gson.annotations.Expose;

public class ReportGenerator {
  
  public ReportGenerator() {}
  public ReportGenerator(ReportTemplate reportTemplate, Date from, Date to) {
    this.reportTemplate = reportTemplate;
    this.from = from;
    this.to = to;
  }
  
  @Expose
  private ReportTemplate reportTemplate;

  @Expose
  private Date from;

  @Expose
  private Date to;

  @Expose
  private List<Report> reports;

  public ReportTemplate getReportTemplate() {
    return reportTemplate;
  }

  public void setReportTemplate(ReportTemplate reportTemplate) {
    this.reportTemplate = reportTemplate;
  }

  public Date getFrom() {
    return from;
  }

  public void setFrom(Date from) {
    this.from = from;
  }

  public Date getTo() {
    return to;
  }

  public void setTo(Date to) {
    this.to = to;
  }

  public List<Report> getReports() {
    return reports;
  }

  public void setReports(List<Report> reports) {
    this.reports = reports;
  }

}

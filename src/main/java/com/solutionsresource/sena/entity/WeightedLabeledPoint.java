package com.solutionsresource.sena.entity;

import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;

import java.util.List;

public class WeightedLabeledPoint extends LabeledPoint {
    private final float weight;

    public WeightedLabeledPoint(double label, Vector features, float weight) {
        super(label, features);
        this.weight = weight;
    }

    public float weight() {
        return weight;
    }

    public LabeledPoint toLabeledPoint() {
        return this;
    }

    public WeightedLabeledPoint withReducedFeatures(Integer[] indexes) {
        double[] uncleanFeatures = this.features().toArray();
        double[] cleanFeatures = new double[indexes.length];

        for (int i = 0; i < indexes.length; i++) {
            int index = indexes[i];
            cleanFeatures[i] = uncleanFeatures[index];
        }

        return new WeightedLabeledPoint(label(), Vectors.dense(cleanFeatures), weight());
    }

    public String toString() {
        return weight + " " + super.toString();
    }

    public WeightedLabeledPoint withMultipliedFeatures(List<Feature> featuresWithMutualInformation) {
        double[] features = this.features().toArray();

        for (int i = 0; i < featuresWithMutualInformation.size(); i++) {
            features[i] = features[i] * featuresWithMutualInformation.get(i).getMutualInformation().value;
        }

        return new WeightedLabeledPoint(label(), Vectors.dense(features), weight());
    }
}

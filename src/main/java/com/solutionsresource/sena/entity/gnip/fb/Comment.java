package com.solutionsresource.sena.entity.gnip.fb;

/**
 * Created by Ronald Erquiza on 4/29/2016.
 */
public class Comment {
    public String comment_content;
    public String comment_id;
    public String posted_date;
    public String user_name;
    public String user_id;
    public String likes;
    public Hashtags hash_tags;

    public String toString(){
        return comment_content + " - " + comment_id + " - " + posted_date + " - " +
                user_name + " - " + user_id + " - " + likes + " - (" +
                hash_tags + ")";
    }
}

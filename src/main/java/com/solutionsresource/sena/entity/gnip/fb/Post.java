package com.solutionsresource.sena.entity.gnip.fb;

/**
 * Created by Ronald Erquiza on 4/29/2016.
 */
public class Post {
    public String title;
    public String link;
    public String description;
    public int likes;
    public int shares;
    public String source_type;
    public String source_id;
    public String caption;
    public String post_id;

    public String toString(){
        return title + " - " + link + " - " + description + " - " +
                likes + " - " + shares + " - " + source_type + " - " +
                source_id + " - " + caption + " - " + post_id;
    }
}

package com.solutionsresource.sena.entity.gnip;

public class Actor {
    public String id;
    public String preferredUsername;
    public String postedTime;
    public String friendsCount;
    public String followersCount;
    public String statusesCount;
    public String favoritesCount;
    public String verified;

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %s %s %s",
                id, preferredUsername, postedTime, friendsCount,
                followersCount, statusesCount, favoritesCount, verified);
    }
}

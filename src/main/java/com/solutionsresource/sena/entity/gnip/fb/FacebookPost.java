package com.solutionsresource.sena.entity.gnip.fb;

/**
 * Created by Ronald Erquiza on 4/29/2016.
 */
public class FacebookPost {
    public Post Post;
    public FacebookComments comments;

    public String toString(){
        return "(" + Post + ") - \n(" + comments + ")";
    }
}

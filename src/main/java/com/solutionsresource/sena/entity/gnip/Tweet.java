package com.solutionsresource.sena.entity.gnip;

public class Tweet {
    public String id;
    public String body;
    public Actor actor;
    public Generator generator;
    public Location location;
    public TwitterEntities twitter_entities;
    public String postedTime;
    public Gnip gnip;
}

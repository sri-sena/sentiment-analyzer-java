package com.solutionsresource.sena.ajax;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.Report;
import com.solutionsresource.sena.entity.ReportGenerator;
import com.solutionsresource.sena.entity.ReportTemplate;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.entity.mapper.MessageMapper;
import com.solutionsresource.sena.function.featureselection.FilterWithOffset;
import com.solutionsresource.sena.function.featureselection.GetHasAnyDomain;
import com.solutionsresource.sena.function.hbase.FilterFinalized;
import com.solutionsresource.sena.main.ExportMessagesFromHbase;
import com.solutionsresource.sena.main.ImportMessagesFromCsv;
import com.solutionsresource.sena.report.ReportService;
import com.solutionsresource.sena.report.ReportTemplateRepository;
import com.solutionsresource.sena.report.ReportTemplateRowMapper;
import com.solutionsresource.sena.util.MessageRowMapper;
import com.solutionsresource.sena.util.csv.BashCsvCommandsGenerator;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.ingest.FilterUnrelated;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.io.*;
import java.util.*;

public class UIAjaxHandler {

  private final HBaseDao hbase;
  private JavaSparkContext sc;
  private Configuration config;
  private Constants constants;
  private Gson gson;

  public UIAjaxHandler(JavaSparkContext sc, Configuration config, Constants constants, Gson gson) throws Exception {
    this.sc = sc;
    this.config = config;
    this.constants = constants;
    this.gson = gson;
    this.hbase = new HBaseDao(sc, config, constants);
  }

  public static void main(String[] args) throws Exception {

    SparkConf conf = new SparkConf().setAppName("SENA UIAjaxHandler");
    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    try (JavaSparkContext sc = new JavaSparkContext(conf)) {
      Configuration config = new HBaseConnector().getConfiguration();
      Constants constants = new Constants();

      String context = args[0];
      Map<String, String> parameters = transform(args);

      output(new UIAjaxHandler(sc, config, constants, gson).handle(context, parameters));
    }
  }

  private String handle(String context, Map<String, String> parameters) throws Exception {
    if (StringUtils.equals(context, "ping")) {
      return ping();
    }
    if (StringUtils.equals(context, "ModifyRowKey")) {
      return modifyRowKey();
    }
    if (StringUtils.equals(context, "MessageList")) {
      return messageList(gson, parameters);
    }
    if (StringUtils.equals(context, "MessageCount")) {
      return messageCount(parameters);
    }
    if (StringUtils.equals(context, "report-generator")) {
      return reportGenerator(gson, parameters);
    }
    if (StringUtils.equals(context, "upload-csv")) {
      return uploadCsv(parameters);
    }
    if (StringUtils.equals(context, "download-csv")) {
      return downloadCsv(parameters);
    }
    throw new IllegalArgumentException("Invalid UI Ajax Handler!");
  }

  public String ping() {
    // output
    return "pong";
  }

  public String modifyRowKey() throws IOException {
    // initialize
    HConnection connection = HConnectionManager.createConnection(config);
    HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
    //      List<Message> list = new ArrayList<Message>();
    MessageRowMapper rowMapper = new MessageRowMapper();
    Scan scan = new Scan();

    // logic
    //Filter paging = new PageFilter(constants.ITEMS_PER_PAGE);
    //Filter ignoreTrainingData = new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE));
    //FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL, ignoreTrainingData, paging);
    //scan.setFilter(filter);

    Iterator<Result> results = table.getScanner(scan).iterator();
    while (results.hasNext()) {
      Result result = results.next();
      Message message = rowMapper.mapRow(result, constants);
      Long timestamp = result.listCells().get(0).getTimestamp();

      String oldUUID = message.getUuid();
      String newUUID = oldUUID.replaceAll("(^[^-]*)", String.valueOf(timestamp));

      System.out.print(oldUUID);
      System.out.print("  ==  ");
      Delete delete = new Delete(oldUUID.getBytes());
      table.delete(delete);

      System.out.print(newUUID);
      System.out.print("  ==  ");
      Put put = new Put(newUUID.getBytes());
      NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyQualifierMap = result.getNoVersionMap();
      for (byte[] familyBytes : familyQualifierMap.keySet()) {
        NavigableMap<byte[], byte[]> qualifierMap = familyQualifierMap.get(familyBytes);
        for (byte[] qualifier : qualifierMap.keySet()) {
          put.add(familyBytes, qualifier, timestamp, qualifierMap.get(qualifier));
        }
      }
      table.put(put);

      System.out.println(message.getContent());
    }

    table.flushCommits();
    table.close();

    return "success";
  }

  public String messageList(Gson gson, Map<String, String> parameters) throws Exception {
    String offset = parameters.get("offset");

    JavaRDD<ResultHashMap> messages = getMessagesFromParameters(parameters).cache();
    return String.format("%d\n%s", messages.count(),
        gson.toJson(messages.filter(new FilterWithOffset(offset))
            .map(new MessageMapper()).take((int) constants.ITEMS_PER_PAGE)));
  }

  public String messageCount(Map<String, String> parameters) throws Exception {
    return String.valueOf(getMessagesFromParameters(parameters).count());
  }

  public JavaRDD<ResultHashMap> getMessagesFromParameters(Map<String, String> parameters) throws Exception {
    DateTime from = parameters.containsKey("from") ?
        parseDate(parameters.get("from")) :
        new DateTime(2015, 1, 1, 0, 0, 0, 0);

    DateTime to = parameters.containsKey("to") ?
        parseDate(parameters.get("to")) :
        new DateTime();

    boolean isFinalized = parseBoolean(parameters.get("isFinalized"));
    boolean isUnrelated = parseBoolean(parameters.get("isUnrelated"));

    List<String> domains = parseComma(parameters.get("domains"));

    DomainSlugs domainSlugs = hbase.getAcceptedDomainSlugs();

    JavaRDD<ResultHashMap> messages = hbase.getMessagesWithinTimeRange(from.getMillis(), to.getMillis())
        .filter(new FilterFinalized(isFinalized));

    if (!isUnrelated) {
      messages = messages.filter(new FilterUnrelated(constants, domainSlugs));
    }

    if (domains != null && !domains.isEmpty()) {
      messages = messages.filter(new GetHasAnyDomain(constants, domains));
    }

    return messages;
  }

  public String reportGenerator(Gson gson, Map<String, String> parameters) throws Exception {
    // initialize
    HConnection connection = HConnectionManager.createConnection(config);
    HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_REPORT_TEXT));

    // parameters
    String reportTemplateName = parameters.get("reportTemplate");
    Date from = parameters.get("from") == null ? null : parseDate(parameters.get("from")).toDate();
    Date to = parameters.get("to") == null ? null : parseDate(parameters.get("to")).toDate();

    // logic
    ReportTemplateRowMapper rowMapper = new ReportTemplateRowMapper(table, constants);
    ReportTemplateRepository reportTemplateRepository = new ReportTemplateRepository(table, constants, rowMapper);
    ReportTemplate reportTemplate = reportTemplateRepository.findByExactName(reportTemplateName);
    ReportGenerator reportGenerator = new ReportGenerator(reportTemplate, from, to);
    ReportService reportService = new ReportService();
    List<Report> reports = reportService.buildReports(hbase, reportGenerator, constants, config);
    reportGenerator.setReports(reports);

    // output
    return gson.toJson(reportGenerator);
  }

  public String uploadCsv(Map<String, String> parameters) throws IOException {
    try {
      String filename = parameters.get("filename");
      new ImportMessagesFromCsv(hbase,
          new BufferedReader(new FileReader(new File(filename))), filename).read();
      return "success";
    } catch (Exception e) {
      e.printStackTrace();
      return e.getMessage();
    }
  }

  private String downloadCsv(Map<String, String> parameters) throws Exception {
//    String filename = String.format("%d-%s", new DateTime().getMillis(), parameters.get("filename"));
    String filename = parameters.get("filename");
    String extractString = parameters.containsKey("extract-string") ?
        parameters.get("extract-string") : "c";
    String topicsFilter = parameters.get("topics");

    long from = parameters.containsKey("from") ? parseDate(parameters.get("from")).getMillis() : 0L;
    long to = parameters.containsKey("to") ? parseDate(parameters.get("to")).getMillis() : 1577808000000L;
    long limit = parameters.containsKey("limit") ? Long.parseLong(parameters.get("limit")) : 999999L;

    long linesCount = new ExportMessagesFromHbase(
        filename,
        extractString,
        constants,
        hbase,
        hbase.getTopicsList(),
        topicsFilter
    ).extract(
        from,
        to
    );

    if (linesCount == 0) {
      return "No data found for filter.";
    }

    ProcessBuilder ps = new ProcessBuilder();
    ps.redirectErrorStream(true);

    BashCsvCommandsGenerator generator = new BashCsvCommandsGenerator(linesCount, limit);

    String commands = StringUtils.join(new String[]{
            "cd exported_csv",
            String.format("hdfs dfs -cat exported_csv/%s/*/* > %s", filename, filename),
            generator.generateFileSplitterCommand(filename),
            generator.generateFilesWithHeadersCommand(filename),
            String.format("tar -zcvf %s.tar.gz %s-*.csv", filename, filename),
            String.format("scp %s.tar.gz %s", filename, constants.MLUI_EXPORTED_CSV_DIRECTORY),
        },
        " ; "
    );

    System.out.println(commands);

    ps.command("bash", "-c", commands);

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(ps.start().getInputStream()))) {
      for (String line = reader.readLine(); line != null; line = reader.readLine()) {
        System.out.format("===> %s\n", line);
      }
    }

    return String.format("exported_csv/%s.tar.gz", filename);
  }


  private static DateTime parseDate(String date) {
    return date == null ? null : DateTime.parse(date, DateTimeFormat.forPattern("M/d/y h:m a"));
  }

  private static boolean parseBoolean(String bool) {
    return bool != null && Boolean.parseBoolean(bool);
  }

  private static List<String> parseComma(String value) {
    if (value == null) {
      return null;
    } else {
      return Arrays.asList(value.split(","));
    }
  }

  private static Map<String, String> transform(String[] arguments) {
    Map<String, String> parameters = new HashMap<>();
    for (String argument : arguments) {
      String[] tmp = argument.split("@");
      if (tmp.length == 2) {
        String key = tmp[0];
        String value = new String(Base64.decodeBase64(tmp[1].getBytes()));
        parameters.put(key, value);
      }
    }
    System.out.println(parameters);
    return parameters;
  }

  private static void output(String data) {
    System.out.println("UIAjaxHandler ===== start");
    System.out.println(data);
    System.out.println("UIAjaxHandler ===== end");
  }

}

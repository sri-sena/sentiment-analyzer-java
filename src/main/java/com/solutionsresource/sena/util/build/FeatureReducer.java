package com.solutionsresource.sena.util.build;

import com.solutionsresource.sena.entity.Feature;

import java.util.ArrayList;
import java.util.List;

public class FeatureReducer {
    public List<Feature> reduce(List<Feature> features, Integer[] featureIndexes) {
        List<Feature> cleanedFeatures = new ArrayList<Feature>();

        for (int index : featureIndexes) {
            cleanedFeatures.add(features.get(index));
        }

        return cleanedFeatures;
    }
}

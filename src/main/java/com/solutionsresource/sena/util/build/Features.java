package com.solutionsresource.sena.util.build;

import com.solutionsresource.sena.entity.Feature;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featureselection.MutualInformation;
import com.solutionsresource.sena.function.featureselection.GetMutualInformationWithBias;
import com.solutionsresource.sena.function.featuretransformation.ConvertToFeature;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.*;

public class Features {
    public static List<String> getValues(List<Feature> selectedFeatures) {
        List<String> featureNames = new ArrayList<String>();
        for (Feature feature : selectedFeatures) {
            featureNames.add(feature.getValue());
        }
        return featureNames;
    }

    public static int indexOfValue(List<Feature> features, String value) {
        for (int i = 0; i < features.size(); i++) {
            if (features.get(i).getValue().equalsIgnoreCase(value)) {
                return i;
            }
        }
        return -1;
    }

    public static boolean hasValue(List<Feature> features, String value) {
        return indexOfValue(features, value) != -1;
    }

    public static MutualInformation mutualInformation(String name, UsageTable ngram,
                                                      HashMap<String, Integer> classCounts, int featuresCount) {
        return mutualInformation(name, ngram, classCounts, featuresCount, false);
    }
    public static MutualInformation mutualInformation(String name, UsageTable ngram,
                                                      HashMap<String, Integer> classCounts, int featuresCount,
                                                      boolean considerOnlyUsedTrue) {

        HashMap<String, Integer> ngramCount = new HashMap<String, Integer>();

        int ngramUsages = 0;

        for (String key : ngram.keySet()) {
            ngramUsages += ngram.get(key);
        }

        ngramCount.put("used", ngramUsages);
        ngramCount.put("not used", featuresCount - ngramUsages);

        return new MutualInformation(name, ngram, ngramCount, classCounts, featuresCount, considerOnlyUsedTrue);
    }

    public static List<Feature> getKeys(JavaSparkContext sc, HashMap<String, UsageTable> allNGrams,
                                        long trainingCount, UsageTable classesCount) {
        return getKeys(sc, allNGrams, trainingCount, classesCount, null, 6000);
    }

    public static List<Feature> getKeys(JavaSparkContext sc, HashMap<String, UsageTable> allNGrams,
                                        long trainingCount, UsageTable classesCount, String className,
                                        int keysCount) {
        List<Tuple2<String, UsageTable>> ngramsTuples = new ArrayList<Tuple2<String, UsageTable>>();

        for (String ngram : allNGrams.keySet()) {
            ngramsTuples.add(new Tuple2<>(ngram.toLowerCase(), allNGrams.get(ngram)));
        }

        JavaRDD<Feature> featuresRDD = sc.parallelizePairs(ngramsTuples, 100)
                .map(new ConvertToFeature(classesCount, trainingCount));

        return new ArrayList<>(featuresRDD.sortBy(new GetMutualInformationWithBias(className), false, 100)
                .take(keysCount));
    }

}

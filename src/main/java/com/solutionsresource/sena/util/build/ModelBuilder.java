package com.solutionsresource.sena.util.build;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Feature;
import com.solutionsresource.sena.entity.WeightedLabeledPoint;
import com.solutionsresource.sena.function.featurescaling.ConvertToLabeledPoints;
import com.solutionsresource.sena.function.featureselection.FilterZero;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.classification.NaiveBayes;
import org.apache.spark.mllib.classification.NaiveBayesModel;
import org.apache.spark.mllib.regression.LabeledPoint;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class ModelBuilder {
    private final JavaSparkContext sc;
    private final boolean shouldSave;
    private boolean shouldOversample;

    public ModelBuilder(JavaSparkContext sc, boolean shouldSave, boolean shouldOversample) {
        this.sc = sc;
        this.shouldSave = shouldSave;
        this.shouldOversample = shouldOversample;
    }

    public Tuple2<NaiveBayesModel, List<Feature>> build(JavaRDD<WeightedLabeledPoint> weightedTraining,
                                                        List<Feature> features, String folderName, String[] labels) {
        /*
        JavaRDD<WeightedLabeledPoint> undersampledTraining = null;

        if (this.shouldUndersample) {
            CountTable trainingCountTable = weightedTraining
                    .map(new GetLabel())
                    .map(new ConvertToCountTable())
                    .reduce(new ReduceCountTables());

            int leastLabelCount = trainingCountTable.getLeastValue();

            JavaRDD[] undersampledDatasets = new JavaRDD[trainingCountTable.size()];

            int i = 0;
            for (double label : trainingCountTable.keySet()) {
                JavaRDD<WeightedLabeledPoint> filteredTrainingByLabel = weightedTraining
                        .filter(new FilterByLabel(label));

                FloatCountTable weightCounts = filteredTrainingByLabel
                        .map(new GetWeight())
                        .map(new ConvertToFloatCountTable())
                        .reduce(new ReduceFloatCountTables());

                int index = leastLabelCount;
                float weightThreshold = weightCounts.getFloatKeyAt(index);

                JavaRDD<WeightedLabeledPoint> undersampledTrainingByLabel = filteredTrainingByLabel
                        .filter(new FilterByWeightThreshold(weightThreshold));

                undersampledDatasets[i++] = undersampledTrainingByLabel;
            }

            undersampledTraining = sc.union(undersampledDatasets);
        }

        if (undersampledTraining == null || undersampledTraining.count() == 0) {
            undersampledTraining = weightedTraining;
        }

        IntegerCountTable countTable = undersampledTraining.map(new GetFeatureIndexCountTable())
                .reduce(new ReduceIntegerCountTable());

        Integer[] featureIndexes = countTable.getIntKeys();

        undersampledTraining = undersampledTraining.map(new ReduceFeatures(featureIndexes));

        features = new FeatureReducer().reduce(features, featureIndexes);

        JavaRDD<LabeledPoint> training = undersampledTraining.map(new ConvertToLabeledPoints());

        UsageTable[] usageTables = training.map(new ConvertToUsageTables(labels)).reduce(new ReduceUsageTables());

        int featuresLength = Math.min(usageTables.length, features.size());

        for (int i = 0; i < featuresLength; i++) {
            features.get(i).setUsages(usageTables[i]);
        }
        */

        JavaRDD<LabeledPoint> training = weightedTraining.map(new ConvertToLabeledPoints());

        if (this.shouldOversample) {
            JavaRDD<LabeledPoint> oversamples = training.filter(new FilterZero());

            for (int i = 0; i < Constants.FALSE_DOMAIN_BIAS_MULTIPLIER; i++) {
                training = training.union(oversamples);
            }
        }

        NaiveBayesModel model = NaiveBayes.train(training.rdd());

        if (this.shouldSave) {
            convertToRdd(model.theta()).saveAsTextFile(folderName + "training-data.txt");

            model.save(sc.sc(), folderName + "model");

            sc.parallelize(features, 5).saveAsTextFile(folderName + "features.txt");
        }

        return new Tuple2<NaiveBayesModel, List<Feature>>(model, features);
    }

    private JavaRDD<List<Double>> convertToRdd(double[][] theta) {
        List<List<Double>> values = new ArrayList<List<Double>>();

        for (double[] row : theta) {
            ArrayList<Double> doubles = new ArrayList<Double>();

            for (Double d : row) {
                doubles.add(d);
            }

            values.add(doubles);
        }

        return sc.parallelize(values, 5);
    }
}

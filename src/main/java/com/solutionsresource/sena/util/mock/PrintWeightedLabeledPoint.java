package com.solutionsresource.sena.util.mock;

import org.apache.spark.api.java.function.VoidFunction;

import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class PrintWeightedLabeledPoint implements VoidFunction<WeightedLabeledPoint> {
    public void call(WeightedLabeledPoint labeledPoint) throws Exception {
        System.out.println(labeledPoint.weight());
    }
}

package com.solutionsresource.sena.util.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.mllib.regression.LabeledPoint;

import com.solutionsresource.sena.entity.Message;

public class SparkContextMock {

    public List<Message> map(List<Message> objects, Function<Message, Message> function) {
        ArrayList<Message> list = new ArrayList<Message>();

        for (Message object : objects) {
            try {
                list.add(function.call(object));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return list;
    }

    public List<HashMap<String, HashMap<String, Integer>>> map2(List<Message> messages, Function<Message, HashMap<String, HashMap<String, Integer>>> function) throws Exception {
        List<HashMap<String, HashMap<String, Integer>>> list = new ArrayList<HashMap<String, HashMap<String, Integer>>>();

        for (Message msg : messages) {
            list.add(function.call(msg));
        }

        return list;
    }


    public HashMap<String, HashMap<String, Integer>>
    reduce(List<HashMap<String, HashMap<String, Integer>>> objects, Function2<HashMap<String, HashMap<String, Integer>>,
            HashMap<String, HashMap<String, Integer>>,
            HashMap<String, HashMap<String, Integer>>> function) throws Exception {

        HashMap<String, HashMap<String, Integer>> hashtable = new HashMap<String, HashMap<String, Integer>>();

        for (HashMap<String, HashMap<String, Integer>> object : objects) {
            hashtable = function.call(hashtable, object);
        }

        return hashtable;
    }

    public List<LabeledPoint> map3(List<Message> messages, Function<Message, LabeledPoint> function) throws Exception {
        List<LabeledPoint> list = new ArrayList<LabeledPoint>();

        for (Message msg : messages) {
            list.add(function.call(msg));
        }

        return list;
    }

}

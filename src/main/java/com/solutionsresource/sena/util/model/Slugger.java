package com.solutionsresource.sena.util.model;

public class Slugger {
    public static String slug(String text) {
        return text.toLowerCase().replaceAll("[^#a-z0-9]", "-");
    }
}

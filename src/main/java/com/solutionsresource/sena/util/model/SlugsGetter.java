package com.solutionsresource.sena.util.model;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.function.featurescaling.GetUniqueSlugs;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;

public class SlugsGetter {
    private Function<Message, DomainSlugs> getKey;

    public SlugsGetter(Function<Message, DomainSlugs> getKey) {
        this.getKey = getKey;
    }

    public DomainSlugs get(JavaRDD<Message> dataset) {
        return dataset.map(getKey).reduce(new GetUniqueSlugs());
    }

    public DomainSlugs get(Message message) throws Exception {
        return getKey.call(message);
    }
}

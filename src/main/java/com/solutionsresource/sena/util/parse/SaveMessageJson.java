package com.solutionsresource.sena.util.parse;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.gnip.Tweet;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.main.StepByStepSentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.VoidFunction;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SaveMessageJson implements VoidFunction<String> {
    private StepByStepSentimentAnalyzer analyzer;
    public DateFormat df;
    public DateFormat timezoneDf;

    Constants constants;
    private HBaseDao hbase;
    private final ContentCleaner contentCleaner;
    private String folderName;

    public SaveMessageJson(StepByStepSentimentAnalyzer analyzer) throws Exception {
        this(analyzer, null);
    }

    public SaveMessageJson(Constants constants, StepByStepSentimentAnalyzer analyzer) throws Exception {
        this(constants, analyzer, null);
    }

    public SaveMessageJson(StepByStepSentimentAnalyzer analyzer, HBaseDao hbase) throws Exception {
        this(new Constants(), analyzer, hbase);
    }

    public SaveMessageJson(Constants constants, StepByStepSentimentAnalyzer analyzer, HBaseDao hbase) {
        this.constants = constants;

        contentCleaner = new ContentCleaner(new RemoveLinks(constants),
                new RemoveStopWords(), new RemoveHandlers(constants), new DatesNormalizer(), new EmojiNormalizer());

        this.analyzer = analyzer;
        this.hbase = hbase;

        this.df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        this.timezoneDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    }

    public void call(String line) throws Exception {
        boolean hasInitialHbase = true;
        if (hbase == null) {
            Configuration config = new HBaseConnector(constants).getConfiguration();
            this.hbase = new HBaseDao(config, constants);
            hasInitialHbase = false;
        }

        Gson gson = new Gson();
        Tweet tweet;
        try {
            tweet = gson.fromJson(line, Tweet.class);
        } catch (Exception e) {
            return;
        }


        if (tweet == null || tweet.id == null || tweet.id.length() == 0) {
            return;
        }

        String[] idSplit = tweet.id.split(":");

        tweet.id = String.format("twitter-%s", idSplit[idSplit.length - 1]);

        Date postedDateTime = timezoneDf.parse(tweet.postedTime);
        postedDateTime = DateUtils.addHours(postedDateTime, 8);
        long timestamp = postedDateTime.getTime();

        Date userCreatedTime = timezoneDf.parse(tweet.actor.postedTime);
        userCreatedTime = DateUtils.addHours(userCreatedTime, 8);
        long userTimestamp = userCreatedTime.getTime();

        System.out.println(String.format("Processing %s...", tweet.id));

        byte[] domainsBytes = hbase.getMessageDomains(contentCleaner.clean(tweet.body));
        boolean isFinalized = true;

        if (domainsBytes == null) {
            List<Domain> domains = analyzer.analyze(tweet.body);
            for (Domain domain : domains) {
                for (Topic topic : domain.getTopics()) {
                    if (topic.getName() != null && !topic.getName().isEmpty()) {
                        topic.setName(topic.getName().replaceAll("-", " "));
                    }
                }
            }
            domainsBytes = Bytes.toBytes(gson.toJson(domains));
            isFinalized = false;
        }

        hbase.createMessageFromTweet(tweet, folderName, timestamp, userTimestamp, df, domainsBytes, isFinalized);

        if (!hasInitialHbase) {
            hbase.close();
            hbase = null;
        }
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderName() {
        return folderName;
    }
}


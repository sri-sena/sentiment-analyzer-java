package com.solutionsresource.sena.util.parse;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.gnip.Tweet;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.HBaseDao;

import java.text.DateFormat;

public class SavePacquiaoMessageJsonPartition extends SaveMessageJsonPartition {
    public SavePacquiaoMessageJsonPartition(SentimentAnalyzer analyzer) throws Exception {
        super(analyzer);
    }

    public SavePacquiaoMessageJsonPartition(Constants constants, SentimentAnalyzer analyzer) throws Exception {
        super(constants, analyzer);
    }

    public SavePacquiaoMessageJsonPartition(SentimentAnalyzer analyzer, HBaseDao hbase) throws Exception {
        super(analyzer, hbase);
    }

    public SavePacquiaoMessageJsonPartition(Constants constants, SentimentAnalyzer analyzer, HBaseDao hbase) {
        super(constants, analyzer, hbase);
    }

    public void createMessageFromTweet(HBaseDao hbase, String folderName, Tweet tweet, long timestamp,
                                       long userTimestamp, DateFormat df, byte[] domainsBytes, boolean isFinalized) {
        hbase.createMessageFromPacquiaoTweet(tweet, folderName, timestamp,
                userTimestamp, df, domainsBytes, isFinalized);
    }
}

package com.solutionsresource.sena.util.parse;

import java.util.ArrayList;
import java.util.List;

public class HashtagParser {

    private final LexicalState lexicalAnalyzer;

    public HashtagParser(String[] lexicons) {
        this.lexicalAnalyzer = new LexicalState(lexicons);
    }

    public List<String> parse(String ngram) {
        ngram = ngram.toLowerCase();
        List<String> ngrams = new ArrayList<String>();

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < ngram.length(); i++) {
            LexicalState state = lexicalAnalyzer.analyze(i, ngram);

            if (state != null && state.isAccepting()) {
                if (builder.length() > 0) {
                    ngrams.add(builder.toString());
                    builder.delete(0, builder.length());
                }
                ngrams.add(state.getValue());
                i += state.getDepth() - 1;
            } else {
                builder.append(ngram.charAt(i));
            }
        }
        if (builder.length() > 0) {
            ngrams.add(builder.toString());
            builder.delete(0, builder.length());
        }
        return ngrams;
    }
}

package com.solutionsresource.sena.util.parse;

import java.util.HashMap;

public class LexicalState extends HashMap<Character, LexicalState> {

    private final int depth;
    private boolean isAccepting;
    private String value;

    public LexicalState(String[] lexicons) {
        this(0, lexicons);
    }

    public LexicalState(int depth, String[] lexicons) {
        this.depth = depth;
        initializeTransitions(depth, lexicons);
    }

    private void initializeTransitions(int depth, String[] lexicons) {
        HashMap<Character, Integer> nextChars = new HashMap<Character, Integer>();

        for (String lexicon : lexicons) {
            if (depth == lexicon.length()) {
                this.isAccepting = true;
                this.value = lexicon;
            } else {
                char c = lexicon.charAt(depth);
                if (!nextChars.containsKey(c)) {
                    nextChars.put(c, 0);
                }
                nextChars.put(c, nextChars.get(c) + 1);
            }
        }

        for (char c : nextChars.keySet()) {
            String[] lexiconsForC = new String[nextChars.get(c)];

            int j = 0;
            for (int i = 0; i < lexiconsForC.length; i++) {
                for (; j < lexicons.length; j++) {
                    if (depth < lexicons[j].length() && lexicons[j].charAt(depth) == c) {
                        lexiconsForC[i] = lexicons[j++];
                        break;
                    }
                }
            }

            this.put(c, new LexicalState(depth + 1, lexiconsForC));
        }
    }

    public boolean isAccepting() {
        return isAccepting;
    }

    public String getValue() {
        return value;
    }

    public LexicalState analyze(String ngram) {
        return analyze(0, ngram);
    }

    public LexicalState analyze(int depth, String ngram) {
        if (ngram.length() == depth) {
            return this;
        }

        char c = ngram.charAt(depth);

        if (!this.containsKey(c)) {
            return this.isAccepting ? this : null;
        }

        return this.get(c).analyze(depth + 1, ngram);
    }

    public LexicalState get(String string) {
        LexicalState result = this;

        for (int i = 0; i < string.length() - 1; i++) {
            char c = string.charAt(i);
            if (!result.containsKey(c)) {
                return null;
            }
            result = result.get(c);
        }

        return result.get(string.charAt(string.length() - 1));
    }

    public boolean containsKey(String string) {
        return get(string) != null;
    }

    public int getDepth() {
        return depth;
    }
}

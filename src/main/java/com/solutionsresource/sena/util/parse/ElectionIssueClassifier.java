package com.solutionsresource.sena.util.parse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class ElectionIssueClassifier implements Serializable {
    private ArrayList<String> results;
    private HashMap<String, ArrayList<String>> topicRegexes;
    public ElectionIssueClassifier(){
        results = new ArrayList<>();
        topicRegexes = new HashMap<>();

        ArrayList<String> presintoIssue = new ArrayList<>();
        presintoIssue.add("(vcm|vote count(ing)? machine)");
        presintoIssue.add("(re(ceipt|sibo))");
        presintoIssue.add("voter'?s receipt");
        presintoIssue.add("(ball?ota?)");
        /**
         * ballota
         * ballot
         * balota
         */
        presintoIssue.add("(pcos)");
        presintoIssue.add("(indelible ink)");
        presintoIssue.add("(list( of voter)?)");
        presintoIssue.add("(bei)");
        presintoIssue.add("(public school)");
        presintoIssue.add("(board of election)");
        presintoIssue.add("(poll(s)?( watchers)?)");
        presintoIssue.add("(exit polls)");
        presintoIssue.add("(precint)");
        presintoIssue.add("(presinto)");
        presintoIssue.add("(paper jam)");
        presintoIssue.add("(s(um)?i(si)?ngit)");
        presintoIssue.add("(ppcrv)");

        ArrayList<String> dayaanIssue = new ArrayList<>();
        dayaanIssue.add("(d(in)?a(ra)?ya)");
        /**
         * daya
         * dinaya
         * mandaraya
         * dinaraya
         * pandaraya
         */
        dayaanIssue.add("(vote[ -]buy(ers|ing)?)");
        dayaanIssue.add("(vote[ -]sell(ers|ing)?)");
        dayaanIssue.add("([sn](in)?uh[uo]l)");
        dayaanIssue.add("((benta|b(um)?ili) ng boto)");
        dayaanIssue.add("((sd|cf) card)");

        ArrayList<String> canvassingIssue = new ArrayList<>();
        canvassingIssue.add("(canvass(ing|ers)?)");
        canvassingIssue.add("(transmission of results)");
        canvassingIssue.add("(vote count)");
        canvassingIssue.add("(counting of vote)");
        canvassingIssue.add("(initial vote count)");

        ArrayList<String> others = new ArrayList<>();
        others.add("(hi(ni)?matay)");
        others.add("((ang |ma|paka)init($|[^i]))");
        others.add("(gun ban)");
        others.add("(siksikan)");
        others.add("(p(um)?i(pi)?la)");
        others.add("(hilo)");

        topicRegexes.put("presinto-issue", presintoIssue);
        topicRegexes.put("dayaan-issue", dayaanIssue);
        topicRegexes.put("canvassing-issue", canvassingIssue);
        topicRegexes.put("others", others);
    }

    public ArrayList<String> classify(String message){

        message = message.toLowerCase();
        for (Entry<String, ArrayList<String>> topicRegex : topicRegexes.entrySet()) {
            boolean topicIsAccepted = false;
            for(int index = 0; index < topicRegex.getValue().size(); index++){
                boolean regexIsOkay = message.matches(String.format("(?i:.*(%s).*)",
                        topicRegex.getValue().get(index)));
                if (regexIsOkay) {
                    topicIsAccepted = true;
                    break;
                }
            }
            if (topicIsAccepted) {
                results.add(topicRegex.getKey());
            }
        }

        return results;
    }

    public static void main(String[] args){
        ElectionIssueClassifier eic = new ElectionIssueClassifier();

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("> ");
            System.out.println(eic.classify(sc.nextLine()));
        }
    }
}

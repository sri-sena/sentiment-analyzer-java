package com.solutionsresource.sena.util.parse;

import com.solutionsresource.sena.constant.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordStemmer {

    private Pattern[] patterns;
    Constants constants;

    public WordStemmer(Constants constants) {
        this.constants = constants;
        String[] regexes = constants.STEMMER_REGEXES;

        Pattern[] patterns = new Pattern[regexes.length];
        for (int i = 0; i < regexes.length; i++) {
            patterns[i] = Pattern.compile(regexes[i]);
        }
        this.patterns = patterns;
    }

    public String stem(String ngram) {
        if (isExempted(ngram)) {
            return ngram;
        }

        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(ngram);

            if (matcher.find()) {
                StringBuilder rootword = new StringBuilder();

                for (int i = 1; i < matcher.groupCount()+1; i++) {
                    rootword.append(matcher.group(i));
                }

                return rootword.toString();
            }
        }

        return ngram;
    }

    private boolean isExempted(String ngram) {
        for (String exemption : constants.STEMMER_EXEMPTIONS) {
            if (ngram.equals(exemption)) {
                return true;
            }
        }
        return false;
    }
}

/**
 * 
 */
package com.solutionsresource.sena.util;

import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;

/**
 * @author dzung
 *
 */
public class MessageRowMapper {
  
  public Message mapRow(Result result, Constants constants) {

    Message message = new Message();
    message.setUuid(Bytes.toString(result.getRow()));
    message.setContent(Bytes.toString(result.getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE)));
//    message.setSource(Bytes.toString(result.getValue(Constants.TABLE_MESSAGE_MSGDATA_BYTE, Constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE)));
//    message.setUsername(Bytes.toString(result.getValue(Constants.TABLE_MESSAGE_MSGDATA_BYTE, Constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE)));
    byte[] finalizeBytes = result.getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE);
    if (finalizeBytes != null) {
        message.setFinalize(BooleanUtils.toBooleanObject(Bytes.toBoolean(finalizeBytes)));
    }
    
    byte[] postedDate = result.getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE);
    if(postedDate != null) {
      message.setPostedDate(Bytes.toString(postedDate));
    }

    String domainsJson = Bytes.toString(result.getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE));
    if (StringUtils.isNotBlank(domainsJson)) {
      List<Domain> domains = new Gson().fromJson(domainsJson, Domain.TYPE);
      message.setDomains(domains);
    }

    return message;
  }

}
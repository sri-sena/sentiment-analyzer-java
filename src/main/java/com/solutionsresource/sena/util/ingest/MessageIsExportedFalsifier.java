package com.solutionsresource.sena.util.ingest;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.field.FalsifyMessageIsExported;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class MessageIsExportedFalsifier {
    static Constants constants;

    public static void main(String[] args) throws Exception {
        constants = new Constants();
        SparkConf conf = new SparkConf().setAppName("SENA");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_MESSAGE_TEXT);

        JavaPairRDD<ImmutableBytesWritable, Result> messageRDD = sc.newAPIHadoopRDD(config,
                TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

        messageRDD.map(new FalsifyMessageIsExported()).sample(true, 0.1f).collect();
    }
}

package com.solutionsresource.sena.util.ingest;

import com.solutionsresource.sena.entity.Metric;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RandomMetricsGenerator {

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);

        List<String> outputList = new ArrayList<String>();

        Metric metric = Metric.mock();
        hbase.insertMetric(metric);

        outputList.add(Arrays.deepToString(metric.getConfusionMatrix()));
        outputList.add("Precision: " + metric.getPrecision() + "%");
        outputList.add("Recall:    " + metric.getRecall() + "%");
        outputList.add("F1 score:  " + metric.getF1Score() + "%");
        outputList.add("Accuracy:  " + metric.getAccuracy() + "%");

        for (String output : outputList) {
            System.out.println(output);
        }
    }
}

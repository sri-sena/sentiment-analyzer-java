package com.solutionsresource.sena.util.ingest;

import com.fasterxml.uuid.Generators;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.InputStream;
import java.util.*;

public class WordBankExcelLoader {
    static Constants constants;

    public static void main(String[] args) throws Exception {
        String filename = args[0];
        constants = new Constants();
        try {
            Path pt = new Path(constants.EXCEL_WORK_BANK_PATH);
            // FileInputStream file = new FileInputStream(new File(".xlsx"));

            // file = new FileInputStream(new File("/home/ec2-user/Work_Bank_1.0.xlsx"));
            FileSystem fs = FileSystem.get(new Configuration());
            FSDataInputStream fis = fs.open(pt);

            InputStream is = fis;
            XSSFWorkbook workbook = new XSSFWorkbook(is);

            String wordbank = constants.WORDBANK_SOURCE;

            /** HBase Jobs **/
            HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
            HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
            HTableInterface topicTable = connection.getTable(TableName.valueOf(constants.TABLE_TOPIC_TEXT));
            /** HBase Jobs **/

            int sheetNo = workbook.getNumberOfSheets();
            int index = 0;

            GregorianCalendar cal = new GregorianCalendar();
            cal.set(2015, 11, 31, 0, 1, 0);
            long timestamp = cal.getTime().getTime();

            for (int sheetIndex = 0; sheetIndex < sheetNo; sheetIndex++) {
                // Get first sheet from the workbook
                XSSFSheet sheet = workbook.getSheetAt(sheetIndex);

                // Get iterator to all the rows in current sheet
                Iterator<Row> rowIterator = sheet.iterator();
                if (rowIterator.hasNext())
                    rowIterator.next();// because in excel file, the first row is label row.
                rowloop:
                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    // Get iterator to all cells of current row
                    /** UUID Configuration **/

                    String refId = String.format("%s-%s-%s", wordbank, filename, index);
                    String uuid = String.format("%s-%s", timestamp, refId);

                    Put p = new Put(Bytes.toBytes(uuid));

                    Iterator<Cell> cellIterator = row.cellIterator();
                    // System.out.println("========> Row no: " + row.getRowNum());
                    Domain domain = new Domain();

                    List<String> topics = new ArrayList<String>();

                    String sentimentCellValue = "";
                    List<String> sentiments = new ArrayList<String>();
                    List<Topic> topicList = new ArrayList<Topic>();
                    Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();
                        // System.out.println("cell no " + cell.getColumnIndex() + " : " + cell.getStringCellValue());
                        p.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_REF_ID_BYTE,
                                timestamp, Bytes.toBytes(refId));

                        p.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, timestamp, Bytes.toBytes(wordbank));
                        p.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, timestamp, Bytes.toBytes(true));
                        if (cell.getColumnIndex() == 0) {
                            String message = cell.getStringCellValue();
                            if (StringUtils.isBlank(message)) {
                                break rowloop;
                            } else {
                                p.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE, timestamp, Bytes.toBytes(message));
                            }
                        }
                        if (cell.getColumnIndex() == 1) {
                            String topicCellValue = cell.getStringCellValue();
                            if (StringUtils.isBlank(topicCellValue)) {
                                break rowloop;
                            } else {
                                if (topicCellValue.contains(";")) {
                                    topics = Arrays.asList(topicCellValue.split(";"));
                                } else {
                                    topics.add(topicCellValue);// only one Topic
                                }
                            }
                        }

                        if (cell.getColumnIndex() == 2) {
                            sentimentCellValue = cell.getStringCellValue();
                            if (sentimentCellValue.contains(";")) {
                                sentiments = Arrays.asList(sentimentCellValue.split(";"));
                            } else {
                                sentiments.add(sentimentCellValue);
                            }
                        }
                    }

                    boolean unrelated = false;
                    // check if topic and sentiment size isn't same then it is wrong format message, then we skip processing
                    if (sentiments.size() == topics.size() && topics.size() > 0) {
                        for (int i = 0; i < sentiments.size(); i++) {
                            Sentiment sentiment = new Sentiment();
                            sentiment.setName(StringUtils.trim(sentiments.get(i).toLowerCase()));
//                            sentiment.setDescription(StringUtils.trim(sentiments.get(i)));
                            Topic topic = new Topic();
//                            topic.setDescription(StringUtils.trim(topics.get(i)));
                            topic.setName(StringUtils.trim(topics.get(i).toLowerCase()));

                            unrelated = constants.TOPIC_UNRELATED.equalsIgnoreCase(topics.get(i));
                            if (unrelated) break;

                            topic.setSentiment(sentiment);
                            topicList.add(topic);
                            domain.setName(constants.DOMAIN_ELECTION);
//                            domain.setDescription("Election");
                            domain.setTopics(topicList);

                            Scan scan = new Scan();
                            scan.setFilter(new SingleColumnValueFilter(constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_NAME_BYTE,
                                    CompareFilter.CompareOp.GREATER_OR_EQUAL, new SubstringComparator(topic.getName())));
                            if (topicTable.getScanner(scan).next() == null) {
                                String topicUUID = new Date().getTime() + "-" + Generators.timeBasedGenerator().generate();
                                Put putTopic = new Put(Bytes.toBytes(topicUUID));
                                putTopic.add(constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_NAME_BYTE, timestamp, Bytes.toBytes(topic.getName()));
//                                putTopic.addColumn(Bytes.toBytes("topic_data"), Bytes.toBytes("description"), Bytes.toBytes(topic.getName()));
                                topicTable.put(putTopic);
                            }
                        }
                    } else {
                        continue;// skip the current message because don't have topic/sentiment or format not correct (topic no must
                        // be equal with sentiment no)
                    }
                    List<Domain> domains = new ArrayList<Domain>();
                    if (!unrelated) domains.add(domain);
                    p.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, timestamp, Bytes.toBytes(gson.toJson(domains)));
                    messageTable.put(p);

                    index += 1;
                    if (index > 4) {
                        // break;
                    }
                }
            }

            workbook.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // if (file != null) file.close();
        }
    }
}

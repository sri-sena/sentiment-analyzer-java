package com.solutionsresource.sena.util.ingest;

import com.fasterxml.uuid.Generators;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.function.featuretransformation.GetDomains;
import com.solutionsresource.sena.main.StepByStepSentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class FbCommentsExcelLoader {
    private final FileSystem fs;
    private final Path fbCommentsPath;
    private final StepByStepSentimentAnalyzer analyzer;
    static Constants constants;

    public FbCommentsExcelLoader(FileSystem fs, Path fbCommentsPath) throws Exception {
        this.fs = fs;
        this.fbCommentsPath = fbCommentsPath;
        constants = new Constants();

        SparkConf conf = new SparkConf().setAppName("SENA");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

        System.out.println(domainSlugs);

        analyzer = new StepByStepSentimentAnalyzer(sc, domainSlugs, constants, true);
    }

    private void loadAll() throws IOException {
        List<Path> excelFiles = getAllFilePath(fbCommentsPath, fs);

        try {
            /** HBase Configuration **/
            Configuration config = HBaseConfiguration.create();
//                config.set("hbase.zookeeper.quorum", constants.INTERNAL_ADDRESS);
//                config.set("hbase.zookeeper.property.clientPort", constants.HBASE_CLIENT_PORT);
//                config.set("zookeeper.znode.parent", constants.ZOOKEEPER_NODE_PARENT);
            /** HBase Configuration **/

            for (Path excelFile : excelFiles) {
                load(config, excelFile);
            }

            fs.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // if (file != null) file.close();
        }

    }

    /**
     * @param filePath
     * @param fs
     * @return list of absolute file path present in given path
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static List<Path> getAllFilePath(Path filePath, FileSystem fs) throws IOException {
        List<Path> fileList = new ArrayList<Path>();
        FileStatus[] fileStatus = fs.listStatus(filePath);
        for (FileStatus fileStat : fileStatus) {
            if (fileStat.isDirectory()) {
                fileList.addAll(getAllFilePath(fileStat.getPath(), fs));
            } else if (fileStat.getPath().getName().endsWith(".xlsx")) {
                fileList.add(fileStat.getPath());
            }
        }
        return fileList;
    }

    private void load(Configuration config, Path excelFile)
            throws IOException {
        /** HBase Jobs **/
        HConnection connection = HConnectionManager.createConnection(config);
        HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));

        /** HBase Jobs **/

        InputStream is = fs.open(excelFile);

        XSSFWorkbook workbook = new XSSFWorkbook(is);

        // Get first sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        // Get iterator to all the rows in current sheet
        Iterator<Row> rowIterator = sheet.iterator();
        if (rowIterator.hasNext())
            rowIterator.next();// because in excel file, the first row is label row.

        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> columns = row.cellIterator();

            // Get iterator to all cells of current row*/
            /** UUID Configuration **/
            String uuid = new Date().getTime() + "-" + Generators.timeBasedGenerator().generate();
            Put put = new Put(Bytes.toBytes(uuid));

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

            Cell refIdCell = columns.next();
            Cell postedDatetimeCell = columns.next();
            Cell usernameCell = columns.next();
            Cell userIdCell = columns.next();
            Cell likesCountCell = columns.next();
            Cell contentCell = columns.next();

            String content = contentCell.getStringCellValue();
            List<Domain> domains = analyzer.analyze(content, false);

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_REF_ID_BYTE,
                    Bytes.toBytes(refIdCell.getStringCellValue()));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE,
                    Bytes.toBytes(postedDatetimeCell.getStringCellValue()));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_USER_ID_BYTE,
                    Bytes.toBytes(userIdCell.getStringCellValue()));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE,
                    Bytes.toBytes(usernameCell.getStringCellValue()));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE,
                    Bytes.toBytes(constants.SOURCE_FACEBOOK));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_LIKES_COUNT_BYTE,
                    Bytes.toBytes((int) likesCountCell.getNumericCellValue()));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE,
                    Bytes.toBytes(content));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE,
                    constants.FACEBOOK_BYTE);

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE,
                    Bytes.toBytes(false));

            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE,
                    Bytes.toBytes(gson.toJson(domains)));

            messageTable.put(put);
        }

        connection.close();

        is.close();

        workbook.close();
    }

    public static void main(String[] args) throws Exception {
        constants = new Constants();
        Path fbCommentsFolder = new Path(constants.FOLDER_FB_COMMENTS_PATH);

        FileSystem fs = FileSystem.get(new Configuration());


//        String[] excelFiles = new String[]{
//                "inquirer1.xlsx",
//                "rappler1.xlsx",
//        };

        new FbCommentsExcelLoader(fs, fbCommentsFolder).loadAll();
    }
}

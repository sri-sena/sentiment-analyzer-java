package com.solutionsresource.sena.util.ingest;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class TwitterFeedsUnfinisher {
    static Constants constants;

    public static void main(String[] args) throws IOException {
        String dateFilter = args.length > 1 ? args[1] : null;

        try {
            Configuration config = new HBaseConnector().getConfiguration();

            FileSystem fs = FileSystem.get(config);
            constants = new Constants();
            String twitterFeeds = args.length > 0 ? args[0] : constants.FOLDER_TW_COMMENTS_PATH;
            FileStatus[] dateFolders = fs.listStatus(new Path(twitterFeeds));
            for (FileStatus dateFolder : dateFolders) {
                Path dateFolderPath = dateFolder.getPath();
                if (dateFilter != null && !dateFilter.equals(dateFolderPath.getName())) {
                    continue;
                }
                FileStatus[] tweetFiles = fs.listStatus(new Path(twitterFeeds + "/" + dateFolderPath.getName()));
                for (FileStatus tweetFile : tweetFiles) {
                    Path tweetFilePath = tweetFile.getPath();
                    String filename = tweetFilePath.getName();
                    if (tweetFile.isFile() && filename.contains(constants.FINISHED_SUFFIX)
                            && !filename.contains(constants.COPYING_SUFFIX)) {
                        System.out.println(filename);
                        fs.rename(tweetFilePath, new Path(dateFolderPath.toString() + "/" +
                                filename.replace(constants.FINISHED_SUFFIX, "")));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

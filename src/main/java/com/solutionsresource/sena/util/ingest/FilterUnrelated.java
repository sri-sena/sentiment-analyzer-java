package com.solutionsresource.sena.util.ingest;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

import java.util.ArrayList;
import java.util.List;

public class FilterUnrelated implements Function<ResultHashMap, Boolean> {
  private Constants constants;
  private DomainSlugs domainSlugs;

  public FilterUnrelated(Constants constants, DomainSlugs domainSlugs) {
    this.constants = constants;
    this.domainSlugs = domainSlugs;
  }

  public Boolean call(ResultHashMap result) throws Exception {
    String domainsJson = Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT));

    List<Domain> domains = new ArrayList<>();
    if (StringUtils.isNotBlank(domainsJson)) {
      domains = new Gson().fromJson(domainsJson, Domain.TYPE);
    }

    for (Domain domain : domains) {
      String domainName = domain.getName();
      for (Topic topic : domain.getTopics()) {
        if (domainSlugs.accepts(domainName) && domainSlugs.accepts(domainName, topic.getName())) {
          return true;
        }
      }
    }

    return false;
  }
}

package com.solutionsresource.sena.util.csv;

public class BashCsvCommandsGenerator {
    private final long linesCount;
    private final long limit;
    private final long partsCount;

    public BashCsvCommandsGenerator(long linesCount, long limit) {
        this.linesCount = linesCount;
        this.limit = limit;
        this.partsCount = (linesCount + limit - 1) / limit;
    }

    public String generateFileSplitterCommand(String filename) {
        return String.format("split -l %d -d %s %s-", limit, filename, filename);
    }

    public String generateFilesWithHeadersCommand(String filename) {
        return String.format("for f in %s* ; do cat header.csv \"$f\" > \"$f.csv\" ; done", filename);
    }
}

package com.solutionsresource.sena.util.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Message;

public class UpdateDomains implements Function<Message, Message> {
    public Message call(Message message) throws Exception {
        Configuration config = new HBaseConnector().getConfiguration();
        HBaseDao hbase = new HBaseDao(config);

        hbase.updateMessageDomains(message.getUuid(), message.getPredictedDomains());
        return message;
    }
}

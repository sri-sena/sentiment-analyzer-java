package com.solutionsresource.sena.util.hbase;

import com.solutionsresource.sena.constant.Constants;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;

public class HBaseConnector {

    Constants constants;
    private Configuration configuration = HBaseConfiguration.create();

    public HBaseConnector() throws Exception {
        this(new Constants());
    }

    public HBaseConnector(Constants constants) {
        this.constants = constants;
        configuration.set("hbase.zookeeper.quorum", constants.HBASE_QUORUM);
        configuration.set("hbase.zookeeper.property.clientPort", constants.HBASE_CLIENT_PORT);
        configuration.set("zookeeper.znode.parent", constants.ZOOKEEPER_NODE_PARENT);
    }

    public Configuration getConfiguration() {
        return configuration;
    }

}


package com.solutionsresource.sena.util.hbase;

import com.fasterxml.uuid.Generators;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.gnip.Hashtag;
import com.solutionsresource.sena.entity.gnip.Tweet;
import com.solutionsresource.sena.entity.hashmap.*;
import com.solutionsresource.sena.entity.mapper.MessageMapper;
import com.solutionsresource.sena.entity.mapper.TopicMapper;
import com.solutionsresource.sena.exception.AlreadyBuildingException;
import com.solutionsresource.sena.exception.BuildingException;
import com.solutionsresource.sena.exception.CancelledBuildingException;
import com.solutionsresource.sena.function.featuretransformation.GetSecondElement;
import com.solutionsresource.sena.function.hbase.FilterFinalized;
import com.solutionsresource.sena.function.hbase.FilterSourceWordBank;
import com.solutionsresource.sena.function.hbase.FilterWithinTimeRange;
import com.solutionsresource.sena.function.hbase.ToResultHashMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author dzung
 */
public class HBaseDao implements AutoCloseable {

  private final JavaSparkContext sc;
  private HConnection connection = null;
  private Configuration config;
  private Gson gson;
  Constants constants;

  public HBaseDao(Configuration config) throws Exception {
    this(config, new Constants());
  }

  public HBaseDao(Configuration config, Constants constants) throws Exception {
    this(null, config, constants);
  }

  public HBaseDao(JavaSparkContext sc, Configuration config) throws Exception {
    this(sc, config, new Constants());
  }

  public HBaseDao(JavaSparkContext sc, Configuration config, Constants constants) throws Exception {
    this.sc = sc;
    this.config = config;
    this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

    this.connection = HConnectionManager.createConnection(config);
    this.constants = constants;
  }

  public JavaRDD<Message> getFinalizedMessages() throws Exception {
    return getMessages(new FilterFinalized());
  }

  public JavaRDD<Message> getUnfinalizedMessages() throws Exception {
    return getMessages(new FilterFinalized(false));
  }

  public JavaRDD<ResultHashMap> getMessagesWithinTimeRange(long start, long end)
      throws Exception {
    return getRawMessages(new FilterWithinTimeRange(constants, start, end))
        .map(new GetSecondElement())
        .map(new ToResultHashMap())
        .filter(new FilterSourceWordBank(constants));
  }

  public JavaPairRDD<ImmutableBytesWritable, Result> getRawMessages() {
    return this.getRawMessages(null);
  }

  public JavaPairRDD<ImmutableBytesWritable, Result> getRawPacquiaoMessages() {
    config.set(TableInputFormat.INPUT_TABLE, Constants.TABLE_PACQUIAO_MESSAGE_TEXT);

    return sc.newAPIHadoopRDD(config, TableInputFormat.class, ImmutableBytesWritable.class, Result.class);
  }

  public JavaPairRDD<ImmutableBytesWritable, Result> getRawMessages(
      Function<Tuple2<ImmutableBytesWritable, Result>, Boolean> filter) {
    config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_MESSAGE_TEXT);

    JavaPairRDD<ImmutableBytesWritable, Result> testRDD = sc.newAPIHadoopRDD(config,
        TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

    if (filter != null) {
      testRDD = testRDD.filter(filter);
    }

    return testRDD;
  }

  public JavaRDD<Message> getMessages() throws Exception {
    return this.getMessages(null);
  }

  public JavaRDD<Message> getMessages(Function<ResultHashMap, Boolean> filter)
      throws Exception {
    JavaRDD<ResultHashMap> rdd = getRawMessages().map(new GetSecondElement()).map(new ToResultHashMap());

    if (filter != null) {
      rdd = rdd.filter(filter);
    }

    return rdd.map(new MessageMapper()).repartition(constants.REPARTITION);
  }

  public JavaRDD<Topic> getTopics() throws Exception {
    config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_TOPIC_TEXT);

    JavaPairRDD<ImmutableBytesWritable, Result> testRDD = sc.newAPIHadoopRDD(config,
        TableInputFormat.class, ImmutableBytesWritable.class, Result.class);

    return testRDD.map(new TopicMapper()).repartition(constants.REPARTITION);
  }

  public void insertMetric(Metric metric) {
    try {
      config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_METRIC_TEXT);

      HTableInterface metricTable = connection.getTable(TableName.valueOf(constants.TABLE_METRIC_TEXT));

      long now = metric.getCreatedDate();
      String metricUUID = now + "-" + Generators.timeBasedGenerator().generate();

      Put putMetric = new Put(Bytes.toBytes(metricUUID));

      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_CONFUSION_MATRIX_BYTE,
          Bytes.toBytes(gson.toJson(metric.getConfusionMatrix())));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_PRECISION_BYTE,
          Bytes.toBytes(metric.getPrecision()));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_RECALL_BYTE,
          Bytes.toBytes(metric.getRecall()));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_ACCURACY_BYTE,
          Bytes.toBytes(metric.getAccuracy()));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_F1_SCORE_BYTE,
          Bytes.toBytes(metric.getF1Score()));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_TRAIN_COUNT_BYTE,
          Bytes.toBytes(metric.getTrainCount()));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_TEST_COUNT_BYTE,
          Bytes.toBytes(metric.getTestCount()));
      putMetric.add(constants.TABLE_METRIC_DATA_BYTE, constants.TABLE_METRIC_DATA_CREATED_DATE_BYTE,
          Bytes.toBytes(now));

      metricTable.put(putMetric);

      metricTable.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void updateMessageDomains(String uuid, List<Domain> domains) {
    try {
      config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_MESSAGE_TEXT);

      HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));

      Put putMessage = new Put(Bytes.toBytes(uuid));

      putMessage.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE,
          Bytes.toBytes(gson.toJson(domains)));

      messageTable.put(putMessage);

      messageTable.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void writeLog(String s) {
    try {
      config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_LOG_TEXT);

      HTableInterface logsTable = connection.getTable(TableName.valueOf(constants.TABLE_LOG_TEXT));

      long now = new Date().getTime();
      String logUUID = now + "-" + Generators.timeBasedGenerator().generate();

      Put putMetric = new Put(Bytes.toBytes(logUUID));

      putMetric.add(constants.TABLE_LOG_DATA_BYTE, constants.TABLE_LOG_DATA_VALUE_BYTE,
          Bytes.toBytes(s));

      logsTable.put(putMetric);

      logsTable.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void updateMessageFinalize(String uuid, boolean b) {
    try {
      config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_MESSAGE_TEXT);

      HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));

      Put putMessage = new Put(Bytes.toBytes(uuid));

      putMessage.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE,
          Bytes.toBytes(b));

      messageTable.put(putMessage);

      messageTable.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void createMessageFromTweet(Tweet tweet, String folderName, long timestamp, long userTimestamp,
                                     DateFormat df, byte[] domainsBytes, Boolean isFinalized) throws IOException {
    createMessageFromTweet(timestamp, tweet.id, new MessageBytes(constants, tweet, folderName,
        timestamp, userTimestamp, df, domainsBytes, isFinalized));
  }

  public void createMessageFromTweet(long timestamp, String refId, BytesTable fields) throws IOException {
    config.set(TableInputFormat.INPUT_TABLE, constants.TABLE_MESSAGE_TEXT);

    String id = String.format("%s-%s", timestamp, refId);

    try (HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT))) {
      Put put = new Put(Bytes.toBytes(id));

      for (byte[] key : fields.keySet()) {
        put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, key, timestamp, fields.get(key));
      }

      System.out.format("Saving %s...\n", id);
      System.out.format("Using fields: %s...\n", fields);

      table.put(put);
    }
  }

  public byte[] getMessageDomains(String content) {
    if (content.length() == 0) {
      return null;
    }

    try {
      String inputTable = constants.TABLE_MESSAGE_DOMAINS_TEXT;
      config.set(TableInputFormat.INPUT_TABLE, inputTable);

      HTableInterface messageDomainsTable = connection.getTable(TableName.valueOf(inputTable));

      Get getMessageDomains = new Get(Bytes.toBytes(content));

      Result result = messageDomainsTable.get(getMessageDomains);

      messageDomainsTable.close();

      if (result != null) {
        return result.getValue(constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_BYTE,
            constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_BYTE);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void close() throws IOException {
    connection.close();
  }

  public void createMessageFromPacquiaoTweet(Tweet tweet, String folderName, long timestamp,
                                             long userTimestamp, DateFormat df,
                                             byte[] domainsBytes, boolean isFinalized) {
    try {
      config.set(TableInputFormat.INPUT_TABLE, Constants.TABLE_PACQUIAO_MESSAGE_TEXT);

      HTableInterface table = connection.getTable(TableName.valueOf(Constants.TABLE_PACQUIAO_MESSAGE_TEXT));

      BytesTable fields = new BytesTable();

      fields.put(constants.TABLE_MESSAGE_MSGDATA_REF_ID_BYTE, Bytes.toBytes(tweet.id));

      String hashtagsString = "";

      for (Hashtag hashtag : tweet.twitter_entities.hashtags) {
        hashtagsString += String.format(" #%s", hashtag.text);
      }

      fields.put(constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE, Bytes.toBytes(tweet.body + hashtagsString));

      if (tweet.generator != null) {
        fields.put(constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, Bytes.toBytes(tweet.generator.displayName));
      }

      if (tweet.location != null) {
        fields.put(constants.TABLE_MESSAGE_MSGDATA_LOCATION_BYTE, Bytes.toBytes(tweet.location.displayName));
      }
      if (tweet.gnip != null && tweet.gnip.profileLocations != null && !tweet.gnip.profileLocations.isEmpty() &&
          tweet.gnip.profileLocations.get(0).geo != null && tweet.gnip.profileLocations.get(0).geo.coordinates
          != null && tweet.gnip.profileLocations.get(0).geo.coordinates.size() == 2) {
        List<String> coordinates = tweet.gnip.profileLocations.get(0).geo.coordinates;
        fields.put(constants.TABLE_MESSAGE_MSGDATA_LATITUDE_BYTE, Bytes.toBytes(coordinates.get(1)));
        fields.put(constants.TABLE_MESSAGE_MSGDATA_LONGITUDE_BYTE, Bytes.toBytes(coordinates.get(0)));
      }

      fields.put(constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE, Bytes.toBytes(tweet.actor.preferredUsername));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_ID_BYTE, Bytes.toBytes(tweet.actor.id));

      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_CREATED_BYTE,
          Bytes.toBytes(df.format(new Date(userTimestamp))));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_FRIENDS_COUNT_BYTE,
          Bytes.toBytes(tweet.actor.friendsCount));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_FOLLOWERS_COUNT_BYTE,
          Bytes.toBytes(tweet.actor.followersCount));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_STATUSES_COUNT_BYTE,
          Bytes.toBytes(tweet.actor.statusesCount));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_FAVORITES_COUNT_BYTE,
          Bytes.toBytes(tweet.actor.favoritesCount));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_USER_VERIFIED_BYTE,
          Bytes.toBytes(tweet.actor.verified));

      fields.put(constants.TABLE_MESSAGE_MSGDATA_SOURCE_FOLDER_BYTE, Bytes.toBytes(folderName));

      String hashtags = "";
      for (Hashtag hashtag : tweet.twitter_entities.hashtags) {
        hashtags += ", " + hashtag.text;
      }
      hashtags = hashtags.length() > 0 ? hashtags.substring(2) : "";
      fields.put(constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, Bytes.toBytes(hashtags));

      fields.put(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE,
          Bytes.toBytes(df.format(new Date(timestamp))));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_CREATED_DATETIME_BYTE, Bytes.toBytes(df.format(new Date())));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, Bytes.toBytes(isFinalized));
      fields.put(constants.TABLE_MESSAGE_MSGDATA_ISEXPORTED_BYTE, Bytes.toBytes(false));

      fields.put(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, domainsBytes);

      Put put = new Put(Bytes.toBytes(String.format("%s-%s", timestamp, tweet.id)));

      for (byte[] key : fields.keySet()) {
        put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, key, timestamp, fields.get(key));
      }

      table.put(put);

      table.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }


  public JavaRDD<ResultHashMap> getPacquiaoMessages() throws Exception {
    return getRawPacquiaoMessages()
        .map(new GetSecondElement())
        .map(new ToResultHashMap());
  }

  public DomainSlugs getAcceptedDomainSlugs() throws IOException {
    DomainSlugs domainSlugs = new DomainSlugs();

    for (Domain domain : getDomains()) {
      for (Topic topic : getTopicsOfDomain(domain)) {
        domainSlugs.getOrCreate(domain.getSlug()).put(topic.getSlug(), Sentiment.getNames());
      }
    }

    return domainSlugs;
  }

  public List<Domain> getDomains() throws IOException {
    List<Domain> domains = new ArrayList<>();

    try (
        HConnection connection = HConnectionManager.createConnection(config);
        HTableInterface domainsTable = connection.getTable(TableName.valueOf(constants.TABLE_DOMAIN_TEXT));
        ResultScanner scanner = domainsTable.getScanner(new Scan())
    ) {
      for (Result domainRow : scanner) {
        boolean domainIsActive = Bytes.toString(domainRow.getValue(constants.TABLE_DOMAIN_DOMDATA_BYTE,
            constants.TABLE_TOPIC_TOPDATA_ACTIVE_BYTE)).equalsIgnoreCase("A");

        if (!domainIsActive) {
          continue;
        }

        String name = Bytes.toString(domainRow.getValue(
            constants.TABLE_DOMAIN_DOMDATA_BYTE, constants.TABLE_DOMAIN_DOMDATA_NAME_BYTE));
        Domain domain = new Domain(name);
        domains.add(domain);
      }
    }
    return domains;
  }

  public List<Topic> getTopicsOfDomain(Domain domain) throws IOException {
    List<Topic> topics = new ArrayList<>();

    try (
        HConnection connection = HConnectionManager.createConnection(config);
        HTableInterface topicsTable = connection.getTable(TableName.valueOf(constants.TABLE_TOPIC_TEXT));
        ResultScanner scanner = topicsTable.getScanner(new Scan())
    ) {
      for (Result topicRow : scanner) {
        boolean topicIsActive = Bytes.toString(topicRow.getValue(constants.TABLE_TOPIC_TOPDATA_BYTE,
            constants.TABLE_TOPIC_TOPDATA_ACTIVE_BYTE)).equalsIgnoreCase("A");

        boolean topicIsOfDomain = Bytes.toString(topicRow.getValue(constants.TABLE_TOPIC_TOPDATA_BYTE,
            constants.TABLE_TOPIC_TOPDATA_DOMAIN_BYTE)).equalsIgnoreCase(domain.getName());

        String name = Bytes.toString(topicRow.getValue(
            constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_NAME_BYTE));

        if (!topicIsActive || !topicIsOfDomain) {
          continue;
        }

        String description = Bytes.toString(topicRow.getValue(
            constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_DESCRIPTION_BYTE));

        topics.add(new Topic(name, description));
      }
    }

    return topics;
  }

  public void writeScorecard(PredictionTable predictions) {
    System.out.println(predictions);
  }

  public void createFromCsv(String[] values, DomainHashMap domains, String source) throws IOException {
    String[] idSplit = values[0].split("-");

    long timestamp = Long.parseLong(idSplit[0]);
    String refId = String.format("%s-%s", idSplit[1], idSplit[2]);

    byte[] domainsBytes = Bytes.toBytes(gson.toJson(domains.toDomains()));

    createMessageFromTweet(timestamp, refId, new MessageBytes(constants, refId, values, domainsBytes, source));
  }

  public List<Topic> getTopicsList() throws IOException {
    List<Topic> topics = new ArrayList<>();

    try (
        HConnection connection = HConnectionManager.createConnection(config);
        HTableInterface topicsTable = connection.getTable(TableName.valueOf(constants.TABLE_TOPIC_TEXT));
        ResultScanner scanner = topicsTable.getScanner(new Scan())
    ) {
      for (Result topicRow : scanner) {
        String name = Bytes.toString(topicRow.getValue(
            constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_NAME_BYTE));

        String description = Bytes.toString(topicRow.getValue(
            constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_DESCRIPTION_BYTE));
        Topic topic =
            new Topic(name, description);
        topics.add(topic);
        System.out.println(topic);
      }
    }
    return topics;
  }

  public void saveModelBuild(String key, long current, long goal, boolean isActive)
      throws BuildingException, IOException {
    try (
        HTableInterface buildStatusTable = connection.getTable(
            TableName.valueOf(constants.TABLE_BUILD_STATUS_TEXT));
        ResultScanner scanner = buildStatusTable.getScanner(new Scan())
    ) {
      for (Result result : scanner) {

        boolean resultIsCurrent = Bytes.toString(result.getRow()).equals(key);
        boolean resultIsActive = Bytes.toBoolean(result.getValue(
            constants.TABLE_BUILD_STATUS_DATA_BYTE, constants.TABLE_BUILD_STATUS_DATA_IS_ACTIVE_BYTE));

        if (resultIsCurrent != resultIsActive) {
          putBuildStatus(buildStatusTable, key, 0, 0, false);
          if (resultIsCurrent) {
            throw new CancelledBuildingException("The model build was already cancelled.");
          } else {
            throw new AlreadyBuildingException("A model is already running");
          }
        }
      }

      putBuildStatus(buildStatusTable, key, current, goal, isActive);
    }
  }

  public void putBuildStatus(HTableInterface buildStatusTable, String key, long current, long goal, boolean isActive)
      throws IOException {
    Put put = new Put(Bytes.toBytes(key));

    put.add(
        constants.TABLE_BUILD_STATUS_DATA_BYTE,
        constants.TABLE_BUILD_STATUS_DATA_CURRENT_BYTE,
        Bytes.toBytes(current)
    );

    put.add(
        constants.TABLE_BUILD_STATUS_DATA_BYTE,
        constants.TABLE_BUILD_STATUS_DATA_GOAL_BYTE,
        Bytes.toBytes(goal)
    );

    put.add(
        constants.TABLE_BUILD_STATUS_DATA_BYTE,
        constants.TABLE_BUILD_STATUS_DATA_IS_ACTIVE_BYTE,
        Bytes.toBytes(isActive)
    );

    buildStatusTable.put(put);
  }
}

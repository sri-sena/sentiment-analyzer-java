package com.solutionsresource.sena.util.hbase;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AcceptedSlugsGetter {
    private HBaseDao hbase;

    public AcceptedSlugsGetter(HBaseDao hbase) {
        this.hbase = hbase;
    }

    public DomainSlugs get() throws IOException {
        return hbase.getAcceptedDomainSlugs();
    }

    public static DomainSlugs readJson(String filename) throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(filename));
        return gson.fromJson(reader, DomainSlugs.class);
    }

    public static DomainSlugs mock() {
        DomainSlugs accepted = new DomainSlugs();

        accepted.put("election", new TopicSlugs());
        accepted.put("restaurants", new TopicSlugs());
        accepted.put("telecommunications", new TopicSlugs());

        TopicSlugs election = accepted.get("election");
        TopicSlugs restaurants = accepted.get("restaurants");
        TopicSlugs telecommunications = accepted.get("telecommunications");

        election.put("alma-moreno", Sentiment.getNames());
        election.put("alan-cayetano", Sentiment.getNames());
        election.put("antonio-trillanes", Sentiment.getNames());
        election.put("bongbong-marcos", Sentiment.getNames());
        election.put("dick-gordon", Sentiment.getNames());
        election.put("francis-escudero", Sentiment.getNames());
        election.put("grace-poe", Sentiment.getNames());
        election.put("gringo-honasan", Sentiment.getNames());
        election.put("jejomar-binay", Sentiment.getNames());
        election.put("leila-de-lima", Sentiment.getNames());
        election.put("leni-robredo", Sentiment.getNames());
        election.put("manny-pacquiao", Sentiment.getNames());
        election.put("mar-roxas", Sentiment.getNames());
        election.put("miriam-defensor-santiago", Sentiment.getNames());
        election.put("rodrigo-duterte", Sentiment.getNames());

        election.put("ali", Sentiment.getNames());
        election.put("alunan", Sentiment.getNames());
        election.put("baligod", Sentiment.getNames());
        election.put("belgica", Sentiment.getNames());
        election.put("bello", Sentiment.getNames());
        election.put("cam", Sentiment.getNames());
        election.put("colmenares", Sentiment.getNames());
        election.put("domagoso", Sentiment.getNames());
        election.put("drilon", Sentiment.getNames());
        election.put("gatchalian", Sentiment.getNames());
        election.put("guingona", Sentiment.getNames());
        election.put("hontiveros", Sentiment.getNames());
        election.put("kabalu", Sentiment.getNames());
        election.put("kapunan", Sentiment.getNames());
        election.put("kiram", Sentiment.getNames());
        election.put("lacson", Sentiment.getNames());
        election.put("langit", Sentiment.getNames());
        election.put("lapid", Sentiment.getNames());
        election.put("maganto", Sentiment.getNames());
        election.put("manzano", Sentiment.getNames());
        election.put("montano", Sentiment.getNames());
        election.put("napenas", Sentiment.getNames());
        election.put("ople", Sentiment.getNames());
        election.put("osmena", Sentiment.getNames());
        election.put("pagdilao", Sentiment.getNames());
        election.put("palparan", Sentiment.getNames());
        election.put("pangilinan", Sentiment.getNames());
        election.put("petilla", Sentiment.getNames());
        election.put("recto", Sentiment.getNames());
        election.put("romualdez", Sentiment.getNames());
        election.put("romulo", Sentiment.getNames());
        election.put("santiago", Sentiment.getNames());
        election.put("sotto", Sentiment.getNames());
        election.put("tolentino", Sentiment.getNames());
        election.put("trillanes", Sentiment.getNames());
        election.put("villanueva", Sentiment.getNames());
        election.put("zubiri", Sentiment.getNames());

        restaurants.put("jollibee", Sentiment.getNames());
        restaurants.put("kfc", Sentiment.getNames());

        telecommunications.put("smart-customer-service", Sentiment.getNames());
        telecommunications.put("smart-888", Sentiment.getNames());
        telecommunications.put("pldt-fixed-line", Sentiment.getNames());

        return accepted;
    }
}

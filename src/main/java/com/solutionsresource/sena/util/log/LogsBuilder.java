package com.solutionsresource.sena.util.log;

public class LogsBuilder {
    private StringBuilder stringBuilder;

    public LogsBuilder(StringBuilder stringBuilder) {
        this.stringBuilder = stringBuilder;
    }

    public void append(Object s) {
        stringBuilder.append(s);
    }

    public void appendln(Object s) {
        append(s.toString().concat("\n"));
    }

    public String toString() {
        return stringBuilder.toString();
    }
}

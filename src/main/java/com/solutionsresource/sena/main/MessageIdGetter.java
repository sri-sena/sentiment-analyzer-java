package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.RegexStringComparator;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;

public class MessageIdGetter {

    public static void main(String[] args) throws Exception {
        Constants constants = new Constants();
        System.out.println("Usage: ./MessageIdGetter.sh <from_milliseconds> <to_milliseconds> [<hashtag_query>]");
        long from = Long.parseLong(args[0]);
        long to = Long.parseLong(args[1]);

        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
        HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));

        try {
            FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

            filter.addFilter(new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("source"),
                    CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

            filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                    constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, CompareOp.EQUAL,
                    new RegexStringComparator(args.length > 2 ? args[2] : ".*")));

            Scan scan = new Scan();
            scan.setFilter(filter);

            scan.setTimeRange(from, to);

            for (Result result : table.getScanner(scan)) {
                String content = Bytes.toString(result.getValue(
                        constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE));

                if (content == null) {
                    continue;
                }

                System.out.println(Bytes.toString(result.getRow()));
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        table.close();
        connection.close();
    }
}

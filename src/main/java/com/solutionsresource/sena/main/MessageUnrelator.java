package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.Scanner;

public class MessageUnrelator {

    public static void main(String[] args) throws Exception {
        Constants constants = new Constants();
        System.out.println("Usage: ./MessageUnrelator.sh");

        ContentCleaner contentCleaner = new ContentCleaner(new RemoveLinks(constants),
                new RemoveStopWords(), new RemoveHandlers(constants), new DatesNormalizer(), new EmojiNormalizer());

        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
        HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
        HTableInterface messageDomainsTable = connection.getTable(
                TableName.valueOf(constants.TABLE_MESSAGE_DOMAINS_TEXT));

        System.out.println("Input your message IDs.");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        while (!input.equals("")) {
            Get get = new Get(Bytes.toBytes(input));

            Result result = messageTable.get(get);

            String content = contentCleaner.clean(Bytes.toString(result.getValue(
                    constants.TABLE_MESSAGE_MSGDATA_BYTE,
                    constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE
            )));

            Put putMessageDomains = new Put(Bytes.toBytes(content));

            putMessageDomains.add(constants.TABLE_MESSAGE_DOMAINS_DATA_BYTE,
                    constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_BYTE, Bytes.toBytes("[]"));

            messageDomainsTable.put(putMessageDomains);

            input = scanner.nextLine();
        }
    }
}


package com.solutionsresource.sena.main;

import au.com.bytecode.opencsv.CSVParser;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.function.featureselection.FilterOutEmpty;
import com.solutionsresource.sena.function.featuretransformation.ConvertToCsv;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExportMessagesFromHbase {

    private final String folderName;
    private final String extractString;
    private final Constants constants;
    private final HBaseDao hbase;
    private final List<Topic> topics;
    private final String[] topicsFilter;

    public ExportMessagesFromHbase(String folderName, String extractString, Constants constants,
                                   HBaseDao hbase, List<Topic> topics, String topicsFilter) throws IOException {
        this.folderName = folderName;
        this.extractString = extractString;
        this.constants = constants;
        this.hbase = hbase;
        this.topics = topics;
        this.topicsFilter = csvParse(topicsFilter);
    }

    public ExportMessagesFromHbase(String folderName, String extractString, Constants constants,
                                   HBaseDao hbase, List<Topic> topics, String[] topicsFilter) {
        this.folderName = folderName;
        this.extractString = extractString;
        this.constants = constants;
        this.hbase = hbase;
        this.topics = topics;
        this.topicsFilter = topicsFilter;
    }

    public long extract(long start, long end) throws Exception {
        List<String> filenamesWritten = new ArrayList<>();

        String filenamePrefix = String.format("%s/%s/%d-%d-",
                constants.HDFS_EXPORTED_CSV_PATH, folderName, start, end);

        String candidatesFile = String.format("%sCandidates.csv", filenamePrefix);
        String hashtagsFile = String.format("%sHashtags.csv", filenamePrefix);
        String debatesFile = String.format("%sPiliPinasDebates2016.csv", filenamePrefix);
        String allFile = String.format("%sAll.csv", filenamePrefix);

        JavaRDD<ResultHashMap> messages = hbase.getMessagesWithinTimeRange(start, end)
                .repartition(constants.REPARTITION).cache();

        JavaRDD<String> candidatesMessages = convertToCsv(
                constants, false, constants.CSV_CANDIDATES_REGEX, messages, topicsFilter);

        JavaRDD<String> hashtagsMessages = convertToCsv(
                constants, true, constants.CSV_HASHTAGS_REGEX, messages, topicsFilter);

        JavaRDD<String> debatesMessages = convertToCsv(
                constants, true, constants.CSV_DEBATE_REGEX, messages, topicsFilter);

        JavaRDD<String> allMessages = convertToCsv(
                constants, true, constants.CSV_CANDIDATES_REGEX, messages, topicsFilter);

        long totalLinesCount = 0;
        for (char c : extractString.toCharArray()) {
            switch (c) {
                case 'c':
                    System.out.format("Writing %s...\n", candidatesFile);
                    try {
                        totalLinesCount += candidatesMessages.count();
                        candidatesMessages.saveAsTextFile(candidatesFile);
                        filenamesWritten.add(candidatesFile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case 'h':
                    System.out.format("Writing %s...", hashtagsFile);
                    try {
                        totalLinesCount += candidatesMessages.count();
                        hashtagsMessages.saveAsTextFile(hashtagsFile);
                        filenamesWritten.add(hashtagsFile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case 'p':
                    System.out.format("Writing %s...", debatesFile);
                    try {
                        totalLinesCount += candidatesMessages.count();
                        debatesMessages.saveAsTextFile(debatesFile);
                        filenamesWritten.add(debatesFile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case 'a':
                    System.out.format("Writing %s...", allFile);
                    try {
                        totalLinesCount += candidatesMessages.count();
                        allMessages.saveAsTextFile(allFile);
                        filenamesWritten.add(allFile);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }

        messages.unpersist();

        for (String filenameWritten : filenamesWritten) {
            System.out.println(filenameWritten);
        }

        return totalLinesCount;
    }

    private static String[] csvParse(String topicsFilter) throws IOException {
        CSVParser parser = new CSVParser();
        return parser.parseLine(topicsFilter);
    }

    public static void main(String[] args) throws Exception {
        String folderName = args[0];
        long start = Long.parseLong(args[1]);
        long end = Long.parseLong(args[2]);
        String extractString = args[3].toLowerCase();
        String[] topicsFilter = args.length > 4 ? csvParse(args[4]) : null;

        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        SparkConf conf = new SparkConf().setAppName("SENA Export Messages to CSV");

        try (
                JavaSparkContext sc = new JavaSparkContext(conf);
                HBaseDao hbase = new HBaseDao(sc, config)
        ) {
            List<Topic> topics = hbase.getTopicsList();

            System.out.println("Usage: ./ExportMessagesFromHbase.sh <csv_filename> <from_milliseconds>" +
                    "<to_milliseconds> [<optional_hashtag_regex_query>]");
            System.out.println("Example: ./ExportMessagesFromHbase.sh Election-Feb21-Feb22 " +
                    "1456027200000 1456156800000");
            System.out.println("Example: ./ExportMessagesFromHbase.sh Election-Feb21-Feb22-BilangPilipino-DU30 " +
                    "1456027200000 1456156800000 \"BilangPilipino|DU30\"");

            ExportMessagesFromHbase exporter = new ExportMessagesFromHbase(folderName, extractString,
                    constants, hbase, topics, topicsFilter);

            exporter.extract(start, end);
        }
    }

    private static JavaRDD<String> convertToCsv(Constants constants, boolean hasFilterHashtag, String filterHashtag,
                                                JavaRDD<ResultHashMap> messages, String[] topicsFilter) {
        return messages.map(new ConvertToCsv(constants, hasFilterHashtag, filterHashtag, topicsFilter))
                .filter(new FilterOutEmpty());
    }
}

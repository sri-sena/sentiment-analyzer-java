package com.solutionsresource.sena.main;

import com.solutionsresource.sena.exception.BuildingException;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;

public class BuildCanceller {
  private final HBaseDao hbase;

  public BuildCanceller(HBaseDao hbase) throws Exception {
    this.hbase = hbase;
  }

  public void cancel(String modelName) throws IOException, BuildingException {
    hbase.saveModelBuild(modelName, 0, 0, false);
  }

  public static void main(String[] args) throws Exception {
    String modelName = args[0];

    SparkConf conf = new SparkConf().setAppName("SENA Build Canceller");

    Configuration config = new HBaseConnector().getConfiguration();

    try (
        JavaSparkContext sc = new JavaSparkContext(conf);
        HBaseDao hbase = new HBaseDao(sc, config)
    ) {
      new BuildCanceller(hbase).cancel(modelName);
    }
  }
}

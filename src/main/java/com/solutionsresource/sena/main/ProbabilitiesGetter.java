package com.solutionsresource.sena.main;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.PredictionTable;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.actual.GetActualTable;
import com.solutionsresource.sena.function.featureselection.FilterOutRetweets;
import com.solutionsresource.sena.function.featuretransformation.GetDomains;
import com.solutionsresource.sena.function.prediction.ReducePredictionTables;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.log.LogsBuilder;
import com.solutionsresource.sena.util.model.SlugsGetter;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.HashMap;

public class ProbabilitiesGetter {

    private DomainSlugs domainSlugs;

    public ProbabilitiesGetter(DomainSlugs domainSlugs) {
        this.domainSlugs = domainSlugs;
    }

    public PredictionTable get(JavaRDD<Message> trainingDataset) {
        return trainingDataset
                .map(new GetActualTable(domainSlugs))
                .reduce(new ReducePredictionTables());
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Prior Probabilities Getter");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages().filter(new FilterOutRetweets());

        LogsBuilder stringBuilder = new LogsBuilder(new StringBuilder());

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

        stringBuilder.append(domainSlugs.toString());

        ProbabilitiesGetter probabilitiesGetter = new ProbabilitiesGetter(domainSlugs);

        PredictionTable predictions = probabilitiesGetter.get(trainingDataset);
        stringBuilder.appendln(predictions);

        String log = stringBuilder.toString();
        hbase.writeLog(log);

        hbase.writeScorecard(predictions);

        System.out.println(log);

        sc.close();
    }
}

package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.function.featuretransformation.GetDomains;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import com.solutionsresource.sena.util.parse.SaveMessageJson;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;

public class ImportMessagesFromSpecifiedJson {


    private final StepByStepSentimentAnalyzer analyzer;
    private final HBaseDao hbase;
    private JavaSparkContext sc;
    private Configuration config;
    private String filename;

    Constants constants;

    public ImportMessagesFromSpecifiedJson(String filename) throws Exception {
        this.filename = filename;

        SparkConf conf = new SparkConf().setAppName("SENA");
        sc = new JavaSparkContext(conf);
        constants = new Constants();
        config = new HBaseConnector(constants).getConfiguration();

        hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

        System.out.println(domainSlugs);

        analyzer = new StepByStepSentimentAnalyzer(sc, domainSlugs, constants, false);
    }

    public void readJson() throws Exception {
        Path tweetFilePath = new Path(filename);

        JavaRDD<String> jsons = sc.textFile(tweetFilePath.toString(), constants.REPARTITION);

        jsons.foreach(new SaveMessageJson(analyzer));
    }

    private void close() throws IOException {
        hbase.close();
        sc.close();
    }

    public static void main(String[] args) throws Exception {
        ImportMessagesFromSpecifiedJson imjson = new ImportMessagesFromSpecifiedJson(args[0]);
        imjson.readJson();
        imjson.close();
    }
}

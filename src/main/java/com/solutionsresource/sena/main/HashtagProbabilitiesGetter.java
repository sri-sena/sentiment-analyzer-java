package com.solutionsresource.sena.main;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTable;
import com.solutionsresource.sena.function.featuretransformation.ExtractHashtags;
import com.solutionsresource.sena.function.featuretransformation.GetHasDomain;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class HashtagProbabilitiesGetter {

    public JavaSparkContext sc;

    public HashtagProbabilitiesGetter(JavaSparkContext sc) {
        this.sc = sc;
    }

    public UsageTable get(JavaRDD<Message> trainingDataset) throws Exception {
        return trainingDataset.map(new ExtractHashtags()).reduce(new ReduceUsageTable());
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> dataset = hbase.getMessages().filter(new GetHasDomain("election"));

        HashtagProbabilitiesGetter probabilitiesGetter = new HashtagProbabilitiesGetter(sc);

        System.out.println(probabilitiesGetter.get(dataset));

        sc.close();
    }
}

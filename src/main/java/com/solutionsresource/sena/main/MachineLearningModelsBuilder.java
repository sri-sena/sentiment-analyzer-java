package com.solutionsresource.sena.main;

import com.solutionsresource.sena.config.Config;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.MultiplierTable;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.exception.BuildingException;
import com.solutionsresource.sena.function.featurescaling.FilterWithDomain;
import com.solutionsresource.sena.function.featurescaling.FilterWithTopic;
import com.solutionsresource.sena.function.featurescaling.GetWeightedLabeledPoints;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTable;
import com.solutionsresource.sena.function.featureselection.FilterOutRetweets;
import com.solutionsresource.sena.function.featureselection.GetNGramsWithKey;
import com.solutionsresource.sena.function.featureselection.RemoveStopHashtags;
import com.solutionsresource.sena.function.featureselection.TotalCount;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.util.build.Features;
import com.solutionsresource.sena.util.build.ModelBuilder;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.classification.NaiveBayesModel;
import scala.Tuple2;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MachineLearningModelsBuilder {

  private final JavaSparkContext sc;
  private final HBaseDao hbase;
  private final DomainSlugs domainSlugs;
  private final String todayString;
  private final boolean shouldSave;
  private final Constants constants;
  private long currentProgress;
  private final long modelsCount;

  public MachineLearningModelsBuilder(JavaSparkContext sc, HBaseDao hbase, DomainSlugs domainSlugs,
                                      String todayString, boolean shouldSave) throws Exception {
    this.sc = sc;
    this.hbase = hbase;
    this.domainSlugs = domainSlugs;
    this.todayString = todayString;
    this.shouldSave = shouldSave;
    this.constants = new Constants();
    this.currentProgress = 0L;
    this.modelsCount = domainSlugs.getModelsCount();
  }

  public Tuple2<NaiveBayesModel, List<Feature>> buildModel(JavaRDD<WeightedLabeledPoint> weightedTraining,
                                                           List<Feature> features, String folderName,
                                                           boolean shouldUndersample)
      throws IOException, BuildingException {
    return buildModel(weightedTraining, features, folderName, new String[]{"false", "true"}, shouldUndersample);
  }

  public Tuple2<NaiveBayesModel, List<Feature>> buildModel(JavaRDD<WeightedLabeledPoint> weightedTraining,
                                                           List<Feature> features, String folderName,
                                                           String[] labels, boolean shouldUndersample)
      throws IOException, BuildingException {

    ModelBuilder builder = new ModelBuilder(sc, shouldSave, shouldUndersample);

    Tuple2<NaiveBayesModel, List<Feature>> model = builder
        .build(weightedTraining, features, folderName, labels);

    if (this.shouldSave) {
      hbase.saveModelBuild(todayString, currentProgress++, modelsCount, true);
    }

    return model;
  }

  public List<Domain> build(JavaRDD<Message> trainingDataset) throws Exception {
    List<Domain> domains = new ArrayList<>();

    if (this.shouldSave) {
      hbase.saveModelBuild(todayString, currentProgress, modelsCount, true);
    }

    System.out.println("Building...");

    String modelFolder = constants.NAIVE_BAYES_MODELS_PATH;

    String folderName = modelFolder + todayString;

    System.out.println("Preprocessing...");

    JavaRDD<Message> cleanedDocuments = trainingDataset
        .map(new ContentCleaner(
            new RemoveLinks(constants),
            new RemoveStopWords(),
            new RemoveHandlers(constants),
            new DatesNormalizer(),
            new EmojiNormalizer()
        )).map(new ExtractNGrams());

    System.out.println("Extracting features for domains...");

    long cleanedDocumentsCount = cleanedDocuments.count();

    for (String domainName : domainSlugs.keySet()) {
      Domain domain = buildDomain(folderName, cleanedDocuments, cleanedDocumentsCount, domainName,
          domainSlugs.get(domainName));
      domains.add(domain);
    }

    if (this.shouldSave) {
      List<String> modelDates = new ArrayList<>();

      modelDates.add(todayString);

      String activeFilename = modelFolder + "active.txt";

      try {
        FileSystem fs = FileSystem.get(sc.hadoopConfiguration());
        fs.delete(new Path(activeFilename), true); // delete file, true for recursive
      } catch (IOException e) {
        e.printStackTrace();
      }

      sc.parallelize(modelDates).saveAsTextFile(activeFilename);

      hbase.saveModelBuild(todayString, modelsCount, modelsCount, false);
    }

    return domains;
  }

  private Domain buildDomain(String folderName, JavaRDD<Message> documents,
                             long cleanedDocumentsCount, String domainName, TopicSlugs topicSlugs) throws Exception {

    JavaRDD<Message> cleanedDocuments = documents.filter(new FilterOutRetweets());

    HashMap<String, UsageTable> allNGrams = cleanedDocuments
        .map(new GetNGramsWithKey(domainName)).reduce(new TotalCount());

    String domainFolder = folderName + "/domains/" + domainName;
    String domainClassifierFolder = domainFolder + "/domain-recognizer/";

    System.out.println("Building models for domain " + domainName + "...");

    UsageTable domainUsages = cleanedDocuments.map(new ConvertToUsageTable(new GetHasDomainString(domainName)))
        .reduce(new ReduceUsageTable());

    MultiplierTable domainMultipliers = MultiplierTable.create(Constants.FALSE_DOMAIN_BIAS_MULTIPLIER);

    List<Feature> selectedFeatures = Features.getKeys(sc, allNGrams, cleanedDocumentsCount,
        domainUsages, domainName, constants.DOMAIN_FEATURES_COUNT);

    Broadcast<List<Feature>> broadcastedSelectedFeatures = sc.broadcast(selectedFeatures);

    JavaRDD<WeightedLabeledPoint> weightedTraining = cleanedDocuments.map(
        new GetWeightedLabeledPoints(broadcastedSelectedFeatures, domainName, null, true, domainMultipliers)
    );

    Tuple2<NaiveBayesModel, List<Feature>> model = buildModel(weightedTraining, selectedFeatures,
        domainClassifierFolder, true);

    NaiveBayesModel domainRecognizer = model._1();
    selectedFeatures = model._2();

    JavaRDD<Message> documentsOfDomain = documents.filter(new FilterWithDomain(domainName));

    Domain domain = new Domain(domainName, selectedFeatures, domainRecognizer);

    long documentsOfDomainCount = documentsOfDomain.count();

    for (String topicName : topicSlugs.keySet()) {
      Topic topic = buildTopic(domainName, domainFolder, documentsOfDomain, documentsOfDomainCount, topicName,
          topicSlugs.get(topicName));

      domain.getTopics().add(topic);
    }

    return domain;
  }

  private Topic buildTopic(String domainName, String domainFolder, JavaRDD<Message> cleanedDocuments,
                           long documentsOfDomainCount, String topicName, String[] sentimentNames) throws Exception {

    HashMap<String, UsageTable> allNGramsOfDomain = cleanedDocuments
        .map(new GetNGramsWithKey(domainName, topicName)).reduce(new TotalCount());

    String topicFolder = domainFolder + "/topics/" + topicName;

    String topicClassifierFolder = topicFolder + "/topic-recognizer/";

    System.out.println("Building models for topic " + topicName + "...");

    UsageTable topicUsages = cleanedDocuments.map(new ConvertToUsageTable(new GetHasDomainTopic(
        domainName, topicName))).reduce(new ReduceUsageTable());

    MultiplierTable topicMultipliers = MultiplierTable.create(Constants.FALSE_TOPIC_BIAS_MULTIPLIER);

    List<Feature> domainFeatures = Features.getKeys(sc, allNGramsOfDomain,
        documentsOfDomainCount, topicUsages, topicName, constants.TOPIC_FEATURES_COUNT);

    Broadcast<List<Feature>> broadcastedDomainFeatures = sc.broadcast(domainFeatures);

    JavaRDD<WeightedLabeledPoint> weightedTrainingOfDomain = cleanedDocuments.map(
        new GetWeightedLabeledPoints(broadcastedDomainFeatures, domainName, topicName, topicMultipliers)
    );

    Tuple2<NaiveBayesModel, List<Feature>> topicModel = buildModel(
        weightedTrainingOfDomain, domainFeatures, topicClassifierFolder, false);

    NaiveBayesModel topicRecognizer = topicModel._1();
    domainFeatures = topicModel._2();

    // sentiment analyzer

    String sentimentAnalyzerFolder = topicFolder + "/sentiment-analyzer/";

    JavaRDD<Message> documentsOfTopic = cleanedDocuments
        .filter(new FilterOutRetweets())
        .filter(new FilterWithTopic(domainName, topicName));

    long documentsOfTopicCount = documentsOfTopic.count();

    documentsOfTopic = documentsOfTopic.map(new RemoveStopHashtags(Config.STOP_HASHTAGS)).map(new ExtractNGrams());

    HashMap<String, UsageTable> allNGramsOfTopic = documentsOfTopic
        .map(new GetNGramsWithKey(new GetSentimentOfTopic(domainName, topicName)))
        .reduce(new TotalCount());

    System.out.println("Building models for topic " + topicName + "'s sentiments...");

    UsageTable sentimentUsages = documentsOfTopic.map(new ConvertToUsageTable(new GetSentimentOfTopic(
        domainName, topicName))).reduce(new ReduceUsageTable());

    MultiplierTable sentimentMultipliers = MultiplierTable.create(true);

    List<Feature> topicFeatures = Features.getKeys(sc, allNGramsOfTopic,
        documentsOfTopicCount, sentimentUsages, null, constants.SENTIMENT_FEATURES_COUNT);

    Broadcast<List<Feature>> broadcastedTopicFeatures = sc.broadcast(topicFeatures);

    JavaRDD<WeightedLabeledPoint> weightedTrainingOfTopic = documentsOfTopic.map(
        new GetWeightedLabeledPoints(broadcastedTopicFeatures, domainName, topicName, sentimentNames,
            sentimentMultipliers
        )
    );

    Tuple2<NaiveBayesModel, List<Feature>> sentimentModel = buildModel(
        weightedTrainingOfTopic, topicFeatures, sentimentAnalyzerFolder, sentimentNames, false);

    NaiveBayesModel sentimentAnalyzer = sentimentModel._1();
    topicFeatures = sentimentModel._2();

    return new Topic(topicName, domainFeatures, topicRecognizer, topicFeatures, sentimentAnalyzer);
  }

  public static void main(String[] args) throws Exception {

    boolean shouldSave = true;

    DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    String todayString = df.format(new Date());

    Configuration config = new HBaseConnector().getConfiguration();

    if (shouldSave) {
      try (HBaseDao hbase = new HBaseDao(null, config)) {
        hbase.saveModelBuild(todayString, 0, 1, shouldSave);
      }
    }

    SparkConf conf = new SparkConf().setAppName("SENA Machine Learning Models Builder");

    try (
        JavaSparkContext sc = new JavaSparkContext(conf);
        HBaseDao hbase = new HBaseDao(sc, config)
    ) {

      if (shouldSave) {
        hbase.saveModelBuild(todayString, 0, 1, shouldSave);
      }

      JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

      DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
      SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

      DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

      System.out.println(domainSlugs);

      MachineLearningModelsBuilder builder = new MachineLearningModelsBuilder(sc, hbase,
          domainSlugs, todayString, true);

      builder.build(trainingDataset);
    }
  }
}

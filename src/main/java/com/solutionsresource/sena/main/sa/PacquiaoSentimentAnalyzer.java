package com.solutionsresource.sena.main.sa;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class PacquiaoSentimentAnalyzer implements Serializable, SentimentAnalyzer {
    @Override
    public List<Domain> analyze(String input) {
        List<Domain> domains = new ArrayList<>();
        List<Topic> topics = new ArrayList<>();

        HashMap<String, HashMap<String, Boolean>> topicRegexes = new HashMap<>();

        HashMap<String, Boolean> pacquiaoRegexes = new HashMap<>();
        pacquiaoRegexes.put("nintendo", false);
        pacquiaoRegexes.put("Manny.*Pacman|Pacman.*Manny", true);
        pacquiaoRegexes.put("Bradley.*Manny|Manny.*Bradley", true);
        pacquiaoRegexes.put("Mayweather.*Manny|Manny.*Mayweather", true);
        pacquiaoRegexes.put("Pacquiao", true);

        HashMap<String, Boolean> bradleyRegexes = new HashMap<>();
        bradleyRegexes.put("university", false);
        bradleyRegexes.put("Tim.*Bradley|Bradley.*Tim", true);
        bradleyRegexes.put("Pacquiao.*Bradley|Bradley.*Pacquiao", true);
        bradleyRegexes.put("Manny.*Bradley|Bradley.*Manny", true);
        bradleyRegexes.put("Mayweather.*Bradley|Bradley.*Mayweather", true);

        HashMap<String, Boolean> mayweatherRegexes = new HashMap<>();
        mayweatherRegexes.put("pink floyd", false);
        mayweatherRegexes.put("Floyd.*Mayweather|Mayweather.*Floyd", true);
        mayweatherRegexes.put("Mayweather.*Jr", true);
        mayweatherRegexes.put("Floyd.*Pacquiao|Pacquiao.*Floyd", true);
        mayweatherRegexes.put("Pacquiao.*Mayweather|Mayweather.*Pacquiao", true);
        mayweatherRegexes.put("Manny.*Mayweather|Mayweather.*Manny", true);
        mayweatherRegexes.put("Bradley.*Mayweather|Mayweather.*Bradley", true);

        topicRegexes.put("Pacquiao", pacquiaoRegexes);
        topicRegexes.put("Bradley", bradleyRegexes);
        topicRegexes.put("Mayweather", mayweatherRegexes);

        for (Entry<String, HashMap<String, Boolean>> topicRegex : topicRegexes.entrySet()) {
            boolean topicIsAccepted = false;
            for (Entry<String, Boolean> regex : topicRegex.getValue().entrySet()) {
                boolean regexShouldBeOkay = regex.getValue();
                boolean regexIsOkay = input.matches(String.format("(?i:.*(%s).*)", regex.getKey()));

                if (regexIsOkay) {
                    topicIsAccepted = regexShouldBeOkay;
                    break;
                }
            }

            if (topicIsAccepted) {
                topics.add(new Topic(topicRegex.getKey(), "pta", new Sentiment("N/A", null, "psa"), false));
            }
        }

        if (topics.size() > 0) {
            domains.add(new Domain("Boxing", "Boxing", "pda", false, topics));
        }

        return domains;
    }

    @Override
    public List<Domain> analyze(String input, boolean isSpecialCase) {
        return analyze(input);
    }

    public static SentimentAnalyzer getInstance() {
        return new PacquiaoSentimentAnalyzer();
    }
}

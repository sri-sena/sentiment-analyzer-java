package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IngestStatusGetter {

    private final SimpleDateFormat folderNameParser;
    private Configuration config;
    Constants constants;

    public IngestStatusGetter() throws Exception {
        constants = new Constants();
        config = new HBaseConnector(constants).getConfiguration();
        folderNameParser = new SimpleDateFormat("yyyyMMddHHmm");
    }

    public Date getMostRecentFinished() throws Exception {

        FileStatus[] dateFolders;

        try (FileSystem fs = FileSystem.get(config)) {
            dateFolders = fs.listStatus(new Path(constants.FOLDER_TW_COMMENTS_PATH));
        }

        String filename = null;

        if (constants.TWITTER_FEEDS_REVERSED) {
            for (int i = dateFolders.length - 1; i >= 0; i--) {
                FileStatus dateFolder = dateFolders[i];

                if (dateFolder.isDirectory()) {
                    String recentFinishedFile = getMostRecentFinishedFile(dateFolder);
                    filename = recentFinishedFile != null ? recentFinishedFile : filename;
                }
            }
        } else {
            for (FileStatus dateFolder : dateFolders) {
                if (dateFolder.isDirectory()) {
                    String recentFinishedFile = getMostRecentFinishedFile(dateFolder);
                    filename = recentFinishedFile != null ? recentFinishedFile : filename;
                }
            }
        }

        System.out.println(filename);

        return folderNameParser.parse(filename);
    }

    public String getMostRecentFinishedFile(FileStatus dateFolder) throws IOException {
        String filename = null;

        Path dateFolderPath = dateFolder.getPath();
        FileStatus[] tweetFiles;

        try (FileSystem fs = FileSystem.get(config)) {
            tweetFiles = fs.listStatus(
                    new Path(constants.FOLDER_TW_COMMENTS_PATH + "/" + dateFolderPath.getName()));
        }

        if (constants.TWITTER_FEEDS_REVERSED) {
            for (int i = tweetFiles.length - 1; i >= 0; i--) {
                FileStatus tweetFile = tweetFiles[i];

                if (fileIsFinished(dateFolderPath, tweetFile)) {
                    filename = dateFolderPath.getName() + tweetFile.getPath().getName().substring(7, 11);
                }
            }
        } else {
            for (FileStatus tweetFile : tweetFiles) {
                if (fileIsFinished(dateFolderPath, tweetFile)) {
                    filename = dateFolderPath.getName() + tweetFile.getPath().getName().substring(7, 11);
                }
            }
        }

        return filename;
    }

    public boolean fileIsFinished(Path dateFolderPath, FileStatus tweetFile) throws IOException {
        Path tweetFilePath = tweetFile.getPath();
        String tweetFilename = tweetFilePath.getName();

        if (tweetFile.isFile() && !tweetFilename.equals("_SUCCESS") &&
                !tweetFilename.contains(constants.FINISHED_SUFFIX) &&
                !tweetFilename.contains(constants.COPYING_SUFFIX)) {
            System.out.println(String.format("To be processed: %s...", dateFolderPath.getName() + tweetFilename));
        }

        return tweetFilename.endsWith(constants.FINISHED_SUFFIX);
    }

    public static void main(String[] args) throws Exception {
        IngestStatusGetter imjson = new IngestStatusGetter();
        System.out.format("Most recent finished time: %s\n", imjson.getMostRecentFinished().getTime() + 600000);
    }
}

package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;

public class FilesRepartitioner {

    private final JavaSparkContext sc;
    Constants constants;
    private String twitterFeedsPath;

    public FilesRepartitioner(JavaSparkContext sc, Constants constants, String twitterFeedsPath) {
        this.sc = sc;
        this.constants = constants;
        this.twitterFeedsPath = twitterFeedsPath;
    }

    public void repartition() throws Exception {
        this.repartition(null);
    }

    public void repartition(String folderPath) throws Exception {
        Configuration config = new HBaseConnector(constants).getConfiguration();

        try (FileSystem fs = FileSystem.get(config)) {
            extractFeed(sc, fs.getFileStatus(new Path(String.format("%s/%s", twitterFeedsPath, folderPath))));
        }
    }

    public void extractFeed(JavaSparkContext sc, FileStatus tweetFile) throws IOException {

        Path tweetFilePath = tweetFile.getPath();
        String tweetFileName = tweetFilePath.getName();

        if (tweetFile.isFile() && !tweetFileName.contains(constants.FINISHED_SUFFIX)
                && !tweetFileName.contains(constants.COPYING_SUFFIX)) {

            System.out.println(String.format("Processing %s...", tweetFileName));

            JavaRDD<String> jsons = sc.textFile(tweetFilePath.toString(), constants.REPARTITION);

            try {
                jsons.saveAsTextFile(tweetFilePath.toString().concat(".partitioned"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Files Repartition");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            FilesRepartitioner repartitioner = new FilesRepartitioner(sc, new Constants(), args[0]);
            repartitioner.repartition(args.length > 1 ? args[1] : null);
        }
    }
}

package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import ph.talas.alexis.engine.LoadRules;
import ph.talas.alexis.engine.TopicAnalyzer;
import ph.talas.alexis.exception.ExcludedTopicException;
import ph.talas.alexis.pojo.Topic;
import java.util.List;
import java.util.Scanner;

public class BiasRulesTester {
    public static void main(String[] args) throws Exception {
        Constants constants = new Constants();
        List<Topic> topics = LoadRules.loadTopics(constants.BIAS_RULES_XML);
        TopicAnalyzer analyzer = new TopicAnalyzer(topics);

        System.out.print("Exit using exit()> ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        while (!input.equals("exit()")) {
            try {
                List<String> matchedTopics = analyzer.match(input);
                System.out.format("Result: %s", matchedTopics);
            } catch (ExcludedTopicException e) {
                System.out.println("Result: unrelated");
            }
            System.out.print("Exit using exit()> ");
            input = scanner.nextLine();
        }
    }
}

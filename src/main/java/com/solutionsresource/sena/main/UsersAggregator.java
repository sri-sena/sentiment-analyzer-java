package com.solutionsresource.sena.main;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTable;
import com.solutionsresource.sena.function.featureselection.FilterOutRetweets;
import com.solutionsresource.sena.function.featuretransformation.GetHasDomain;
import com.solutionsresource.sena.function.featuretransformation.GetUsernameUsageTable;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.log.LogsBuilder;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class UsersAggregator {

    public UsageTable get(JavaRDD<Message> trainingDataset) {
        return trainingDataset
                .map(new GetUsernameUsageTable())
                .reduce(new ReduceUsageTable());
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Users Aggregation");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getMessages()
                .filter(new GetHasDomain("election"))
                .filter(new FilterOutRetweets());

        LogsBuilder stringBuilder = new LogsBuilder(new StringBuilder());

        UsersAggregator usersAggregator = new UsersAggregator();

        stringBuilder.appendln(usersAggregator.get(trainingDataset).toSortedTuples(sc));

        String log = stringBuilder.toString();
        hbase.writeLog(log);

        System.out.println(log);

        sc.close();
    }
}

package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.main.sa.PacquiaoSentimentAnalyzer;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.parse.SaveMessageJsonPartition;
import com.solutionsresource.sena.util.parse.SavePacquiaoMessageJsonPartition;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.Scanner;

public class ImportMessagesFromPacquiaoBradleyMayweather implements AutoCloseable {


    private String specifiedFolder;
    private SentimentAnalyzer analyzer;
    private JavaSparkContext sc;
    private Configuration config;
    private Constants constants;
    private boolean requiresConfirmation;
    public String currentFolderName;

    public ImportMessagesFromPacquiaoBradleyMayweather(JavaSparkContext sc, Constants constants, Configuration config,
                                                       SentimentAnalyzer analyzer) throws Exception {
        this.initialize(sc, constants, config, analyzer, specifiedFolder, false);
    }

    public ImportMessagesFromPacquiaoBradleyMayweather(JavaSparkContext sc, Constants constants, Configuration config,
                                                       SentimentAnalyzer analyzer, boolean requiresConfirmation)
            throws Exception {
        this.initialize(sc, constants, config, analyzer, specifiedFolder, requiresConfirmation);
    }

    public void initialize(JavaSparkContext sc, Constants constants, Configuration config,
                           SentimentAnalyzer analyzer, String specifiedFolder,
                           boolean requiresConfirmation) throws Exception {
        this.sc = sc;
        this.constants = constants;
        this.config = config;
        this.analyzer = analyzer;
        this.specifiedFolder = specifiedFolder;
        this.requiresConfirmation = requiresConfirmation;
    }

    public ImportMessagesFromPacquiaoBradleyMayweather(String specifiedFolder, boolean requiresConfirmation)
            throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Import Messages From Pacquiao-Bradley-Mayweather");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        SentimentAnalyzer analyzer = PacquiaoSentimentAnalyzer.getInstance();

        this.initialize(sc, constants, config, analyzer, specifiedFolder, requiresConfirmation);
    }

    public void readJson() throws Exception {

        try (FileSystem fs = FileSystem.get(config)) {
            FileStatus[] dateFolders = fs.listStatus(new Path(constants.FOLDER_TW_COMMENTS_PATH));

            SaveMessageJsonPartition saveMessageJsonPartition = new SavePacquiaoMessageJsonPartition
                    (constants, analyzer);

            if (constants.TWITTER_FEEDS_REVERSED) {
                for (int i = dateFolders.length - 1; i >= 0; i--) {
                    FileStatus dateFolder = dateFolders[i];

                    if (dateFolder.isDirectory()) {
                        extractFeeds(fs, saveMessageJsonPartition, dateFolder);
                    }
                }
            } else {
//                boolean skipFolder = true;
                for (FileStatus dateFolder : dateFolders) {
                    /*
                    if (skipFolder) {
                        if (currentFolderName == null || currentFolderName.equals(dateFolder.getPath().toString())) {
                            skipFolder = false;
                        } else {
                            continue;
                        }
                    }
                    */

                    if (dateFolder.isDirectory()) {
                        extractFeeds(fs, saveMessageJsonPartition, dateFolder);
                    }
                }
            }
        }
    }

    public void extractFeeds(FileSystem fs, SaveMessageJsonPartition saveMessageJsonPartition, FileStatus dateFolder)
            throws IOException {
        Scanner scanner = new Scanner(System.in);

        do {
            Path dateFolderPath = dateFolder.getPath();

            if (!dateFolderPath.getName().contains(specifiedFolder)) {
                return;
            }

            FileStatus[] tweetFiles = fs.listStatus(
                    new Path(constants.FOLDER_TW_COMMENTS_PATH + "/" + dateFolderPath.getName()));

            saveMessageJsonPartition.setSpecialCase(
                    dateFolderPath.getName().equals(constants.DATE_SPECIFIC_BIAS_RULES_FOLDER_NAME)
            );

            if (constants.TWITTER_FEEDS_REVERSED) {
                for (int i = tweetFiles.length - 1; i >= 0; i--) {
                    FileStatus tweetFile = tweetFiles[i];

                    extractFeed(fs, saveMessageJsonPartition, dateFolderPath, tweetFile);
                }
            } else {
                for (FileStatus tweetFile : tweetFiles) {
                    extractFeed(fs, saveMessageJsonPartition, dateFolderPath, tweetFile);
                }
            }
            if (requiresConfirmation) {
                System.out.print("Press the enter key to run more analyses.\n> ");
            }
        } while (requiresConfirmation && !scanner.nextLine().equals("exit"));
    }

    public void extractFeed(FileSystem fs, SaveMessageJsonPartition saveMessageJsonPartition,
                            Path dateFolderPath, FileStatus tweetFile) throws IOException {
        Path tweetFilePath = tweetFile.getPath();
        if (tweetFile.isFile() && !tweetFilePath.getName().contains(constants.FINISHED_SUFFIX)
                && !tweetFilePath.getName().contains(constants.COPYING_SUFFIX)
                && !tweetFilePath.getName().equals("_SUCCESS")) {

            String dateFolderName = dateFolderPath.toString();

            System.out.println(String.format("Processing %s %s...",
                    dateFolderPath.getName(), tweetFilePath.getName()));

            saveMessageJsonPartition.setFolderName(dateFolderName);

            JavaRDD<String> jsons = sc.textFile(tweetFilePath.toString(), constants.REPARTITION);

            jsons.foreachPartition(saveMessageJsonPartition);

            fs.rename(tweetFilePath, new Path(String.format("%s/%s%s", dateFolderName,
                    tweetFilePath.getName(), constants.FINISHED_SUFFIX)));

            this.currentFolderName = dateFolderName;
        }
    }

    public void close() throws IOException {
        sc.close();
    }

    public static void main(String[] args) throws Exception {
        try (ImportMessagesFromPacquiaoBradleyMayweather imjson = new ImportMessagesFromPacquiaoBradleyMayweather(
                args[0], args.length > 1)) {
            imjson.readJson();
        }
    }
}


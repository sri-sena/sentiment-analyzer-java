package com.solutionsresource.sena.main.template;

import com.solutionsresource.sena.entity.Domain;

import java.util.List;

public interface SentimentAnalyzer {
    List<Domain> analyze(String input);
    List<Domain> analyze(String input, boolean isSpecialCase);
}

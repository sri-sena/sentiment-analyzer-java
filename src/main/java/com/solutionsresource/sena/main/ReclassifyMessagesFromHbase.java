package com.solutionsresource.sena.main;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

public class ReclassifyMessagesFromHbase {

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Reimport Message From Hbase");
        JavaSparkContext sc = new JavaSparkContext(conf);
        Configuration config = new HBaseConnector().getConfiguration();
        Constants constants = new Constants();

        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
        HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
        HBaseDao hbase = new HBaseDao(sc, config);

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        StepByStepSentimentAnalyzer analyzer = new StepByStepSentimentAnalyzer(
                sc, acceptedDomainSlugs, constants, false);

        FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

        filter.addFilter(new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("source"),
                CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

        if (args.length > 2) {
            filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                    constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, CompareOp.EQUAL,
                    new RegexStringComparator(args[3])));
        }

        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes(false))));

        Scan scan = new Scan();
        scan.setFilter(filter);

        if (args.length > 0) {
            long from = Long.parseLong(args[0]);
            long to = Long.parseLong(args[1]);
            scan.setTimeRange(from, to);
        }

        Gson gson = new Gson();

        for (Result result : table.getScanner(scan)) {
            String content = Bytes.toString(result.getValue(
                    constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE));

            if (content == null) {
                continue;
            }

            List<Domain> domains = analyzer.analyze(Bytes.toString(result.getValue(
                    constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE
            )), false);

            byte[] domainsBytes = Bytes.toBytes(gson.toJson(domains));

            Put put = new Put(result.getRow());
            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE,
                    result.rawCells()[0].getTimestamp(), domainsBytes);
            table.put(put);
        }
    }
}

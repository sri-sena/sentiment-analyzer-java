package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featuretransformation.GetDomains;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ExportRandomMessagesFromHbase {

    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static Constants constants;
    private static SimpleDateFormat hbaseFormat;
    private static SimpleDateFormat csvFormat;

    public static void main(String[] args) throws Exception {
        hbaseFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        csvFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        SparkConf conf = new SparkConf().setAppName("SENA Export Random Messages");
        JavaSparkContext sc = new JavaSparkContext(conf);
        Configuration config = new HBaseConnector().getConfiguration();
        constants = new Constants();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

        FileWriter fileWriter = null;
        try {
            System.out.println("Usage: ./ExportRandomMessagesFromHbase.sh <csv_filename> <from_milliseconds>" +
                    "<to_milliseconds> [<optional_hashtag_regex_query>]");
            System.out.println("Example: ./ExportRandomMessagesFromHbase.sh Election-Feb21-Feb22 " +
                    "1456027200000 1456156800000");
            System.out.println("Example: ./ExportRandomMessagesFromHbase.sh Election-Feb21-Feb22-BilangPilipino-DU30 " +
                    "1456027200000 1456156800000 \"BilangPilipino|DU30\"");

            FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

            filter.addFilter(new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("source"),
                    CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

            long minBoundary = Long.parseLong(args[1]);
            long maxBoundary = Long.parseLong(args[2]);

            if (args.length > 3) {
                filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                        constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, CompareOp.EQUAL,
                        new RegexStringComparator(args[3])));
            }

            filter.addFilter(new RandomRowFilter(constants.RANDOM_SAMPLING_CHANCE));

            UsageTable topicUsages = new UsageTable();

            Scan scan = new Scan();
            scan.setFilter(filter);
            scan.setTimeRange(minBoundary, maxBoundary);

            DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
            SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

            DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

            System.out.println(domainSlugs);

            StepByStepSentimentAnalyzer analyzer = new StepByStepSentimentAnalyzer(sc, domainSlugs, constants, true);

            HConnection connection = HConnectionManager.createConnection(config);
            HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
            List<com.solutionsresource.sena.entity.Topic> topics = hbase.getTopics().collect();

            fileWriter = new FileWriter(constants.EXPORTED_CSV_DIRECTORY + "/" + args[0] + ".csv");
            fileWriter.append("id,ref_id,body,source,location,latitude,longitude,username,hashtags," +
                    "posted_date,posted_time,created_date,created_time,finalize,domain,domain_flag,topic,topic_flag," +
                    "positive,neutral,negative,sentiment,sentiment_flag");
            fileWriter.append(NEW_LINE_SEPARATOR);

            int i = 1;

            Iterator<Result> iterator = messageTable.getScanner(scan).iterator();

            while (i < constants.RANDOM_SAMPLING_SIZE + 1 && iterator.hasNext()) {
                Result result = iterator.next();
                System.out.format("%d %s\n", i, Bytes.toString(result.getRow()));

                String content = Bytes.toString(result.getValue(
                        constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE));

                if (content == null) {
                    continue;
                }

                boolean hasTopic = false;

                List<Domain> domains = analyzer.analyze(content, false);

                if (domains == null) {
                    continue;
                }

                for (Domain domain : domains) {
                    for (Topic topic : domain.getTopics()) {
                        Sentiment sentiment = topic.getSentiment();

                        String domainName = domain.getName();
                        String domainFlag = domain.getFlag();

                        String topicName = getTopicWithName(topics, topic.getName()).getDescription();
                        String topicFlag = topic.getFlag();

                        String sentimentName = sentiment.getName();
                        String sentimentFlag = sentiment.getFlag();

                        topicUsages.add(topicName, 1);

                        writeFullDetails(fileWriter, i, result,
                                domainName, domainFlag,
                                topicName, topicFlag,
                                sentimentName, sentimentFlag);

                        hasTopic = true;
                    }
                }

                if (!hasTopic) {
                    writeMessageCommonData(fileWriter, i, result);
                    fileWriter.append(NEW_LINE_SEPARATOR);
                }
                i++;
            }
            System.out.format("Topic Usages: %s\n", topicUsages);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (fileWriter != null) {
                fileWriter.flush();
                fileWriter.close();
            }
        }
    }

    public static void writeFullDetails(FileWriter fileWriter, int i, Result result,
                                        String domainName, String domainFlag,
                                        String topicName, String topicFlag,
                                        String sentimentName, String sentimentFlag)
            throws IOException, ParseException {
        writeMessageCommonData(fileWriter, i, result);
        writeSentiment(fileWriter,
                domainName, domainFlag,
                topicName, topicFlag,
                sentimentName, sentimentFlag);
        fileWriter.append(NEW_LINE_SEPARATOR);
    }

    public static void writeSentiment(FileWriter fileWriter,
                                      String domainName, String domainFlag,
                                      String topicName, String topicFlag,
                                      String sentimentName, String sentimentFlag) throws IOException {
        fileWriter.append(cleanString(domainName));
        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(domainFlag));
        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(topicName));
        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(topicFlag));
        fileWriter.append(COMMA_DELIMITER);

        if (sentimentName.equals("positive")) {
            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("0");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("0");
            fileWriter.append(COMMA_DELIMITER);
        }
        if (sentimentName.equals("neutral")) {
            fileWriter.append("0");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("0");
            fileWriter.append(COMMA_DELIMITER);
        }
        if (sentimentName.equals("negative")) {
            fileWriter.append("0");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("0");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("1");
            fileWriter.append(COMMA_DELIMITER);
        }

        fileWriter.append(cleanString(sentimentName));
        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(sentimentFlag));
        fileWriter.append(COMMA_DELIMITER);
    }

    public static void writeMessageCommonData(FileWriter fileWriter, int i, Result result)
            throws IOException, ParseException {
        fileWriter.append(String.valueOf(i));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getRow())));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_LOCATION_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_LATITUDE_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_LONGITUDE_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        fileWriter.append(cleanString(Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE
        ))));

        fileWriter.append(COMMA_DELIMITER);
        String postedDateTimeStr = Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE
        ));

        writeDateTimeCols(postedDateTimeStr, fileWriter);

        fileWriter.append(COMMA_DELIMITER);
        String createdDateTimeStr = Bytes.toString(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_CREATED_DATETIME_BYTE
        ));

        writeDateTimeCols(createdDateTimeStr, fileWriter);

        fileWriter.append(COMMA_DELIMITER);
        byte[] isFinalized = result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE
        );
        fileWriter.append(cleanString(String.valueOf(isFinalized != null && Bytes.toBoolean(isFinalized))));

        fileWriter.append(COMMA_DELIMITER);
    }

    private static com.solutionsresource.sena.entity.Topic getTopicWithName(
            List<com.solutionsresource.sena.entity.Topic> topics, String name) {

        for (int i = 0; i < topics.size(); i++) {
            if (topics.get(i).getName().equalsIgnoreCase(name))
                return topics.get(i);
        }

        return new com.solutionsresource.sena.entity.Topic(null, name);
    }

    static void writeDateTimeCols(String dateTimeStr, FileWriter fileWriter) throws IOException, ParseException {
        if (dateTimeStr == null) {
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
        } else {
            Date dateTime = hbaseFormat.parse(dateTimeStr);
            dateTimeStr = csvFormat.format(dateTime);

            fileWriter.append(cleanString(StringUtils.substringBefore(dateTimeStr, " ")));//posted_date;
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(cleanString(StringUtils.substringAfter(dateTimeStr, " ")));
        }
    }

    static String cleanString(String string) {
        if (string == null) {
            return "";
        }
        string = string.replaceAll("\"", "\"\"");
        string = string.replaceAll("\n", " ");
        string = "\"" + string + "\"";
        return string.trim();
    }
}

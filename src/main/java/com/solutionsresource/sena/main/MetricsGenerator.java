package com.solutionsresource.sena.main;

import com.solutionsresource.sena.classifier.InitializePredictedTopicSentiments;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.function.featurescaling.ExtractConfusionMatrixContribution;
import com.solutionsresource.sena.function.featurescaling.MergeConfusionMatrix;
import com.solutionsresource.sena.function.featuretransformation.GetDomains;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;

public class MetricsGenerator {

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Metrics Generator");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();
        Constants constants = new Constants();

        HBaseDao hbase = new HBaseDao(sc, config, constants);
        JavaRDD<Message> dataset = hbase.getFinalizedMessages();

        // todo: remove this one
        /*
        dataset = dataset.filter(new Function<Message, Boolean>() {
            public Boolean call(Message message) throws Exception {
                return message.getDomains().size() > 0;
            }
        });
        */

        JavaRDD<Message>[] datasetSplit = dataset.randomSplit(new double[]{0.7, 0.3}, 28L);

        JavaRDD<Message> trainingDataset = datasetSplit[0].repartition(constants.REPARTITION);
        JavaRDD<Message> testDataset = datasetSplit[1].map(new InitializePredictedTopicSentiments());

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

        System.out.println(domainSlugs);

        MachineLearningModelsBuilder builder80 = new MachineLearningModelsBuilder(sc, hbase, domainSlugs, null, false);

        List<Domain> machineLearningModels = builder80.build(trainingDataset);
/*
        MessageClassifier classifier = new MessageClassifier(machineLearningModels);
        testDataset = classifier.classify(testDataset);

        List<String> outputList = new ArrayList<String>();

        PredictionTable predictionTable = testDataset.map(new GetPredictionTable())
                .reduce(new ReducePredictionTables());

        outputList.add("Predictions:");
        outputList.add(predictionTable.toString());

        ProbabilitiesGetter probabilitiesGetter = new ProbabilitiesGetter(domainSlugs);
        outputList.add("Actual:");
        outputList.add(probabilitiesGetter.get(testDataset).toString());

        MetricsGenerator generator = new MetricsGenerator();

        long trainCount = trainingDataset.count();
        long testCount = testDataset.count();
        outputList.add("Training count:  " + trainCount);
        outputList.add("Test count:      " + testCount);

        Integer[][] confusionMatrix = generator.generate(domainSlugs, testDataset);

        Tuple2<Integer, Integer> correctnessRatio = testDataset
                .map(new ConvertToCorrectnessTuple())
                .reduce(new ReduceTuple2());

        Metric metric = new Metric(confusionMatrix, correctnessRatio, trainCount, testCount);
        hbase.insertMetric(metric);

*//*
        domainNames = domainSlugsGetter.get(trainingDataset);

        // todo: Change this at phase 2. Topics don't belong to all domains
        topicNames = topicSlugsGetter.get(trainingDataset);

        sentimentNames = Sentiment.getNames();

        MachineLearningModelsBuilder builder100 = new MachineLearningModelsBuilder(
            sc, domainNames, topicNames, sentimentNames, true);
        builder100.build(dataset);
*//*

        sc.close();

        outputList.add(Arrays.deepToString(confusionMatrix));
        outputList.add("Precision: " + metric.getPrecision() + "%");
        outputList.add("Recall:    " + metric.getRecall() + "%");
        outputList.add("F1 score:  " + metric.getF1Score() + "%");
        outputList.add("Accuracy:  " + metric.getAccuracy() + "%");

        for (String output : outputList) {
            System.out.println(output);
        }*/
    }

    public Integer[][] generate(DomainSlugs domainSlugs, JavaRDD<Message> testDataset) {
        return testDataset
                .map(new ExtractConfusionMatrixContribution(domainSlugs))
                .reduce(new MergeConfusionMatrix());
    }
}

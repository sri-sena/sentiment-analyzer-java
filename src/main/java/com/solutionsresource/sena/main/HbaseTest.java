package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;

public class HbaseTest {
    public static void main(String[] args) throws Exception {
        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        try (
                HConnection connection = HConnectionManager.createConnection(config);
                HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_BYTE))
        ) {
            FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

            filter.addFilter(new RowFilter(CompareFilter.CompareOp.EQUAL,
                    new SubstringComparator("election-april-10")));

            Scan scan = new Scan();
            scan.setFilter(filter);

            try (ResultScanner scanner = table.getScanner(scan)) {
                for (Result result : scanner) {
                    System.out.println(Bytes.toString(result.getRow()));

                    /*
                    Delete delete = new Delete(result.getRow());

                    table.delete(delete);
                    */
                }
            }
        }
    }
}

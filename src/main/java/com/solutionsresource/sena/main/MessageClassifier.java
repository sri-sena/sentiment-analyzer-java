package com.solutionsresource.sena.main;
/*
import com.google.gson.Gson;
import com.solutionsresource.sena.classifier.ClassifyDocuments;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import com.solutionsresource.sena.exception.ClassifierMustBeMockException;
import com.solutionsresource.sena.function.featurescaling.ParseFeature;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.classification.NaiveBayesModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MessageClassifier {

    private List<Domain> domains;
    private boolean isMock = false;

   static Constants constants;
    public MessageClassifier() throws Exception {
        this.isMock = true;
        constants = new Constants();
    }

    public MessageClassifier(List<Domain> domains) throws Exception {
        this.domains = domains;
        constants = new Constants();
    }

    public MessageClassifier(JavaSparkContext sc, DomainSlugs domainSlugs) throws Exception {
        this.domains = new ArrayList<Domain>();

        String modelFolder = constants.NAIVE_BAYES_MODELS_PATH;
        String modelDate = sc.textFile(modelFolder + "active.txt").collect().get(0);
        String folderName = modelFolder + modelDate;

        for (String domainName : domainSlugs.keySet()) {
            TopicSlugs topicSlugs = domainSlugs.get(domainName);

            String domainFolder = folderName + "/domains/" + domainName;
            String domainClassifierFolder = domainFolder + "/domain-recognizer/";

            List<Feature> selectedFeatures = sc.textFile(domainClassifierFolder + "features.txt")
                    .map(new ParseFeature()).collect();

            NaiveBayesModel domainRecognizer = NaiveBayesModel.load(sc.sc(), domainClassifierFolder + "model");

            Domain domain = new Domain(domainName, selectedFeatures, domainRecognizer);

            for (String topicName : topicSlugs.keySet()) {
                String topicFolder = domainFolder + "/topics/" + topicName;
                String topicClassifierFolder = topicFolder + "/topic-recognizer/";

                List<Feature> domainFeatures = sc.textFile(topicClassifierFolder + "features.txt")
                        .map(new ParseFeature()).collect();

                NaiveBayesModel topicRecognizer = NaiveBayesModel.load(sc.sc(), topicClassifierFolder + "model");

                String sentimentAnalyzerFolder = topicFolder + "/sentiment-analyzer/";

                List<Feature> topicFeatures = sc.textFile(sentimentAnalyzerFolder + "features.txt")
                        .map(new ParseFeature()).collect();
                NaiveBayesModel sentimentAnalyzer = NaiveBayesModel.load(sc.sc(), sentimentAnalyzerFolder + "model");

                Topic topic = new Topic(topicName, domainFeatures, topicRecognizer, topicFeatures, sentimentAnalyzer);
                domain.getTopics().add(topic);
            }

            this.domains.add(domain);
        }
        constants = new Constants();
    }

    public Message[] classify(String[] messageContents) throws Exception {
        Message[] messages = new Message[messageContents.length];

        for (int i = 0; i < messages.length; i++) {
            messages[i] = new Message(messageContents[i]);
        }

        return classify(messages);
    }

    public Message[] classify(Message[] messages) throws Exception {
        if (!this.isMock) {
            throw new ClassifierMustBeMockException();
        }

        Topic topic = new Topic("jejomar-binay");
        topic.setSentiment(new Sentiment("negative"));

        for (Message message : messages) {
            message.addPredictedTopic("election", topic);
        }

        return messages;
    }

    public JavaRDD<Message> classify(JavaRDD<Message> testDataset) throws Exception {
        JavaRDD<Message> cleanedDocuments = testDataset
                .map(new ContentCleaner(
                        new RemoveLinks(constants),
                        new RemoveStopWords(),
                        new RemoveHandlers(constants),
                        new DatesNormalizer(),
                        new EmojiNormalizer()
                )).map(new ExtractNGrams());

        for (Domain domain : domains) {

            NaiveBayesModel domainRecognizer = domain.getNaiveBayesModel();
            List<Feature> selectedFeatures = domain.getSelectedFeatures();

            for (Topic topic : domain.getTopics()) {
                List<Feature> domainFeatures = topic.getTopicSelectedFeatures();
                List<Feature> topicFeatures = topic.getSentimentSelectedFeatures();

                NaiveBayesModel topicRecognizer = topic.getTopicNaiveBayesModel();
                NaiveBayesModel sentimentAnalyzer = topic.getSentimentNaiveBayesModel();

                ClassifyDocuments classifyDocuments = new ClassifyDocuments(
                        domainRecognizer, topicRecognizer, sentimentAnalyzer, domain.getSlug(), topic.getSlug(),
                        selectedFeatures, domainFeatures, topicFeatures, constants.DOMAIN_BIASES);

                cleanedDocuments = cleanedDocuments.map(classifyDocuments);
            }
        }

        return cleanedDocuments;
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();
        config.set("timeout", "120000");

        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
        HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));

        FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

        filter.addFilter(new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("source"),
                CompareFilter.CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, CompareFilter.CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes(false))));

        Scan scan = new Scan();
        scan.setFilter(filter);

        Iterator<Result> iterator = table.getScanner(scan).iterator();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

        *//*
        JavaRDD<Message> testDataset = hbase.getUnfinalizedMessages();

        List<com.solutionsresource.sena.entity.Topic> topics = hbase.getTopics().collect();
        *//*

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

        System.out.println(domainSlugs);

        int i = 0;

        Gson gson = new Gson();
        DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        StepByStepSentimentAnalyzer analyzer = new StepByStepSentimentAnalyzer(sc, domainSlugs, constants, false);

        while (iterator.hasNext()) {
            Result result = iterator.next();
            String content = Bytes.toString(result.getValue(
                    constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE));

            if (content == null) {
                continue;
            }

            List<Domain> domains = analyzer.analyze(content, false);
            for (Domain domain : domains) {
                for (Topic topic : domain.getTopics()) {
                    if (topic.getName() != null && !topic.getName().isEmpty()) {
                        topic.setName(topic.getName().replaceAll("-", " "));
                    }
                }
            }

            long timestamp = df.parse(Bytes.toString(result.getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                    constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE))).getTime();

            Put put = new Put(result.getRow());
            Delete del = new Delete(result.getRow());

            del.deleteColumn(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE);
            put.add(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE,
                    timestamp, Bytes.toBytes(gson.toJson(domains)));

            table.delete(del);
            table.put(put);

            System.out.println(i++);
        }

        *//*
        MessageClassifier classifier = new MessageClassifier(sc, domainSlugs);

        PredictionTable predictionTable = classifier.classify(testDataset).map(new UpdateDomains())
                .map(new GetPredictionTable()).reduce(new ReducePredictionTables());

        System.out.println(predictionTable);
        *//*

        sc.close();
    }
}*/

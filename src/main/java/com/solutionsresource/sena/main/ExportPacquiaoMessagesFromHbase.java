package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.function.featureselection.FilterOutEmpty;
import com.solutionsresource.sena.function.featuretransformation.ConvertToPacquiaoCsv;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class ExportPacquiaoMessagesFromHbase {

    public static void main(String[] args) throws Exception {
        String folderName = args[0];

        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        SparkConf conf = new SparkConf().setAppName("SENA Export Pacquiao Messages to CSV");

        try (
                JavaSparkContext sc = new JavaSparkContext(conf);
                HBaseDao hbase = new HBaseDao(sc, config)
        ) {

            System.out.println("Usage: ./ExportPacquiaoMessagesFromHbase.sh <csv_filename> <from_milliseconds>" +
                    "<to_milliseconds> [<optional_hashtag_regex_query>]");
            System.out.println("Example: ./ExportPacquiaoMessagesFromHbase.sh Election-Feb21-Feb22 " +
                    "1456027200000 1456156800000");
            System.out.println("Example: ./ExportPacquiaoMessagesFromHbase.sh Election-Feb21-Feb22-BilangPilipino-DU30 " +
                    "1456027200000 1456156800000 \"BilangPilipino|DU30\"");

            String filenamePrefix = String.format("%s/%s/0000-", constants.HDFS_EXPORTED_CSV_PATH, folderName);

            String candidatesFile = String.format("%s.csv", filenamePrefix);

            JavaRDD<ResultHashMap> messages = hbase.getPacquiaoMessages();

            JavaRDD<String> candidatesMessages = convertToCsv(constants, messages);

            System.out.format("Writing %s...", candidatesFile);
            candidatesMessages.saveAsTextFile(candidatesFile);

            messages.unpersist();
        }
    }

    public static JavaRDD<String> convertToCsv(Constants constants, JavaRDD<ResultHashMap> messages) {
        return messages.map(new ConvertToPacquiaoCsv(constants))
                .filter(new FilterOutEmpty());
    }
}

package com.solutionsresource.sena.main;

import au.com.bytecode.opencsv.CSVParser;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.MultiplierTable;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.ExtractFeatures;
import com.solutionsresource.sena.function.featurescaling.GetWeightedLabeledPoints;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTable;
import com.solutionsresource.sena.function.featureselection.GetNGramsWithKey;
import com.solutionsresource.sena.function.featureselection.TotalCount;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.util.build.Features;
import com.solutionsresource.sena.util.build.ModelBuilder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.classification.NaiveBayesModel;
import scala.Tuple2;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class SwsSentimentAnalyzer implements Serializable {
    private final RemoveLinks removeLinks;
    private final RemoveHandlers removeHandlers;
    private final RemoveStopWords removeStopWords;
    private final ExtractNGrams extractNgrams;

    static class TopicAnalyzer {

        private Pattern pattern;

        public TopicAnalyzer(Pattern pattern) {
            this.pattern = pattern;
        }

        public boolean accepts(String content) {
            return pattern.matcher(content).matches();
        }
    }

    public SwsSentimentAnalyzer() throws Exception {
        Constants constants = new Constants();

        String domainName = "surveys";
        String topic1 = "BilangPilipinoSWSMobileSurvey";
        String[] sentimentNames = Sentiment.getNames();

        this.removeLinks = new RemoveLinks(constants);
        this.removeHandlers = new RemoveHandlers(constants);
        this.removeStopWords = new RemoveStopWords();
        this.extractNgrams = new ExtractNGrams(constants);

        TopicAnalyzer topicAnalyzer = new TopicAnalyzer(Pattern.compile(".+"));

        List<Double> labels = new ArrayList<>();

        List<Message> messages = new ArrayList<>();

        Path trainingCsv = new Path(constants.ACE_DEV_FOLDER_PATH + "sws-training-data.xlsx");

        FileSystem fs = FileSystem.get(new Configuration());
        FSDataInputStream fis = fs.open(trainingCsv);

        InputStream is = fis;
        XSSFWorkbook workbook = new XSSFWorkbook(is);

        int sheetNo = workbook.getNumberOfSheets();

        for (int sheetIndex = 0; sheetIndex < sheetNo; sheetIndex++) {
            XSSFSheet sheet = workbook.getSheetAt(sheetIndex);

            Iterator<Row> rowIterator = sheet.iterator();

            if (rowIterator.hasNext()) {
                rowIterator.next();
            }

            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();

                Iterator<Cell> cellIterator = row.cellIterator();

                cellIterator.next(); // comment id
                cellIterator.next(); // created at
                cellIterator.next(); // username
                cellIterator.next(); // user id
                cellIterator.next(); // comment likes

                String content = cellIterator.next().getStringCellValue();

                cellIterator.next(); // topic

                String sentiment = cellIterator.next().getStringCellValue().trim();

                Message message = createMessage(content);

                Domain media = new Domain(domainName);

                labels.add(sentimentToDouble(sentiment));
                messages.add(message);

                media.addTopic(new Topic("survey", "training", new Sentiment(sentiment), false));

                message.getDomains().add(media);
            }
        }

        DomainSlugs domainSlugs = new DomainSlugs(new Tuple2[]{
                new Tuple2<>(
                        domainName,
                        new TopicSlugs(
                                new Tuple2[]{
                                        new Tuple2<>(
                                                topic1,
                                                sentimentNames
                                        ),
                                }
                        )
                )
        });

        System.out.println(domainSlugs.toString());

        SparkConf conf = new SparkConf().setAppName("SENA SWS");

        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<Message> trainingDataset1 = sc.parallelize(messages);

        Tuple2<NaiveBayesModel, List<Feature>> model1 = createModel(sc, trainingDataset1, topic1);

        sc.close();

        NaiveBayesModel sentimentAnalyzer = model1._1();
        List<Feature> features = model1._2();

        ExtractFeatures extractFeatures = new ExtractFeatures();

        CSVParser parser = new CSVParser();

        BufferedReader reader = new BufferedReader(new FileReader(new File("input.csv")));
        PrintWriter writer = new PrintWriter(new FileWriter(new File("output.csv")));

        writer.append("id,ref_id,body,source,location,latitude,longitude,username,hashtags," +
                "posted_date,posted_time,created_date,created_time,finalize,domain,topic," +
                "positive,neutral,negative,sentiment\n");

        String line;
        String prev = "";

        reader.readLine();

        while ((line = reader.readLine()) != null) {
            String[] values;
            try {
                values = parser.parseLine(prev + line);

                if (values.length < 15) {
                    throw new ArrayIndexOutOfBoundsException();
                }

                prev = "";

            } catch (IOException exception) {
                System.out.println(line);
                exception.printStackTrace();

                prev += String.format("%s ", line);

                continue;
            } catch (ArrayIndexOutOfBoundsException exception) {
                System.out.println(line);
                exception.printStackTrace();
                continue;
            }

            boolean hasTopic = false;

            if (topicAnalyzer.accepts(line)) {
                int sentimentValue = (int) predict(createMessage(values[2]), extractFeatures,
                        features, sentimentAnalyzer);

                String sentiment = Sentiment.nameLookup(sentimentValue);

                writer.write(commonData(values));
                writer.write(String.format("%s,%s,%s,%s,%s\n",
                                cleanString("survey"),

                                cleanString(sentimentValue == 3 ? 1 : 0),
                                cleanString(sentimentValue == 2 ? 1 : 0),
                                cleanString(sentimentValue == 1 ? 1 : 0),

                                cleanString(sentiment))
                );

                hasTopic = true;
            }

            if (!hasTopic) {
                writer.write(commonData(values));
                writer.write("\n");
            }
        }

        reader.close();
        writer.close();
    }

    private String commonData(String[] values) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < 15; i++) {
            builder.append(cleanString(values[i]));
            builder.append(",");
        }

        return builder.toString();
    }

    static String cleanString(Object string) {
        if (string == null) {
            return "";
        }

        return String.format("\"%s\"", string.toString().replaceAll("\"", "\"\"").replaceAll("\n", " "));
    }

    public static void main(String[] args) throws Exception {
        new SwsSentimentAnalyzer();
    }

    private double predict(Message message, ExtractFeatures extractFeatures,
                           List<Feature> features, NaiveBayesModel classifier) throws Exception {
        extractFeatures.setFeatures(features);
        return classifier.predict(extractFeatures.call(message));
    }

    public Message createMessage(String content) throws Exception {
        Message message = new Message("null", content, new ArrayList<Domain>(), true);

        removeLinks.call(message);
//        System.out.println(message.getContent());

//        System.out.println("\n\nRemoving handlers...\n");

        removeHandlers.call(message);
//        System.out.println(message.getContent());

//        System.out.println("\n\nRemoving stop words...\n");

        removeStopWords.call(message);
//        System.out.println(message.getContent());

//        System.out.println("\n\nExtracting ngrams (n=" + Config.NGRAMS_MAX + ")...\n");

        extractNgrams.call(message);
//        System.out.println(message.getNgrams() + "\n\n");
//        System.out.println(message.logNgrams());
        return message;
    }

    public Tuple2<NaiveBayesModel, List<Feature>> createModel(JavaSparkContext sc, JavaRDD<Message> trainingDataset, String className) {
        long trainingDatasetCount = trainingDataset.count();

        HashMap<String, UsageTable> allNGramsOfTopic = trainingDataset
                .map(new GetNGramsWithKey(new GetSentimentOfTopic("media", className))).reduce(new TotalCount());

        System.out.println("Building models for topic " + className + "'s sentiments...");

        UsageTable sentimentUsages = trainingDataset.map(new ConvertToUsageTable(new GetSentimentOfTopic(
                "media", className))).reduce(new ReduceUsageTable());

        MultiplierTable sentimentMultipliers = MultiplierTable.create(true);

        List<Feature> topicFeatures = Features.getKeys(sc, allNGramsOfTopic, trainingDatasetCount, sentimentUsages);

        Broadcast<List<Feature>> broadcastedTopicFeatures = sc.broadcast(topicFeatures);

        JavaRDD<WeightedLabeledPoint> weightedTrainingOfTopic = trainingDataset.map(
                new GetWeightedLabeledPoints(broadcastedTopicFeatures, "media", className, Sentiment.getNames(),
                        sentimentMultipliers
                )
        );

        return buildModel(
                sc, weightedTrainingOfTopic, topicFeatures, null, Sentiment.getNames(), false
        );
    }

    public Tuple2<NaiveBayesModel, List<Feature>> buildModel(
            JavaSparkContext sc,
            JavaRDD<WeightedLabeledPoint> weightedTraining,
            List<Feature> features, String folderName,
            String[] labels, boolean shouldUndersample) {

        return new ModelBuilder(sc, false, shouldUndersample)
                .build(weightedTraining, features, folderName, labels);
    }

    private Double sentimentToDouble(String sentiment) {
        if (sentiment.equals("positive")) {
            return 3.0;
        }
        if (sentiment.equals("neutral")) {
            return 2.0;
        }
        return 1.0;
    }
}

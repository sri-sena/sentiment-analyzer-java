package com.solutionsresource.sena.main;

import com.solutionsresource.sena.config.Config;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.bias.DomainBiases;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import com.solutionsresource.sena.function.featurescaling.ExtractFeatures;
import com.solutionsresource.sena.function.featurescaling.ParseFeature;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.model.SlugsGetter;
import com.solutionsresource.sena.util.parse.ElectionIssueClassifier;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.classification.NaiveBayesModel;
import ph.talas.alexis.engine.LoadRules;
import ph.talas.alexis.engine.TopicAnalyzer;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class StepByStepSentimentAnalyzer implements Serializable, SentimentAnalyzer {

    private final TopicAnalyzer topicAnalyzer;
    private final TopicAnalyzer specialTopicAnalyzer;
    private final ContentCleaner contentCleaner;
    private final ElectionIssueClassifier electionIssueClassifier;
    private ArrayList<Domain> domains;
    private ExtractFeatures extractFeatures;
    private final ExtractNGrams extractNgrams;
    private final boolean canLog;

    Constants constants;

    public StepByStepSentimentAnalyzer(JavaSparkContext sc, DomainSlugs domainSlugs,
                                       Constants constants, boolean canLog) throws Exception {
        this.constants = constants;
        this.canLog = canLog;

        log("\n\nInitializing...\n");

        this.domains = new ArrayList<>();
        this.extractFeatures = new ExtractFeatures(new ArrayList<String>());
        this.contentCleaner = new ContentCleaner(
                new RemoveLinks(constants),
                new RemoveStopWords(),
                new RemoveHandlers(constants),
                new DatesNormalizer(),
                new EmojiNormalizer());
        this.extractNgrams = new ExtractNGrams(constants);
        this.constants = constants;

        List<ph.talas.alexis.pojo.Topic> specialTopics = LoadRules.loadTopics(
                constants.DATE_SPECIFIC_BIAS_RULES_XML_PATH);
        List<ph.talas.alexis.pojo.Topic> topics = LoadRules.loadTopics(constants.BIAS_RULES_XML);
        topicAnalyzer = new TopicAnalyzer(topics);
        specialTopicAnalyzer = new TopicAnalyzer(specialTopics);

        String modelFolder = constants.NAIVE_BAYES_MODELS_PATH;
        String modelDate = sc.textFile(modelFolder + "active.txt").collect().get(0);
        String folderName = modelFolder + modelDate;

        for (String domainName : domainSlugs.keySet()) {
            TopicSlugs topicSlugs = domainSlugs.get(domainName);

            String domainFolder = folderName + "/domains/" + domainName;
            String domainClassifierFolder = domainFolder + "/domain-recognizer/";

            List<Feature> selectedFeatures = sc.textFile(domainClassifierFolder + "features.txt")
                    .map(new ParseFeature()).collect();

            NaiveBayesModel domainRecognizer = NaiveBayesModel.load(sc.sc(), domainClassifierFolder + "model");

            Domain domain = new Domain(domainName, selectedFeatures, domainRecognizer);

            for (String topicName : topicSlugs.keySet()) {
                try {
                  String topicFolder = domainFolder + "/topics/" + topicName;
                  String topicClassifierFolder = topicFolder + "/topic-recognizer/";

                  List<Feature> domainFeatures = sc.textFile(topicClassifierFolder + "features.txt")
                      .map(new ParseFeature()).collect();

                  NaiveBayesModel topicRecognizer = NaiveBayesModel.load(sc.sc(), topicClassifierFolder + "model");

                  String sentimentAnalyzerFolder = topicFolder + "/sentiment-analyzer/";

                  List<Feature> topicFeatures = sc.textFile(sentimentAnalyzerFolder + "features.txt")
                      .map(new ParseFeature()).collect();

                  NaiveBayesModel sentimentAnalyzer = NaiveBayesModel.load(sc.sc(), sentimentAnalyzerFolder + "model");

                  Topic topic = new Topic(topicName, domainFeatures, topicRecognizer, topicFeatures, sentimentAnalyzer);
                  domain.getTopics().add(topic);
                } catch (Exception e) {
                  e.printStackTrace();
                }
            }

            this.domains.add(domain);
        }

        electionIssueClassifier = new ElectionIssueClassifier();
    }

    public static StepByStepSentimentAnalyzer getInstance(JavaSparkContext sc, Constants constants, HBaseDao hbase,
                                                          JavaRDD<Message> trainingDataset) throws Exception {
        /*
        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);
        */
        DomainSlugs domainSlugs = new AcceptedSlugsGetter(hbase).get();

        System.out.println(domainSlugs);

        return new StepByStepSentimentAnalyzer(sc, domainSlugs, constants, true);
    }

    private void log(Object object) {
        if (this.canLog) {
            String content = object.toString();

            try {
                BufferedWriter writer = new BufferedWriter(new FileWriter(new File(constants.LOGS_FILE), true));

                writer.write(content);

                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private double predict(Message message, List<Feature> features, NaiveBayesModel classifier) throws Exception {
        extractFeatures.setFeatures(features);
        return classifier.predict(extractFeatures.call(message));
    }

    public List<Domain> analyze(String input) {
        return analyze(input, false);
    }
    public List<Domain> analyze(String input, boolean isSpecialCase) {
        List<Domain> predictedDomains = new ArrayList<Domain>();

        log("\n\n\n[" + new Date().toString() + "] " + input);

        try {
            log("\nCreating message...\n");

            Message message = new Message(input);
            log(message.getContent());

            log("\n\nCleaning content...\n");

            message.setContent(contentCleaner.call(message.getContent()));
            log(message.getContent());

            log("\n\nExtracting ngrams (n=" + Config.NGRAMS_MAX + ")...\n");

            extractNgrams.call(message);
            log(message.getNgrams() + "\n\n");
            log(message.logNgrams());

            for (Domain domain : domains) {
                log("\n\nClassifying for " + domain.getName() + " domain...");

                NaiveBayesModel domainRecognizer = domain.getNaiveBayesModel();

                boolean isDomain = false;

                DomainBiases electionBiases = constants.DOMAIN_BIASES;

                StringBuilder domainLog = new StringBuilder();

                String domainFlag = null;
                try {
                    double domainBiasScore = electionBiases.get(domain.getName()).score(message);

                    if (domainBiasScore < 0) {
                        domainBiasScore = topicAnalyzer.score(input);

                        if (isSpecialCase && domainBiasScore != 0.0) {
                            domainBiasScore = specialTopicAnalyzer.score(input);
                        }
                    } else {
                        domainFlag = "eb";
                        log("bias1 ");
                    }

                    if (domainBiasScore < 0) {
                        domainBiasScore = this.predict(message, domain.getSelectedFeatures(), domainRecognizer);
                        domainFlag = "da";
                    } else if (domainFlag == null) {
                        domainFlag = "pp";
                        log("bias2 ");
                    }

                    isDomain = domainBiasScore > 0;

                    domainLog.append(isDomain);
                } catch (IllegalArgumentException e) {
                    domainLog.append("error");
                    e.printStackTrace();
                }

                if (isDomain) {
                    isDomain = false;

                    List<Topic> predictedTopics = new ArrayList<Topic>();

                    for (Topic topic : domain.getTopics()) {
                        String topicName = topic.getName();

                        domainLog.append("\nClassifying for ").append(topicName).append(" topic...");

                        NaiveBayesModel topicRecognizer = topic.getTopicNaiveBayesModel();

                        boolean isTopic = false;

                        String topicFlag = null;

                        try {
                            double topicBiasScore = electionBiases.get(domain.getName()).topicScore(topicName, message);

                            if (topicBiasScore < 0) {
                                topicBiasScore = topicAnalyzer.score(input, topicName);
                            } else {
                                topicFlag = "tb";
                                domainLog.append("bias1 ");
                            }

                            if (topicBiasScore < 0) {
                                topicBiasScore = this.predict(message,
                                        topic.getTopicSelectedFeatures(), topicRecognizer);
                                topicFlag = "ta";
                            } else if (topicFlag == null) {
                                topicFlag = "pp";
                                domainLog.append("bias2 ");
                            }

                            isTopic = topicBiasScore > 0;

                            domainLog.append(isTopic);
                        } catch (IllegalArgumentException e) {
                            domainLog.append("error");
                            e.printStackTrace();
                        }

                        if (isTopic) {
                            isDomain = true;
                            domainLog.append("\nClassifying for ").append(topicName).append(" topic's sentiment...");

                            NaiveBayesModel sentimentAnalyzer = topic.getSentimentNaiveBayesModel();

                            String sentiment = "error";

                            String sentimentFlag = null;

                            try {
                                double sentimentBiasScore = electionBiases.get(domain.getName()).sentimentScore(topicName, message);

                                if (sentimentBiasScore < 0) {
                                    sentimentFlag = "sa";
                                    sentimentBiasScore = this.predict(
                                            message, topic.getSentimentSelectedFeatures(), sentimentAnalyzer);
                                } else {
                                    domainLog.append("bias ");
                                    sentimentFlag = "sb";
                                }

                                sentiment = Sentiment.nameLookup((int) sentimentBiasScore);
                            } catch (IllegalArgumentException e) {
                                e.printStackTrace();
                            }

                            domainLog.append(sentiment);

                            predictedTopics.add(new Topic(topicName, topicFlag,
                                    new Sentiment(sentiment, null, sentimentFlag), false));
                        }
                    }

                    if (!predictedTopics.isEmpty()) {
                        predictedDomains.add(new Domain(domain.getName(), domain.getName(),
                                domainFlag, false, predictedTopics));
                    }
                }

                if (isDomain) {
                    log(domainLog.toString());
                } else {
                    log(false);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> electionIssueTopics = electionIssueClassifier.classify(input);

        if (electionIssueTopics.size() > 0) {
            ArrayList<Topic> predictedTopics = new ArrayList<>();

            for (String topicName : electionIssueTopics) {
                predictedTopics.add(new Topic(topicName, "na", new Sentiment("na", null, "na"), false));
            }

            predictedDomains.add(new Domain("election-issue", "election-issue", "na", false, predictedTopics));
        }

        return predictedDomains;
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Step By Step Sentiment Analysis");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

        DomainSlugs acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();
        SlugsGetter domainSlugsGetter = new SlugsGetter(new GetDomains(acceptedDomainSlugs));

        DomainSlugs domainSlugs = domainSlugsGetter.get(trainingDataset);

        System.out.println(domainSlugs);

        StepByStepSentimentAnalyzer analyzer = new StepByStepSentimentAnalyzer(sc, domainSlugs, constants, true);

        Scanner scanner = new Scanner(System.in);

        System.out.print("Exit using exit() >");
        String input = scanner.nextLine();

        while (!input.equals("exit()")) {
            analyzer.analyze(input, false);

            System.out.print("Exit using exit() >");
            input = scanner.nextLine();
        }

        hbase.close();
        sc.close();
    }
}

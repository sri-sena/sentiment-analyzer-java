package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

public class HistoricalIngestor {

    private final FilesRepartitioner repartitioner;
    private final ImportMessagesFromFolder importer;

    public HistoricalIngestor(FilesRepartitioner repartitioner, ImportMessagesFromFolder importer) {
        this.repartitioner = repartitioner;
        this.importer = importer;
    }

    private void ingest(String path) throws Exception {
        repartitioner.repartition(path);
        importer.readJson(path.concat(".partitioned"));
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            throw new IllegalArgumentException("Needs 1 arguments: <twitter feeds path> [<tweet file name>]");
        }

        Constants constants = new Constants();
        String twitterFeedsPath = args[0];

        SparkConf conf = new SparkConf().setAppName("SENA Historical Ingest");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            FilesRepartitioner repartitioner = new FilesRepartitioner(sc, constants, twitterFeedsPath);
            ImportMessagesFromFolder importer = new ImportMessagesFromFolder(sc, constants, twitterFeedsPath);

            HistoricalIngestor ingestor = new HistoricalIngestor(repartitioner, importer);
            ingestor.ingest(args.length > 1 ? args[1] : null);
        }
    }
}

package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.parse.SaveMessageJsonPartition;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;

public class ImportMessagesFromFolder {
    private String twitterFeedsFolder;
    private SentimentAnalyzer analyzer;
    private JavaSparkContext sc;
    private Configuration config;
    private Constants constants;

    public ImportMessagesFromFolder(JavaSparkContext sc, Constants constants, Configuration config,
                                    SentimentAnalyzer analyzer, String specifiedFolderPath)
            throws Exception {
        this.initialize(sc, constants, config, analyzer, specifiedFolderPath);
    }

    public void initialize(JavaSparkContext sc, Constants constants, Configuration config,
                           SentimentAnalyzer analyzer, String twitterFeedsFolder)
            throws Exception {
        this.sc = sc;
        this.constants = constants;
        this.config = config;
        this.analyzer = analyzer;
        this.twitterFeedsFolder = twitterFeedsFolder;
    }

    public ImportMessagesFromFolder(JavaSparkContext sc, Constants constants, String twitterFeedsPath)
            throws Exception {
        this.constants = constants;
        Configuration config = new HBaseConnector(constants).getConfiguration();

        try (HBaseDao hbase = new HBaseDao(sc, config)) {
            SentimentAnalyzer analyzer = StepByStepSentimentAnalyzer.getInstance(
                    sc, constants, hbase, hbase.getFinalizedMessages());

            this.initialize(sc, constants, config, analyzer, twitterFeedsPath);
        }
    }

    public void readJson() throws Exception {
        readJson(null);
    }

    public void readJson(String specifiedFolder) throws Exception {
        Path twitterFeedsPath = new Path(this.twitterFeedsFolder);

        FileStatus[] dateFolders;
        try (FileSystem fs = FileSystem.get(config)) {
            dateFolders = fs.listStatus(twitterFeedsPath);
        }

        SaveMessageJsonPartition saveMessageJsonPartition = new SaveMessageJsonPartition(constants, analyzer);

        if (constants.TWITTER_FEEDS_REVERSED) {
            for (int i = dateFolders.length - 1; i >= 0; i--) {
                FileStatus dateFolder = dateFolders[i];

                if (dateFolder.isDirectory()) {
                    extractFeeds(saveMessageJsonPartition, dateFolder.getPath(), specifiedFolder != null ?
                            new Path(String.format("%s/%s", twitterFeedsPath, specifiedFolder)) : null);
                }
            }
        } else {
            for (FileStatus dateFolder : dateFolders) {
                if (dateFolder.isDirectory()) {
                    extractFeeds(saveMessageJsonPartition, dateFolder.getPath(), specifiedFolder != null ?
                            new Path(String.format("%s/%s", twitterFeedsPath, specifiedFolder)) : null);
                }
            }
        }
    }

    public void extractFeeds(SaveMessageJsonPartition saveMessageJsonPartition, Path dateFolderPath, Path specifiedPath)
            throws IOException, InterruptedException {
        boolean isScanningInReversedOrder = constants.TWITTER_FEEDS_REVERSED;

        if (!pathsAreEqual(specifiedPath, dateFolderPath)) {
            System.out.println(String.format("Skipping %s...", dateFolderPath));
            return;
        }

        FileStatus[] tweetFiles;

        try (FileSystem fs = FileSystem.get(config)) {
            tweetFiles = fs.listStatus(dateFolderPath);
        }

        saveMessageJsonPartition.setSpecialCase(
                dateFolderPath.getName().equals(constants.DATE_SPECIFIC_BIAS_RULES_FOLDER_NAME)
        );

        System.out.println(String.format("Scanning%s...",
                isScanningInReversedOrder ? " in reversed order" : ""));

        if (isScanningInReversedOrder) {
            for (int i = tweetFiles.length - 1; i >= 0; i--) {
                FileStatus tweetFile = tweetFiles[i];

                extractFeed(saveMessageJsonPartition, dateFolderPath, tweetFile);
            }
        } else {
            for (FileStatus tweetFile : tweetFiles) {
                extractFeed(saveMessageJsonPartition, dateFolderPath, tweetFile);
            }
        }
    }

    public boolean pathsAreEqual(Path folderNamePath, Path dateFolderPath) {
        return folderNamePath == null || folderNamePath.getName().equals(dateFolderPath.getName());
    }

    public void extractFeed(SaveMessageJsonPartition saveMessageJsonPartition,
                            Path dateFolderPath, FileStatus tweetFile) throws IOException {
        Path tweetFilePath = tweetFile.getPath();

        System.out.println(String.format("Checking %s %s...\n",
                dateFolderPath.getName(), tweetFilePath.getName()));

        if (tweetFile.isFile() && !tweetFilePath.getName().contains(constants.FINISHED_SUFFIX)
                && !tweetFilePath.getName().contains(constants.COPYING_SUFFIX)
                && !tweetFilePath.getName().equals("_SUCCESS")) {

            String dateFolderName = dateFolderPath.toString();

            System.out.println(String.format("Processing %s %s...",
                    dateFolderPath.getName(), tweetFilePath.getName()));

            saveMessageJsonPartition.setFolderName(dateFolderName);

            JavaRDD<String> jsons = sc.textFile(tweetFilePath.toString(), constants.REPARTITION);

            jsons.foreachPartition(saveMessageJsonPartition);

            try (FileSystem fs = FileSystem.get(config)) {
                fs.rename(tweetFilePath, new Path(String.format("%s/%s%s", dateFolderName,
                        tweetFilePath.getName(), constants.FINISHED_SUFFIX)));
            }
        }
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Import From Folder");
        try (JavaSparkContext sc = new JavaSparkContext(conf)) {
            ImportMessagesFromFolder imjson = new ImportMessagesFromFolder(sc, new Constants(), args[0]);
            imjson.readJson(args.length > 1 ? args[1] : null);
        }
    }
}

package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.function.featurescaling.ReduceUsageTable;
import com.solutionsresource.sena.function.featureselection.GetNgrams;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.log.LogsBuilder;
import org.apache.hadoop.conf.Configuration;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class MostUsedWordsGetter {

    private Constants constants;

    public MostUsedWordsGetter(Constants constants) {
        this.constants = constants;
    }

    public UsageTable get(JavaRDD<Message> trainingDataset) throws Exception {
        return trainingDataset
                .map(new ContentCleaner(
                        new RemoveLinks(constants),
                        new RemoveStopWords(),
                        new RemoveHandlers(constants),
                        new DatesNormalizer(),
                        new EmojiNormalizer()
                )).map(new ExtractNGrams(2))
                .map(new GetNgrams())
                .reduce(new ReduceUsageTable());
    }

    public static void main(String[] args) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Configuration config = new HBaseConnector().getConfiguration();

        HBaseDao hbase = new HBaseDao(sc, config);
        JavaRDD<Message> trainingDataset = hbase.getFinalizedMessages();

        LogsBuilder stringBuilder = new LogsBuilder(new StringBuilder());

        MostUsedWordsGetter getter = new MostUsedWordsGetter(new Constants());

        UsageTable ngrams = getter.get(trainingDataset);
        stringBuilder.appendln(ngrams.toSortedTuples(sc));

        String log = stringBuilder.toString();
//        hbase.writeLog(log);

        System.out.println(log);

        sc.close();
    }
}

package com.solutionsresource.sena.main;

import au.com.bytecode.opencsv.CSVParser;
import com.solutionsresource.sena.entity.hashmap.DomainHashMap;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.util.hbase.AcceptedSlugsGetter;
import com.solutionsresource.sena.util.hbase.HBaseDao;

import java.io.BufferedReader;
import java.io.IOException;

public class ImportMessagesFromCsv {
  private final CSVParser parser;
  private final HBaseDao hbase;
  private final BufferedReader reader;
  private final String source;
  private final DomainSlugs acceptedDomainSlugs;

  public ImportMessagesFromCsv(HBaseDao hbase, BufferedReader reader, String source) throws IOException {
    this.hbase = hbase;
    this.reader = reader;
    this.source = source;
    this.parser = new CSVParser();
    this.acceptedDomainSlugs = new AcceptedSlugsGetter(hbase).get();

    System.out.println(acceptedDomainSlugs);
  }

  public void read() throws IOException {

    // read header line
    reader.readLine();

    int lineNumber = 1;

    DomainHashMap domains = new DomainHashMap();

    String id = null;
    String[] prevValues = null;

    String prevLine = "";
    for (String line = reader.readLine(); line != null; line = reader.readLine()) {
      String[] values;
      try {
        values = parser.parseLine(prevLine + line);

        if (values.length < 27) {
          throw new ArrayIndexOutOfBoundsException();
        }

        prevLine = "";

      } catch (IOException | ArrayIndexOutOfBoundsException exception) {
        System.out.println(line);
        exception.printStackTrace();

        prevLine += String.format("%s ", line);

        continue;
      }

      for (int i = 0; i < values.length; i++) {
        values[i] = values[i].trim();
      }

      if (id != null && !values[0].equals(id)) {
        hbase.createFromCsv(prevValues, domains, source);
        domains = new DomainHashMap();
      }

      String domain = values[21].toLowerCase();
      String topic = values[22].toLowerCase();
      String sentiment = values[26].toLowerCase();

      if (!domain.isEmpty() || !topic.isEmpty() || !sentiment.isEmpty()) {
        if (acceptedDomainSlugs.rejects(domain)) {
          throw new IllegalArgumentException(
              String.format("Line %d: \"%s\" is an invalid domain.", lineNumber, domain));
        }

        if (acceptedDomainSlugs.rejects(domain, topic)) {
          throw new IllegalArgumentException(
              String.format("Line %d: \"%s\" is an invalid topic for domain \"%s\".",
                  lineNumber, topic, domain));
        }

        if (acceptedDomainSlugs.rejects(domain, topic, sentiment)) {
          throw new IllegalArgumentException(
              String.format("Line %d: \"%s\" is an invalid sentiment.", lineNumber, sentiment));
        }

        domains.add(domain, topic, sentiment);
      }

      id = values[0];

      prevValues = values;

      lineNumber++;
    }

    if (id != null) {
      hbase.createFromCsv(prevValues, domains, source);
    }
  }

}


package com.solutionsresource.sena.main;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.function.featuretransformation.*;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.BinaryPrefixComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

public class BackupMessageDomains {

    static Constants constants;

    public static void main(String[] args) throws Exception {
        constants = new Constants();

        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());

        HTableInterface messageTable = connection.getTable(
                TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
        HTableInterface messageDomainsTable = connection.getTable(
                TableName.valueOf(constants.TABLE_MESSAGE_DOMAINS_TEXT));

        System.out.println("Usage: ./BackupMessageDomains.sh");

        FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, CompareOp.EQUAL,
                new BinaryPrefixComparator(Bytes.toBytes(true))));

        Scan scan = new Scan();
        scan.setFilter(filter);

        ContentCleaner contentCleaner = new ContentCleaner(new RemoveLinks(constants),
                new RemoveStopWords(), new RemoveHandlers(constants), new DatesNormalizer(), new EmojiNormalizer());

        for (Result result : messageTable.getScanner(scan)) {
            String content = Bytes.toString(result.getValue(
                    constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE));

            if (content == null) {
                continue;
            }

            byte[] domains = result.getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                    constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE);

            if (domains == null) {
                continue;
            }

            content = contentCleaner.clean(content);

            if (content.length() == 0) {
                continue;
            }

            Put putMessageDomains = new Put(Bytes.toBytes(content));

            putMessageDomains.add(constants.TABLE_MESSAGE_DOMAINS_DATA_BYTE,
                    constants.TABLE_MESSAGE_DOMAINS_DATA_DOMAINS_BYTE, domains);

            messageDomainsTable.put(putMessageDomains);
        }
    }
}

package com.solutionsresource.sena.main;

import au.com.bytecode.opencsv.CSVParser;
import com.solutionsresource.sena.ImportMessagesFromJson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.List;

public class AutomaticIngestAndCsvGeneration {
    public static void main(String[] args) throws Exception {
        CSVParser parser = new CSVParser();

        String folderName = args[0];
        long start = args.length > 1 ? Long.parseLong(args[1]) : 0L;
        long end = args.length > 2 ? Long.parseLong(args[2]) : 1577808000000L;
        String extractString = args.length > 3 ? args[3].toLowerCase() : "x";
        String[] topicsFilter = args.length > 4 ? parser.parseLine(args[4]) : null;

        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        SparkConf conf = new SparkConf().setAppName("SENA Automatic Ingest And CSV Generation");

        List<Topic> topics = new ArrayList<>();

        try (
                HConnection connection = HConnectionManager.createConnection(config);
                HTableInterface topicsTable = connection.getTable(TableName.valueOf(constants.TABLE_TOPIC_TEXT));
                ResultScanner scanner = topicsTable.getScanner(new Scan());
        ) {
            for (Result topicRow : scanner) {
                String name = Bytes.toString(topicRow.getValue(
                        constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_NAME_BYTE));

                String description = Bytes.toString(topicRow.getValue(
                        constants.TABLE_TOPIC_TOPDATA_BYTE, constants.TABLE_TOPIC_TOPDATA_DESCRIPTION_BYTE));
                com.solutionsresource.sena.entity.Topic topic =
                        new com.solutionsresource.sena.entity.Topic(name, description);
                topics.add(topic);
                System.out.println(topic);
            }
        }

        System.out.println("Usage: ./AutomaticIngestAndCsvGeneration.sh <csv_filename> <from_milliseconds>" +
                "<to_milliseconds> [<optional_hashtag_regex_query>]");
        System.out.println("Example: ./AutomaticIngestAndCsvGeneration.sh Election-Feb21-Feb22 " +
                "1456027200000 1456156800000");
        System.out.println("Example: ./AutomaticIngestAndCsvGeneration.sh Election-Feb21-Feb22-BilangPilipino-DU30 " +
                "1456027200000 1456156800000 \"BilangPilipino|DU30\"");

        long startExport = start;

        IngestStatusGetter ingestStatusGetter = new IngestStatusGetter();

        try (
                JavaSparkContext sc = new JavaSparkContext(conf);
                HBaseDao hbase = new HBaseDao(sc, config)
        ) {
            SentimentAnalyzer analyzer = StepByStepSentimentAnalyzer.getInstance(
                    sc, constants, hbase, hbase.getFinalizedMessages());

            ImportMessagesFromJson importer = new ImportMessagesFromJson(sc, constants, config, analyzer);

            ExportMessagesFromHbase exporter = new ExportMessagesFromHbase(folderName, extractString,
                    constants, hbase, topics, topicsFilter);

            while (startExport < end) {
                try {
                    long ingestEnd = getIngestEnd(importer, ingestStatusGetter);

                    while (startExport >= ingestEnd) {
                        Thread.sleep(constants.EXTRACTION_INTERVAL);
                        ingestEnd = getIngestEnd(importer, ingestStatusGetter);
                    }

                    exporter.extract(startExport, ingestEnd);

                    startExport = ingestEnd;
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("An error occurred! Possibly because of unhealthy nodes. Sleeping...");
                    Thread.sleep(constants.EXTRACTION_INTERVAL);
                }
            }
        }
    }

    public static long getIngestEnd(ImportMessagesFromJson importer, IngestStatusGetter ingestStatusGetter)
            throws Exception {
        importer.readJson();
        return ingestStatusGetter.getMostRecentFinished().getTime() + 600000;
    }
}

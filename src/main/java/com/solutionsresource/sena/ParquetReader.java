package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import org.apache.avro.generic.GenericRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import parquet.avro.AvroParquetInputFormat;
import scala.Tuple2;

public class ParquetReader {
    static Constants constants;


    public static void main(String[] args) throws Exception {
        /** Spark Configuration **/
        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName("Spark Test");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        constants = new Constants();
        Path parquetPath = new Path(constants.PARQUET_SAMPLE_PATH);
        Configuration conf = new Configuration();
        Job job = new Job(conf);
        job.setInputFormatClass(AvroParquetInputFormat.class);
//        FileInputFormat.addInputPath(job, parquetPath);
        FileInputFormat.setInputPaths(job, constants.PARQUET_SAMPLE_PATH);
        JavaPairRDD<Void, GenericRecord> rdd = sc.newAPIHadoopRDD(
                job.getConfiguration(), AvroParquetInputFormat.class,
                Void.class, GenericRecord.class);

        rdd.foreach(new VoidFunction<Tuple2<Void, GenericRecord>>() {
            public void call(Tuple2<Void, GenericRecord> arg0) throws Exception {
                System.out.println(arg0._2().toString());
            }
        });
        sc.stop();

//	    final Broadcast<Constants> SAconstant = sc.broadcast(new Constants());
        //method 1
//	    SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sc);
//	    DataFrame dfMsgParquet = sqlContext.parquetFile("hdfs://" + SAconstant.getValue().INTERNAL_ADDRESS + "/tmp/parquet/part-r-00000.parquet");
//	    DataFrame dfMsgParquet = sqlContext.parquetFile("file:///home/ec2-user/parquet/part-r-11132015.parquet");
//	    dfMsgParquet.registerTempTable("message");
//	    DataFrame dfMessage = sqlContext.sql("SELECT * FROM message");
//	    System.out.println("****************>part-r-00000.parquet ");
//	    System.out.println("***************> col no: " + dfMessage.columns().length);
//	    for (String columnName : dfMessage.columns()) {
//			System.out.println("*******************> colum Name: " + columnName);
//		}
//	    System.out.println("**************************parquetFile schema: " + dfMessage.schema().fieldNames());
//	    dfMessage.javaRDD().foreach(new VoidFunction<Row>() {
//			public void call(Row row) throws Exception {
//				Row generator = row.getStruct(2);// position is 2, I have constant but give u one file, so just temporary hardcode 
//				System.out.println("---------> DisplayName: ): " + generator.getString(1));
//				System.out.println("--------------------------------------------------");
//				System.out.println("--------------------------------------------------");
//				System.out.println();
//				
//			}
//		});   

        // method 2
//	    Configuration config = new Configuration();
//	    String path = "file:///home/ec2-user/parquet/part-r-0000.parquet";
//	    JavaPairRDD<ImmutableBytesWritable, Row> testRDD = sc.newAPIHadoopFile(path, ParquetInputFormat.class, ImmutableBytesWritable.class, Row.class, config);
//	    testRDD.foreach(new VoidFunction<Tuple2<ImmutableBytesWritable,Row>>() {
//			public void call(Tuple2<ImmutableBytesWritable, Row> t) throws Exception {
//				Row row = t._2();
//				System.out.println("---------> twitter msg content (body in parquet - position 11): " + row.getString(bcSAconstant.getValue().PARQ_POS_CONTENT));
//			}
//		});

    }
}

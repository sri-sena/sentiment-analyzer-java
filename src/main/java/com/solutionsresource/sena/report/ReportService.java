package com.solutionsresource.sena.report;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.function.featureselection.FilterFinalizedUnfinalized;
import com.solutionsresource.sena.function.featureselection.GetHasDomainTopic;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.coprocessor.AggregationClient;
import org.apache.spark.api.java.JavaRDD;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.ArrayList;
import java.util.List;

public class ReportService {

  public List<Report> buildReports(HBaseDao hbase, ReportGenerator reportGenerator,
                                   Constants constants, Configuration config) throws Exception {
    List<Report> reports = new ArrayList<>();

    ReportTemplate reportTemplate = reportGenerator.getReportTemplate();

    DateTime from = reportGenerator.getFrom() == null ? new DateTime(2015, 1, 1, 0, 0, 0, 0) :
        new DateTime(reportGenerator.getFrom());

    DateTime to = reportGenerator.getTo() == null ? new DateTime() :
        new DateTime(reportGenerator.getTo());

    int diff = Days.daysBetween(from, to).getDays();

    List<String> partitions = reportTemplate.getPartitions();
    String[] dimensions = reportTemplate.getSentiment().split("-");

    AggregationClient aggregationClient = new AggregationClient(config);
    ReportRepository reportRepository = new ReportRepository(aggregationClient, constants);

    Boolean isFinalized = reportTemplate.getFinalize();
    Boolean isUnfinalized = reportTemplate.getUnfinalize();

    JavaRDD<ResultHashMap> messages = hbase.getMessagesWithinTimeRange(from.getMillis(), to.getMillis())
        .filter(new FilterFinalizedUnfinalized(constants, isFinalized, isUnfinalized)).cache();

    // 1. resource = topic
    if (CollectionUtils.isNotEmpty(reportTemplate.getTopics())) {
      List<Topic> topics = reportTemplate.getTopics();
      for (Topic topic : topics) {

        JavaRDD<ResultHashMap> messagesForTopic = messages
            .filter(new GetHasDomainTopic(constants, topic.getDomain(), topic.getName()))
            .cache();

        Report report = new Report();
        report.setLabel(topic.getName());
        List<Double> numbers = new ArrayList<>();
        List<String> legends = new ArrayList<>();

        // 1.1 resource      = topic
        //     dimensions[0] = all
        //     dimensions[1] = volume
        if (StringUtils.equals(dimensions[0], constants.REPORT_ALL)) {

          // 1.1.1 resource      = topic
          //       dimensions[0] = all
          //       dimensions[1] = volume
          //       partitions    = NULL
          //                     => search by topic
          if (CollectionUtils.isEmpty(partitions)) {
            numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, null));
            legends.add("All sentiments");
          }

          // 1.1.2 resource      = topic
          //       dimensions[0] = all
          //       dimensions[1] = volume
          //       partitions    = LIST
          else {
            DateTime fromTmp = from;
            DateTime toTmp = null;

            // 1.1.2.1 resource      = topic
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            if (partitions.indexOf(constants.REPORT_FREQUENCY) == 0) {

              // 1.1.2.1.1 resource      = topic
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 year
              //                         => search by topic + yearly time
              if (diff > 365) {
                int delta = (int) Math.ceil((double) diff / 365);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 365);
                  numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, null));
                  legends.add(fromTmp.toString("yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 1.1.2.1.2 resource      = topic
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 month
              //                         => search by topic + monthly time
              else if (diff > 31) {
                int delta = (int) Math.ceil((double) diff / 31);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 31);
                  numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, null));
                  legends.add(fromTmp.toString("MMM-yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 1.1.2.1.3 resource      = topic
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 week
              //                         => search by topic + weekly time
              else if (diff > 7) {
                int delta = (int) Math.ceil((double) diff / 7);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 7);
                  numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, null));
                  legends.add(fromTmp.toString("'W'w-yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 1.1.2.1.4 resource      = topic
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 day
              //                         => search by topic + daily time
              else {
                for (int i = 1; i <= diff; i++) {
                  toTmp = from.plusDays(i);
                  numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, null));
                  legends.add(fromTmp.toString("MM/dd/yyyy"));
                  fromTmp = toTmp;
                }
              }
            }

            // 1.1.2.2 resource      = topic
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = [sentiments]
            //                       => search by topic + each sentiment
            else {
              for (String partition : partitions) {
                numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, partition));
                legends.add(partition);
              }
            }
          }
        }

        // 1.2 resource      = topic
        //     dimensions[0] = sentiment
        //     dimensions[1] = volume
        else {

          // 1.2.1 resource      = topic
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = NULL
          //                       => search by topic + sentiment
          if (CollectionUtils.isEmpty(partitions)) {
            numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, dimensions[0]));
          }

          // 1.2.2 resource      = topic
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = [frequency]
          else if (partitions.indexOf(constants.REPORT_FREQUENCY) == 0) {
            DateTime fromTmp = from;
            DateTime toTmp = null;

            // 1.2.2.1 resource      = topic
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 year
            //                       => search by topic + sentiment + yearly time
            if (diff > 365) {
              int delta = (int) Math.ceil((double) diff / 365);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 365);
                numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, dimensions[0]));
                legends.add(fromTmp.toString("yyyy"));
                fromTmp = toTmp;
              }
            }

            // 1.2.2.2 resource      = topic
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 month
            //                       => search by topic + sentiment + monthly time
            else if (diff > 31) {
              int delta = (int) Math.ceil((double) diff / 31);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 31);
                numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, dimensions[0]));
                legends.add(fromTmp.toString("MMM-yyyy"));
                fromTmp = toTmp;
              }
            }

            // 1.2.2.3 resource      = topic
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 week
            //                       => search by topic + sentiment + weekly time
            else if (diff > 7) {
              int delta = (int) Math.ceil((double) diff / 7);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 7);
                numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, dimensions[0]));
                legends.add(fromTmp.toString("'W'w-yyyy"));
                fromTmp = toTmp;
              }
            }

            // 1.2.2.4 resource      = topic
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 day
            //                       => search by topic + sentiment + daily time
            else {
              for (int i = 1; i <= diff; i++) {
                toTmp = from.plusDays(i);
                numbers.add(reportRepository.getTopicVolume(messagesForTopic, topic, dimensions[0]));
                legends.add(fromTmp.toString("MM/dd/yyyy"));
                fromTmp = toTmp;
              }
            }
          }

          // 1.2.3 resource      = topic
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = [sentiments]
          //                     => do nothing
          else {
            // This block intended to be blank, since there is no such of case.
            // This case is prevented by front-end, and this is how it's prevented by back-end
          }
        }

        // 1.3 report by topic + percentage
        if (StringUtils.equals(dimensions[1], constants.REPORT_PERCENTAGE)) {
          double total = 0;
          for (int i = 0; i < numbers.size(); i++) {
            total += numbers.get(i);
          }
          for (int i = 0; i < numbers.size(); i++) {
            numbers.set(i, numbers.get(i) / total * 100);
          }
        }

        report.setNumbers(numbers);
        report.setLegends(legends);

        reports.add(report);

        messagesForTopic.unpersist();
      }
    }

    // 2. report by source
    if (CollectionUtils.isNotEmpty(reportTemplate.getSources())) {
      List<Source> sources = reportTemplate.getSources();
      for (Source source : sources) {
        Report report = new Report();
        report.setLabel(source.getName());
        List<Double> numbers = new ArrayList<Double>();
        List<String> legends = new ArrayList<String>();

        // 2.1 resource      = source
        //     dimensions[0] = all
        //     dimensions[1] = volume
        if (StringUtils.equals(dimensions[0], constants.REPORT_ALL)) {

          // 2.1.1 resource      = source
          //       dimensions[0] = all
          //       dimensions[1] = volume
          //       partitions    = NULL
          //                     => search by source
          if (CollectionUtils.isEmpty(partitions)) {
            numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), null, from.getMillis(), to.getMillis()));
            legends.add("All sentiments");
          }

          // 2.1.2 resource      = source
          //       dimensions[0] = all
          //       dimensions[1] = volume
          //       partitions    = LIST
          else {
            DateTime fromTmp = from;
            DateTime toTmp = null;

            // 2.1.2.1 resource      = source
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            if (partitions.indexOf(constants.REPORT_FREQUENCY) == 0) {

              // 2.1.2.1.1 resource      = source
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 year
              //                         => search by source + yearly time
              if (diff > 365) {
                int delta = (int) Math.ceil((double) diff / 365);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 365);
                  numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 2.1.2.1.2 resource      = source
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 month
              //                         => search by source + monthly time
              else if (diff > 31) {
                int delta = (int) Math.ceil((double) diff / 31);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 31);
                  numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("MMM-yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 2.1.2.1.3 resource      = source
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 week
              //                         => search by source + weekly time
              else if (diff > 7) {
                int delta = (int) Math.ceil((double) diff / 7);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 7);
                  numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("'W'w-yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 2.1.2.1.4 resource      = source
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 day
              //                         => search by source + daily time
              else {
                for (int i = 1; i <= diff; i++) {
                  toTmp = from.plusDays(i);
                  numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("MM/dd/yyyy"));
                  fromTmp = toTmp;
                }
              }
            }

            // 2.1.2.2 resource      = source
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = [sentiments]
            //                       => search by source + each sentiment
            else {
              for (String partition : partitions) {
                numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), partition, from.getMillis(), to.getMillis()));
                legends.add(partition);
              }
            }
          }
        }

        // 2.2 resource      = source
        //     dimensions[0] = sentiment
        //     dimensions[1] = volume
        else {

          // 2.2.1 resource      = source
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = NULL
          //                       => search by source + sentiment
          if (CollectionUtils.isEmpty(partitions)) {
            numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), dimensions[0], from.getMillis(), to.getMillis()));
          }

          // 2.2.2 resource      = source
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = [frequency]
          else if (partitions.indexOf(constants.REPORT_FREQUENCY) == 0) {
            DateTime fromTmp = from;
            DateTime toTmp = null;

            // 2.2.2.1 resource      = source
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 year
            //                       => search by source + sentiment + yearly time
            if (diff > 365) {
              int delta = (int) Math.ceil((double) diff / 365);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 365);
                numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("yyyy"));
                fromTmp = toTmp;
              }
            }

            // 2.2.2.2 resource      = source
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 month
            //                       => search by source + sentiment + monthly time
            else if (diff > 31) {
              int delta = (int) Math.ceil((double) diff / 31);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 31);
                numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("MMM-yyyy"));
                fromTmp = toTmp;
              }
            }

            // 2.2.2.3 resource      = source
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 week
            //                       => search by source + sentiment + weekly time
            else if (diff > 7) {
              int delta = (int) Math.ceil((double) diff / 7);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 7);
                numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("'W'w-yyyy"));
                fromTmp = toTmp;
              }
            }

            // 2.2.2.4 resource      = source
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 day
            //                       => search by source + sentiment + daily time
            else {
              for (int i = 1; i <= diff; i++) {
                toTmp = from.plusDays(i);
                numbers.add(reportRepository.getSourceVolume(isFinalized, isUnfinalized, source.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("MM/dd/yyyy"));
                fromTmp = toTmp;
              }
            }
          }

          // 2.2.3 resource      = source
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = [sentiments]
          //                     => do nothing
          else {
            // This block intended to be blank, since there is no such of case.
            // This case is prevented by front-end, and this is how it's prevented by back-end
          }
        }

        // 2.3 report by source + percentage
        if (StringUtils.equals(dimensions[1], constants.REPORT_PERCENTAGE)) {
          double total = 0;
          for (int i = 0; i < numbers.size(); i++) {
            total += numbers.get(i);
          }
          for (int i = 0; i < numbers.size(); i++) {
            numbers.set(i, numbers.get(i) / total * 100);
          }
        }

        report.setNumbers(numbers);
        report.setLegends(legends);

        reports.add(report);
      }
    }

    // 3. report by tophandle
    if (CollectionUtils.isNotEmpty(reportTemplate.getTophandles())) {
      List<Tophandle> tophandles = reportTemplate.getTophandles();
      for (Tophandle tophandle : tophandles) {
        Report report = new Report();
        report.setLabel(tophandle.getName());
        List<Double> numbers = new ArrayList<Double>();
        List<String> legends = new ArrayList<String>();

        // 3.1 resource      = tophandle
        //     dimensions[0] = all
        //     dimensions[1] = volume
        if (StringUtils.equals(dimensions[0], constants.REPORT_ALL)) {

          // 3.1.1 resource      = tophandle
          //       dimensions[0] = all
          //       dimensions[1] = volume
          //       partitions    = NULL
          //                     => search by tophandle
          if (CollectionUtils.isEmpty(partitions)) {
            numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), null, from.getMillis(), to.getMillis()));
            legends.add("All sentiments");
          }

          // 3.1.2 resource      = tophandle
          //       dimensions[0] = all
          //       dimensions[1] = volume
          //       partitions    = LIST
          else {
            DateTime fromTmp = from;
            DateTime toTmp = null;

            // 3.1.2.1 resource      = tophandle
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            if (partitions.indexOf(constants.REPORT_FREQUENCY) == 0) {

              // 3.1.2.1.1 resource      = tophandle
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 year
              //                         => search by tophandle + yearly time
              if (diff > 365) {
                int delta = (int) Math.ceil((double) diff / 365);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 365);
                  numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 3.1.2.1.2 resource      = tophandle
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 month
              //                         => search by tophandle + monthly time
              else if (diff > 31) {
                int delta = (int) Math.ceil((double) diff / 31);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 31);
                  numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("MMM-yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 3.1.2.1.3 resource      = tophandle
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 week
              //                         => search by tophandle + weekly time
              else if (diff > 7) {
                int delta = (int) Math.ceil((double) diff / 7);
                for (int i = 1; i <= delta; i++) {
                  toTmp = from.plusDays(i * 7);
                  numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("'W'w-yyyy"));
                  fromTmp = toTmp;
                }
              }

              // 3.1.2.1.4 resource      = tophandle
              //           dimensions[0] = all
              //           dimensions[1] = volume
              //           partitions    = frequency
              //           different     = more than 1 day
              //                         => search by tophandle + daily time
              else {
                for (int i = 1; i <= diff; i++) {
                  toTmp = from.plusDays(i);
                  numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), null, fromTmp.getMillis(), toTmp.getMillis()));
                  legends.add(fromTmp.toString("MM/dd/yyyy"));
                  fromTmp = toTmp;
                }
              }
            }

            // 3.1.2.2 resource      = tophandle
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = [sentiments]
            //                       => search by tophandle + each sentiment
            else {
              for (String partition : partitions) {
                numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), partition, from.getMillis(), to.getMillis()));
                legends.add(partition);
              }
            }
          }
        }

        // 3.2 resource      = tophandle
        //     dimensions[0] = sentiment
        //     dimensions[1] = volume
        else {

          // 3.2.1 resource      = tophandle
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = NULL
          //                       => search by tophandle + sentiment
          if (CollectionUtils.isEmpty(partitions)) {
            numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), dimensions[0], from.getMillis(), to.getMillis()));
          }

          // 3.2.2 resource      = tophandle
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = [frequency]
          else if (partitions.indexOf(constants.REPORT_FREQUENCY) == 0) {
            DateTime fromTmp = from;
            DateTime toTmp = null;

            // 3.2.2.1 resource      = tophandle
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 year
            //                       => search by tophandle + sentiment + yearly time
            if (diff > 365) {
              int delta = (int) Math.ceil((double) diff / 365);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 365);
                numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("yyyy"));
                fromTmp = toTmp;
              }
            }

            // 3.2.2.2 resource      = tophandle
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 month
            //                       => search by tophandle + sentiment + monthly time
            else if (diff > 31) {
              int delta = (int) Math.ceil((double) diff / 31);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 31);
                numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("MMM-yyyy"));
                fromTmp = toTmp;
              }
            }

            // 3.2.2.3 resource      = tophandle
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 week
            //                       => search by tophandle + sentiment + weekly time
            else if (diff > 7) {
              int delta = (int) Math.ceil((double) diff / 7);
              for (int i = 1; i <= delta; i++) {
                toTmp = from.plusDays(i * 7);
                numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("'W'w-yyyy"));
                fromTmp = toTmp;
              }
            }

            // 3.2.2.4 resource      = tophandle
            //         dimensions[0] = all
            //         dimensions[1] = volume
            //         partitions    = frequency
            //         different     = more than 1 day
            //                       => search by tophandle + sentiment + daily time
            else {
              for (int i = 1; i <= diff; i++) {
                toTmp = from.plusDays(i);
                numbers.add(reportRepository.getHandlerVolume(isFinalized, isUnfinalized, tophandle.getName(), dimensions[0], fromTmp.getMillis(), toTmp.getMillis()));
                legends.add(fromTmp.toString("MM/dd/yyyy"));
                fromTmp = toTmp;
              }
            }
          }

          // 3.2.3 resource      = tophandle
          //       dimensions[0] = sentiment
          //       dimensions[1] = volume
          //       partitions    = [sentiments]
          //                     => do nothing
          else {
            // This block intended to be blank, since there is no such of case.
            // This case is prevented by front-end, and this is how it's prevented by back-end
          }
        }

        // 3.3 report by tophandle + percentage
        if (StringUtils.equals(dimensions[1], constants.REPORT_PERCENTAGE)) {
          double total = 0;
          for (int i = 0; i < numbers.size(); i++) {
            total += numbers.get(i);
          }
          for (int i = 0; i < numbers.size(); i++) {
            numbers.set(i, numbers.get(i) / total * 100);
          }
        }

        report.setNumbers(numbers);
        report.setLegends(legends);

        reports.add(report);
      }
    }

    messages.unpersist();
    return reports;
  }

}

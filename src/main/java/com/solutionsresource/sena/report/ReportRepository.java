package com.solutionsresource.sena.report;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import com.solutionsresource.sena.function.featureselection.FilterSentimentOfTopic;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.coprocessor.AggregationClient;
import org.apache.hadoop.hbase.client.coprocessor.LongColumnInterpreter;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.JavaRDD;


public class ReportRepository {

  private AggregationClient aggregationClient;
  private Constants constants;

  public ReportRepository(AggregationClient aggregationClient, Constants constants) {
    this.aggregationClient = aggregationClient;
    this.constants = constants;
  }

  public double getTopicVolume(JavaRDD<ResultHashMap> messagesOfTopic, Topic topic, String sentiment) {
    return sentiment == null ? messagesOfTopic.count() :
        messagesOfTopic
            .filter(new FilterSentimentOfTopic(constants, topic.getDomain(), topic.getName(), sentiment))
            .count();
    /*
    double result = 0;

    try {
      FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

      //only the checkbox Finalized in ReportTemplate is checked, then we search finalized message,
      // if the checkbox is uncheck, we just search all message
      
      
      
      if ((BooleanUtils.isTrue(isFinalized) && BooleanUtils.isFalse(isUnFinalized)) 
            || (BooleanUtils.isFalse(isFinalized) && BooleanUtils.isTrue(isUnFinalized))) {
        Boolean getFinalized = isFinalized ? true : false;
        System.out.format(">>>>>>>> FILTERING %sFINALIZED IN %s %s VOLUME...\n", isFinalized ? "" : "UN", topic, sentiment);
        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(getFinalized))));
      }

      // Add CF to search through
      filter.addFilter(new ColumnPrefixFilter(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE));
      filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

      if (StringUtils.isNotBlank(topic)) {
        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, CompareOp.EQUAL, new SubstringComparator("\"name\":\"" + topic + "\"")));

        if (StringUtils.isNotBlank(sentiment)) {
          FilterList or = new FilterList(FilterList.Operator.MUST_PASS_ONE);
          or.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, CompareOp.EQUAL, new SubstringComparator("\"name\":\"" + topic + "\",\"sentiment\":{\"name\":\"" + sentiment.split("-")[0] + "\"")));
          or.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, CompareOp.EQUAL, new SubstringComparator("\"name\":\"" + topic + "\",\"description\":\"\",\"domain\":\"\",\"sentiment\":{\"name\":\"" + sentiment.split("-")[0] + "\"")));
          filter.addFilter(or);
        }
      }

      Scan scan = new Scan();
      scan.setFilter(filter);
      scan.setTimeRange(from, to);

//      System.err.println("====>report scan to string: " + scan.toString());

      result = aggregationClient.rowCount(TableName.valueOf(constants.TABLE_MESSAGE_TEXT), new LongColumnInterpreter(), scan);
    } catch (Throwable e) {
      e.printStackTrace();
    }

    return result;
    */
  }

  public double getSourceVolume(Boolean isFinalized, Boolean isUnFinalized, String source, String sentiment, Long from, Long to) {
    double result = 0;

    try {
      FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

      //only the checkbox Finalized in ReportTemplate is checked, then we search finalized message,
      // if the checkbox is uncheck, we just search all message
      if (BooleanUtils.isTrue(isFinalized)) {
        System.out.format(">>>>>>>> FILTERING %sFINALIZED IN SOURCE VOLUME...\n", isFinalized ? "" : "UN");
        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(isFinalized))));
      }

      // Add CF to search through
      filter.addFilter(new ColumnPrefixFilter(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE));
      filter.addFilter(new ColumnPrefixFilter(constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE));
      filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

      if (StringUtils.isNotBlank(source)) {
        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, CompareOp.EQUAL, new SubstringComparator(source)));

        if (StringUtils.isNotBlank(sentiment)) {
          filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, CompareOp.EQUAL, new SubstringComparator("\"sentiment\":{\"name\":\"" + sentiment.split("-")[0] + "\"")));
        }
      }

      Scan scan = new Scan();
      scan.setFilter(filter);
      scan.setTimeRange(from, to);

      result = aggregationClient.rowCount(TableName.valueOf(constants.TABLE_MESSAGE_TEXT), new LongColumnInterpreter(), scan);
    } catch (Throwable e) {
      e.printStackTrace();
    }

    return result;
  }

  public double getHandlerVolume(Boolean isFinalized, Boolean isUnFinalized, String handler, String sentiment, Long from, Long to) {
    double result = 0;

    try {
      FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL);

      //only the checkbox Finalized in ReportTemplate is checked, then we search finalized message,
      // if the checkbox is uncheck, we just search all message
      if (BooleanUtils.isTrue(isFinalized)) {
        System.out.format(">>>>>>>> FILTERING %sFINALIZED IN HANDLER VOLUME...\n", isFinalized ? "" : "UN");
        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE, CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(isFinalized))));
      }

      // Add CF to search through
      filter.addFilter(new ColumnPrefixFilter(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE));
      filter.addFilter(new ColumnPrefixFilter(constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE));
      filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE, CompareOp.NOT_EQUAL, new SubstringComparator(constants.WORDBANK_SOURCE)));

      if (StringUtils.isNotBlank(handler)) {
        filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE, CompareOp.EQUAL, new SubstringComparator(handler)));

        if (StringUtils.isNotBlank(sentiment)) {
          filter.addFilter(new SingleColumnValueFilter(constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE, CompareOp.EQUAL, new SubstringComparator("\"sentiment\":{\"name\":\"" + sentiment.split("-")[0] + "\"")));
        }
      }

      Scan scan = new Scan();
      scan.setFilter(filter);
      scan.setTimeRange(from, to);

      result = aggregationClient.rowCount(TableName.valueOf(constants.TABLE_MESSAGE_TEXT), new LongColumnInterpreter(), scan);
    } catch (Throwable e) {
      e.printStackTrace();
    }

    return result;
  }

}

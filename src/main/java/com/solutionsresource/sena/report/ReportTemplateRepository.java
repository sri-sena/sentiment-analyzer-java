package com.solutionsresource.sena.report;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.CollectionUtils;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.ReportTemplate;

public class ReportTemplateRepository {

    private Constants constants;
    private HTableInterface table;
    private ReportTemplateRowMapper rowMapper;

    public ReportTemplateRepository() {}

    public ReportTemplateRepository(HTableInterface table, Constants constants,
            ReportTemplateRowMapper rowMapper) {
        this.table = table;
        this.constants = constants;
        this.rowMapper = rowMapper;
    }

    public List<ReportTemplate> findByName(String name) {
        if (StringUtils.isBlank(name))
            throw new IllegalArgumentException("Name is null");

        Scan scan = new Scan();
        scan.addFamily(Bytes.toBytes(constants.TABLE_REPORT_REPDATA_TEXT));
        scan.setFilter(new SingleColumnValueFilter(constants.TABLE_REPORT_REPDATA_BYTE,
                constants.TABLE_REPORT_REPDATA_NAME_BYTE, CompareFilter.CompareOp.GREATER_OR_EQUAL,
                new SubstringComparator(name)));

        ResultScanner scanner = null;
        List<ReportTemplate> list = new ArrayList<ReportTemplate>();
        try {
            scanner = table.getScanner(scan);
            for (Result result = scanner.next(); (result != null); result = scanner.next()) {
                list.add(rowMapper.mapRow(result));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (scanner != null)
                scanner.close();
        }
        return list;
    }

    public ReportTemplate findByExactName(String name) {
        List<ReportTemplate> reportTemplatess = findByName(name);
        return reportTemplatess.size() > 0 ? CollectionUtils.getFirst(reportTemplatess) : null;
    }

}

package com.solutionsresource.sena.report;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.ReportTemplate;
import com.solutionsresource.sena.entity.Source;
import com.solutionsresource.sena.entity.Tophandle;
import com.solutionsresource.sena.entity.Topic;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.List;

public class ReportTemplateRowMapper {
  
  private Constants constants;
  public ReportTemplateRowMapper() {}
  public ReportTemplateRowMapper(HTableInterface table, Constants constants) {
    this.constants = constants;
  }
  

  public ReportTemplate mapRow(Result result) throws Exception {
    ReportTemplate reportTemplate = new ReportTemplate();
    reportTemplate.setUuid(Bytes.toString(result.getRow()));
    reportTemplate.setType(Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_TYPE_BYTE)));
    reportTemplate.setName(Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_NAME_BYTE)));
    reportTemplate.setTitle(Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_TITLE_BYTE)));
    reportTemplate.setSubtitle(Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_SUBTITLE_BYTE)));
    reportTemplate.setSentiment(Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_SENTIMENT_BYTE)));

    String partitionsJson = Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_PARTITIONS_BYTE));
    if (StringUtils.isNotBlank(partitionsJson)) {
      List<String> partitions = new Gson().fromJson(partitionsJson, new TypeToken<List<String>>() {}.getType());
      reportTemplate.setPartitions(partitions);
    }

    String topicsJson = Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_TOPICS_BYTE));
    if (StringUtils.isNotBlank(topicsJson)) {
      List<Topic> topics = new Gson().fromJson(topicsJson, new TypeToken<List<Topic>>() {}.getType());
      reportTemplate.setTopics(topics);
    }

    String sourcesJson = Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_SOURCES_BYTE));
    if (StringUtils.isNotBlank(sourcesJson)) {
      List<Source> sources = new Gson().fromJson(sourcesJson, Source.TYPE);
      reportTemplate.setSources(sources);
    }

    String tophandlesJson = Bytes.toString(result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_TOPHANDLES_BYTE));
    if (StringUtils.isNotBlank(tophandlesJson)) {
      List<Tophandle> tophandles = new Gson().fromJson(tophandlesJson, Tophandle.TYPE);
      reportTemplate.setTophandles(tophandles);
    }
    
    byte[] finalizeBytes = result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_FINALIZE_BYTE);
    if (finalizeBytes != null) {
        reportTemplate.setFinalize(BooleanUtils.toBooleanObject(Bytes.toBoolean(finalizeBytes)));
    }
    
    byte[] unfinalizeBytes = result.getValue(constants.TABLE_REPORT_REPDATA_BYTE, constants.TABLE_REPORT_REPDATA_UNFINALIZE_BYTE);
    if (unfinalizeBytes != null) {
        reportTemplate.setFinalize(BooleanUtils.toBooleanObject(Bytes.toBoolean(unfinalizeBytes)));
    }

    return reportTemplate;
  }

}

package com.solutionsresource.sena;

import java.util.Iterator;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;

import com.solutionsresource.sena.util.hbase.HBaseConnector;

public class FindByName {

  
  public static void main(String[] args) throws Exception, Throwable {
    /** General Configuration **/
    String name = "vince";
    /** General Configuration **/
    
    
    /** HBase Jobs - Scan **/
    HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
    HTableInterface table = connection.getTable(TableName.valueOf("topic"));
    Scan sb = new Scan();
    sb.addFamily(Bytes.toBytes("topic_data"));
    sb.setFilter(new SingleColumnValueFilter(Bytes.toBytes("topic_data"), Bytes.toBytes("name"), CompareFilter.CompareOp.GREATER_OR_EQUAL, new SubstringComparator(name)));
    Iterator<Result> i = table.getScanner(sb).iterator();
    while(i.hasNext()) {
      Result result = i.next();
      System.out.println(Bytes.toString(result.getValue(Bytes.toBytes("topic_data"), Bytes.toBytes("description"))));
    }
    /** HBase Jobs - Scan **/
    
    
//    /** HBase Jobs - Put **/
//    Put p = new Put(Bytes.toBytes(uuid));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("content"), Bytes.toBytes(t.body));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("source"), Bytes.toBytes("twitter"));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("username"), Bytes.toBytes(t.actor.displayName));
//    table.put(p);
//    /** HBase Jobs - Put **/
    
    connection.close();
  }
  
}
package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.util.Bytes;

public class DeleteHBaseRecord {
	public static void main(String[] args) throws Exception {
        /** HBase Jobs **/
        Constants constants = new Constants();
        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
        HTableInterface table = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));
        
        try {
            Delete d=new Delete(Bytes.toBytes("1447144079965-f3c19184-8784-11e5-8b43-cb06e6ace43f"));
            table.delete(d);
          }
          finally {
        	  connection.close();
          }
	}
}

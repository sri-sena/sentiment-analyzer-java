package com.solutionsresource.sena.classifier;

import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.function.featurescaling.ExtractFeatures;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.classification.NaiveBayesModel;

import java.util.List;
/*

public class ClassifyDocuments implements Function<Message, Message> {
    private final NaiveBayesModel domainRecognizer;
    private final NaiveBayesModel topicRecognizer;
    private final NaiveBayesModel sentimentAnalyzer;
    private final List<Feature> selectedFeatures;
    private final String domainName;
    private final String topicName;
    private final List<Feature> domainFeatures;
    private final List<Feature> topicFeatures;
    private final ExtractFeatures extractFeatures;
    private final DomainBiases domainBiases;

    public ClassifyDocuments(NaiveBayesModel domainRecognizer, NaiveBayesModel topicRecognizer,
                             NaiveBayesModel sentimentAnalyzer, String domainName, String topicName,
                             List<Feature> selectedFeatures, List<Feature> domainFeatures,
                             List<Feature> topicFeatures, DomainBiases classBiases) {
        this.domainRecognizer = domainRecognizer;
        this.topicRecognizer = topicRecognizer;
        this.sentimentAnalyzer = sentimentAnalyzer;
        this.domainName = domainName;
        this.topicName = topicName;
        this.selectedFeatures = selectedFeatures;
        this.domainFeatures = domainFeatures;
        this.topicFeatures = topicFeatures;
        this.extractFeatures = new ExtractFeatures();
        this.domainBiases = classBiases;
    }

    public Message call(Message message) throws Exception {
        if (this.recognizesDomain(message)) {
            if (this.recognizesTopic(message)) {
                int id = (int) analyzeSentiment(message);

                Topic topic = new Topic(topicName);

                topic.setSentiment(new Sentiment(Sentiment.nameLookup(id)));
                message.addPredictedTopic(domainName, topic);
            }
        }
        return message;
    }

    private boolean recognizesDomain(Message message) throws Exception {
        double score = domainBiases.get().score(message);
        if (score < 0) {
            score = predictWithFeaturesAndClassifier(message, selectedFeatures, domainRecognizer);
        }
        return score > 0;
    }

    private boolean recognizesTopic(Message message) throws Exception {
        double score = domainBiases.get().topicScore(topicName, message);
        if (score < 0) {
            score = predictWithFeaturesAndClassifier(message, domainFeatures, topicRecognizer);
        }
        return score > 0;
    }

    private double analyzeSentiment(Message message) throws Exception {
        double score = domainBiases.get().topicScore(topicName, message);
        if (score < 0) {
            score = predictWithFeaturesAndClassifier(message, topicFeatures, sentimentAnalyzer);
        }
        return score;
    }

    private double predictWithFeaturesAndClassifier(Message message, List<Feature> features,
                                                    NaiveBayesModel classifier) throws Exception {
        extractFeatures.setFeatures(features);
        return classifier.predict(extractFeatures.call(message));
    }
}
*/

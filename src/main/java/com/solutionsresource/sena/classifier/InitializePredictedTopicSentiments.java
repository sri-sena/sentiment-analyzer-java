package com.solutionsresource.sena.classifier;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Message;

public class InitializePredictedTopicSentiments implements Function<Message, Message> {
    public Message call(Message message) throws Exception {
        message.initializeTopicSentiments();
        return message;
    }
}

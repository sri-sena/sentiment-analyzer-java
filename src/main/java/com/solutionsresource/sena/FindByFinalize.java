package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.coprocessor.AggregationClient;
import org.apache.hadoop.hbase.client.coprocessor.LongColumnInterpreter;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.util.Bytes;

public class FindByFinalize {

  
  public static void main(String[] args) throws Exception, Throwable {
    Configuration config = new HBaseConnector().getConfiguration();

    Constants constants = new Constants();
    
    /** HBase Jobs - Scan **/
    HConnection connection = HConnectionManager.createConnection(config);
    
    Scan scan = new Scan();
//    scan.setReversed(true);
    scan.addFamily(Bytes.toBytes("message_data"));
    
//    Filter paging = new PageFilter(25);
//    Filter finalize = new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("finalize"), CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(true)));
    Filter removeUnrelatedFilter = new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("domains"), CompareOp.EQUAL, new SubstringComparator("\"name\":\"unrelated\""));
//    Filter ignoreTrainingData = new SingleColumnValueFilter(Bytes.toBytes("message_data"), Bytes.toBytes("source"), CompareOp.EQUAL, new SubstringComparator("wordbank-record"));
//    FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL, ignoreTrainingData, finalize, removeUnrelatedFilter, paging);
    FilterList filter = new FilterList(FilterList.Operator.MUST_PASS_ALL, removeUnrelatedFilter);
    scan.setFilter(filter);

//    HTableInterface table = connection.getTable(TableName.valueOf("message"));
//    Iterator<Result> i = table.getScanner(scan).iterator();
//    while(i.hasNext()) {
//      Result result = i.next();
//      System.out.println(Bytes.toString(result.getValue(Bytes.toBytes("message_data"), Bytes.toBytes("content"))));
//    }
    /** HBase Jobs - Scan **/
    
    
    AggregationClient aggregationClient = new AggregationClient(config);
    long rowCount = aggregationClient.rowCount(TableName.valueOf(constants.TABLE_MESSAGE_TEXT), new LongColumnInterpreter(), scan);
    System.out.println("===============> count = " + rowCount);
    
    
//    /** HBase Jobs - Put **/
//    Put p = new Put(Bytes.toBytes(uuid));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("content"), Bytes.toBytes(t.body));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("source"), Bytes.toBytes("twitter"));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("username"), Bytes.toBytes(t.actor.displayName));
//    table.put(p);
//    /** HBase Jobs - Put **/
    
    connection.close();
  }
  
}
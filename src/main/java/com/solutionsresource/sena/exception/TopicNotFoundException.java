package com.solutionsresource.sena.exception;

/**
 * Created by Ronald Erquiza on 5/3/2016.
 */
public class TopicNotFoundException extends Exception {
    public TopicNotFoundException(String topic) {
        super(topic + " does not exist in the domain.");
    }

}
package com.solutionsresource.sena.exception;

public class AlreadyBuildingException extends BuildingException {
  public AlreadyBuildingException(String message) {
    super(message);
  }
}

package com.solutionsresource.sena.exception;

public class CancelledBuildingException extends BuildingException {
  public CancelledBuildingException(String message) {
    super(message);
  }
}

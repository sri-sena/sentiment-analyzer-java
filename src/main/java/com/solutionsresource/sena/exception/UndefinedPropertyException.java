package com.solutionsresource.sena.exception;

public class UndefinedPropertyException extends Exception {
    public UndefinedPropertyException(String propName) {
        super(propName);
    }

    @Override
    public String getMessage() {
        return String.format("%s. Please define it in your properties file.", super.getMessage());
    }
}

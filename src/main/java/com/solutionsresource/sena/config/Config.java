package com.solutionsresource.sena.config;

import org.apache.commons.lang3.StringUtils;

public class Config {
    public static final String TOKENIZER_REGEX = StringUtils.join(new String[]{
            "([:;=xX8B][-^]?[\\(\\)\\[\\]\\*DPp3O\\|\\\\\\/])", // side view faces emoticons
            "([>oO0\\.][\\.\\_3][<oO0\\.])", // front view faces emoticons
            "(<\\/?3)", // heart symbol
            "(\\d+)", // numbers
            "([:\\?\\!\\.\\/\\\\\\(\\)\\#\\@\\'\\\"\\|\\-_;<>\\d])", // symbols
            "(\\w+-\\w+)", // words
            "(\\w+)", // words
            "(\\$[\\d\\.]+)", // dollars
            "(\\S+)" // white spaces
    }, "|");
    public static final int NGRAMS_MAX = 5;

    public static final String[] STOP_WORDS = new String[]{
             ",",
            // ".",
            // "-",
            "?",

            "bi", // temporary for elections
            "hon", // temporary for elections

            // "si",
            // "ang",
            // "ng",
            // "na",
            // "ng",
            // "ni",
            // "ay",

            // "at",
            // "to",

            // "as",
            // "is",
            // "are",
            // "the",
            // "a",
            // "an",
            // "then",
            // "and",
    };
    public static String[] STOP_HASHTAGS = new String[] {
            "# pili pinas debates 2016",
            "# pili pinas debate 2016",
    };
}

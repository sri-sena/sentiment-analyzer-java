package com.solutionsresource.sena;

import java.util.Iterator;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.util.Bytes;

import com.google.common.collect.Iterators;
import com.solutionsresource.sena.util.hbase.HBaseConnector;

public class App1 {

  
  public static void main(String[] args) throws Exception, Throwable {
    /** General Configuration **/
    int itemPerPage = 2;
    String uuid = "b58711e579c38f97019217432aa86ab7";
    /** General Configuration **/
        
    /** HBase Jobs - Scan **/
    HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
    HTableInterface table = connection.getTable(TableName.valueOf("message"));
    Scan sb = new Scan(Bytes.toBytes(uuid));
    sb.setReversed(true);
    sb.setFilter(new PageFilter(itemPerPage + 1));
    Result previous = Iterators.getLast(table.getScanner(sb).iterator());
    System.out.println("Prev = " + Bytes.toString(previous.getValue(Bytes.toBytes("message_data"), Bytes.toBytes("content"))));
    
    Scan sc = new Scan(Bytes.toBytes(uuid));
    sc.setReversed(false);
    sc.setFilter(new PageFilter(itemPerPage));
    Iterator<Result> i = table.getScanner(sc).iterator();
    String x = "Current = ";
    while(i.hasNext()) {
      Result result = i.next();
      x += Bytes.toString(result.getValue(Bytes.toBytes("message_data"), Bytes.toBytes("content"))) + ", ";
    }
    System.out.println(x);
    
    Scan sf = new Scan(Bytes.toBytes(uuid));
    sf.setReversed(false);
    sf.setFilter(new PageFilter(itemPerPage + 1));
    Result next = Iterators.getLast(table.getScanner(sf).iterator());
    System.out.println("Next = " + Bytes.toString(next.getValue(Bytes.toBytes("message_data"), Bytes.toBytes("content"))));
    /** HBase Jobs - Scan **/
    
    
//    /** HBase Jobs - Put **/
//    Put p = new Put(Bytes.toBytes(uuid));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("content"), Bytes.toBytes(t.body));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("source"), Bytes.toBytes("twitter"));
//    p.addColumn(Bytes.toBytes("message_data"), Bytes.toBytes("username"), Bytes.toBytes(t.actor.displayName));
//    table.put(p);
//    /** HBase Jobs - Put **/
    
    connection.close();
  }
  
}
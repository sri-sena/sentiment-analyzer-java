package com.solutionsresource.sena;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.main.StepByStepSentimentAnalyzer;
import com.solutionsresource.sena.main.template.SentimentAnalyzer;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import com.solutionsresource.sena.util.hbase.HBaseDao;
import com.solutionsresource.sena.util.parse.SaveMessageJsonPartition;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.Scanner;

public class ImportMessagesFromJson {


    private SentimentAnalyzer analyzer;
    private JavaSparkContext sc;
    private Configuration config;
    private Constants constants;
    private boolean requiresConfirmation;
    public String currentFolderName;

    public ImportMessagesFromJson() throws Exception {
        this(false);
    }

    public ImportMessagesFromJson(JavaSparkContext sc, Constants constants, Configuration config,
                                  SentimentAnalyzer analyzer) throws Exception {
        this.initialize(sc, constants, config, analyzer, false);
    }

    public ImportMessagesFromJson(JavaSparkContext sc, Constants constants, Configuration config,
                                  SentimentAnalyzer analyzer, boolean requiresConfirmation) throws Exception {
        this.initialize(sc, constants, config, analyzer, requiresConfirmation);
    }

    public void initialize(JavaSparkContext sc, Constants constants, Configuration config,
                           SentimentAnalyzer analyzer, boolean requiresConfirmation) throws Exception {
        this.sc = sc;
        this.constants = constants;
        this.config = config;
        this.analyzer = analyzer;
        this.requiresConfirmation = requiresConfirmation;
    }

    public ImportMessagesFromJson(boolean requiresConfirmation) throws Exception {
        SparkConf conf = new SparkConf().setAppName("SENA Import Messages From Json");
        JavaSparkContext sc = new JavaSparkContext(conf);

        Constants constants = new Constants();
        Configuration config = new HBaseConnector(constants).getConfiguration();

        try (HBaseDao hbase = new HBaseDao(sc, config)) {
            SentimentAnalyzer analyzer = StepByStepSentimentAnalyzer.getInstance(
                    sc, constants, hbase, hbase.getFinalizedMessages());

            this.initialize(sc, constants, config, analyzer, requiresConfirmation);
        }
    }

    public void readJson() throws Exception {
        FileStatus[] dateFolders;
        try (FileSystem fs = FileSystem.get(config)) {
            dateFolders = fs.listStatus(new Path(constants.FOLDER_TW_COMMENTS_PATH));
        }

        SaveMessageJsonPartition saveMessageJsonPartition = new SaveMessageJsonPartition(constants, analyzer);
        boolean isScanningInReversedOrder = constants.TWITTER_FEEDS_REVERSED;

        System.out.println(String.format("Scanning%s...", isScanningInReversedOrder ? " in reversed order" : ""));

        if (isScanningInReversedOrder) {
            for (int i = dateFolders.length - 1; i >= 0; i--) {
                FileStatus dateFolder = dateFolders[i];

                if (dateFolder.isDirectory()) {
                    extractFeeds(saveMessageJsonPartition, dateFolder);
                }
            }
        } else {
//                boolean skipFolder = true;
            for (FileStatus dateFolder : dateFolders) {
                    /*
                    if (skipFolder) {
                        if (currentFolderName == null || currentFolderName.equals(dateFolder.getPath().toString())) {
                            skipFolder = false;
                        } else {
                            continue;
                        }
                    }
                    */

                if (dateFolder.isDirectory()) {
                    extractFeeds(saveMessageJsonPartition, dateFolder);
                }
            }
        }
    }

    public void extractFeeds(SaveMessageJsonPartition saveMessageJsonPartition, FileStatus dateFolder)
            throws IOException {
        Scanner scanner = new Scanner(System.in);

        Path dateFolderPath = dateFolder.getPath();
        boolean isScanningInReversedOrder = constants.TWITTER_FEEDS_REVERSED;

        do {
            FileStatus[] tweetFiles;

            try (FileSystem fs = FileSystem.get(config)) {
                tweetFiles = fs.listStatus(
                        new Path(constants.FOLDER_TW_COMMENTS_PATH + "/" + dateFolderPath.getName()));
            }

            saveMessageJsonPartition.setSpecialCase(
                    dateFolderPath.getName().equals(constants.DATE_SPECIFIC_BIAS_RULES_FOLDER_NAME)
            );

            if (isScanningInReversedOrder) {
                for (int i = tweetFiles.length - 1; i >= 0; i--) {
                    FileStatus tweetFile = tweetFiles[i];

                    extractFeed(saveMessageJsonPartition, dateFolderPath, tweetFile);
                }
            } else {
                for (FileStatus tweetFile : tweetFiles) {
                    extractFeed(saveMessageJsonPartition, dateFolderPath, tweetFile);
                }
            }
            if (requiresConfirmation) {
                System.out.print("Press the enter key to run more analyses.\n> ");
            }
        } while (requiresConfirmation && !scanner.nextLine().contains("exit"));
    }

    public void extractFeed(SaveMessageJsonPartition saveMessageJsonPartition,
                            Path dateFolderPath, FileStatus tweetFile) throws IOException {
        Path tweetFilePath = tweetFile.getPath();

        if (tweetFile.isFile() && !tweetFilePath.getName().contains(constants.FINISHED_SUFFIX)
                && !tweetFilePath.getName().contains(constants.COPYING_SUFFIX)
                && !tweetFilePath.getName().equals("_SUCCESS")) {

            String dateFolderName = dateFolderPath.toString();

            System.out.println(String.format("Processing %s %s...",
                    dateFolderPath.getName(), tweetFilePath.getName()));

            saveMessageJsonPartition.setFolderName(dateFolderName);

            JavaRDD<String> jsons = sc.textFile(tweetFilePath.toString(), constants.REPARTITION);

            jsons.foreachPartition(saveMessageJsonPartition);

            try (FileSystem fs = FileSystem.get(config)) {
                fs.rename(tweetFilePath, new Path(String.format("%s/%s%s", dateFolderName,
                        tweetFilePath.getName(), constants.FINISHED_SUFFIX)));
            }

            this.currentFolderName = dateFolderName;
        }
    }

    public static void main(String[] args) throws Exception {
        ImportMessagesFromJson imjson = new ImportMessagesFromJson(args.length > 0);
        imjson.readJson();
    }
}

package com.solutionsresource.sena.function.actual;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.PredictionTable;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

import java.util.HashMap;
import java.util.List;

public class GetActualTable implements Function<Message, PredictionTable> {
    private DomainSlugs domainSlugs;

    public GetActualTable(DomainSlugs domainSlugs) {

        this.domainSlugs = domainSlugs;
    }

    public PredictionTable call(Message message) throws Exception {
        PredictionTable manualTags = new PredictionTable();
        List<Domain> domains = message.getDomains();

        if (domains.isEmpty()) {
            manualTags.put("unrelated", new HashMap<String, UsageTable>());
            manualTags.get("unrelated").put("unrelated", new UsageTable());
            manualTags.get("unrelated").get("unrelated").put("unrelated", 1);
        }
        for (Domain domain : domains) {
            String domainName = domain.getSlug();

            if (domainName == null || !domainSlugs.containsKey(domainName)) {
                continue;
            }

            TopicSlugs topicSlugs = domainSlugs.get(domainName);

            manualTags.put(domainName, new HashMap<String, UsageTable>());

            HashMap<String, UsageTable> domainTable = manualTags.get(domainName);

            for (Topic topic : domain.getTopics()) {
                String topicName = topic.getSlug();

                if (!topicSlugs.containsKey(topicName)) {
                    continue;
                }

                domainTable.put(topicName, new UsageTable());

                HashMap<String, Integer> topicTable = domainTable.get(topicName);

                String sentimentName = topic.getSentiment().getName();

                topicTable.put(sentimentName, 1);
            }
        }

        return manualTags;
    }
}


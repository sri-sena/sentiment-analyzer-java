package com.solutionsresource.sena.function.prediction;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.PredictionTable;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class GetPredictionTable implements Function<Message, PredictionTable>, Serializable {
    public PredictionTable call(Message message) throws Exception {
        PredictionTable predictions = new PredictionTable();
        List<Domain> messageDomains = message.getPredictedDomains();

        String domainName = "elections";

        predictions.put(domainName, new HashMap<String, UsageTable>());

        HashMap<String, UsageTable> domain = predictions.get(domainName);

        if (messageDomains.isEmpty()) {
            predictions.put("unrelated", new HashMap<String, UsageTable>());
            predictions.get("unrelated").put("unrelated", new UsageTable());
            predictions.get("unrelated").get("unrelated").put("unrelated", 1);
        }
        for (Domain predictedDomain : messageDomains) {
            for (Topic messagePrediction : predictedDomain.getTopics()) {
                String topicName = messagePrediction.getName();
                domain.put(topicName, new UsageTable());

                HashMap<String, Integer> topic = domain.get(topicName);

                String sentimentName = messagePrediction.getSentiment().getName();

                topic.put(sentimentName, 1);
            }
        }

        return predictions;
    }
}

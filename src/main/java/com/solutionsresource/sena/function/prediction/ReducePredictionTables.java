package com.solutionsresource.sena.function.prediction;

import com.solutionsresource.sena.entity.hashmap.PredictionTable;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function2;

import java.io.Serializable;
import java.util.HashMap;

public class ReducePredictionTables implements Function2<PredictionTable, PredictionTable, PredictionTable>,
        Serializable {

    private void transferPredictions(PredictionTable input, PredictionTable result) {
        for (String domainName : input.keySet()) {
            HashMap<String, UsageTable> domain = input.get(domainName);

            if (!result.containsKey(domainName)) {
                result.put(domainName, new HashMap<String, UsageTable>());
            }

            HashMap<String, UsageTable> resultDomain = result.get(domainName);

            for (String topicName : domain.keySet()) {
                HashMap<String, Integer> topic = domain.get(topicName);

                if (!resultDomain.containsKey(topicName)) {
                    resultDomain.put(topicName, new UsageTable());
                }

                HashMap<String, Integer> resultTopic = resultDomain.get(topicName);

                for (String sentimentName : topic.keySet()) {
                    Integer resultCount = topic.get(sentimentName);

                    if (!resultTopic.containsKey(sentimentName)) {
                        resultTopic.put(sentimentName, 0);
                    }

                    Integer count = resultTopic.get(sentimentName);

                    resultTopic.put(sentimentName, count + resultCount);
                }
            }
        }
    }

    public PredictionTable call(PredictionTable pt1, PredictionTable pt2) throws Exception {
        PredictionTable merged = new PredictionTable();

        this.transferPredictions(pt1, merged);
        this.transferPredictions(pt2, merged);

        return merged;
    }
}

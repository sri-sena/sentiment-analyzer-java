package com.solutionsresource.sena.function.accuracy;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class ConvertToCorrectnessTuple implements Function<Message, Tuple2<Integer, Integer>> {

    public Tuple2<Integer, Integer> call(Message message) throws Exception {
        return message.isCorrect() ? new Tuple2<Integer, Integer>(1, 0) : new Tuple2<Integer, Integer>(0, 1);
    }
}

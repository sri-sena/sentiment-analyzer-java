package com.solutionsresource.sena.function.accuracy;

import org.apache.spark.api.java.function.Function2;
import scala.Tuple2;

public class ReduceTuple2 implements
        Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Tuple2<Integer, Integer>> {
    public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> tuple1, Tuple2<Integer, Integer> tuple2) throws Exception {
        return new Tuple2<Integer, Integer>(tuple1._1() + tuple2._1(), tuple1._2() + tuple2._2());
    }
}

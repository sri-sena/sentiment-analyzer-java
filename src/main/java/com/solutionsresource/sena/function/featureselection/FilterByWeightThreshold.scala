package com.solutionsresource.sena.function.featureselection

import com.solutionsresource.sena.entity.WeightedLabeledPoint
import org.apache.spark.api.java.function.Function

class FilterByWeightThreshold extends Function[WeightedLabeledPoint, Boolean] {
  private var weightThreshold: Float = 0.0f

  def this(weightThreshold: Float) {
    this()
    this.weightThreshold = weightThreshold
  }

  @throws(classOf[Exception])
  def call(labeledPoint: WeightedLabeledPoint): Boolean = {
    labeledPoint.weight >= weightThreshold
  }
}
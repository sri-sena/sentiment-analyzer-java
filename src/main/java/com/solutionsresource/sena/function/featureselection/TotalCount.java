package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function2;

import java.util.HashMap;

public class TotalCount implements
		Function2<HashMap<String, UsageTable>, HashMap<String, UsageTable>, HashMap<String, UsageTable>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4697893358094266427L;

	public void transferNGrams(HashMap<String, UsageTable> hashtable,
			HashMap<String, UsageTable> ht1) throws Exception {

        for (String ngram : ht1.keySet()) {
            HashMap<String, Integer> ngramHashMap;

            if ( ! hashtable.containsKey(ngram)) {
            	hashtable.put(ngram, new UsageTable());
            }

            ngramHashMap = hashtable.get(ngram);

            for (String class_ : ht1.get(ngram).keySet()) {
                if ( ! ngramHashMap.containsKey(class_)) {
                    ngramHashMap.put(class_, 0);
                }

                ngramHashMap.put(class_, ngramHashMap.get(class_) + ht1.get(ngram).get(class_));
            }
        }
	}

	public HashMap<String, UsageTable> call(HashMap<String, UsageTable> ht1,
			HashMap<String, UsageTable> ht2) throws Exception {
        HashMap<String, UsageTable> hashtable = new HashMap<String, UsageTable>();

        this.transferNGrams(hashtable, ht1);
        this.transferNGrams(hashtable, ht2);

		return hashtable;
	}

}

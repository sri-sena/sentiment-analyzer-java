package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

public class FilterWithOffset implements Function<ResultHashMap, Boolean> {
  private String offset;

  public FilterWithOffset(String offset) {
    this.offset = offset;
  }

  @Override
  public Boolean call(ResultHashMap resultHashMap) throws Exception {
    if (this.offset == null) {
      return true;
    }

    String rowKey = Bytes.toString(resultHashMap.getRow());

    return rowKey.compareTo(offset) > 0;
  }
}

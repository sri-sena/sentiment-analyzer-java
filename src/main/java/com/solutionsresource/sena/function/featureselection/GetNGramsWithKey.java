package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

import java.util.HashMap;


public class GetNGramsWithKey implements Function<Message, HashMap<String, UsageTable>> {

    /**
     *
     */
    private static final long serialVersionUID = 5888166246181632363L;

    private Function<Message, String> getKey;
    private String domainName;
    private String topicName;

    public GetNGramsWithKey(Function<Message, String> f) {
        this.getKey = f;
        this.domainName = null;
        this.topicName = null;
    }

    public GetNGramsWithKey(String domainName, String topicName) {
        this.getKey = null;
        this.domainName = domainName;
        this.topicName = topicName;
    }

    public GetNGramsWithKey(String domainName) {
        this.getKey = null;
        this.domainName = domainName;
        this.topicName = null;
    }

    public HashMap<String, UsageTable> call(Message msg) throws Exception {
        HashMap<String, UsageTable> ngramsWithKey =
                new HashMap<String, UsageTable>();

        HashMap<String, Integer> ngrams = msg.getNgrams();
        for (String ngram : ngrams.keySet()) {
            UsageTable value = new UsageTable();

            if (msg.isUnrelated()) {
                value.put("false", ngrams.get(ngram));
            } else {
                if (getKey != null) {
                    value.put(this.getKey.call(msg), 1);
                } else {
                    value.put((this.topicName == null ? msg.hasDomain(domainName) :
                            msg.hasTopic(domainName, topicName)) ? "true" : "false", 1);
                }
            }

            ngramsWithKey.put(ngram.toLowerCase(), value);
        }

        return ngramsWithKey;
    }

}

package com.solutionsresource.sena.function.featureselection;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Topic;

public class GetTopicSlug implements Function<Topic, String> {

    public String call(Topic topic) throws Exception {
        return topic.getSlug();
    }
}


package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

public class GetNgrams implements Function<Message, UsageTable> {
    public UsageTable call(Message message) throws Exception {
        return message.getNgrams();
    }
}

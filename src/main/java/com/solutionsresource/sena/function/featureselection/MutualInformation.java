package com.solutionsresource.sena.function.featureselection;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MutualInformation implements Serializable {

    public String name;
    public HashMap<String, Integer> XY;
    public HashMap<String, Integer> X;
    public HashMap<String, Integer> Y;
    public int totalCount;
    public HashMap<String[], Float> probabilities;
    public String[] highestProbabilityKey;
    public float highestProbability;
    public float value;

    public MutualInformation(String name, HashMap<String, Integer> XY, HashMap<String, Integer> X,
                             HashMap<String, Integer> Y, int totalCount) {
        this(name, XY, X, Y, totalCount, false);
    }

    public MutualInformation(String name, HashMap<String, Integer> XY, HashMap<String, Integer> X,
                             HashMap<String, Integer> Y, int totalCount, boolean considerOnlyUsedTrue) {
        this.name = name;
        this.XY = XY;
        this.X = X;
        this.Y = Y;
        this.totalCount = totalCount;
        this.probabilities = new HashMap<String[], Float>();
        this.highestProbability = 0.0f;
        this.highestProbabilityKey = new String[]{"not", "set"};
        this.value = 0;

        BigDecimal bigTotalCount = new BigDecimal(totalCount > 0 ? totalCount : 1);

        for (String xkey : X.keySet()) {
            for (String ykey : Y.keySet()) {
                if (considerOnlyUsedTrue && !(xkey.equals("used") && ykey.equals("true"))) {
                    continue;
                }

                if (XY.containsKey(ykey)) {
                    int pxy = XY.get(ykey);
                    int px = X.get(xkey);
                    int py = Y.get(ykey);

                    BigDecimal p_xy = new BigDecimal(pxy > 0 ? pxy : 1);
                    BigDecimal p_x = new BigDecimal(px > 0 ? px : 1);
                    BigDecimal p_y = new BigDecimal(py > 0 ? py : 1);

                    if (xkey.equals("not used")) {
                        p_xy = p_y.subtract(p_xy);
                    }

                    if (p_xy.compareTo(BigDecimal.ZERO) == 0) {
                        p_xy = new BigDecimal("0.1");
                    }

                    float probability = p_xy.divide(bigTotalCount, 20, BigDecimal.ROUND_CEILING).floatValue() *
                            log2((bigTotalCount.multiply(p_xy)).divide(p_x.multiply(p_y), 20, BigDecimal.ROUND_CEILING)
                                    .floatValue());

                    if (probability == Double.NaN) {
                        probability = 0;
                    }

                    String[] key = new String[]{xkey, ykey};

                    this.probabilities.put(key, probability);

                    if (probability > highestProbability) {
                        this.highestProbability = probability;
                        this.highestProbabilityKey = key;
                    }
                }
            }
        }

        for (String[] key : probabilities.keySet()) {
            this.value += probabilities.get(key);
        }

        if (this.value < 0 || Double.isNaN(this.value)) {
            this.value = 0;
        }
    }

    public MutualInformation(float value) {
        this.value = value;
    }

    private float log2(double a) {
        return (float) (Math.log(a) / Math.log(2));
    }

    public String probabilityToString(String[] key) {
        Float probability = probabilities.get(key);
        return String.format("[%s, %s] %f", key[0], key[1], probability);
    }

    public String toString() {
        List<String> probabilityStrings = new ArrayList<String>();

        for (String[] key : probabilities.keySet()) {
            probabilityStrings.add(probabilityToString(key));
        }

        return String.format("%s ===== %s | %s ======= %s %s %s %d", this.name, this.value,
                StringUtils.join(probabilityStrings.toArray(), " | "),
                XY, X, Y, totalCount);
    }
}

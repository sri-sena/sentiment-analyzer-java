package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class FilterOutRetweets implements Function<Message, Boolean> {
    public Boolean call(Message message) throws Exception {
        return !message.getContent().startsWith("RT ");
    }
}

package com.solutionsresource.sena.function.featureselection;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

import java.util.ArrayList;
import java.util.List;

public class GetHasAnyDomain implements Function<ResultHashMap, Boolean> {
  private final Constants constants;
  private List<String> filterDomains;

  public GetHasAnyDomain(Constants constants, List<String> domains) {
    this.constants = constants;
    this.filterDomains = domains;
  }

  @Override
  public Boolean call(ResultHashMap result) throws Exception {
    String domainsJson = Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT));

    List<Domain> messageDomains = new ArrayList<>();
    if (StringUtils.isNotBlank(domainsJson)) {
      messageDomains = new Gson().fromJson(domainsJson, Domain.TYPE);
    }

    if (messageDomains.isEmpty()) {
      return true;
    }

    for (String filterDomain : filterDomains) {
      for (Domain messageDomain : messageDomains) {
        if (filterDomain.equalsIgnoreCase(messageDomain.getName()) && messageDomain.hasTopics()) {
          return true;
        }
      }
    }
    return false;
  }
}

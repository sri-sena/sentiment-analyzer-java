package com.solutionsresource.sena.function.featureselection;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Topic;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

public class FilterSentimentOfTopic implements Function<ResultHashMap, Boolean> {
  private final Constants constants;
  private final String domainName;
  private final String topicName;
  private final String sentiment;

  public FilterSentimentOfTopic(Constants constants, String domainName, String topicName, String sentiment) {
    this.constants = constants;
    this.domainName = domainName;
    this.topicName = topicName;
    this.sentiment = sentiment;
  }

  public Boolean call(ResultHashMap result) throws Exception {
    Gson gson = new Gson();
    Domain[] domains = gson.fromJson(
        Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT)), Domain[].class);

    for (Domain domain : domains) {
      if (domain.getName().equalsIgnoreCase(domainName)) {
        Topic topic = domain.getTopic(topicName);
        return topic != null && topic.getSentiment().getName().equals(sentiment);
      }
    }

    return false;
  }
}

package com.solutionsresource.sena.function.featureselection;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.regression.LabeledPoint;

public class FilterZero implements Function<LabeledPoint, Boolean> {
    public Boolean call(LabeledPoint labeledPoint) throws Exception {
        return labeledPoint.label() == 0.0;
    }
}

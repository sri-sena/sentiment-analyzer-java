package com.solutionsresource.sena.function.featureselection;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

public class GetHasDomainTopic implements Function<ResultHashMap, Boolean> {
  private final Constants constants;
  private final String domainName;
  private final String topicName;

  public GetHasDomainTopic(Constants constants, String domainName, String topicName) {
    this.constants = constants;
    this.domainName = domainName;
    this.topicName = topicName;
  }

  public Boolean call(ResultHashMap result) throws Exception {
    Gson gson = new Gson();
    Domain[] domains = gson.fromJson(
        Bytes.toString(result.get(constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT)), Domain[].class);

    for (Domain domain : domains) {
      if (domain.getName().equalsIgnoreCase(domainName)) {
        return domain.hasTopic(topicName);
      }
    }

    return false;
  }
}

package com.solutionsresource.sena.function.featureselection

import org.apache.spark.api.java.function.Function

class FilterOutEmpty extends Function[java.lang.String, java.lang.Boolean] {
  @throws(classOf[Exception])
  def call(s: java.lang.String): java.lang.Boolean = {
    !s.isEmpty
  }
}
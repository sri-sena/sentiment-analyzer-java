package com.solutionsresource.sena.function.featureselection

import com.solutionsresource.sena.constant.Constants
import com.solutionsresource.sena.entity.hashmap.ResultHashMap
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.api.java.function.Function

class FilterFinalizedUnfinalized(c: Constants, f: Boolean, u: Boolean) extends Function[ResultHashMap, java.lang.Boolean] {
  private val constants: Constants = c
  private val isFinalized: Boolean = f
  private val isUnfinalized: Boolean = u

  @throws(classOf[Exception])
  def call(result: ResultHashMap): java.lang.Boolean = {
    val finalizedBytes: Array[Byte] = result.get(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT)

    isFinalized == isUnfinalized ||
      (if (finalizedBytes.length == 1)
        Bytes.toBoolean(finalizedBytes) == isFinalized
      else
        Bytes.toString(finalizedBytes).equalsIgnoreCase("true") == isFinalized)
  }
}
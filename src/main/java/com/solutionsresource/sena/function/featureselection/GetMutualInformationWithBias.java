package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.Feature;
import org.apache.spark.api.java.function.Function;

public class GetMutualInformationWithBias implements Function<Feature, Float> {

    private final String[] biases;

    public GetMutualInformationWithBias(String className) {
        this.biases = className != null ? className.split("-") : new String[0];
    }

    public boolean isInBiases(String value) {
        for (String bias : biases) {
            if (bias.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public Float call(Feature feature) throws Exception {
        if (isInBiases(feature.getValue())) {
            return 1.0f;
        }
        return feature.getMutualInformation().value;
    }
}

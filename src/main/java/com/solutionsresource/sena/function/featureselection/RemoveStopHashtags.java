package com.solutionsresource.sena.function.featureselection;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class RemoveStopHashtags implements Function<Message, Message> {
    private String[] stopHashtags;

    public RemoveStopHashtags(String[] stopHashtags) {
        this.stopHashtags = stopHashtags;
    }

    @Override
    public Message call(Message message) throws Exception {
        message.setContent(removeStopHashtags(message.getContent()));

        return message;
    }

    public String removeStopHashtags(String content) {
        for (String stopHashtag : stopHashtags) {
            content = content.replace(stopHashtag, "");
        }
        return content;
    }
}

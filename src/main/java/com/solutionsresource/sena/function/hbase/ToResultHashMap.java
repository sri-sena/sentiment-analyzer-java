package com.solutionsresource.sena.function.hbase;

import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

public class ToResultHashMap implements Function<Result, ResultHashMap> {
    @Override
    public ResultHashMap call(Result result) throws Exception {
        ResultHashMap hashMap = new ResultHashMap(result.getRow());

        for (Cell cell : result.listCells()) {
            hashMap.put(Bytes.toString(cell.getQualifier()), cell.getValue());
        }

        return hashMap;
    }
}

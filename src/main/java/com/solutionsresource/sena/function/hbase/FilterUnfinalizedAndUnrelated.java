package com.solutionsresource.sena.function.hbase;

import com.solutionsresource.sena.constant.Constants;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class FilterUnfinalizedAndUnrelated implements Function<Tuple2<ImmutableBytesWritable, Result>, Boolean> {

    Constants constants;

    public FilterUnfinalizedAndUnrelated() throws Exception {
        constants = new Constants();
    }

    public Boolean call(Tuple2<ImmutableBytesWritable, Result> tuple) throws Exception {
        return !Bytes.toBoolean(tuple._2().getValue(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE)) && Bytes.toString(tuple._2().getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_BYTE)).length() <= 2;
    }
}

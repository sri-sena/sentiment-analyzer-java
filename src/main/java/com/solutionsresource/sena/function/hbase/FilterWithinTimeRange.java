package com.solutionsresource.sena.function.hbase;

import com.solutionsresource.sena.constant.Constants;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class FilterWithinTimeRange implements Function<Tuple2<ImmutableBytesWritable, Result>, Boolean> {
    private Constants constants;
    private final long start;
    private final long end;

    public FilterWithinTimeRange(Constants constants, long start, long end) {
        this.constants = constants;
        this.start = start;
        this.end = end;
    }

    @Override
    public Boolean call(Tuple2<ImmutableBytesWritable, Result> tuple) throws Exception {
        long timestamp = tuple._2().getColumnLatestCell(
                constants.TABLE_MESSAGE_MSGDATA_BYTE,
                constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE
        ).getTimestamp();
        return start <= timestamp && timestamp < end;
    }
}

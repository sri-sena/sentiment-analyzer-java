package com.solutionsresource.sena.function.hbase;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

public class FilterSourceWordBank implements Function<ResultHashMap, Boolean> {
    private Constants constants;

    public FilterSourceWordBank(Constants constants) {
        this.constants = constants;
    }

    @Override
    public Boolean call(ResultHashMap resultHashMap) throws Exception {
        String sourceFieldName = Bytes.toString(constants.TABLE_MESSAGE_MSGDATA_SOURCE_BYTE);
        return resultHashMap.containsKey(sourceFieldName) &&
                !Bytes.toString(resultHashMap.get(sourceFieldName))
                        .equals(constants.WORDBANK_SOURCE);
    }
}

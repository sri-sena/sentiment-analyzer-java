package com.solutionsresource.sena.function.hbase;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.util.hbase.HBaseConnector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class FinalizeMessage implements Function<Tuple2<ImmutableBytesWritable, Result>, Boolean> {

    Constants constants;

    public FinalizeMessage() throws Exception {
        constants = new Constants();
    }

    public Boolean call(Tuple2<ImmutableBytesWritable, Result> tuple) throws Exception {
        Result result = tuple._2();

        boolean isFinalized = Bytes.toBoolean(result.getValue(
                constants.TABLE_MESSAGE_MSGDATA_BYTE, constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE));

        if (!isFinalized) {
            String uuid = Bytes.toString(tuple._1().get());

            Configuration config = new HBaseConnector(constants).getConfiguration();
            HConnection connection = HConnectionManager.createConnection(config);

            HTableInterface messageTable = connection.getTable(TableName.valueOf(constants.TABLE_MESSAGE_TEXT));

            Put putMessage = new Put(Bytes.toBytes(uuid));

            putMessage.add(constants.TABLE_MESSAGE_MSGDATA_BYTE,
                    constants.TABLE_MESSAGE_MSGDATA_FINALIZE_BYTE,
                    Bytes.toBytes(true));

            messageTable.put(putMessage);
        }

        return true;
    }
}

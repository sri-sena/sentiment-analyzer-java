package com.solutionsresource.sena.function.hbase;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

public class FilterFinalized implements Function<ResultHashMap, Boolean> {

  Constants constants;
  private boolean shouldBeFinalized;

  public FilterFinalized() throws Exception {
    this(true);
  }

  public FilterFinalized(boolean shouldBeFinalized) throws Exception {
    constants = new Constants();
    this.shouldBeFinalized = shouldBeFinalized;
  }

  public Boolean call(ResultHashMap result) throws Exception {
    byte[] bytes = result.get(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT);

    return bytes != null && (bytes.length == 1 ?
        Bytes.toBoolean(bytes) == shouldBeFinalized :
        Bytes.toString(bytes).equalsIgnoreCase("true") == shouldBeFinalized);
  }
}


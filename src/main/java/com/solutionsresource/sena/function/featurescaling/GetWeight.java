package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class GetWeight implements Function<WeightedLabeledPoint, Float> {
    public Float call(WeightedLabeledPoint labeledPoint) throws Exception {
        return labeledPoint.weight();
    }
}

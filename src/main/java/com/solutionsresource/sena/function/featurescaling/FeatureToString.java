package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Feature;

public class FeatureToString implements Function<Feature, String> {
    public String call(Feature feature) throws Exception {
        return feature.toString();
    }
}

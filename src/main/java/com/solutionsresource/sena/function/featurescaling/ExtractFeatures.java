package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.Feature;
import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.ArrayList;
import java.util.List;

public class ExtractFeatures implements Function<Message, Vector> {

    /**
     *
     */
    private static final long serialVersionUID = 5888166246181632363L;
    private List<String> featureNames;
    private List<Float> multipliers;

    public ExtractFeatures() {
        this(new ArrayList<String>());
    }

    public ExtractFeatures(List<String> featureNames) {
        this.featureNames = featureNames;
        this.multipliers = new ArrayList<Float>();

        for (int i = 0; i < featureNames.size(); i++) {
            this.multipliers.add(1f);
        }
    }

    public void setFeatures(List<Feature> features) {
        this.featureNames = new ArrayList<String>();
        this.multipliers = new ArrayList<Float>();

        for (Feature feature : features) {
            featureNames.add(feature.getValue());
        }

        float highestMultiplier = getHighestMultiplier(features);

        for (Feature feature : features) {
            multipliers.add(feature.getMutualInformation().value / highestMultiplier);
        }
    }

    private float getHighestMultiplier(List<Feature> features) {
        float highest = 0;

        for (Feature feature : features) {
            float multiplier = feature.getMutualInformation().value;

            if (multiplier > highest) {
                highest = multiplier;
            }
        }

        return highest;
    }

    public Vector call(Message msg) throws Exception {
        double[] values = new double[this.featureNames.size()];

        for (int i = 0; i < values.length; i++) {
            values[i] = 0;
        }

        for (String ngram : msg.getNgrams().keySet()) {
            if (this.featureNames.contains(ngram)) {
                int i = this.featureNames.indexOf(ngram);
                values[i] += msg.getNgrams().get(ngram) * multipliers.get(i);

                if (values[i] < 0 || Double.isNaN(values[i])) {
                    values[i] = 0;
                }
            }
        }
        return Vectors.dense(values);
    }

}

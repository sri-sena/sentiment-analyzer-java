package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.*;
import com.solutionsresource.sena.entity.hashmap.MultiplierTable;
import com.solutionsresource.sena.util.build.Features;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.List;

public class GetWeightedLabeledPoints implements Function<Message, WeightedLabeledPoint> {

    private static final long serialVersionUID = 5888166246181632363L;
    public List<Feature> features;
    public String domainName;
    public String topicName;
    private boolean isDomain;
    private final MultiplierTable multipliers;
    public boolean labelsAreMany;

    public GetWeightedLabeledPoints(List<Feature> features, String domainName, String topicName, boolean isDomain,
                                    MultiplierTable multipliers) {
        this.features = features;
        this.domainName = domainName;
        this.topicName = topicName;
        this.isDomain = isDomain;
        this.multipliers = multipliers;

        this.labelsAreMany = false;
    }

    public GetWeightedLabeledPoints(List<Feature> features, String domainName, String topicName,
                                    MultiplierTable multipliers) {
        this(features, domainName, topicName, false, multipliers);
    }

    public GetWeightedLabeledPoints(List<Feature> features, String domainName, String topicName, String[] sentiments,
                                    MultiplierTable multipliers) {
        this.features = features;
        this.domainName = domainName;
        this.topicName = topicName;
        this.multipliers = multipliers;

        this.labelsAreMany = true;
    }

    public GetWeightedLabeledPoints(Broadcast<List<Feature>> broadcastedFeatures, String domainName,
                                    String topicName, boolean isDomain, MultiplierTable multipliers) {
        this(broadcastedFeatures.value(), domainName, topicName, isDomain, multipliers);
    }

    public GetWeightedLabeledPoints(Broadcast<List<Feature>> broadcastedFeatures, String domainName, String topicName,
                                    MultiplierTable multipliers) {
        this(broadcastedFeatures.value(), domainName, topicName, multipliers);

    }

    public GetWeightedLabeledPoints(Broadcast<List<Feature>> broadcastedFeatures, String domainName,
                                    String topicName, String[] sentiments, MultiplierTable multipliers) {
        this(broadcastedFeatures.value(), domainName, topicName, sentiments, multipliers);
    }

    public WeightedLabeledPoint call(Message msg) throws Exception {
        double label;
        if (this.labelsAreMany) {
            Topic topic = msg.getTopic(this.domainName, this.topicName);
            Sentiment sentiment = topic.getSentiment();
            String string = sentiment.getName();
            label = Sentiment.idLookup(string);
        } else {
            label = (this.isDomain ? msg.hasDomain(this.domainName) : msg.hasTopic(this.domainName, this.topicName)) ?
                    1.0 : 0.0;
        }


        double[] values = new double[this.features.size()];

        float weight = 0;

        for (String ngram : msg.getNgrams().keySet()) {
            if (Features.hasValue(features, ngram)) {
                int index = Features.indexOfValue(features, ngram);
                values[index] += msg.getNgrams().get(ngram) * multipliers.get(label);

                weight += features.get(index).getMutualInformation().value;
            }
        }

        return new WeightedLabeledPoint(label, Vectors.dense(values), weight);
    }

}

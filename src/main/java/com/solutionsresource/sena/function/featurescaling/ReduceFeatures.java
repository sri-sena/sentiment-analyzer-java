package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class ReduceFeatures implements Function<WeightedLabeledPoint, WeightedLabeledPoint> {
    private Integer[] indexes;

    public ReduceFeatures(Integer[] indexes) {
        this.indexes = indexes;
    }

    public WeightedLabeledPoint call(WeightedLabeledPoint labeledPoint) throws Exception {
        return labeledPoint.withReducedFeatures(indexes);
    }
}

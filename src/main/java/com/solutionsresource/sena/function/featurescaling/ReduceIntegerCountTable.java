package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function2;

import com.solutionsresource.sena.entity.hashmap.IntegerCountTable;

public class ReduceIntegerCountTable implements Function2<IntegerCountTable, IntegerCountTable, IntegerCountTable> {
    public IntegerCountTable call(IntegerCountTable countTable1, IntegerCountTable countTable2) throws Exception {
        IntegerCountTable result = new IntegerCountTable();

        transferValues(countTable1, result);
        transferValues(countTable2, result);

        return result;
    }

    public void transferValues(IntegerCountTable countTable, IntegerCountTable result) {
        for (int key : countTable.keySet()) {
            if (!result.containsKey(key)) {
                result.put(key, 0);
            }
            result.put(key, result.get(key) + countTable.get(key));
        }
    }
}

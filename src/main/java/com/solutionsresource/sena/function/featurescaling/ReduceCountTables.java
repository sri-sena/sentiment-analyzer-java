package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function2;

import com.solutionsresource.sena.entity.hashmap.CountTable;

public class ReduceCountTables implements Function2<CountTable, CountTable, CountTable> {
    public CountTable call(CountTable countTable1, CountTable countTable2) throws Exception {
        CountTable result = new CountTable();

        transferValues(countTable1, result);
        transferValues(countTable2, result);

        return result;
    }

    public void transferValues(CountTable countTable, CountTable result) {
        for (Double key : countTable.keySet()) {
            if (!result.containsKey(key)) {
                result.put(key, 0);
            }
            result.put(key, result.get(key) + countTable.get(key));
        }
    }
}

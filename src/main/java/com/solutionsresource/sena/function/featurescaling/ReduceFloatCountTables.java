package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.hashmap.FloatCountTable;
import org.apache.spark.api.java.function.Function2;

public class ReduceFloatCountTables implements Function2<FloatCountTable, FloatCountTable, FloatCountTable> {
    public FloatCountTable call(FloatCountTable countTable1, FloatCountTable countTable2) throws Exception {
        FloatCountTable result = new FloatCountTable();

        transferValues(countTable1, result);
        transferValues(countTable2, result);

        return result;
    }

    public void transferValues(FloatCountTable countTable, FloatCountTable result) {
        for (float key : countTable.keySet()) {
            if (!result.containsKey(key)) {
                result.put(key, 0);
            }
            result.put(key, result.get(key) + countTable.get(key));
        }
    }
}

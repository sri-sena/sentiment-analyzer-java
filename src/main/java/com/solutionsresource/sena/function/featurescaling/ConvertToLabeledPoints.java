package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.regression.LabeledPoint;

import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class ConvertToLabeledPoints implements Function<WeightedLabeledPoint, LabeledPoint> {
    public LabeledPoint call(WeightedLabeledPoint w) throws Exception {
        return w.toLabeledPoint();
    }
}

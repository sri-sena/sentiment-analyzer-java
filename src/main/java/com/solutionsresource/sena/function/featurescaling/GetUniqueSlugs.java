package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import org.apache.spark.api.java.function.Function2;

public class GetUniqueSlugs implements Function2<DomainSlugs, DomainSlugs, DomainSlugs> {
    public DomainSlugs transfer(DomainSlugs donorDomainSlugs, DomainSlugs receiverDomainSlugs) {
        for (String domainName : donorDomainSlugs.keySet()) {
            if (!receiverDomainSlugs.containsKey(domainName)) {
                receiverDomainSlugs.put(domainName, new TopicSlugs());
            }

            TopicSlugs donorTopicSlugs = donorDomainSlugs.get(domainName);
            TopicSlugs receiverTopicSlugs = receiverDomainSlugs.get(domainName);

            for (String topicName : donorTopicSlugs.keySet()) {
                String[] sentimentNames = donorTopicSlugs.get(topicName);
                if (!receiverTopicSlugs.containsKey(topicName)) {
                    receiverTopicSlugs.put(topicName, sentimentNames);
                }
            }
        }
        return receiverDomainSlugs;
    }

    public DomainSlugs call(DomainSlugs slugs1, DomainSlugs slugs2) throws Exception {
        DomainSlugs slugs3 = new DomainSlugs();

        transfer(slugs1, slugs3);
        transfer(slugs2, slugs3);

        return slugs3;
    }
}

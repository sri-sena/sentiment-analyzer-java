package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.Sentiment;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import com.solutionsresource.sena.entity.hashmap.TopicSlugs;
import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.Topic;

public class ExtractConfusionMatrixContribution implements Function<Message, Integer[][]> {

    private final DomainSlugs domainSlugs;

    public ExtractConfusionMatrixContribution(DomainSlugs domainSlugs) {
        this.domainSlugs = domainSlugs;
    }

    public Integer[][] call(Message message) throws Exception {
        int length = Sentiment.getNames().length;

        Integer[][] confusionMatrix = new Integer[length][length];

        for (int x = 0; x < length; x++) {
            for (int y = 0; y < length; y++) {
                confusionMatrix[x][y] = 0;
            }
        }

        for (String domainName : domainSlugs.keySet()) {
            TopicSlugs topicSlugs = domainSlugs.get(domainName);

            for (String topicName : topicSlugs.keySet()) {
                Topic actualTopic = message.getTopic(domainName, topicName);
                Topic predictedTopic = message.getPredictedTopic(domainName, topicName);

                // `i` is rows, `j` is columns

                int i = actualTopic == null ? 0 : actualTopic.getSentiment().getSentimentIndex();
                int j = predictedTopic == null ? 0 : predictedTopic.getSentiment().getSentimentIndex();

                confusionMatrix[i][j] += 1;
            }
        }

        return confusionMatrix;
    }
}

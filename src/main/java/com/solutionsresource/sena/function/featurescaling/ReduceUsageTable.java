package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function2;

public class ReduceUsageTable implements Function2<UsageTable, UsageTable, UsageTable> {
    public UsageTable call(UsageTable usageTable, UsageTable usageTable2) throws Exception {
        UsageTable result = new UsageTable();

        transferValues(usageTable, result);
        transferValues(usageTable2, result);

        return result;
    }

    public void transferValues(UsageTable countTable, UsageTable result) {
        for (String key : countTable.keySet()) {
            if (!result.containsKey(key)) {
                result.put(key, 0);
            }
            result.put(key, result.get(key) + countTable.get(key));
        }
    }
}

package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.Feature;
import org.apache.spark.api.java.function.Function;

public class ParseFeature implements Function<String, Feature> {
    public Feature call(String s) throws Exception {
        String[] split = s.split("=====");

        String name = split[0].trim();
        float value = Float.parseFloat(split[1].split("\\|")[0].trim());

        return new Feature(name, value);
    }
}

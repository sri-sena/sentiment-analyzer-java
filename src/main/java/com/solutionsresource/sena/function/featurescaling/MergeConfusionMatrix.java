package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function2;

public class MergeConfusionMatrix implements Function2<Integer[][], Integer[][], Integer[][]> {
    public Integer[][] call(Integer[][] confusionMatrix1, Integer[][] confusionMatrix2) throws Exception {
        int length = confusionMatrix1.length;

        Integer[][] confusionMatrix = new Integer[length][length];

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                confusionMatrix[i][j] = confusionMatrix1[i][j] + confusionMatrix2[i][j];
            }
        }

        return confusionMatrix;
    }
}

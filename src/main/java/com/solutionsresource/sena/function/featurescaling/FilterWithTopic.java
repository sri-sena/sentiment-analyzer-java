package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Message;

public class FilterWithTopic implements Function<Message, Boolean> {
    private String domain;
    private final String topic;

    public FilterWithTopic(String domain, String topic) {
        this.domain = domain;
        this.topic = topic;
    }

    public Boolean call(Message msg) throws Exception {
        return msg.hasTopic(domain, topic);
    }
}

package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Message;

public class FilterWithDomain implements Function<Message, Boolean> {

    String domain;

    public FilterWithDomain(String domain) {
        this.domain = domain;
    }

    public Boolean call(Message msg) throws Exception {
        return msg.hasDomain(domain);
    }
}

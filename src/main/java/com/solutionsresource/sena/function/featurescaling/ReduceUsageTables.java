package com.solutionsresource.sena.function.featurescaling;

import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function2;

public class ReduceUsageTables implements Function2<UsageTable[], UsageTable[], UsageTable[]> {
    public UsageTable[] call(UsageTable[] usageTables, UsageTable[] usageTables2) throws Exception {
        UsageTable[] result = new UsageTable[usageTables.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = new UsageTable();
        }

        transferValues(usageTables, result);
        transferValues(usageTables2, result);

        return result;
    }

    public void transferValues(UsageTable[] usageTables2, UsageTable[] result) {
        for (int i = 0; i < result.length; i++) {
            UsageTable usageTable = result[i];
            UsageTable usageTable2 = usageTables2[i];

            for (String key : usageTable2.keySet()) {
                if (!usageTable.containsKey(key)) {
                    usageTable.put(key, 0);
                }
                usageTable.put(key, usageTable.get(key) + usageTable2.get(key));
            }
        }
    }
}

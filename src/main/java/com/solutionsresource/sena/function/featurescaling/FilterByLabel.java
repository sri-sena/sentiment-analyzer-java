package com.solutionsresource.sena.function.featurescaling;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class FilterByLabel implements Function<WeightedLabeledPoint, Boolean> {
    private double label;

    public FilterByLabel(double label) {
        this.label = label;
    }

    public Boolean call(WeightedLabeledPoint labeledPoint) throws Exception {
        return labeledPoint.label() == label;
    }
}

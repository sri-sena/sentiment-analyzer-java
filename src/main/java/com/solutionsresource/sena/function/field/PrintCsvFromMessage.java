package com.solutionsresource.sena.function.field;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.BytesTable;
import com.solutionsresource.sena.entity.gnip.Hashtag;
import com.solutionsresource.sena.entity.gnip.Tweet;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.spark.api.java.function.VoidFunction;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PrintCsvFromMessage implements VoidFunction<String> {
    Constants constants;
    private PrintWriter writer;

    public PrintCsvFromMessage(PrintWriter writer) throws Exception {
        this.writer = writer;
        constants = new Constants();
    }

    public void call(String line) throws Exception {
        SimpleDateFormat timezoneDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Gson gson = new Gson();
        Tweet tweet = gson.fromJson(line, Tweet.class);

        String[] idSplit = tweet.id.split(":");

        tweet.id = String.format("twitter-%s", idSplit[idSplit.length - 1]);

        Date postedDateTime = timezoneDf.parse(tweet.postedTime);
        postedDateTime = DateUtils.addHours(postedDateTime, 8);
        long timestamp = postedDateTime.getTime();

        printTweet(tweet, timestamp, df);
    }

    private void printTweet(Tweet tweet, long timestamp, SimpleDateFormat df) throws IOException {
        BytesTable bytesTable = new BytesTable();

        String hashtagsString = "";

        for (Hashtag hashtag : tweet.twitter_entities.hashtags) {
            hashtagsString += String.format(" #%s", hashtag.text);
        }

        printClean("%s", tweet.id);
        printClean("%s", tweet.body + hashtagsString);
        printClean("%s", tweet.generator.displayName);
        printClean("%s", tweet.location != null ? tweet.location.displayName : "");

        if (tweet.gnip != null && tweet.gnip.profileLocations != null && !tweet.gnip.profileLocations.isEmpty() &&
                tweet.gnip.profileLocations.get(0).geo != null && tweet.gnip.profileLocations.get(0).geo.coordinates
                != null && tweet.gnip.profileLocations.get(0).geo.coordinates.size() == 2) {
            List<String> coordinates = tweet.gnip.profileLocations.get(0).geo.coordinates;
            printClean("%s", coordinates.get(1));
            printClean("%s", coordinates.get(0));
        } else {
            printClean("");
            printClean("");
        }

        printClean("%s", tweet.actor.preferredUsername);

        String hashtags = "";
        for (Hashtag hashtag : tweet.twitter_entities.hashtags) {
            hashtags += ", " + hashtag.text;
        }
        hashtags = hashtags.length() > 0 ? hashtags.substring(2) : "";

        printClean("%s", hashtags);

        String postedDate = df.format(new Date(timestamp));
        String createdDate = df.format(new Date());

        printDateTimeCols(postedDate);
        printDateTimeCols(createdDate);
        writer.write("\n");
    }

    public void printDateTimeCols(String dateTimeStr) {
        if (dateTimeStr == null) {
            printClean(",,");
        } else {
            printClean("%s", StringUtils.substringBefore(dateTimeStr, " "));
            printClean("%s", StringUtils.substringAfter(dateTimeStr, " "));

        }
    }

    public void printClean(String format, Object... args) {
        writer.write(cleanString(String.format(format, args)) + ",");
    }

    static String cleanString(String string) {
        if (string == null) {
            return "";
        }
        string = string.replaceAll("\"", "\"\"");
        string = string.replaceAll("\n", " ");
        string = "\"" + string + "\"";
        return string.trim();
    }

}

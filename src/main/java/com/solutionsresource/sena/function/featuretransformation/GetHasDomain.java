package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class GetHasDomain implements Function<Message, Boolean> {
    private String domainName;

    public GetHasDomain(String domainName) {

        this.domainName = domainName;
    }

    public Boolean call(Message message) throws Exception {
        return message.hasDomain(domainName);
    }
}

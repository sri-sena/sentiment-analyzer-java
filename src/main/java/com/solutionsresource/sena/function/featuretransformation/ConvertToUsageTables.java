package com.solutionsresource.sena.function.featuretransformation;

import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.regression.LabeledPoint;

import com.solutionsresource.sena.entity.hashmap.UsageTable;

public class ConvertToUsageTables implements Function<LabeledPoint, UsageTable[]> {
    private String[] labels;

    public ConvertToUsageTables(String[] labels) {
        this.labels = labels;
    }

    public UsageTable[] call(LabeledPoint labeledPoint) throws Exception {
        double[] features = labeledPoint.features().toArray();

        int labelIndex = (int) labeledPoint.label();

        UsageTable[] usageTables = new UsageTable[features.length];

        for (int i = 0; i < usageTables.length; i++) {
            usageTables[i] = new UsageTable();
            usageTables[i].put(labels[labelIndex], (int) features[i]);
        }

        return usageTables;
    }
}


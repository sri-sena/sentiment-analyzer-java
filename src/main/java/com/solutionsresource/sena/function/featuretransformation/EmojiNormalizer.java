package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

import java.io.Serializable;

public class EmojiNormalizer implements Serializable, Function<Message, Message> {
    public String normalize(String content) {
        StringBuilder cleanContentBuilder = new StringBuilder();

        int tweetLength = content.length();
        for (int i = 0; i < tweetLength; i++) {
            char c = content.charAt(i);

            if ((int)c < 256) {
                cleanContentBuilder.append(c);
            } else {
                cleanContentBuilder.append(String.format("UNICODE_%d ", (int) c));
            }
        }

        return cleanContentBuilder.toString();
    }

    @Override
    public Message call(Message message) throws Exception {
        message.setContent(normalize(message.getContent()));
        return message;
    }
}

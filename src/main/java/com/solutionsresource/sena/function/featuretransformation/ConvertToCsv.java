package com.solutionsresource.sena.function.featuretransformation;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ConvertToCsv implements Function<ResultHashMap, String> {
    private Constants constants;
    private final boolean hasFilterHashtag;
    private String filterHashtag;
    private String[] topicsFilter;

    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";

    private SimpleDateFormat hbaseFormat;
    private SimpleDateFormat csvFormat;

    public ConvertToCsv(Constants constants, boolean hasFilterHashtag, String filterHashtag, String[] topicsFilter) {
        this.constants = constants;
        this.hasFilterHashtag = hasFilterHashtag;
        this.filterHashtag = filterHashtag;
        this.topicsFilter = topicsFilter;
        this.hbaseFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        this.csvFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public String call(ResultHashMap result) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        String content = getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT, false);

        if (content == null) {
            return "";
        }

        Gson gson = new Gson();

        boolean hasTopic = false;

        String json = getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT, false);

        Domain[] domains = gson.fromJson(json, Domain[].class);

        if (domains == null) {
            return "";
        }

        if (isFiltered(result, filterHashtag)) {
            return "";
        }

        String messageCommonData = getMessageCommonData(result);

        boolean messageIsNeutral = isMessageNeutral(getMessageValue(
                result, constants.TABLE_MESSAGE_MSGDATA_USERNAME_TEXT));

        for (Domain domain : domains) {
            String domainName = cleanString(domain.name);
            for (Topic topic : domain.topics) {

                if (topicsFilter != null && topicIsFiltered(topicsFilter, topic.name)) {
                    continue;
                }

                String topicName = cleanString(topic.name);
                String sentimentName = messageIsNeutral ? "neutral" : topic.sentiment.name;

                if (hasTopic) {
                    stringBuilder.append(NEW_LINE_SEPARATOR);
                }

                stringBuilder.append(messageCommonData);
                stringBuilder.append(getSentiment(domainName, topicName, sentimentName));

                hasTopic = true;
            }
        }

        if (hasFilterHashtag && !hasTopic) {
            stringBuilder.append(messageCommonData);
        }

        return stringBuilder.toString();
    }

    public static boolean topicIsFiltered(String[] topicsFilter, String name) {
        for (String filter : topicsFilter) {
            if (filter.equalsIgnoreCase(name)) {
                return false;
            }
        }
        return true;
    }

    private boolean contains(String source, String... searches) {
        for (String search : searches) {
            if (source.contains(search)) {
                return true;
            }
        }
        return false;
    }

    private boolean isFiltered(ResultHashMap result, String filterHashtag) {
        String hashtags = getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_HASHTAG_TEXT, false);
        return hashtags == null || !hashtags.matches(filterHashtag);
    }

    public boolean isMessageNeutral(String author) {
        for (String neutralHandler : constants.NEUTRAL_HANDLER_WHITELIST) {
            if (neutralHandler.equalsIgnoreCase(author)) {
                return true;
            }
        }

        return false;
    }

    public String getSentiment(String domainName, String topicName,
                               String sentimentName) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(domainName);
        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(topicName);
        stringBuilder.append(COMMA_DELIMITER);

        stringBuilder.append(getSentimentValue(sentimentName, "positive"));
        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getSentimentValue(sentimentName, "neutral"));
        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getSentimentValue(sentimentName, "negative"));
        stringBuilder.append(COMMA_DELIMITER);

        stringBuilder.append(cleanString(sentimentName));
        stringBuilder.append(COMMA_DELIMITER);

        return stringBuilder.toString();
    }

    public String getSentimentValue(String actual, String expectation) {
        return actual.equals(expectation) ? "1" : "0";
    }

    public String getMessageValue(ResultHashMap result, String field) {
        return getMessageValue(result, field, true);
    }

    public String getMessageValue(ResultHashMap result, String field, boolean cleanString) {
        String s = Bytes.toString(result.get(field));

        return cleanString ? cleanString(s) : s;
    }

    public String getMessageCommonData(ResultHashMap result)
            throws IOException, ParseException {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(cleanString(Bytes.toString(result.getRow())));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_SOURCE_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_LOCATION_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_LATITUDE_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_LONGITUDE_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USERNAME_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USER_ID_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        String userCreatedStr = getMessageValue(
                result, constants.TABLE_MESSAGE_MSGDATA_USER_CREATED_TEXT, false).trim();
        stringBuilder.append(getDateTimeCols(userCreatedStr));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USER_FRIENDS_COUNT_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USER_FOLLOWERS_COUNT_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USER_STATUSES_COUNT_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USER_FAVORITES_COUNT_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(cleanString("temporarily NA"));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_HASHTAG_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        String postedDateTimeStr = getMessageValue(
                result, constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_TEXT, false).trim();

        stringBuilder.append(getDateTimeCols(postedDateTimeStr));

        stringBuilder.append(COMMA_DELIMITER);
        String createdDateTimeStr = getMessageValue(
                result, constants.TABLE_MESSAGE_MSGDATA_CREATED_DATETIME_TEXT, false).trim();

        stringBuilder.append(getDateTimeCols(createdDateTimeStr));

        stringBuilder.append(COMMA_DELIMITER);
        byte[] isFinalized = result.get(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT);
        if (isFinalized.length == 5) {
            isFinalized = null;
        }
        stringBuilder.append(cleanString(String.valueOf(isFinalized != null && Bytes.toBoolean(isFinalized))));

        stringBuilder.append(COMMA_DELIMITER);

        return stringBuilder.toString();
    }

    private String getDateTimeCols(String dateTimeStr) throws IOException, ParseException {
        StringBuilder stringBuilder = new StringBuilder();

        if (dateTimeStr == null || dateTimeStr.isEmpty()) {
            stringBuilder.append("");
            stringBuilder.append(COMMA_DELIMITER);
            stringBuilder.append("");
        } else {
            Date dateTime = hbaseFormat.parse(dateTimeStr.replace("/", "-"));
            dateTimeStr = csvFormat.format(dateTime);

            stringBuilder.append(cleanString(StringUtils.substringBefore(dateTimeStr, " ")));//posted_date;
            stringBuilder.append(COMMA_DELIMITER);
            stringBuilder.append(cleanString(StringUtils.substringAfter(dateTimeStr, " ")));
        }

        return stringBuilder.toString();
    }

    private String cleanString(String string) {
        if (string == null) {
            return "";
        }
        return String.format("\"%s\"", string.replaceAll("\"", "\"\"").replaceAll("\n", " ")).trim();
    }

    private static com.solutionsresource.sena.entity.Topic getTopicWithName(
            List<com.solutionsresource.sena.entity.Topic> topics, String name) {

        for (com.solutionsresource.sena.entity.Topic topic : topics) {
            if (topic.getName().equalsIgnoreCase(name))
                return topic;
        }

        return new com.solutionsresource.sena.entity.Topic(null, name);
    }

    class Domain {
        String name;
        List<Topic> topics;
    }

    class Topic {
        String name;
        Sentiment sentiment;
    }

    class Sentiment {
        String name;
    }

}

package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ronald Erquiza on 4/7/2016.
 */
public class DatesNormalizer implements Serializable, Function<Message, Message> {
    private String newContent;
    final String REGEX_PATTERN = "\\b(([jJ]an(uary)?|[fF]eb(ruary)?|[mM]ar(ch)?|[aA]pr(il)?|[mM]ay|[jJ]un(e)?" +
            "|[jJ]ul(y)?|[aA]ug(ust)?|[sS]ep(t(ember)?)?|[oO]ct(ober)?|[nN]ov(ember)?|[dD]ec(ember)?)\\b" +
            "(\\b\\s*([\\d]{1,2}(st|nd|rd|th)?\\b))?" +
            "(,?\\s*([\\d]{4}))?)|" +
            "(([\\d]{1,2})?((st|nd|rd|th)\\s*of\\s*)" +
            "\\b([jJ]an(uary)?|[fF]eb(ruary)?|[mM]ar(ch)?|[aA]pr(il)?|[mM]ay|[jJ]un(e)?" +
            "|[jJ]ul(y)?|[aA]ug(ust)?|[sS]ep(t(ember)?)?|[oO]ct(ober)?|[nN]ov(ember)?|[dD]ec(ember)?)\\b" +
            "(\\s+([\\d]{4}))?)";;

    public DatesNormalizer() {

    }

    public String normalize(String content) {
        newContent = "";
        return replaceString(content);
    }

    private String replaceString(String content) {
        Pattern r = Pattern.compile(REGEX_PATTERN);
        Matcher m = r.matcher(content);
        String wholeDate = new String();
        String dateFormat = new String();
        if (m.find()) {
            if(m.group(21)==null&&m.group(24)!=null&&m.group(38)==null ||
                    m.group(16)==null&&m.group(2)!=null&&m.group(19)==null){
                return content;
            }
            else if (m.group(20) != null) {
                wholeDate = m.group(20);
                dateFormat = dateAssignment(m.group(21),getMonth(m.group(24).toUpperCase().substring(0, 3)),m.group(38));
            } else if (m.group(1) != null) {
                wholeDate = m.group(1);
                dateFormat = dateAssignment(m.group(16),getMonth(m.group(2).toUpperCase().substring(0, 3)),m.group(19));
            }
            newContent = replaceDate(wholeDate, content, dateFormat);
            replaceString(newContent);
            content = newContent;
        }
        newContent = content;
        return newContent;
    }

    private String dateAssignment(String date, String month, String year){
        return dateFormatting(year, month, date);
    }
    private String getMonth(String mon) {
        String month = "";
        switch (mon) {
            case "JAN":
                month = "JANUARY";
                break;
            case "FEB":
                month = "FEBRUARY";
                break;
            case "MAR":
                month = "MARCH";
                break;
            case "APR":
                month = "APRIL";
                break;
            case "MAY":
                month = "MAY";
                break;
            case "JUN":
                month = "JUNE";
                break;
            case "JUL":
                month = "JULY";
                break;
            case "AUG":
                month = "AUGUST";
                break;
            case "SEP":
                month = "SEPTEMBER";
                break;
            case "OCT":
                month = "OCTOBER";
                break;
            case "NOV":
                month = "NOVEMBER";
                break;
            case "DEC":
                month = "DECEMBER";
                break;
        }

        return month;
    }

    private String dateFormatting(String year, String month, String date) {
        if (month != null && year != null && date == null) {
            return year + "_" + month;
        }
        if (year == null && date != null && month != null) {
            return month + "_" + date;
        }
        if (year != null && date != null && month != null) {
            return year + "_" + month + "_" + date;
        }
        return "";
    }

    private String replaceDate(String datePattern, String content, String date) {
        Pattern r = Pattern.compile(datePattern);
        Matcher m = r.matcher(content);
        if (m.find()) {
            return content.substring(0, m.start()) + date + content.substring(m.end(), content.length());
        }
        return content;
    }

    @Override
    public Message call(Message message) throws Exception {
        message.setContent(normalize(message.getContent()));
        return message;
    }
}

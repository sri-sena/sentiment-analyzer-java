package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

public class GetUsernameUsageTable implements Function<Message, UsageTable> {
    @Override
    public UsageTable call(Message message) throws Exception {
        return new UsageTable(message.getUsername(), 1);
    }
}

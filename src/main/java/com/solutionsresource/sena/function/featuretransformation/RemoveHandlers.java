package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveHandlers implements Function<Message, Message> {

    Constants constants;

    public RemoveHandlers() throws Exception {
        this(new Constants());
    }

    public RemoveHandlers(Constants constants) {
        this.constants = constants;
    }

    public Message call(Message message) {
        message.setContent(removeHandlers(message.getContent()));
        return message;
    }

    public String removeHandlers(String content) {
        Pattern handlerRegex = constants.HANDLER_REGEX;

        Matcher matcher = handlerRegex.matcher(content);

        while(matcher.find()) {
            String handler = matcher.group();
            if (rejectedHandler(handler)) {
                content = content.replaceAll(handler, "");
            }
        }

        return content;
    }

    private boolean rejectedHandler(String handler) {
        for (String acceptedHandler : constants.HANDLER_WHITELIST) {
            if (("@"+acceptedHandler).equalsIgnoreCase(handler)) {
                return false;
            }
        }
        for (String acceptedHandler : constants.NEUTRAL_HANDLER_WHITELIST) {
            if (("@"+acceptedHandler).equalsIgnoreCase(handler)) {
                return false;
            }
        }
        return true;
    }
}

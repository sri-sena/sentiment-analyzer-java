package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class GetHasDomainTopic implements Function<Message, String> {
    private final String domainName;
    private final String topicName;

    public GetHasDomainTopic(String domainName, String topicName) {
        this.domainName = domainName;
        this.topicName = topicName;
    }

    public String call(Message message) throws Exception {
        return message.hasDomain(domainName) && message.getDomain(domainName).hasTopic(topicName) ? "true" : "false";
    }
}

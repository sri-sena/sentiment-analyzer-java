package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class GetSentimentOfTopic implements Function<Message, String> {
    private final String domainName;
    private final String topicName;

    public GetSentimentOfTopic(String domainName, String topicName) {
        this.domainName = domainName;
        this.topicName = topicName;
    }

    public String call(Message message) throws Exception {
        return message.hasDomain(domainName) && message.getDomain(domainName).hasTopic(topicName) ?
                message.getDomain(domainName).getTopic(topicName).getSentiment().getName() : "";
    }
}

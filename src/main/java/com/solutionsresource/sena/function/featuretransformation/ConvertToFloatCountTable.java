package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.hashmap.FloatCountTable;
import org.apache.spark.api.java.function.Function;

public class ConvertToFloatCountTable implements Function<Float, FloatCountTable> {
    public FloatCountTable call(Float key) throws Exception {
        FloatCountTable countTable = new FloatCountTable();
        countTable.put(key, 1);
        return countTable;
    }
}

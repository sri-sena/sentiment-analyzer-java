package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

public class ConvertToUsageTable implements Function<Message, UsageTable> {
    private Function<Message, String> getKey;

    public ConvertToUsageTable(Function<Message, String> f) {
        getKey = f;
    }

    public UsageTable call(Message message) throws Exception {
        UsageTable usageTable = new UsageTable();

        usageTable.put(getKey.call(message), 1);

        return usageTable;
    }
}

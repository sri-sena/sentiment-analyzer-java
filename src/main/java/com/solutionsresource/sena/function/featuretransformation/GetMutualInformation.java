package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Feature;
import org.apache.spark.api.java.function.Function;

public class GetMutualInformation implements Function<Feature, Float> {
    public Float call(Feature feature) throws Exception {
        return feature.getMutualInformation().value;
    }
}

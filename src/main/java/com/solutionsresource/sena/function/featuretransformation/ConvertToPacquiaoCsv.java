package com.solutionsresource.sena.function.featuretransformation;

import com.google.gson.Gson;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.ResultHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.api.java.function.Function;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConvertToPacquiaoCsv implements Function<ResultHashMap, String> {
    private Constants constants;

    private static final String COMMA_DELIMITER = ",";

    private SimpleDateFormat hbaseFormat;
    private SimpleDateFormat csvFormat;

    public ConvertToPacquiaoCsv(Constants constants) {
        this.constants = constants;
        this.hbaseFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        this.csvFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    }

    @Override
    public String call(ResultHashMap result) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();

        String content = getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT, false);

        if (content == null) {
            return "";
        }

        Gson gson = new Gson();

        String json = getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_DOMAINS_TEXT, false);

        Domain[] domains = gson.fromJson(json, Domain[].class);

        if (domains == null) {
            return "";
        }

        String messageCommonData = getMessageCommonData(result);

        for (Domain domain : domains) {

            List<String> topicNames = new ArrayList<>();

            if (topicsContain(domain.topics, "Pacquiao")) {
                topicNames.add("Pacquiao");
            }
            if (topicsContain(domain.topics, "Bradley")) {
                topicNames.add("Bradley");
            }
            if (topicsContain(domain.topics, "Mayweather")) {
                topicNames.add("Mayweather");
            }

            if (topicNames.size() > 0) {
                StringBuilder topicsString = new StringBuilder();

                boolean first = true;
                for (String topicName : topicNames) {
                    if (!first) {
                        topicsString.append("-");
                    }
                    topicsString.append(topicName);
                    first = false;
                }

                stringBuilder.append(messageCommonData);
                stringBuilder.append(topicsString);
            }
        }

        return stringBuilder.toString();
    }

    private boolean topicsContain(List<Topic> topics, String name) {
        for (Topic topic : topics) {
            if (topic.name.equals(name)) {
                return true;
            }
        }
        return false;
    }

    private boolean contains(String source, String... searches) {
        for (String search : searches) {
            if (source.contains(search)) {
                return true;
            }
        }
        return false;
    }

    private boolean isFiltered(ResultHashMap result, String filterHashtag) {
        String hashtags = getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_HASHTAG_TEXT, false);
        return hashtags == null || !hashtags.matches(filterHashtag);
    }

    public boolean isMessageNeutral(String author) {
        for (String neutralHandler : constants.NEUTRAL_HANDLER_WHITELIST) {
            if (neutralHandler.equalsIgnoreCase(author)) {
                return true;
            }
        }

        return false;
    }

    public String getSentiment(String domainName, String topicName,
                               String sentimentName) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(domainName);
        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(topicName);
        stringBuilder.append(COMMA_DELIMITER);

        stringBuilder.append(getSentimentValue(sentimentName, "positive"));
        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getSentimentValue(sentimentName, "neutral"));
        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getSentimentValue(sentimentName, "negative"));
        stringBuilder.append(COMMA_DELIMITER);

        stringBuilder.append(cleanString(sentimentName));
        stringBuilder.append(COMMA_DELIMITER);

        return stringBuilder.toString();
    }

    public String getSentimentValue(String actual, String expectation) {
        return actual.equals(expectation) ? "1" : "0";
    }

    public String getMessageValue(ResultHashMap result, String field) {
        return getMessageValue(result, field, true);
    }

    public String getMessageValue(ResultHashMap result, String field, boolean cleanString) {
        String s = Bytes.toString(result.get(field));

        return cleanString ? cleanString(s) : s;
    }

    public String getMessageCommonData(ResultHashMap result)
            throws IOException, ParseException {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(cleanString(Bytes.toString(result.getRow())));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_CONTENT_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_SOURCE_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_LOCATION_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_LATITUDE_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_LONGITUDE_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_USERNAME_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        stringBuilder.append(getMessageValue(result, constants.TABLE_MESSAGE_MSGDATA_HASHTAG_TEXT));

        stringBuilder.append(COMMA_DELIMITER);
        String postedDateTimeStr = getMessageValue(
                result, constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_TEXT, false);

        stringBuilder.append(getDateTimeCols(postedDateTimeStr));

        stringBuilder.append(COMMA_DELIMITER);
        String createdDateTimeStr = getMessageValue(
                result, constants.TABLE_MESSAGE_MSGDATA_CREATED_DATETIME_TEXT, false);

        stringBuilder.append(getDateTimeCols(createdDateTimeStr));

        stringBuilder.append(COMMA_DELIMITER);
        byte[] isFinalized = result.get(constants.TABLE_MESSAGE_MSGDATA_FINALIZE_TEXT);
        stringBuilder.append(cleanString(String.valueOf(isFinalized != null && Bytes.toBoolean(isFinalized))));

        stringBuilder.append(COMMA_DELIMITER);

        return stringBuilder.toString();
    }

    private String getDateTimeCols(String dateTimeStr) throws IOException, ParseException {
        StringBuilder stringBuilder = new StringBuilder();

        if (dateTimeStr == null) {
            stringBuilder.append("");
            stringBuilder.append(COMMA_DELIMITER);
            stringBuilder.append("");
        } else {
            Date dateTime = hbaseFormat.parse(dateTimeStr);
            dateTimeStr = csvFormat.format(dateTime);

            stringBuilder.append(cleanString(StringUtils.substringBefore(dateTimeStr, " ")));//posted_date;
            stringBuilder.append(COMMA_DELIMITER);
            stringBuilder.append(cleanString(StringUtils.substringAfter(dateTimeStr, " ")));
        }

        return stringBuilder.toString();
    }

    private String cleanString(String string) {
        if (string == null) {
            return "";
        }
        return String.format("\"%s\"", string.replaceAll("\"", "\"\"").replaceAll("\n", " ")).trim();
    }

    private static com.solutionsresource.sena.entity.Topic getTopicWithName(
            List<com.solutionsresource.sena.entity.Topic> topics, String name) {

        for (com.solutionsresource.sena.entity.Topic topic : topics) {
            if (topic.getName().equalsIgnoreCase(name))
                return topic;
        }

        return new com.solutionsresource.sena.entity.Topic(null, name);
    }

    class Domain {
        String name;
        List<Topic> topics;
    }

    class Topic {
        String name;
        Sentiment sentiment;
    }

    class Sentiment {
        String name;
    }

}

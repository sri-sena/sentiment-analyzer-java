package com.solutionsresource.sena.function.featuretransformation;

import java.util.Set;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.Message;

public class GetSentiments implements Function<Message, Set<String>> {
    public Set<String> call(Message message) throws Exception {
        return message.getSentiments();
    }
}


package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.DomainSlugs;
import org.apache.spark.api.java.function.Function;

public class GetDomains implements Function<Message, DomainSlugs> {
    private DomainSlugs acceptedDomainSlugs;

    public GetDomains(DomainSlugs acceptedDomainSlugs) {
        this.acceptedDomainSlugs = acceptedDomainSlugs;
    }

    public DomainSlugs call(Message message) throws Exception {
        return message.getDomainSlugs(acceptedDomainSlugs);
    }
}

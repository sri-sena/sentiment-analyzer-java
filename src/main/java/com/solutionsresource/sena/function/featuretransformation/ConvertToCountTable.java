package com.solutionsresource.sena.function.featuretransformation;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.hashmap.CountTable;

public class ConvertToCountTable implements Function<Double, CountTable> {
    public CountTable call(Double key) throws Exception {
        CountTable countTable = new CountTable();
        countTable.put(key, 1);
        return countTable;
    }
}

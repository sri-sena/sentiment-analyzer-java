package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.config.Config;
import com.solutionsresource.sena.entity.Message;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.api.java.function.Function;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RemoveStopWords implements Function<Message, Message> {

    /**
     *
     */
    private static final long serialVersionUID = -4406132503295307183L;

    public List<String> removeStopWords(String content) {
        List<String> ngrams = getNgrams(content);

        return removeStopWords(ngrams);
    }

    private List<String> removeStopWords(List<String> ngrams) {
        String[] stopWords = Config.STOP_WORDS;
        List<String> filteredWords = new ArrayList<String>();

        for (String ngram : ngrams) {
            boolean isStopWord = false;
            for (String stopWord : stopWords) {
                if (stopWord.toLowerCase().equals(ngram.toLowerCase())) {
                    isStopWord = true;
                    break;
                }
            }
            if (!isStopWord) {
                if (!filteredWords.isEmpty() && filteredWords.get(filteredWords.size() - 1).equals("#")) {
                    filteredWords.add(ngram);
                } else {
                    filteredWords.add(ngram.toLowerCase());
                }
            }
        }
        return filteredWords;
    }

    public List<String> getNgrams(String content) {
        List<String> ngrams = new ArrayList<String>();

        boolean prevIsHashtag = false;
        boolean prevIsHandler = false;

        Pattern pattern = Pattern.compile(Config.TOKENIZER_REGEX);
        Matcher matcher = pattern.matcher(content);
/*
        HashtagParser parser = new HashtagParser(Constants.LEXICON_SEEDS);
*/

        while (matcher.find()) {
            String ngram = matcher.group(0);

            List<String> ngramSplit;
            if (!prevIsHandler) {
                ngramSplit = splitNgram(ngram);
            } else {
                ngramSplit = new ArrayList<String>();
                ngramSplit.add(ngram);
            }

            for (int i = 0; i < ngramSplit.size(); i++) {
                String ngramPart = ngramSplit.get(i);

                /*
                if (prevIsHashtag || prevIsHandler) {
                    List<String> lexicons = parser.parse(ngramPart);
                    ngramPart = StringUtils.join(removeStopWords(lexicons), "");
                    if (!ngramPart.isEmpty()) {
                        ngramSplit.set(i, ngramPart);
                    } else {
                        ngramSplit.remove(i--);
                    }
                }
                */

                if (!prevIsHashtag && !prevIsHandler) {
                    ngramSplit.set(i, ngramPart.toLowerCase());
                }
            }

            ngrams.addAll(ngramSplit);

            prevIsHashtag = ngram.equals("#");
            prevIsHandler = ngram.equals("@");
        }

        return ngrams;
    }

    public static List<String> splitNgram(String ngram) {
        List<String> splits = new ArrayList<String>();

        StringBuilder builder = new StringBuilder();

        boolean prevIsCapital = false;
        boolean prevIsDigit = false;
        int capitalStreak = 0;

        for (int i = 0; i < ngram.length(); i++) {
            char c = ngram.charAt(i);
            boolean isUpperCase = Character.isUpperCase(c);
            boolean isDigit = Character.isDigit(c);

            if (prevIsCapital && isUpperCase) {
                capitalStreak++;
            }

            if (builder.length() > 0) {
                String content = builder.toString().toLowerCase();
                if (isDigit && !prevIsDigit) {
                    splits.add(content);
                    builder.delete(0, builder.length());
                } else if (!isUpperCase) {
                    if (capitalStreak > 2) {
                        splits.add(content);

                        char lastChar = content.charAt(content.length()-1);

                        builder.delete(0, builder.length());
                        builder.append(lastChar);
                    }
                } else if (!prevIsCapital) {
                    splits.add(content);
                    builder.delete(0, builder.length());
                }
            }

            if (isUpperCase) {
                capitalStreak++;
            } else {
                capitalStreak = 0;
            }
            prevIsCapital = isUpperCase;
            prevIsDigit = isDigit;

            builder.append(c);
        }

        if (builder.length() > 0) {
            splits.add(builder.toString().toLowerCase());
        }

        return splits;
    }

    public Message call(Message msg) throws Exception {
        msg.setContent(combineNonstopWords(msg.getContent()));

        return msg;
    }

    public String combineNonstopWords(String content) {
        return StringUtils.join(this.removeStopWords(content), " ");
    }

}

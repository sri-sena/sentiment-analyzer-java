package com.solutionsresource.sena.function.featuretransformation;

/**
 * Created by Ronald Erquiza on 4/29/2016.
 */

import com.google.gson.Gson;
import com.solutionsresource.sena.entity.gnip.fb.FacebookPost;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class FacebookJsonReader {
    private final Gson gson;

    public FacebookJsonReader() {
        this.gson = new Gson();
    }
    public FacebookPost read(Reader reader) {
        return gson.fromJson(reader, FacebookPost.class);
    }

    public static void main(String[] args) {
        FacebookJsonReader readFBJson = new FacebookJsonReader();
        try {
            BufferedReader br = new BufferedReader(new FileReader("FBComment.json"));
            System.out.println(readFBJson.read(br));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import org.apache.spark.api.java.function.Function;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractHashtags implements Function<Message, UsageTable> {
    Constants constants;

    public ExtractHashtags() throws Exception {
        constants = new Constants();
    }

    public UsageTable call(Message message) throws Exception {
        return getHashtagUsages(message.getContent());
    }

    public UsageTable getHashtagUsages(String content) {
        UsageTable usageTable = new UsageTable();

        Pattern pattern = Pattern.compile(constants.HASHTAG_REGEX);
        Matcher matcher = pattern.matcher(content);

        while (matcher.find()) {
            String hashtag = matcher.group(0).toLowerCase();

            if (!usageTable.containsKey(hashtag)) {
                usageTable.put(hashtag, 0);
            }
            usageTable.put(hashtag, usageTable.get(hashtag) + 1);
        }

        return usageTable;
    }
}

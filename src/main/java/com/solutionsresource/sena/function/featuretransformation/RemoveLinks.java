package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class RemoveLinks implements Function<Message, Message> {

    private final Constants constants;

    public RemoveLinks() throws Exception {
        this(new Constants());
    }

    public RemoveLinks(Constants constants) {
        this.constants = constants;
    }

    public Message call(Message message) {
        message.setContent(removeLinks(message.getContent()));
        return message;
    }

    public String removeLinks(String content) {
        return content.replaceAll(constants.URL_REGEX, "");
    }
}

package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.hashmap.BytesTable;
import com.solutionsresource.sena.entity.gnip.fb.Comment;
import com.solutionsresource.sena.entity.gnip.fb.FacebookPost;

public class FacebookBytesTableGenerator {
    private final Constants constants;

    public FacebookBytesTableGenerator(Constants constants) {
        this.constants = constants;
    }

    public BytesTable generate(FacebookPost post, Comment comment) {
        BytesTable table = new BytesTable();

        table.put(constants.TABLE_MESSAGE_MSGDATA_CONTENT_BYTE, comment.comment_content);
        table.put(constants.TABLE_MESSAGE_MSGDATA_REF_ID_BYTE, comment.comment_id);
        table.put(constants.TABLE_MESSAGE_MSGDATA_POSTED_DATETIME_BYTE, comment.posted_date);
        table.put(constants.TABLE_MESSAGE_MSGDATA_USERNAME_BYTE, comment.user_name);
        table.put(constants.TABLE_MESSAGE_MSGDATA_USER_ID_BYTE, comment.user_id);
        table.put(constants.TABLE_MESSAGE_MSGDATA_LIKES_COUNT_BYTE, comment.likes);
        String hashtags = "";
        for (String hashtag : comment.hash_tags) {
            hashtags += ", " + hashtag;
        }
        hashtags = hashtags.length() > 0 ? hashtags.substring(2) : "";
        table.put(constants.TABLE_MESSAGE_MSGDATA_HASHTAG_BYTE, hashtags);

        return table;
    }
}

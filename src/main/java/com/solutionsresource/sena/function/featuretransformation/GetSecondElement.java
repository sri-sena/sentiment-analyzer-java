package com.solutionsresource.sena.function.featuretransformation;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class GetSecondElement implements Function<Tuple2<ImmutableBytesWritable, Result>, Result> {
    @Override
    public Result call(Tuple2<ImmutableBytesWritable, Result> tuple) throws Exception {
        return tuple._2();
    }
}

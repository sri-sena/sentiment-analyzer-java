package com.solutionsresource.sena.function.featuretransformation;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.hashmap.IntegerCountTable;
import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class GetFeatureIndexCountTable implements Function<WeightedLabeledPoint, IntegerCountTable> {
    public IntegerCountTable call(WeightedLabeledPoint labeledPoint) throws Exception {
        IntegerCountTable countTable = new IntegerCountTable();
        double[] features = labeledPoint.features().toArray();

        for (int i = 0; i < features.length; i++) {
            int feature = (int) features[i];

            if (feature > 0) {
                countTable.put(i, feature);
            }
        }

        return countTable;
    }
}

package com.solutionsresource.sena.function.featuretransformation;

import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class SortUsageTableTuples implements Function<Tuple2<String, Integer>, Integer> {
    public Integer call(Tuple2<String, Integer> tuple) throws Exception {
        return tuple._2();
    }
}

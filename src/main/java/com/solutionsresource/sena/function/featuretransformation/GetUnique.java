package com.solutionsresource.sena.function.featuretransformation;

import java.util.Set;

import org.apache.spark.api.java.function.Function2;

public class GetUnique implements Function2<Set<String>, Set<String>, Set<String>> {
    public Set<String> call(Set<String> domains, Set<String> domains2) throws Exception {
        domains.addAll(domains2);
        return domains;
    }
}

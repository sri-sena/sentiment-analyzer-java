package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Feature;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.util.build.Features;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

public class ConvertToFeature implements Function<Tuple2<String, UsageTable>, Feature> {
    private final UsageTable classesCount;
    private final long trainingCount;

    public ConvertToFeature(UsageTable classesCount, long trainingCount) {
        this.classesCount = classesCount;
        this.trainingCount = trainingCount;
    }

    public Feature call(Tuple2<String, UsageTable> ngramTuple) throws Exception {
        return (new Feature(ngramTuple._1(), ngramTuple._2(),
                Features.mutualInformation(ngramTuple._1(), ngramTuple._2(), classesCount,
                        (int) trainingCount)));
    }
}

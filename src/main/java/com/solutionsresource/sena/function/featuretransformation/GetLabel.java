package com.solutionsresource.sena.function.featuretransformation;

import org.apache.spark.api.java.function.Function;

import com.solutionsresource.sena.entity.WeightedLabeledPoint;

public class GetLabel implements Function<WeightedLabeledPoint, Double> {
    public Double call(WeightedLabeledPoint labeledPoint) throws Exception {
        return labeledPoint.label();
    }
}

package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class GetHasDomainString implements Function<Message, String> {
    private String domainName;

    public GetHasDomainString(String domainName) {

        this.domainName = domainName;
    }

    public String call(Message message) throws Exception {
        return message.hasDomain(domainName) ? "true" : "false";
    }
}

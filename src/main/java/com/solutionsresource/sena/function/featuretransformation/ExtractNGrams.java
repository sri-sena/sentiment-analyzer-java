package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.config.Config;
import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.entity.hashmap.UsageTable;
import com.solutionsresource.sena.util.parse.WordStemmer;
import org.apache.commons.lang.StringUtils;
import org.apache.spark.api.java.function.Function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractNGrams implements Function<Message, Message> {

    /**
     *
     */
    private static final long serialVersionUID = -3403084491684740405L;
    Constants constants;

    private int ngramsMax;

    public ExtractNGrams() throws Exception {
        this(Config.NGRAMS_MAX);
    }

    public ExtractNGrams(int ngramsMax) throws Exception {
        this(ngramsMax, new Constants());
    }

    public ExtractNGrams(Constants constants) throws Exception {
        this(Config.NGRAMS_MAX, constants);
    }

    public ExtractNGrams(int ngramsMax, Constants constants) throws Exception {
        this.ngramsMax = ngramsMax;
        this.constants = constants;
    }

    public Message call(Message msg) throws Exception {
        List<String> ngrams = new ArrayList<String>();
        List<String> hashtagAndHandlerNgrams = new ArrayList<String>();
        List<String> wholeWords;

        getNgrams(msg.getContent(), ngrams, hashtagAndHandlerNgrams);

        ngrams = removeHashtagsAndHandlers(ngrams);
        wholeWords = stemAndReplaceWholeWords(ngrams);

        expandNgrams(ngrams);
        expandNgrams(hashtagAndHandlerNgrams);

        // todo: remove this and use the other ngrams for another naive bayes
        ngrams.addAll(hashtagAndHandlerNgrams);
        ngrams.addAll(wholeWords);

        msg.setNgrams(new UsageTable());
        msg.setHashtagAndHandlerNgrams(new UsageTable());

        HashMap<String, Integer> documentNgrams = msg.getNgrams();
        HashMap<String, Integer> documentHashtagAndHandlerNgrams = msg.getHashtagAndHandlerNgrams();

        setNgrams(ngrams, documentNgrams);
        setNgrams(hashtagAndHandlerNgrams, documentHashtagAndHandlerNgrams);

        return msg;
    }

    private List<String> stemAndReplaceWholeWords(List<String> ngrams) throws Exception {
        List<String> originalWords = new ArrayList<String>();

        WordStemmer stemmer = new WordStemmer(constants);

        for (int i = 0; i < ngrams.size(); i++) {
            String ngram = ngrams.get(i);
            String rootword = stemmer.stem(ngram);

            if (!rootword.equals(ngram)) {
                originalWords.add(ngram);
                ngrams.set(i, rootword);
            }
        }

        return originalWords;
    }

    private void setNgrams(List<String> ngrams, HashMap<String, Integer> documentNgrams) {
        for (String ngram : ngrams) {
            ngram = ngram.toLowerCase();
            if (!documentNgrams.containsKey(ngram)) {
                documentNgrams.put(ngram, 0);
            }

            documentNgrams.put(ngram, documentNgrams.get(ngram) + 1);
        }
    }

    private void expandNgrams(List<String> ngrams) {
        int ngramsCount = ngrams.size();
        for (int k = 2; k <= Math.min(ngramsMax, ngramsCount); k++) {
            for (int i = k - 1; i < ngramsCount; i++) {
                ArrayList<String> builder = new ArrayList<String>();

                for (int j = k - 1; j >= 0; j--) {
                    builder.add(ngrams.get(i - j));
                }

                ngrams.add(StringUtils.join(builder, " "));
            }
        }
    }

    private static List<String> removeHashtagsAndHandlers(List<String> ngrams) {
        List<String> newNgrams = new ArrayList<String>();

        for (String ngram : ngrams) {
            if (!ngram.equals("#") && !ngram.equals("@")) {
                newNgrams.add(ngram);
            }
        }

        return newNgrams;
    }

    public static void getNgrams(String content, List<String> ngrams, List<String> hashtagAndHandlerNgrams) {
        boolean prevIsHashtag = false;
        boolean prevIsHandler = false;

        Pattern pattern = Pattern.compile(Config.TOKENIZER_REGEX);
        Matcher matcher = pattern.matcher(content);
/*
        HashtagParser parser = new HashtagParser(constants.LEXICON_SEEDS);
*/

        while (matcher.find()) {
            String ngram = matcher.group(0);
/*
            if (prevIsHashtag || prevIsHandler) {
                hashtagAndHandlerNgrams.add(ngram.toLowerCase());
            }
*/

//            if (!prevIsHashtag && !prevIsHandler) {
                ngrams.add(ngram.toLowerCase());
//            }
            /*
            prevIsHashtag = ngram.equals("#");
            prevIsHandler = ngram.equals("@");
            */
        }
    }

}

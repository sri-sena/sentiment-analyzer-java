package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Domain;
import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

import java.util.ArrayList;
import java.util.List;

public class GetDomainNames implements Function<Message, List<String>> {
    public List<String> call(Message message) throws Exception {
        List<String> domainNames = new ArrayList<String>();

        for(Domain domain : message.getDomains()) {
            domainNames.add(domain.getName());
        }

        return domainNames;
    }
}

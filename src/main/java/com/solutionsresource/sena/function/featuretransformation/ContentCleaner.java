package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Message;
import org.apache.spark.api.java.function.Function;

public class ContentCleaner implements Function<Message, Message> {
    private final RemoveLinks removeLinks;
    private final RemoveStopWords removeStopWords;
    private final RemoveHandlers removeHandlers;
    private final DatesNormalizer datesNormalizer;
    private final EmojiNormalizer emojiNormalizer;

    public ContentCleaner(RemoveLinks removeLinks, RemoveStopWords removeStopWords,
                          RemoveHandlers removeHandlers, DatesNormalizer datesNormalizer,
                          EmojiNormalizer emojiNormalizer) {
        this.removeLinks = removeLinks;
        this.removeStopWords = removeStopWords;
        this.removeHandlers = removeHandlers;
        this.datesNormalizer = datesNormalizer;
        this.emojiNormalizer = emojiNormalizer;
    }

    public Message call(Message message) throws Exception {
        message.setContent(call(message.getContent()));
        return message;
    }

    public String call(String content) throws Exception {
        if (content.startsWith("RT ")) {
            content = content.substring(3);
        }

        return emojiNormalizer.normalize(
                datesNormalizer.normalize(
                        removeStopWords.combineNonstopWords(
                                removeHandlers.removeHandlers(
                                        removeLinks.removeLinks(content)
                                )
                        )
                )
        );
    }

    public String clean(String content) throws Exception {
        return this.call(content);
    }
}

package com.solutionsresource.sena.function.featuretransformation;

import com.solutionsresource.sena.entity.Feature;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.regression.LabeledPoint;

import java.util.List;

public class MultiplyFeatures implements Function<LabeledPoint, LabeledPoint> {
    private List<Feature> features;

    public MultiplyFeatures(List<Feature> features) {
        this.features = features;
    }

    public LabeledPoint call(LabeledPoint labeledPoint) throws Exception {
        return withMultipliedFeatures(labeledPoint, features);
    }

    public LabeledPoint withMultipliedFeatures(LabeledPoint labeledPoint,
                                               List<Feature> featuresWithMutualInformation) {
        double[] features = labeledPoint.features().toArray();

        for (int i = 0; i < featuresWithMutualInformation.size(); i++) {
            features[i] = features[i] * featuresWithMutualInformation.get(i).getMutualInformation().value;
        }

        return new LabeledPoint(labeledPoint.label(), Vectors.dense(features));
    }
}

package com.solutionsresource.sena;

import java.util.Date;

import com.solutionsresource.sena.constant.Constants;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;

import com.fasterxml.uuid.Generators;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.solutionsresource.sena.entity.Tweet;
import com.solutionsresource.sena.util.hbase.HBaseConnector;

public class App {

    public static int i = 0;
    static Constants constants;


    @SuppressWarnings("serial")
    public static void main(String[] args) throws Exception {

        constants = new Constants();
        /** Spark Configuration **/
        SparkConf sparkConf = new SparkConf();
        sparkConf.setAppName("App");
        JavaSparkContext ctx = new JavaSparkContext(sparkConf);
        JavaRDD<String> textFile = ctx.textFile("hdfs://" + constants.INTERNAL_ADDRESS + "/tmp/20151015_twitter_data.txt", 1);

        /** Spark Jobs **/
        textFile.foreach(new VoidFunction<String>() {
            public void call(String s) throws Exception {
                if (i > 50) {
                    throw new Exception("***** END ****");
                }
                // start the shit
                if (StringUtils.isNotBlank(s)) {
                    try {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
                        Gson gson = gsonBuilder.create();
                        Tweet t = gson.fromJson(s, Tweet.class);

                        System.out.println(">>> " + s);

                        /** UUID Configuration **/
                        String uuid = new Date().getTime() + "-" + Generators.timeBasedGenerator().generate();
                        /** UUID Configuration **/

                        /** HBase Jobs **/
                        HConnection connection = HConnectionManager.createConnection(new HBaseConnector().getConfiguration());
                        HTableInterface table = connection.getTable(TableName.valueOf("message"));
                        Put p = new Put(Bytes.toBytes(uuid));
                        p.add(Bytes.toBytes("message_data"), Bytes.toBytes("content"), Bytes.toBytes(t.body));
                        p.add(Bytes.toBytes("message_data"), Bytes.toBytes("source"), Bytes.toBytes("twitter"));
                        p.add(Bytes.toBytes("message_data"), Bytes.toBytes("username"), Bytes.toBytes(t.actor.displayName));
                        p.add(Bytes.toBytes("message_data"), Bytes.toBytes("finalize"), Bytes.toBytes(false));
                        table.put(p);
                        /** HBase Jobs **/

                        connection.close();

                        i++;
                    } catch (JsonSyntaxException e) {
                        System.err.println(">>> ERROR - BAD FORMAT: " + s);
                    }
                } else {
                    System.err.println(">>> ERROR - BLANK: " + s);
                }

            }
        });

        ctx.stop();
        ctx.close();
    }

}
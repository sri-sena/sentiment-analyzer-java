package com.solutionsresource.sena.bias;

import com.solutionsresource.sena.constant.Constants;
import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.bias.exception.MissingChildNodeException;
import com.solutionsresource.sena.function.featuretransformation.*;
import org.w3c.dom.Element;

import java.io.Serializable;
import java.util.List;

public class ClassBiases implements Serializable {
  private final Constants constants;
  private final String name;
  private final ClassBiasList domainBiases;
  private final TopicBiasesList topicBiases;

  public ClassBiases(String name, ClassBiasList domainBiases, TopicBiasesList topicBiases, Constants constants) {
    this.name = name;
    this.domainBiases = domainBiases;
    this.topicBiases = topicBiases;
    this.constants = constants;
  }

  public ClassBiases(String name, Element domainNode, Constants constants) throws MissingChildNodeException {
    this(name, new ClassBiasList(name, "domainrecognition", domainNode),
        new TopicBiasesList(name, "topics", domainNode), constants);
  }

  public String getName() {
    return name;
  }

  public List<ClassBias> getDomainBiases() {
    return domainBiases;
  }

  public List<TopicBiases> getTopicBiases() {
    return topicBiases;
  }

  public double score(String content) throws Exception {
    return score(getMessage(content));
  }

  public double score(Message message) {
    String content = message.getContent();
    for (ClassBias domainBias : domainBiases) {
      if (domainBias.finds(content)) {
        return domainBias.getValue();
      }
    }

    return -1.0;
  }

  private Message getMessage(String content) throws Exception {
    ContentCleaner contentCleaner = new ContentCleaner(
        new RemoveLinks(constants),
        new RemoveStopWords(),
        new RemoveHandlers(constants),
        new DatesNormalizer(),
        new EmojiNormalizer());

    Message message = new Message(content);

    message = contentCleaner.call(message);

    return message;
  }

  public double topicScore(String topicName, String content) throws Exception {
    return topicScore(topicName, getMessage(content));
  }

  public double topicScore(String topicName, Message message) {
    topicName = topicName.toLowerCase();
    for (TopicBiases topicBiases : this.topicBiases) {
      if (topicName.contains(topicBiases.getKey())) {
        return topicBiases.topicScore(message);
      }
    }

    return -1.0;
  }

  public double sentimentScore(String topicName, String content) throws Exception {
    return sentimentScore(topicName, getMessage(content));
  }

  public double sentimentScore(String topicName, Message message) {
    topicName = topicName.toLowerCase();
    for (TopicBiases topicBiases : this.topicBiases) {
      if (topicName.contains(topicBiases.getKey())) {
        return topicBiases.sentimentScore(message);
      }
    }

    return -1.0;
  }

  @Override
  public String toString() {
    return String.format("\tRecognition:\n%s\tTopics:\n%s", domainBiases, topicBiases);
  }
}

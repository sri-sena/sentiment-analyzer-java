package com.solutionsresource.sena.bias;

import com.solutionsresource.sena.bias.exception.MissingChildNodeException;
import com.solutionsresource.sena.constant.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DomainBiases extends HashMap<String, ClassBiases> {
  public DomainBiases(File xmlFile, Constants constants)
      throws IOException, SAXException, ParserConfigurationException, MissingChildNodeException {
    DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();

    Document document = parser.parse(xmlFile);
    document.getDocumentElement().normalize();

    Node domainsNode = document.getElementsByTagName("domains").item(0);

    if (domainsNode == null) {
      throw new MissingChildNodeException("domains", "xml");
    }
    NodeList domainNodes = domainsNode.getChildNodes();

    for (int i = 0; i < domainNodes.getLength(); i++) {
      Node domain = domainNodes.item(i);
      if (!domain.getClass().getSimpleName().equals("DeferredTextImpl")) {
        Element domainNode = (Element) domain;

        String domainName = domainNode.getAttribute("name").trim();
        this.put(domainName, new ClassBiases(domainName, domainNode, constants));
      }
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    for (Map.Entry<String, ClassBiases> entry : entrySet()) {
      builder.append(String.format("%s\n%s\n", entry.getKey(), entry.getValue()));
    }

    return builder.toString();
  }
}

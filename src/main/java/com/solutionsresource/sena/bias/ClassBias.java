package com.solutionsresource.sena.bias;

import org.apache.commons.lang.math.NumberUtils;
import org.w3c.dom.Element;

import java.io.Serializable;
import java.util.regex.Pattern;

public class ClassBias implements Serializable {
  private final Pattern key;
  private final double value;

  public ClassBias(String key, double value) {
    this.key = Pattern.compile(key);
    this.value = value;
  }

  public ClassBias(String key, String value) {
    this.key = Pattern.compile(key);
    if (!NumberUtils.isNumber(value)) {
      throw new IllegalArgumentException(String.format("Invalid value \"%s\" for bias \"%s\".", value, key));
    }
    this.value = Double.parseDouble(value);
  }

  public ClassBias(Element wordNode) {
    this(wordNode.getTextContent().trim(), wordNode.getAttribute("value"));
  }

  public double getValue() {
    return value;
  }

  public String getKey() {
    return key.pattern();
  }

  public boolean finds(String ngram) {
    return key.matcher(ngram).find();
  }

  @Override
  public String toString() {
    return String.format("[%4s] %s", value, key);
  }
}

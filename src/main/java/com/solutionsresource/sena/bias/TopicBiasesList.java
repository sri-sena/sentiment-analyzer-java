package com.solutionsresource.sena.bias;

import com.solutionsresource.sena.bias.exception.MissingChildNodeException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

public class TopicBiasesList extends ArrayList<TopicBiases> {
  public TopicBiasesList(String name, String tagName, Element parentNode) throws MissingChildNodeException {
    Node topicsNode = parentNode.getElementsByTagName(tagName).item(0);
    if (topicsNode == null) {
      throw new MissingChildNodeException(tagName, name);
    }
    NodeList topics = topicsNode.getChildNodes();
    for (int i = 0; i < topics.getLength(); i++) {
      Node topicNode = topics.item(i);
      if (!topicNode.getClass().getSimpleName().equals("DeferredTextImpl")) {
        Element topic = (Element) topicNode;
        this.add(new TopicBiases(topic.getAttribute("name").trim(), topic));
      }
    }
  }

  public TopicBiasesList(NodeList topics) throws MissingChildNodeException {
    for (int i = 0; i < topics.getLength(); i++) {
      Node topicNode = topics.item(i);
      if (!topicNode.getClass().getSimpleName().equals("DeferredTextImpl")) {
        Element topic = (Element) topicNode;
        this.add(new TopicBiases(topic.getAttribute("name").trim(), topic));
      }
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    for (TopicBiases bias : this) {
      builder.append(bias);
    }

    return builder.toString();
  }
}

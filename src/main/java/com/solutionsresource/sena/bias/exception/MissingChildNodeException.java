package com.solutionsresource.sena.bias.exception;

public class MissingChildNodeException extends Exception {
  public MissingChildNodeException(String tagName, String name) {
    super(String.format("Missing \"%s\" tag under \"%s\".", tagName, name));
  }
}

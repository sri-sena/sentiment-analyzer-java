package com.solutionsresource.sena.bias;

import com.solutionsresource.sena.bias.exception.MissingChildNodeException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

public class ClassBiasList extends ArrayList<ClassBias> {
  public ClassBiasList(String name, String tagName, Element parentNode) throws MissingChildNodeException {
    Node wordsNode = parentNode.getElementsByTagName(tagName).item(0);
    if (wordsNode == null) {
      throw new MissingChildNodeException(tagName, name);
    }
    NodeList words = wordsNode.getChildNodes();
    for (int i = 0; i < words.getLength(); i++) {
      Node word = words.item(i);
      if (!word.getClass().getSimpleName().equals("DeferredTextImpl")) {
        this.add(new ClassBias((Element) word));
      }
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    for (ClassBias bias : this) {
      builder.append(String.format("\t\t\t\t%s\n", bias));
    }

    return builder.toString();
  }
}

package com.solutionsresource.sena.bias;

import com.solutionsresource.sena.entity.Message;
import com.solutionsresource.sena.bias.exception.MissingChildNodeException;
import org.w3c.dom.Element;

import java.io.Serializable;
import java.util.List;

public class TopicBiases implements Serializable {
  private final String key;
  private final ClassBiasList topicBiases;
  private final ClassBiasList sentimentBiases;

  public TopicBiases(String key, ClassBiasList topicBiases, ClassBiasList sentimentBiases) {
    this.key = key.toLowerCase();
    this.topicBiases = topicBiases;
    this.sentimentBiases = sentimentBiases;
  }

  public TopicBiases(String name, Element topic) throws MissingChildNodeException {
    this(name, new ClassBiasList(name, "recognition", topic),
        new ClassBiasList(name, "sentiment", topic));
  }

  public String getKey() {
    return key;
  }

  public List<ClassBias> getTopicBiases() {
    return topicBiases;
  }

  public List<ClassBias> getSentimentBiases() {
    return sentimentBiases;
  }

  public double score(Message message, List<ClassBias> biases) {
    String content = message.getContent();
    for (ClassBias topicBias : biases) {
      if (topicBias.finds(content)) {
        return topicBias.getValue();
      }
    }
    return -1.0;
  }

  public double topicScore(Message message) {
    return score(message, topicBiases);
  }

  public double sentimentScore(Message message) {
    return score(message, sentimentBiases);
  }

  @Override
  public String toString() {
    return String.format("\t\t%s\n\t\t\tRecognition:\n%s\t\t\tSentiment:\n%s", key, topicBiases, sentimentBiases);
  }
}
